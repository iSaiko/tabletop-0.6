﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.Types;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;

public class m_UNETChess : MonoBehaviour
{
    public static m_UNETChess instance { set; get; }
    public string currentPlayerName;
	public string currentPlayerCountry;
    public string secondPlayerName;
    public string secondPlayerCountry;
    public int secondPlayerID;
    // Matchmaker related
    List<MatchInfoSnapshot> m_MatchList = new List<MatchInfoSnapshot>();
    bool m_MatchCreated = false;
    bool m_MatchJoined = false;
    MatchInfo m_MatchInfo;
    string m_MatchName = "";
    NetworkMatch m_NetworkMatch;
    private int reliableChannel;
    private int unreliableChannel;
    // Connection/communication related
    int m_HostId = -1;
    // On the server there will be multiple connections, on the client this will only contain one ID
    public List<int> m_ConnectionIds = new List<int>();

    byte[] m_ReceiveBuffer = new byte[1024];
    string m_NetworkMessage = "H";
    string m_LastReceivedMessage = "";
    NetworkWriter m_Writer;
    NetworkReader m_Reader;
    bool m_ConnectionEstablished;

    const int k_ServerPort = 25000;
    const int k_MaxMessageSize = 1024;
    private byte error;
    //Client Related Variables


    public ulong MatchIDUlong;

    // Chess Related Variables

    public string clientName;
    //public PlayerControl myPlayerControl;
    private bool socketReady;
    public bool isHost;
    private GameObject pieceToMove;
    private GameObject attackingPiece;
    private GameObject attackedPiece;
    private GameObject promotedPawn;
    private GameObject promoteToPiece;
    public PlayerControl myPlayerControl;
    public GameManager myManager;
    public GameObject player1Promoter;
    public GameObject player2Promoter;
    public GameObject pieceTab1, pieceTab2, pieceTabTemp;


    bool isGameStarted;
    bool startCountDown;
    float countDownTime;
    GameObject countDownPanel;
    Text countDownLabel;

	public int roundTime;

    private int ourClientId;
    private int connectionId;


    protected string pingURL = "https://multigame.4mobily.net/public/api/v1/user/ping";
    void Awake()
    {
        m_NetworkMatch = gameObject.AddComponent<NetworkMatch>();
    }

    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
		currentPlayerName = (Login.instance !=  null) ? Login.instance.userName :  UiMenuValues.instance.userName ;
		currentPlayerCountry = Login.instance.successfulLoginUser.data.user.country_code;
        instance = this;
        m_ReceiveBuffer = new byte[k_MaxMessageSize];
        m_Writer = new NetworkWriter();
        // While testing with multiple standalone players on one machine this will need to be enabled
        Application.runInBackground = true;
        Debug.Log(UiMenuValues.instance.userName);
        secondPlayerName = "";
        
    }

    void FixedUpdate()
    {
        if (startCountDown && !isGameStarted)
        {

            countDownLabel.text = Mathf.CeilToInt(countDownTime).ToString();
            print(Mathf.CeilToInt(countDownTime).ToString());
            countDownTime -= Time.fixedDeltaTime;
            if (countDownTime < 0)
            {
                //countDownPanel.SetActive(false);
                isGameStarted = true;
                startCountDown = false;
                print("game should be started");
				Resources.UnloadUnusedAssets();
                SceneManager.LoadScene("chessScene");
                //invokePing();
            }


        }
        else
        {

            return;
        }


    }


    void OnApplicationQuit()
    {
        NetworkTransport.Shutdown();
    }
	void OnDestroy()
	{
		
		//ShutDownRoom();
	}

    public void CreateRoomUNET()
    {
        string roomName;
        string password;
        roomName = UiMenuValues.instance.roomNameToBeCreated;
        password = UiMenuValues.instance.roomToEnterPassword;

        m_MatchName = roomName;

        if (m_MatchName != "")
		{
			m_NetworkMatch.CreateMatch(m_MatchName, 2, true, "", "", "", 0, 0, OnMatchCreate);
		}
        else
        {
            StartCoroutine(UiAnimationManager.instance.showFailureMassage("Can't create empty name"));
            UiAnimationManager.instance.hideLoadingPanel();
			Destroy(this.gameObject,2.5f);

        }


    }

    public void JoinRoomButton(long Match_ID,string password)
    {
        Debug.Log("Password : " + password);
        Debug.Log("Connect to chess" + Match_ID);

        JoinRoomUNET((NetworkID)Match_ID, password);

    }
    public void JoinRoomUNET(NetworkID matchID, string roomPassword)
    {
        Debug.Log("Final Password" + roomPassword);

        m_NetworkMatch.JoinMatch(matchID, "", "", "", 0, 0, OnMatchJoined);
        Debug.Log("Match ID to join" + matchID);
    }

    public void ShutDownConnection()
    {
        m_NetworkMatch.DropConnection(m_MatchInfo.networkId, m_MatchInfo.nodeId, 0, OnConnectionDropped);
    }
    public void ShutDownRoom()
    {
        NetworkTransport.Shutdown();

    }
    public void OnConnectionDropped(bool success, string extendedInfo)
    {
        Debug.Log("Connection has been dropped on matchmaker server");
        NetworkTransport.Shutdown();
        m_HostId = -1;
        m_ConnectionIds.Clear();
        m_MatchInfo = null;
        m_MatchCreated = false;
        m_MatchJoined = false;
        m_ConnectionEstablished = false;
    }

    public virtual void OnMatchCreate(bool success, string extendedInfo, MatchInfo matchInfo)
    {
        if (success)
        {
            Debug.Log("Create match succeeded");
            Utility.SetAccessTokenForNetwork(matchInfo.networkId, matchInfo.accessToken);

            m_MatchCreated = true;
            m_MatchInfo = matchInfo;
            StartServer(matchInfo.address, matchInfo.port, matchInfo.networkId,
                matchInfo.nodeId);
            MatchIDUlong = (ulong)matchInfo.networkId;
            Rooms.instance.createRoom(MatchIDUlong);
        }
        else
        {
            Debug.LogError("Create match failed: " + extendedInfo);
        }

    }

    //public void OnMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
    //{
    //    if (success && matches != null)
    //    {
    //        m_MatchList = matches;
    //    }
    //    else if (!success)
    //    {
    //        Debug.LogError("List match failed: " + extendedInfo);
    //    }
    //}

    // When we've joined a match we connect to the server/host
    public virtual void OnMatchJoined(bool success, string extendedInfo, MatchInfo matchInfo)
    {
        Debug.Log(matchInfo.networkId.ToString());
        if (success)
        {
            Debug.Log("Join match succeeded");
            Utility.SetAccessTokenForNetwork(matchInfo.networkId, matchInfo.accessToken);

            m_MatchJoined = true;
            m_MatchInfo = matchInfo;

            Debug.Log("Connecting to Address:" + matchInfo.address +
                " Port:" + matchInfo.port +
                " NetworKID: " + matchInfo.networkId +
                " NodeID: " + matchInfo.nodeId);
            ConnectThroughRelay(matchInfo.address, matchInfo.port, matchInfo.networkId,
                matchInfo.nodeId);

        }
        else
        {
            Debug.Log(matchInfo.address + " " + matchInfo.port + " " + matchInfo.networkId + " " + matchInfo.nodeId);

            Debug.LogError("Join match failed: " + extendedInfo);
			StartCoroutine(UiAnimationManager.instance.showFailureMassage("Join match failed: " + extendedInfo));
        }
    }

    void SetupHost(bool isServer)
    {
        Debug.Log("Initializing network transport");
        NetworkTransport.Init();
        var config = new ConnectionConfig();
        config.NetworkDropThreshold = 10;
        reliableChannel = config.AddChannel(QosType.Reliable);
        unreliableChannel = config.AddChannel(QosType.Unreliable);
        var topology = new HostTopology(config, 2);
        if (isServer)
            m_HostId = NetworkTransport.AddHost(topology, k_ServerPort);
        else
            m_HostId = NetworkTransport.AddHost(topology);
    }

    void StartServer(string relayIp, int relayPort, NetworkID networkId, NodeID nodeId)
    {
        isHost = true;
        SetupHost(isHost);

        byte error;
        NetworkTransport.ConnectAsNetworkHost(
            m_HostId, relayIp, relayPort, networkId, Utility.GetSourceID(), nodeId, out error);
    }

    void ConnectThroughRelay(string relayIp, int relayPort, NetworkID networkId, NodeID nodeId)
    {
        isHost = false;
        SetupHost(isHost);

        byte error;
        NetworkTransport.ConnectToNetworkPeer(
            m_HostId, relayIp, relayPort, 0, 0, networkId, Utility.GetSourceID(), nodeId, out error);
    }

    void Update(){
    
        if (m_HostId == -1)
            return;
        //ReceivedText.text = 
        NetworkEventType networkEvent; //= NetworkEventType.Nothing;
        int connectionId;
        int channelId;
        int receivedSize;
        byte error;

        // Get events from the relay connection
        networkEvent = NetworkTransport.ReceiveRelayEventFromHost(m_HostId, out error);
        if (networkEvent == NetworkEventType.ConnectEvent)
            Debug.Log("Relay server connected");
        if (networkEvent == NetworkEventType.DisconnectEvent)
            Debug.Log("Relay server disconnected");

        do
        {
            //Debug.Log("Inside the do while loop");
            // Get events from the server/client game connection
            networkEvent = NetworkTransport.Receive(out m_HostId, out connectionId, out channelId,
                m_ReceiveBuffer, (int)m_ReceiveBuffer.Length, out receivedSize, out error);
            // Debug.Log(error +"|"+ Encoding.UTF8.GetString(m_ReceiveBuffer));
            if ((NetworkError)error != NetworkError.Ok)
            {
                Debug.LogError("Error while receiveing network message: " + (NetworkError)error);
            }

            switch (networkEvent)
            {
                case NetworkEventType.ConnectEvent:
                    {
                        Debug.Log("Connected through relay, ConnectionID:" + connectionId +
                            " ChannelID:" + channelId);
                        m_ConnectionEstablished = true;
                        m_ConnectionIds.Add(connectionId);
                        OnConnection(connectionId);

                        break;
                    }
                case NetworkEventType.DataEvent:
                    {
                        Debug.Log("Data event, ConnectionID:" + connectionId +
                            " ChannelID: " + channelId +
                            " Received Size: " + receivedSize);

                        m_Reader = new NetworkReader(m_ReceiveBuffer);
                        m_LastReceivedMessage = m_Reader.ReadString();
                        Debug.Log(m_LastReceivedMessage);

                        ClientChess(m_LastReceivedMessage);
						

                        break;
                    }
                case NetworkEventType.DisconnectEvent:
                    {
                        Debug.Log("Connection disconnected, ConnectionID:" + connectionId);
						m_ConnectionIds.Remove(connectionId);
                        if (!isGameStarted)
                        {
                            //Player disconnected at the waiting room
                        }
                        else
                        {
                            //Player Disconnected through the game
                        }
                        break;
                    }
                case NetworkEventType.Nothing:
                    break;
            }
        } while (networkEvent != NetworkEventType.Nothing);
    }

    //Methods for dealing with chess game

    public void ClientChess(string message)
    {
        string[] aData = message.Split('|');
        switch (aData[0])
        {

            case "ASKNAME":
                OnAskName(aData);
                break;
            case "NAMEIS":
                OnNameIs(aData[1] +"|"+ aData[2] + "|" + aData[3]);
                break;
            case "CMOV":
                myManager = GameObject.Find("GameManager").GetComponent<GameManager>();
                myPlayerControl = myManager.GetComponent<PlayerControl>();
                for (int i = 0; i < myPlayerControl.chessPieces.Count; i++)
                {
                    if (myPlayerControl.chessPieces[i].GetComponent<Piece>().pieceIndex == int.Parse(aData[1]))
                    {
                        pieceToMove = myPlayerControl.chessPieces[i];
                    }
                }
                myManager.SelectPiece(pieceToMove);
                myManager.SelectedPiece.GetComponent<Piece>().row = int.Parse(aData[5]);
                myManager.SelectedPiece.GetComponent<Piece>().column = int.Parse(aData[6]);
                pieceToMove.GetComponent<Piece>().updateAfterAttack();
                Vector3 newPlace = new Vector3(float.Parse(aData[2]), float.Parse(aData[3]), float.Parse(aData[4]));
                myManager.MovePiece(newPlace);
                myPlayerControl.playerTurn = int.Parse(aData[7]);
                break;

            case "CATK":
                myManager = GameObject.Find("GameManager").GetComponent<GameManager>();
                myPlayerControl = myManager.GetComponent<PlayerControl>();
                for (int i = 0; i < myPlayerControl.chessPieces.Count; i++)
                {
                    if (myPlayerControl.chessPieces[i].GetComponent<Piece>().pieceIndex == int.Parse(aData[1]))
                    {
                        attackingPiece = myPlayerControl.chessPieces[i];
                    }
                    if (myPlayerControl.chessPieces[i].GetComponent<Piece>().pieceIndex == int.Parse(aData[2]))
                    {
                        attackedPiece = myPlayerControl.chessPieces[i];
                    }


                }
                myManager.SelectPiece(attackingPiece);
                myManager.SelectedPiece.GetComponent<Piece>().row = attackedPiece.GetComponent<Piece>().row;
                myManager.SelectedPiece.GetComponent<Piece>().column = attackedPiece.GetComponent<Piece>().column;
                Vector3 attackingNewPlace = new Vector3(attackedPiece.gameObject.transform.position.x, attackingPiece.transform.position.y, attackedPiece.gameObject.transform.position.z);
                myManager.MovePiece(attackingNewPlace);
                attackingPiece.GetComponent<Piece>().updateAfterAttack();
                myPlayerControl.playerTurn = int.Parse(aData[3]);
                attackedPiece.transform.GetComponent<Piece>().jump();
                PlayerControl.instance.chessPieces.Remove(attackedPiece);
                if (attackedPiece.GetComponent<Piece>().isPiecePlayer1)
                {
                    ChessBoard.instance.team1.Remove(attackedPiece);
                }
                else
                {
                    ChessBoard.instance.team2.Remove(attackedPiece);
                }
                //              attackedPiece.gameObject.SetActive(false);
                break;

            case "CPROM":
                Debug.Log("Server Promoting to Clients");

                ChessPromoter.instance.PromoteFromNetwork(aData);
                //player1Promoter = GameObject.Find("Player1PromotablePieces");
                //player2Promoter = GameObject.Find("Player2PromotablePieces");
                ////if player 1 is promoting
                //if (int.Parse(aData[3]) == 2)
                //{
                //    for (int i = 0; i < myPlayerControl.chessPieces.Count; i++)
                //    {
                //        if (myPlayerControl.chessPieces[i].GetComponent<Piece>().pieceIndex == int.Parse(aData[1]))
                //        {
                //            promotedPawn = myPlayerControl.chessPieces[i];
                //            Debug.Log(promotedPawn.GetComponent<Piece>().pieceIndex);
                //        }
                //    }
                //    for (int i = 0; i < player1Promoter.GetComponent<Promoter>().promotionPieces.Count; i++)
                //    {
                //        if (player1Promoter.GetComponent<Promoter>().promotionPieces[i].GetComponent<Piece>().pieceIndex == int.Parse(aData[2]))
                //        {
                //            promoteToPiece = player1Promoter.GetComponent<Promoter>().promotionPieces[i];

                //            Debug.Log(promoteToPiece.GetComponent<Piece>().pieceIndex);

                //        }
                //    }
                //    promoteToPiece.GetComponent<Piece>().row = promotedPawn.GetComponent<Piece>().row;
                //    promoteToPiece.GetComponent<Piece>().column = promotedPawn.GetComponent<Piece>().column;
                //    promoteToPiece.GetComponent<Piece>().updateAfterAttack();
                //    myPlayerControl.chessPieces.Add(promoteToPiece);
                //    promoteToPiece.gameObject.transform.position = promotedPawn.gameObject.transform.position;
                //    myPlayerControl.playerTurn = 2;
                //    promoteToPiece.SetActive(true);
                //    promotedPawn.SetActive(false);
                //}
                ////if player 2 is promoting
                //if (int.Parse(aData[3]) == 1)
                //{
                //    for (int i = 0; i < myPlayerControl.chessPieces.Count; i++)
                //    {
                //        if (myPlayerControl.chessPieces[i].GetComponent<Piece>().pieceIndex == int.Parse(aData[1]))
                //        {
                //            promotedPawn = myPlayerControl.chessPieces[i];
                //        }
                //    }
                //    for (int i = 0; i < player2Promoter.GetComponent<Promoter>().promotionPieces.Count; i++)
                //    {
                //        if (player2Promoter.GetComponent<Promoter>().promotionPieces[i].GetComponent<Piece>().pieceIndex == int.Parse(aData[2]))
                //        {
                //            promoteToPiece = player2Promoter.GetComponent<Promoter>().promotionPieces[i];
                //        }
                //    }
                //    promoteToPiece.GetComponent<Piece>().row = promotedPawn.GetComponent<Piece>().row;
                //    promoteToPiece.GetComponent<Piece>().column = promotedPawn.GetComponent<Piece>().column;
                //    promoteToPiece.GetComponent<Piece>().updateAfterAttack();
                //    myPlayerControl.chessPieces.Add(promoteToPiece);
                //    promoteToPiece.gameObject.transform.position = promotedPawn.gameObject.transform.position;
                //    myPlayerControl.playerTurn = 1;
                //    promoteToPiece.SetActive(true);
                //    promotedPawn.SetActive(false);
                //}
                break;
            //Tabyeet Code for network.
            case "CTAB":
                print("stab");
                myManager = GameObject.Find("GameManager").GetComponent<GameManager>();
                myPlayerControl = myManager.GetComponent<PlayerControl>();
                for (int i = 0; i < myPlayerControl.chessPieces.Count; i++)
                {
                    if (myPlayerControl.chessPieces[i].GetComponent<Piece>().pieceIndex == int.Parse(aData[1]))
                    {
                        pieceTab1 = myPlayerControl.chessPieces[i];
                    }
                    if (myPlayerControl.chessPieces[i].GetComponent<Piece>().pieceIndex == int.Parse(aData[7]))
                    {
                        pieceTab2 = myPlayerControl.chessPieces[i];
                    }
                }

                myManager.SelectPiece(pieceTab1);
                myManager.SelectedPiece.GetComponent<Piece>().row = int.Parse(aData[5]);
                myManager.SelectedPiece.GetComponent<Piece>().column = int.Parse(aData[6]);
                pieceTab1.GetComponent<Piece>().updateAfterAttack();
                Vector3 TempPlace1 = new Vector3(float.Parse(aData[2]), float.Parse(aData[3]), float.Parse(aData[4]));
                //			Debug.Log(aData[2] + "" + aData[3] + "" + aData[4]);
                Debug.Log(TempPlace1);


                myManager.MovePiece(TempPlace1);

                //                 myManager.SelectPiece(pieceTab2);
                pieceTab2.GetComponent<Piece>().row = int.Parse(aData[11]);
                pieceTab2.GetComponent<Piece>().column = int.Parse(aData[12]);
                pieceTab2.GetComponent<Piece>().updateAfterAttack();
                Vector3 TempPlace2 = new Vector3(float.Parse(aData[8]), float.Parse(aData[9]), float.Parse(aData[10]));
                Debug.Log(TempPlace2);
                myManager.MovePiece(pieceTab2.GetComponent<Rook>(), TempPlace2);

                myPlayerControl.playerTurn = int.Parse(aData[13]);


                break;
            case "CMSG":
                myManager = GameObject.Find("GameManager").GetComponent<GameManager>();
                myPlayerControl = myManager.GetComponent<PlayerControl>();
                print("server sent a message from a player");
                myPlayerControl.ChatMessage(aData[1]);
                break;
            case "CMMSG":
                MenuChat mchat = GameObject.Find("MenuChat").GetComponent<MenuChat>();
                print("server sent a message from a player");

                mchat.ChatMessage(aData[1]);
                break;
            case "CRDY":
                switch (aData[1])
                {
                    case "1":
                        UiMenuValues.instance.excuteToggleReady(true, true);
                        break;
                    case "2":
                        UiMenuValues.instance.excuteToggleReady(false, true);
                        break;
                }
                break;
            case "CNRDY":
                switch (aData[1])
                {
                    case "1":
                        UiMenuValues.instance.excuteToggleReady(true, false);
                        break;
                    case "2":
                        UiMenuValues.instance.excuteToggleReady(false, false);
                        break;
                }

                break;
            case "CSTRT":
                //MenuChat menuchat = GameObject.Find("MenuChat").GetComponent<MenuChat>();
                this.countDownPanel = GameObject.Find("Canvas").transform.FindChild("countDownPanel").gameObject;
                this.countDownPanel.SetActive(true);
                this.countDownLabel = countDownPanel.transform.GetComponentInChildren<Text>();
                countDownTime = 3f;
                isGameStarted = false;
                startCountDown = true;

                break;

            case "CLFT":
                print("SLFT");
                if (int.Parse(aData[1]) == 2)
                {
                    print("we entered SLFT Case ");
                    if (isHost)
                    {
                        print("we entered SLFT Case isHost ");
                        //players.RemoveAt(players.Count - 1);
                        UiMenuValues.instance.clientLeftWaitingRoomInfo();
                        Rooms.instance.SetNotReadyForRoom(UiMenuValues.instance.activeRoomId);
                    }
                    else
                    {
                        m_UNETChess c = FindObjectOfType<m_UNETChess>();
                        ShutDownConnection();
                        ShutDownRoom();
                        Destroy(this.gameObject);
                    }

                }
                else if (int.Parse(aData[1]) == 1)
                {
                    if (!isHost)
                    {
                        UiAnimationManager.instance.showOnlinesGamesMenuFromCreatRoomMenu();
                        UiAnimationManager.instance.hostClosedRoom(); 
                        ShutDownConnection();

                    }

                    m_UNETChess c = FindObjectOfType<m_UNETChess>();
                    ShutDownConnection();
                    ShutDownRoom();
                    Destroy(c.gameObject);
                }
                break;

            case "CSTOP":
                myPlayerControl.playerTurn = -1;
                break;
            case "SRNDR":

					print("we received surrender command");
                   //TODO check if player 1 who surrendered or player 2
                    if(int.Parse(aData[1]) == 1) 
                    {
						if(!isHost)
						{
							print("client should win");
							StartCoroutine(ChessBoard.instance.otherChessPlayerSurrendered());	
						}
                       //Player 1 surrendered
                    }
                    else if (int.Parse(aData[1]) == 2)
                    {
                        //Player 2 Surrendered
						if(isHost)
						{
							print("host should win");
							StartCoroutine(ChessBoard.instance.otherChessPlayerSurrendered());	
						}

					}


                   
                break;
            default:
                Debug.Log("Invalid Message! : " + message);
                break;
        }
    }




    //Server Methods
    private void OnConnection(int cnnId)
    {
        //when the player joins the server, tell him his id
        //Request his name and send the name of the all other players
        string msg = "ASKNAME|";
        UNETSend(msg);
    }

    //Client Methods

    private void OnAskName(string[] data)
    {
        // Send out name to the server

        UNETSend("NAMEIS|" + currentPlayerName + "|" + Login.instance.successfulLoginUser.data.user.country_code + "|" + Login.instance.successfulLoginUser.data.user.id);

    }

    private void OnNameIs(string playerNameCountry)
    {
		UiAnimationManager.instance.hideLoadingPanel();

        string[] aData = playerNameCountry.Split('|');

        //Link the name to the connection ID
        secondPlayerName = aData[0];
        secondPlayerCountry = aData[1];
        secondPlayerID = int.Parse(aData[2]);
        if (!isHost)
        {
            //show lobby layout.
            UiAnimationManager.instance.showWaitingRoomMenu();
            //	ServerManager.Instance.StartGame();      
            UiMenuValues.instance.updateWaitingRoomInfo(secondPlayerName, currentPlayerName, false, false, 2, false);

        }
        else
        {
            UiMenuValues.instance.updateWaitingRoomInfo(currentPlayerName, secondPlayerName, false, false, 2, false);
        }


    }

    public void UNETSend(string message)
    {
        m_Writer.SeekZero();
        m_Writer.Write(message);
        byte error;
        for (int i = 0; i < m_ConnectionIds.Count; ++i)
        {
            NetworkTransport.Send(m_HostId,
                m_ConnectionIds[i], 0, m_Writer.AsArray(), m_Writer.Position, out error);
            Debug.Log("Sending: " + message + "to :" + m_ConnectionIds[i]);
            if ((NetworkError)error != NetworkError.Ok)
                Debug.LogError("Failed to send message: " + (NetworkError)error);
        }
    }

}

[System.Serializable]
public class PingResponse
{
    public int status;
}

