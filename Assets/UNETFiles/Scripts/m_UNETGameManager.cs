﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class m_UNETGameManager : MonoBehaviour {
    public static m_UNETGameManager instance { set; get; }

    public GameObject UNETChessPrefab;
    public GameObject UNETDominoPrefab;
    public GameObject UNETSnakePrefab;

    public string currentPlayerName;

	// Use this for initialization
	void Start () {
        instance = this;
        DontDestroyOnLoad(this);
		if(Login.instance != null && Login.instance.hasLogined)
		{
			currentPlayerName = Login.instance.userName;
		}
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    //Chess Host and Connect Methods
    public void HostChess()
    {
        m_UNETChess c = Instantiate(UNETChessPrefab).GetComponent<m_UNETChess>();
        c.name = "UNETChessObject";
        c.currentPlayerName = currentPlayerName;
        c.isHost = true;
        c.CreateRoomUNET();
    }
    public void ConnectToChess(long match_id, string password)
    {
        Debug.Log("Connect to chess" + password);

        m_UNETChess c = Instantiate(UNETChessPrefab).GetComponent<m_UNETChess>();
        c.currentPlayerName = currentPlayerName;
        c.name = "UNETChessObject";
        c.isHost = false;
        c.JoinRoomButton(match_id,password);
    }

    //Domino Host and Connect Methods
    public void HostDomino()
    {
        m_UNETDomino c = Instantiate(UNETDominoPrefab).GetComponent<m_UNETDomino>();
        c.name = "UNETDominoObject";
        c.currentPlayerName = currentPlayerName;
        c.isHost = true;
        c.CreateRoomUNET();
    }

    public void ConnectToDomino(long match_id, string password)
    {
        m_UNETDomino c = Instantiate(UNETDominoPrefab).GetComponent<m_UNETDomino>();
        c.currentPlayerName = currentPlayerName;
        c.name = "UNETDominoObject";
        c.isHost = false;
        c.JoinRoomButton(match_id, password);
    }

    //Snake Host and Connect Methods
    public void HostSnake()
    {
        m_UNETSnake c = Instantiate(UNETSnakePrefab).GetComponent<m_UNETSnake>();
        c.name = "UNETSnakeObject";
        c.currentPlayerName = currentPlayerName;
        c.isHost = true;
        c.CreateRoomUNET();
    }

    public void ConnectToSnake(long match_id, string password)
    {
        m_UNETSnake c = Instantiate(UNETSnakePrefab).GetComponent<m_UNETSnake>();
        c.currentPlayerName = currentPlayerName;
        c.name = "UNETSnakeObject";
        c.isHost = false;
        c.JoinRoomButton(match_id, password);
    }

    public void HostGame()
    {
		UiAnimationManager.instance.showLoadingPanel();
        string gameType = UiMenuValues.instance.gameType;
        switch (gameType)
        {
            case "chess":
                m_UNETGameManager.instance.HostChess();
                break;
            case "snake":
                m_UNETGameManager.instance.HostSnake();

                break;

            case "domino":
                m_UNETGameManager.instance.HostDomino();

                break;
        }
    }
}
