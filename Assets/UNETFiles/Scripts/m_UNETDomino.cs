﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.Types;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class m_UNETDomino : MonoBehaviour {

    public static m_UNETDomino instance { set; get; }
	// Use this for initialization

    public bool isHost;
    public string currentPlayerName;
    public string secondPlayerName;
    public string secondPlayerCountry;
    public int secondPlayerID;


    // Matchmaker related
    List<MatchInfoSnapshot> m_MatchList = new List<MatchInfoSnapshot>();
    bool m_MatchCreated = false;
    bool m_MatchJoined = false;
    MatchInfo m_MatchInfo;
    string m_MatchName = "";
    NetworkMatch m_NetworkMatch;
    private int reliableChannel;
    private int unreliableChannel;
    // Connection/communication related
    int m_HostId = -1;
    // On the server there will be multiple connections, on the client this will only contain one ID
    public List<int> m_ConnectionIds = new List<int>();

    byte[] m_ReceiveBuffer = new byte[1024];
    string m_NetworkMessage = "H";
    string m_LastReceivedMessage = "";
    NetworkWriter m_Writer;
    NetworkReader m_Reader;
    bool m_ConnectionEstablished;

    const int k_ServerPort = 25000;
    const int k_MaxMessageSize = 1024;
    private byte error;
    //Client Related Variables


    public ulong MatchIDUlong;

    // Domino Related Variables

    public int GameScore;
    public int hostScore;
    public int clientScore;
    public List<int> player1roundScores;
    public List<int> player2roundScores;
    public int roundNumber;
    public DominoRoundEndController dominoRoundEndController;
    public DominoRoundsTable dominoRoundTable;



    public float gameTimeCounter;
    public int gameSeconds;
    public int gameMinutes;
    public int gameHours;
    private PiecesManager myManager;
//    public PlayerControl myPlayerControl;


    bool isGameStarted;
    bool startCountDown;
    float countDownTime;
    GameObject countDownPanel;
    Text countDownLabel;

    protected string pingURL = "https://multigame.4mobily.net/public/api/v1/user/ping";


    void Awake()
    {
        m_NetworkMatch = gameObject.AddComponent<NetworkMatch>();
    }

    void FixedUpdate()
    {
        if (startCountDown && !isGameStarted)
        {

            countDownLabel.text = Mathf.CeilToInt(countDownTime).ToString();
            countDownTime -= Time.fixedDeltaTime;
            if (countDownTime < 0)
            {
                //countDownPanel.SetActive(false);
                isGameStarted = true;
                startCountDown = false;
                print("game should be started");
				Resources.UnloadUnusedAssets();
                SceneManager.LoadScene("Domino");
                //invokePing();
            }


        }
        else
        {
            return;
        }


    }

    void Start()
    {

        DontDestroyOnLoad(this.gameObject);
		currentPlayerName = (Login.instance !=  null) ? Login.instance.userName :  UiMenuValues.instance.userName ;
        instance = this;
        m_ReceiveBuffer = new byte[k_MaxMessageSize];
        m_Writer = new NetworkWriter();
        // While testing with multiple standalone players on one machine this will need to be enabled
        Application.runInBackground = true;
        secondPlayerName = "";
        DontDestroyOnLoad(gameObject);
    }
    
	// Update is called once per frame
    void OnApplicationQuit()
    {
        NetworkTransport.Shutdown();
    }

    public void CreateRoomUNET()
    {
        string roomName;
        string password;
        roomName = UiMenuValues.instance.roomNameToBeCreated;
        password = UiMenuValues.instance.roomToEnterPassword;

        m_MatchName = roomName;

        if (m_MatchName != "")
		{
			m_NetworkMatch.CreateMatch(m_MatchName, 2, true, "", "", "", 0, 0, OnMatchCreate);
		}
        else
        {
            StartCoroutine(UiAnimationManager.instance.showFailureMassage("Can't create empty name"));
            UiAnimationManager.instance.hideLoadingPanel();
			Destroy(this.gameObject,2.5f);

        }


    }

    public void JoinRoomButton(long Match_ID, string password)
    {

        JoinRoomUNET((NetworkID)Match_ID, password);

    }
    public void JoinRoomUNET(NetworkID matchID, string roomPassword)
    {
        m_NetworkMatch.JoinMatch(matchID, "", "", "", 0, 0, OnMatchJoined);
    }

    public void ShutDownConnection()
    {
        m_NetworkMatch.DropConnection(m_MatchInfo.networkId, m_MatchInfo.nodeId, 0, OnConnectionDropped);
    }
    public void ShutDownRoom()
    {
        NetworkTransport.Shutdown();

    }
    public void OnConnectionDropped(bool success, string extendedInfo)
    {
        Debug.Log("Connection has been dropped on matchmaker server");
        NetworkTransport.Shutdown();
        m_HostId = -1;
        m_ConnectionIds.Clear();
        m_MatchInfo = null;
        m_MatchCreated = false;
        m_MatchJoined = false;
        m_ConnectionEstablished = false;
    }

    public virtual void OnMatchCreate(bool success, string extendedInfo, MatchInfo matchInfo)
    {
        if (success)
        {
            Debug.Log("Create match succeeded");
            Utility.SetAccessTokenForNetwork(matchInfo.networkId, matchInfo.accessToken);

            m_MatchCreated = true;
            m_MatchInfo = matchInfo;
            StartServer(matchInfo.address, matchInfo.port, matchInfo.networkId,
                matchInfo.nodeId);
            MatchIDUlong = (ulong)matchInfo.networkId;

            Rooms.instance.createRoom(MatchIDUlong);

        }
        else
        {
            Debug.LogError("Create match failed: " + extendedInfo);
        }

    }

    //public void OnMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
    //{
    //    if (success && matches != null)
    //    {
    //        m_MatchList = matches;
    //    }
    //    else if (!success)
    //    {
    //        Debug.LogError("List match failed: " + extendedInfo);
    //    }
    //}

    // When we've joined a match we connect to the server/host
    public virtual void OnMatchJoined(bool success, string extendedInfo, MatchInfo matchInfo)
    {
        if (success)
        {
            Debug.Log("Join match succeeded");
            Utility.SetAccessTokenForNetwork(matchInfo.networkId, matchInfo.accessToken);

            m_MatchJoined = true;
            m_MatchInfo = matchInfo;

            Debug.Log("Connecting to Address:" + matchInfo.address +
                " Port:" + matchInfo.port +
                " NetworKID: " + matchInfo.networkId +
                " NodeID: " + matchInfo.nodeId);
            ConnectThroughRelay(matchInfo.address, matchInfo.port, matchInfo.networkId,
                matchInfo.nodeId);
        }
        else
        {
            Debug.LogError("Join match failed: " + extendedInfo);
			StartCoroutine(UiAnimationManager.instance.showFailureMassage("Join match failed: " + extendedInfo));
        }
    }

    void SetupHost(bool isServer)
    {
        Debug.Log("Initializing network transport");
        NetworkTransport.Init();
        var config = new ConnectionConfig();
        config.NetworkDropThreshold = 10;
        reliableChannel = config.AddChannel(QosType.Reliable);
        unreliableChannel = config.AddChannel(QosType.Unreliable);
        var topology = new HostTopology(config, 2);
        if (isServer)
            m_HostId = NetworkTransport.AddHost(topology, k_ServerPort);
        else
            m_HostId = NetworkTransport.AddHost(topology);
    }

    void StartServer(string relayIp, int relayPort, NetworkID networkId, NodeID nodeId)
    {
        isHost = true;
        SetupHost(isHost);

        byte error;
        NetworkTransport.ConnectAsNetworkHost(
            m_HostId, relayIp, relayPort, networkId, Utility.GetSourceID(), nodeId, out error);
    }

    void ConnectThroughRelay(string relayIp, int relayPort, NetworkID networkId, NodeID nodeId)
    {
        isHost = false;
        SetupHost(isHost);

        byte error;
        NetworkTransport.ConnectToNetworkPeer(
            m_HostId, relayIp, relayPort, 0, 0, networkId, Utility.GetSourceID(), nodeId, out error);
    }

    void Update()
    {
        
        if (m_HostId == -1)
            return;
        //ReceivedText.text = 
        NetworkEventType networkEvent; //= NetworkEventType.Nothing;
        int connectionId;
        int channelId;
        int receivedSize;
        byte error;

        // Get events from the relay connection
        networkEvent = NetworkTransport.ReceiveRelayEventFromHost(m_HostId, out error);
        if (networkEvent == NetworkEventType.ConnectEvent)
            Debug.Log("Relay server connected");
        if (networkEvent == NetworkEventType.DisconnectEvent)
            Debug.Log("Relay server disconnected");

        do
        {
            //Debug.Log("Inside the do while loop");
            // Get events from the server/client game connection
            networkEvent = NetworkTransport.Receive(out m_HostId, out connectionId, out channelId,
                m_ReceiveBuffer, (int)m_ReceiveBuffer.Length, out receivedSize, out error);
            // Debug.Log(error +"|"+ Encoding.UTF8.GetString(m_ReceiveBuffer));
            if ((NetworkError)error != NetworkError.Ok)
            {
                Debug.LogError("Error while receiveing network message: " + (NetworkError)error);
            }

            switch (networkEvent)
            {
                case NetworkEventType.ConnectEvent:
                    {
                        Debug.Log("Connected through relay, ConnectionID:" + connectionId +
                            " ChannelID:" + channelId);
                        m_ConnectionEstablished = true;
                        m_ConnectionIds.Add(connectionId);
                        OnConnection(connectionId);
                        break;
                    }
                case NetworkEventType.DataEvent:
                    {
                        Debug.Log("Data event, ConnectionID:" + connectionId +
                            " ChannelID: " + channelId +
                            " Received Size: " + receivedSize);

                        m_Reader = new NetworkReader(m_ReceiveBuffer);
                        m_LastReceivedMessage = m_Reader.ReadString();
                        Debug.Log(m_LastReceivedMessage);

                        ClientDomino(m_LastReceivedMessage);



                        break;
                    }
                case NetworkEventType.DisconnectEvent:
                    {
                        Debug.Log("Connection disconnected, ConnectionID:" + connectionId);
					m_ConnectionIds.Remove(connectionId);

                        break;
                    }
                case NetworkEventType.Nothing:
                    break;
            }
        } while (networkEvent != NetworkEventType.Nothing);
    }

    
    //Methods for dealing with chess game

    public void ClientDomino(string message)
    {
        string[] aData = message.Split('|');
        switch (aData[0])
        {

            case "ASKNAME":
                OnAskName(aData);
                break;
            case "NAMEIS":
                OnNameIs(aData[1] + "|" + aData[2] + "|" + aData[3]);
                break;
            case "CMSG":
                 myManager = GameObject.Find("piecesManager").GetComponent<PiecesManager>();
                print("server sent a message from a player");
                myManager.ChatMessage(aData[1]);
                break;
            case "CASN":
                //                Debug.Log("Entered Assigning from server");
                //                Debug.Log(PiecesManager.instance.isHost);
                if (PiecesManager.instance.isHost)
                {
                    PiecesManager.instance.showPiecesToPlayer1();
                }
                else
                {
                    for (int i = 1; i < 8; i++)
                    {
                        for (int j = 0; j < PiecesManager.instance.allPiecesServer.Count; j++)
                        {
                            if (PiecesManager.instance.allPiecesServer[j].PieceID == int.Parse(aData[i]))
                            {
                                DominoGameManager.instance.player2.playerPieces.Add(PiecesManager.instance.allPiecesServer[j]);
                            }
                        }
                    }

                    for (int i = 8; i < 15; i++)
                    {
                        for (int j = 0; j < PiecesManager.instance.allPiecesServer.Count; j++)
                        {
                            if (PiecesManager.instance.allPiecesServer[j].PieceID == int.Parse(aData[i]))
                            {
                                DominoGameManager.instance.player1.playerPieces.Add(PiecesManager.instance.allPiecesServer[j]);
                            }
                        }
                    }

                    for (int i = 15; i < 29; i++)
                    {
                        for (int j = 0; j < PiecesManager.instance.allPiecesServer.Count; j++)
                        {
                            if (PiecesManager.instance.allPiecesServer[j].PieceID == int.Parse(aData[i]))
                            {
                                PiecesManager.instance.stockPieces.Add(PiecesManager.instance.allPiecesServer[j]);
                            }
                        }
                    }
                    PiecesManager.instance.ShowPieces();
                }
                break;
            case "CMOV":
                for (int i = 0; i < PiecesManager.instance.allPiecesServer.Count; i++)
                {
                    if (PiecesManager.instance.allPiecesServer[i].PieceID == int.Parse(aData[1]))
                    {

                        //                    Vector3 newPosition = new Vector3(float.Parse(aData[2]),float.Parse(aData[3]),float.Parse(aData[4]));
                        //                    Quaternion newRotation = new Quaternion(float.Parse(aData[5]), float.Parse(aData[6]), float.Parse(aData[7]),0f);
                        int moveDirection = int.Parse(aData[2]);
                        bool isEnd1 = (aData[3] == "1") ? true : false;
                        DominoPiece pieceToMove = DominoGameManager.instance.getPieceByID(int.Parse(aData[1]));

                        switch (moveDirection)
                        {
                            case Constants.moveLeft:
                                DominoGround.instance.moveLeft(pieceToMove, isEnd1);
                                break;
                            case Constants.moveUp:
                                DominoGround.instance.moveUp(pieceToMove, isEnd1);
                                break;
                            case Constants.moveRight:
                                DominoGround.instance.moveRight(pieceToMove, isEnd1);
                                //						DominoGround.instance.updateCorrectEnd(isEnd1);
                                break;
                            case Constants.moveDown:
                                DominoGround.instance.moveDown(pieceToMove, isEnd1);
                                break;
                            case Constants.firstMove:
                                DominoGround.instance.moveFirstPiece(pieceToMove);
                                DominoGameManager.instance.switchTurns();


                                break;
                        }

                        DominoGameManager.instance.destroyPieceByID(int.Parse(aData[1]));
                        print(aData[4]);
                        PiecesManager.instance.removeDummyPiece((aData[4] == "1") ? true : false);
                        //					DominoGameManager.instance.checkRoundWinning();
                        //					DominoGameManager.instance.switchTurns();
                        //					DominoGameManager.instance.autoZoom();
                        Debug.Log(DominoGameManager.instance.player1.isMyTurn);
                        PiecesManager.instance.allPiecesServer.RemoveAt(i);
                        break;
                    }
                }
                break;

            case "CTURN":

                //if aData[1] == 1
                //turn for host
                //else if aData[2]
                //turn for player 2
                Debug.Log("Receiving assign turns from server");
                print("player turn from STURN =  " + aData[1]);
                DominoGameManager.instance.switchTurns((aData[1] == "1") ? true : false);
                break;

            case "CSNP":
                print("inside  SSNP Case");
                DominoGameManager.instance.switchTurns();
                DominoGameManager.instance.hideStockPiecePanel();
                //			DominoGameManager.instance.showContinuePlayPanel();
                Invoke("showContinuePlayingPanelAfterDelay", 0.1f);

                break;

            case "CSTOCK":
                // stock case for network
                int pieceID = int.Parse(aData[1]);
                bool isHostWhoDrew = (aData[2] == "1") ? true : false;
                PiecesManager.instance.applyStockDrawPieceFromNetwork(pieceID, isHostWhoDrew);
                break;

            case "CSCORE":
                DominoGameManager.instance.assignScore();
                break;

            //assign scores here 
            case "CMMSG":
                MenuChat mchat = GameObject.Find("MenuChat").GetComponent<MenuChat>();
                print("server sent a message from a player");
                mchat.ChatMessage(aData[1]);
                break;
            case "CRDY":
                switch (aData[1])
                {
                    case "1":
                        UiMenuValues.instance.excuteToggleReady(true, true);
                        break;
                    case "2":
                        UiMenuValues.instance.excuteToggleReady(false, true);
                        break;
                }
                break;
            case "CNRDY":
                switch (aData[1])
                {
                    case "1":
                        UiMenuValues.instance.excuteToggleReady(true, false);
                        break;
                    case "2":
                        UiMenuValues.instance.excuteToggleReady(false, false);
                        break;
                }

                break;
            case "CSTRT":
                //MenuChat menuchat = GameObject.Find("MenuChat").GetComponent<MenuChat>();
                this.countDownPanel = GameObject.Find("Canvas").transform.FindChild("countDownPanel").gameObject;
                this.countDownPanel.SetActive(true);
                this.countDownLabel = countDownPanel.transform.GetComponentInChildren<Text>();
                countDownTime = 3f;
                isGameStarted = false;
                startCountDown = true;

                break;

            case "CLFT":
                //print("SLFT");
                if (int.Parse(aData[1]) == 2)
                {
                    print("we entered SLFT Case ");
                    if (isHost)
                    {
                        print("we entered SLFT Case isHost ");

                        UiMenuValues.instance.clientLeftWaitingRoomInfo();
                        Rooms.instance.SetNotReadyForRoom(UiMenuValues.instance.activeRoomId);
                    }
                    else
                    {
                        m_UNETDomino c = FindObjectOfType<m_UNETDomino>();
                        ShutDownConnection();
                        ShutDownRoom();
                        Destroy(c.gameObject);
                    }

                }
                else if (int.Parse(aData[1]) == 1)
                {
                    if (!isHost)
                    {
                        UiAnimationManager.instance.showOnlinesGamesMenuFromCreatRoomMenu();
                        UiAnimationManager.instance.hostClosedRoom();
                    }

                    ShutDownConnection();
                    ShutDownRoom();
                    Destroy(this.gameObject);
                }
                break;
            case "SRNDR":
                //TODO check if player 1 who surrendered or player 2
			if(int.Parse(aData[1]) == 1) 
			{
				if(!isHost)
				{
					print("client should win");
					StartCoroutine(DominoGameManager.instance.otherDominoPlayerSurrendered());	
				}
				//Player 1 surrendered
			}
			else if (int.Parse(aData[1]) == 2)
			{
				//Player 2 Surrendered
				if(isHost)
				{
					print("host should win");
					StartCoroutine(DominoGameManager.instance.otherDominoPlayerSurrendered());	
				}

			}

                break;
            default:
                Debug.Log("Invalid Message! : " + message);
                break;
        }
    }




    //Server Methods
    private void OnConnection(int cnnId)
    {
        //when the player joins the server, tell him his id
        //Request his name and send the name of the all other players
        string msg = "ASKNAME|";
        UNETSend(msg);
    }

    //Client Methods

    private void OnAskName(string[] data)
    {
        // Send out name to the server

        UNETSend("NAMEIS|" + currentPlayerName + "|" + Login.instance.successfulLoginUser.data.user.country_code + "|" + Login.instance.successfulLoginUser.data.user.id);

    }


    private void OnNameIs(string playerNameCountry)
    {
		UiAnimationManager.instance.hideLoadingPanel();

        string[] aData = playerNameCountry.Split('|');

        //Link the name to the connection ID
        secondPlayerName = aData[0];
        secondPlayerCountry = aData[1];
        secondPlayerID = int.Parse(aData[2]);

        if (!isHost)
        {
            //show lobby layout.
            UiAnimationManager.instance.showWaitingRoomMenu();
            //	ServerManager.Instance.StartGame();      
            UiMenuValues.instance.updateWaitingRoomInfo(secondPlayerName, currentPlayerName, false, false, 2, false);
        }
        else
        {
            UiMenuValues.instance.updateWaitingRoomInfo(currentPlayerName, secondPlayerName, false, false, 2, false);
        }
    }

    public void UNETSend(string message)
    {
        m_Writer.SeekZero();
        m_Writer.Write(message);
        byte error;
        for (int i = 0; i < m_ConnectionIds.Count; ++i)
        {
            NetworkTransport.Send(m_HostId,
                m_ConnectionIds[i], 0, m_Writer.AsArray(), m_Writer.Position, out error);
            Debug.Log("Sending: " + message + "to :" + m_ConnectionIds[i]);
            if ((NetworkError)error != NetworkError.Ok)
                Debug.LogError("Failed to send message: " + (NetworkError)error);
        }
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnDominoLevelFinishedLoading;
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnDominoLevelFinishedLoading;
    }

    void OnDominoLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "Domino")
        {
            if (roundNumber == 0)
            {

                ShuffleController.instance.hideStockPiecesUiTiles();
                ShuffleController.instance.startShuffling();

                return;
            }
            else if (roundNumber > 0)
            {
                //			 do stuff after new round here
                dominoRoundEndController.gameObject.SetActive(true);
                dominoRoundEndController.showRoundData(player1roundScores[roundNumber - 1]);
                dominoRoundTable.CleanRoundStatsTable();
                for (int i = 0; i < roundNumber; i++)
                {
                    dominoRoundTable.addRoundStats(i + 1, player1roundScores[i], player2roundScores[i]);
                }
                dominoRoundTable.addRoundStats(roundNumber + 1, -1, -1);
            }
        }

    }


    public void reloadDominoScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void saveGameTime(float gameTimeCounter, int gameSeconds, int gameMinutes, int gameHours)
    {
        this.gameTimeCounter = gameTimeCounter;
        this.gameSeconds = gameSeconds;
        this.gameMinutes = gameMinutes;
        this.gameHours = gameHours;
    }

    public float[] loadGameTime()
    {
        float[] times = { gameTimeCounter, gameSeconds, gameMinutes, gameHours };
        return times;
    }

    public void showContinuePlayingPanelAfterDelay()
    {
        if (DominoGameManager.instance.player1.isMyTurn)
        {
            print("show continue play panel should be excuted");
            DominoGameManager.instance.showContinuePlayPanel();
        }
    }

}
