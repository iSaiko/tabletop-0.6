﻿using GooglePlayGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.Types;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class m_UNETSnake : MonoBehaviour {

    public static m_UNETSnake instance { set; get; }

    public bool isHost;

    public string currentPlayerName;
	public string currentPlayerCountry;
    public string secondPlayerName; 
    public string secondPlayerCountry;
    public int secondPlayerID;
    public int BoardNumber;

    // Matchmaker related
    List<MatchInfoSnapshot> m_MatchList = new List<MatchInfoSnapshot>();
    bool m_MatchCreated = false;
    bool m_MatchJoined = false;
    MatchInfo m_MatchInfo;
    string m_MatchName = "";
    NetworkMatch m_NetworkMatch;
    private int reliableChannel;
    private int unreliableChannel;
    // Connection/communication related
    int m_HostId = -1;
    // On the server there will be multiple connections, on the client this will only contain one ID
    public List<int> m_ConnectionIds = new List<int>();

    byte[] m_ReceiveBuffer = new byte[1024];
    string m_NetworkMessage = "H";
    string m_LastReceivedMessage = "";
    NetworkWriter m_Writer;
    NetworkReader m_Reader;
    bool m_ConnectionEstablished;

    const int k_ServerPort = 25000;
    const int k_MaxMessageSize = 1024;
    private byte error;
    //Client Related Variables


    public ulong MatchIDUlong;

    // Snake Related Variables

    public int player1Roll, player2Roll;

    bool isGameStarted;
    bool startCountDown;
    float countDownTime;
    GameObject countDownPanel;
    Text countDownLabel;
    private SnakeBoard myManager;
    public PlayerControl myPlayerControl;

	// Use this for initialization


    void Awake()
    {
        m_NetworkMatch = gameObject.AddComponent<NetworkMatch>();
    }

    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
		currentPlayerName = (Login.instance !=  null) ? Login.instance.userName :  UiMenuValues.instance.userName ;
		currentPlayerCountry = Login.instance.successfulLoginUser.data.user.country_code;
        instance = this;
        m_ReceiveBuffer = new byte[k_MaxMessageSize];
        m_Writer = new NetworkWriter();
        // While testing with multiple standalone players on one machine this will need to be enabled
        Application.runInBackground = true;
        secondPlayerName = "";

    }

    // Update is called once per frame
    void OnApplicationQuit()
    {
        NetworkTransport.Shutdown();
    }

    void FixedUpdate()
    {
        if (startCountDown && !isGameStarted)
        {

            countDownLabel.text = Mathf.CeilToInt(countDownTime).ToString();
            print(Mathf.CeilToInt(countDownTime).ToString());
            countDownTime -= Time.fixedDeltaTime;
            if (countDownTime < 0)
            {
                //countDownPanel.SetActive(false);
                isGameStarted = true;
                startCountDown = false;
                print("game should be started");
				Resources.UnloadUnusedAssets();
                SceneManager.LoadScene("Snake");
                //invokePing();
            }


        }
        else
        {

            return;
        }


    }

   


    public void CreateRoomUNET()
    {
        string roomName;
        string password;
        roomName = UiMenuValues.instance.roomNameToBeCreated;
        password = UiMenuValues.instance.roomToEnterPassword;

        m_MatchName = roomName;

        if (m_MatchName != "")
		{
			m_NetworkMatch.CreateMatch(m_MatchName, 2, true, "", "", "", 0, 0, OnMatchCreate);
		}
        else
        {
            StartCoroutine(UiAnimationManager.instance.showFailureMassage("Can't create empty name"));
            UiAnimationManager.instance.hideLoadingPanel();
			Destroy(this.gameObject,2.5f);


        }


    }

    public void JoinRoomButton(long Match_ID, string password)
    {

        JoinRoomUNET((NetworkID)Match_ID, password);

    }
    public void JoinRoomUNET(NetworkID matchID, string roomPassword)
    {
        m_NetworkMatch.JoinMatch(matchID, "", "", "", 0, 0, OnMatchJoined);
    }

    public void ShutDownConnection()
    {
        m_NetworkMatch.DropConnection(m_MatchInfo.networkId, m_MatchInfo.nodeId, 0, OnConnectionDropped);
    }
    public void ShutDownRoom()
    {
        NetworkTransport.Shutdown();

    }
    public void OnConnectionDropped(bool success, string extendedInfo)
    {
        Debug.Log("Connection has been dropped on matchmaker server");
        NetworkTransport.Shutdown();
        m_HostId = -1;
        m_ConnectionIds.Clear();
        m_MatchInfo = null;
        m_MatchCreated = false;
        m_MatchJoined = false;
        m_ConnectionEstablished = false;
    }

    public virtual void OnMatchCreate(bool success, string extendedInfo, MatchInfo matchInfo)
    {
        if (success)
        {
            Debug.Log("Create match succeeded");
            Utility.SetAccessTokenForNetwork(matchInfo.networkId, matchInfo.accessToken);

            m_MatchCreated = true;
            m_MatchInfo = matchInfo;
            StartServer(matchInfo.address, matchInfo.port, matchInfo.networkId,
                matchInfo.nodeId);
            MatchIDUlong = (ulong)matchInfo.networkId;

            Rooms.instance.createRoom(MatchIDUlong);
        }
        else
        {
            Debug.LogError("Create match failed: " + extendedInfo);
        }

    }

    //public void OnMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
    //{
    //    if (success && matches != null)
    //    {
    //        m_MatchList = matches;
    //    }
    //    else if (!success)
    //    {
    //        Debug.LogError("List match failed: " + extendedInfo);
    //    }
    //}

    // When we've joined a match we connect to the server/host
    public virtual void OnMatchJoined(bool success, string extendedInfo, MatchInfo matchInfo)
    {
        if (success)
        {
            Debug.Log("Join match succeeded");
            Utility.SetAccessTokenForNetwork(matchInfo.networkId, matchInfo.accessToken);

            m_MatchJoined = true;
            m_MatchInfo = matchInfo;

            Debug.Log("Connecting to Address:" + matchInfo.address +
                " Port:" + matchInfo.port +
                " NetworKID: " + matchInfo.networkId +
                " NodeID: " + matchInfo.nodeId);
            ConnectThroughRelay(matchInfo.address, matchInfo.port, matchInfo.networkId,
                matchInfo.nodeId);
        }
        else
        {
            Debug.LogError("Join match failed: " + extendedInfo);
			StartCoroutine(UiAnimationManager.instance.showFailureMassage("Join match failed: " + extendedInfo));
        }
    }

    void SetupHost(bool isServer)
    {
        Debug.Log("Initializing network transport");
        NetworkTransport.Init();
        var config = new ConnectionConfig();
        config.NetworkDropThreshold = 10;
        reliableChannel = config.AddChannel(QosType.Reliable);
        unreliableChannel = config.AddChannel(QosType.Unreliable);
        var topology = new HostTopology(config, 2);
        if (isServer)
            m_HostId = NetworkTransport.AddHost(topology, k_ServerPort);
        else
            m_HostId = NetworkTransport.AddHost(topology);
    }

    void StartServer(string relayIp, int relayPort, NetworkID networkId, NodeID nodeId)
    {
        isHost = true;
        SetupHost(isHost);

        byte error;
        NetworkTransport.ConnectAsNetworkHost(
            m_HostId, relayIp, relayPort, networkId, Utility.GetSourceID(), nodeId, out error);
    }

    void ConnectThroughRelay(string relayIp, int relayPort, NetworkID networkId, NodeID nodeId)
    {
        isHost = false;
        SetupHost(isHost);

        byte error;
        NetworkTransport.ConnectToNetworkPeer(
            m_HostId, relayIp, relayPort, 0, 0, networkId, Utility.GetSourceID(), nodeId, out error);
    }

    void Update()
    {
        if (m_HostId == -1)
            return;
        //ReceivedText.text = 
        NetworkEventType networkEvent; //= NetworkEventType.Nothing;
        int connectionId;
        int channelId;
        int receivedSize;
        byte error;

        // Get events from the relay connection
        networkEvent = NetworkTransport.ReceiveRelayEventFromHost(m_HostId, out error);
        if (networkEvent == NetworkEventType.ConnectEvent)
            Debug.Log("Relay server connected");
        if (networkEvent == NetworkEventType.DisconnectEvent)
            Debug.Log("Relay server disconnected");

        do
        {
            //Debug.Log("Inside the do while loop");
            // Get events from the server/client game connection
            networkEvent = NetworkTransport.Receive(out m_HostId, out connectionId, out channelId,
                m_ReceiveBuffer, (int)m_ReceiveBuffer.Length, out receivedSize, out error);
            // Debug.Log(error +"|"+ Encoding.UTF8.GetString(m_ReceiveBuffer));
            if ((NetworkError)error != NetworkError.Ok)
            {
                Debug.LogError("Error while receiveing network message: " + (NetworkError)error);
            }

            switch (networkEvent)
            {
                case NetworkEventType.ConnectEvent:
                    {
                        Debug.Log("Connected through relay, ConnectionID:" + connectionId +
                            " ChannelID:" + channelId);
                        m_ConnectionEstablished = true;
                        m_ConnectionIds.Add(connectionId);
                        OnConnection(connectionId);
                        //Rooms.instance.up
                        break;
                    }
                case NetworkEventType.DataEvent:
                    {
                        Debug.Log("Data event, ConnectionID:" + connectionId +
                            " ChannelID: " + channelId +
                            " Received Size: " + receivedSize);

                        m_Reader = new NetworkReader(m_ReceiveBuffer);
                        m_LastReceivedMessage = m_Reader.ReadString();
                        Debug.Log(m_LastReceivedMessage);

                        ClientSnake(m_LastReceivedMessage);



                        break;
                    }
                case NetworkEventType.DisconnectEvent:
                    {
                        Debug.Log("Connection disconnected, ConnectionID:" + connectionId);
					m_ConnectionIds.Remove(connectionId);

                        break;
                    }
                case NetworkEventType.Nothing:
                    break;
            }
        } while (networkEvent != NetworkEventType.Nothing);
    }
    //Methods for dealing with chess game

    public void ClientSnake(string message)
    {
        string[] aData = message.Split('|');
        switch (aData[0])
        {

            case "ASKNAME":
                OnAskName(aData);
                break;
            case "NAMEIS":
                    if(isHost)
                        OnNameIs(aData[1] + "|" + aData[2] + "|" + aData[3]);
                    else
                        OnNameIs(aData[1] + "|" + aData[2] + "|" + aData[3] + "|" + aData[4]);
                break;


            case "CMOV":

                if (int.Parse(aData[1]) == 1)
                {
                    SnakeBoard.instance.player1.moveWithAnimation(SnakeBoard.instance.player1.index - player1Roll + 1);
                    if (player1Roll != 6)
                    {
                        SnakeBoard.instance.player1Turn = false;
                    }
                }
                if (int.Parse(aData[1]) == 2)
                {
                    SnakeBoard.instance.player2.moveWithAnimation(SnakeBoard.instance.player2.index - player2Roll + 1);
                    if (player2Roll != 6)
                    {
                        SnakeBoard.instance.player1Turn = true;
                    }
                }


                // Debug.Log(aData[1] + " " + aData[2] + " " + aData[3] + " " + aData[4]);

                break;
            case "CPEC":
                if (int.Parse(aData[1]) == 1)
                {
                    SnakeBoard.instance.player1Entered = true;
                    //Debug.Log("Player1 Entered");
                }
                else if (int.Parse(aData[1]) == 2)
                {
                    SnakeBoard.instance.player2Entered = true;
                    //Debug.Log("Player2 Entered");

                }
                else
                {
                    Debug.Log("Player Sent Error");
                }
                break;
            case "CSWTCH":
                //Debug.Log("Switch Turn On Client!");
                SnakeBoard.instance.switchTurns();
                break;

            case "CROLL":
                print("we recieved any thing from sroll");
                m_UNETSnake client = GameObject.Find("UNETSnakeObject").GetComponent<m_UNETSnake>();
                DicePlayersValue diceScript = GameObject.Find("DiceCanvas").GetComponent<DicePlayersValue>();
                if (int.Parse(aData[1]) == 6 && int.Parse(aData[2]) == 1)
                    SnakeBoard.instance.player1Entered = true;
                if (int.Parse(aData[1]) == 6 && int.Parse(aData[2]) == 2)
                    SnakeBoard.instance.player2Entered = true;
                if (client.isHost && int.Parse(aData[2]) == 1 || !client.isHost && int.Parse(aData[2]) == 2)
                {
                    //Debug.Log(aData[0] + " " + aData[1] + " " + aData[2]);
                    diceScript.ModifyDiceThrowImage1(int.Parse(aData[1]));
                    //Debug.Log(client.isHost + " and Modifying image 1");
                }
                else if (client.isHost && int.Parse(aData[2]) == 2 || !client.isHost && int.Parse(aData[2]) == 1)
                {
                    // Debug.Log(aData[0] + " " + aData[1] + " " + aData[2]);
                    diceScript.ModifyDiceThrowImage2(int.Parse(aData[1]));
                    //Debug.Log(client.isHost + " and Modifying image2");
                }



                if (int.Parse(aData[2]) == 1)
                {
                    player1Roll = int.Parse(aData[1]);
                    //Debug.Log(aData[1] + " " + aData[2]);
                    //if (!SnakeBoard.instance.player1Entered)
                    //{
                    //    SnakeBoard.instance.player1.index = 0;
                    //    SnakeBoard.instance.player1Entered = true;
                    //}
                    //else { 
                    // SnakeBoard.instance.player1.index = int.Parse(aData[4]);
                    // if (SnakeBoard.instance.player1.index + int.Parse(aData[4]) <= 100)
                    //{
                    SnakeBoard.instance.player1.index += int.Parse(aData[1]);
                    if (SnakeBoard.instance.player1.index == 99)
                    {
                        SnakeBoard.instance.someoneWon = true;
                        GameObject Canvas = GameObject.Find("CanvasPlayer");
                        GameObject VictoryFrame = Canvas.transform.FindChild("VictoryFrame").gameObject;
                        VictoryFrame.SetActive(true);
                        GameObject victoryFrameImage = VictoryFrame.transform.FindChild("VictoryFrameImage").gameObject;
                        Text VictoryText = victoryFrameImage.transform.FindChild("WinnerText").GetComponent<Text>();
                        VictoryText.text = "PLAYER 1 HAS WON THE GAME";
                        Debug.Log("Player 1 Won!! ");
                        if (isHost)
                        {
                            Rooms.instance.EndGameRoom(Login.instance.activeRoomId, Login.instance.successfulLoginUser.data.user.id);
                            Social.ReportProgress(TableTopPlayServiceResources.achievement_snakelet, 100.0f, (bool success) =>
                            {
                                Debug.Log("Achievment Successfully Unlocked");
                            });

                            PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_rattling, 2, (bool success) =>
                            {
                                Debug.Log("Achievment Successfully Unlocked");
                            });
                            PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_cobra, 2, (bool success) =>
                            {
                                Debug.Log("Achievment Successfully Unlocked");
                            });
                            PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_anaconda, 2, (bool success) =>
                            {
                                Debug.Log("Achievment Successfully Unlocked");
                            });
                        }
                    }
                    //Debug.Log("Player 1 Move from Client");
                    //    }
                    Debug.Log(SnakeBoard.instance.player1.index);
                    if (SnakeBoard.instance.player1.index < 100)
                    {
                        if (int.Parse(aData[1]) == 6)
                        {
                            SnakeBoard.instance.player1HasBonusMove = true;
                            //SnakeBoard.instance.player1Turn = true;
                        }
                        else
                        {
                            SnakeBoard.instance.player1HasBonusMove = false;
                        }
                    }
                    else
                    {
                        SnakeBoard.instance.player1.index -= int.Parse(aData[1]);
                        SnakeBoard.instance.player1Turn = false;
                    }
                }
                else if (int.Parse(aData[2]) == 2)
                {
                    player2Roll = int.Parse(aData[1]);

                    //Debug.Log(aData[1] + " " + aData[2]);

                    //if (!SnakeBoard.instance.player2Entered)
                    //{
                    //    SnakeBoard.instance.player2.index = 0;
                    //    SnakeBoard.instance.player2Entered = true;
                    //}
                    //else
                    //  if (SnakeBoard.instance.player2.index + int.Parse(aData[4]) <= 100)
                    //  {
                    SnakeBoard.instance.player2.index += int.Parse(aData[1]);
                    //Debug.Log("Player 2 Move from Client");
                    //    }
                    if (SnakeBoard.instance.player2.index == 99)
                    {
                        SnakeBoard.instance.someoneWon = true;

                        GameObject Canvas = GameObject.Find("CanvasPlayer");
                        GameObject VictoryFrame = Canvas.transform.FindChild("VictoryFrame").gameObject;
                        VictoryFrame.SetActive(true);
                        GameObject victoryFrameImage = VictoryFrame.transform.FindChild("VictoryFrameImage").gameObject;
                        Text VictoryText = victoryFrameImage.transform.FindChild("WinnerText").GetComponent<Text>();
                        VictoryText.text = "PLAYER 2 HAS WON THE GAME";
                        Debug.Log("Player 2 Won!! ");
                        if (!isHost)
                        {
                            Social.ReportProgress(TableTopPlayServiceResources.achievement_snakelet, 100.0f, (bool success) =>
                            {
                                Debug.Log("Achievment Successfully Unlocked");
                            });

                            PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_rattling, 2, (bool success) =>
                            {
                                Debug.Log("Achievment Successfully Unlocked");
                            });
                            PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_cobra, 2, (bool success) =>
                            {
                                Debug.Log("Achievment Successfully Unlocked");
                            });
                            PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_anaconda, 2, (bool success) =>
                            {
                                Debug.Log("Achievment Successfully Unlocked");
                            });
                            Rooms.instance.EndGameRoom(Login.instance.activeRoomId, Login.instance.successfulLoginUser.data.user.id);
                        }
                    }
                    Debug.Log(SnakeBoard.instance.player2.index);

                    if (SnakeBoard.instance.player2.index < 100)
                    {
                        if (int.Parse(aData[1]) == 6)
                        {
                            SnakeBoard.instance.player2HasBonusMove = true;
                            //SnakeBoard.instance.player1Turn = false;
                        }
                        else
                        {
                            SnakeBoard.instance.player2HasBonusMove = false;
                        }
                    }
                    else
                    {
                        SnakeBoard.instance.player2.index -= int.Parse(aData[1]);
                        SnakeBoard.instance.player1Turn = true;
                    }
                }
                else
                {
                    //Debug.Log("Error Player");
                }




                //if (client.isHost)
                //{
                //    if (int.Parse(aData[1]) != 6 && /*int.Parse(aData[2]) == 1 &&*/ !SnakeBoard.instance.player1Entered)
                //        SnakeBoard.instance.switchTurns();

                //}
                //else
                //{
                //    if (int.Parse(aData[1]) != 6 && !SnakeBoard.instance.player2Entered)
                //        SnakeBoard.instance.switchTurns();

                //}
                //if (SnakeBoard.instance.player1.index + int.Parse(aData[1]) > 101 && int.Parse(aData[2]) == 1)
                //{
                //    SnakeBoard.instance.switchTurns();
                //    //Debug.Log("Switching from case from roll");
                //}
                //if (SnakeBoard.instance.player2.index + int.Parse(aData[1]) > 101 && int.Parse(aData[2]) == 2)
                //{
                //    SnakeBoard.instance.switchTurns();
                //    //Debug.Log("Switching from case from roll");
                //}
                break;
            case "CMSG":
                myManager = GameObject.Find("snakeBoard").GetComponent<SnakeBoard>();
                print("server sent a message from a player");
                myManager.ChatMessage(aData[1]);
                break;

            case "CMMSG":
                MenuChat mchat = GameObject.Find("MenuChat").GetComponent<MenuChat>();
                print("server sent a message from a player");
                mchat.ChatMessage(aData[1]);
                break;

            case "CRDY":
                switch (aData[1])
                {
                    case "1":
                        UiMenuValues.instance.excuteToggleReady(true, true);
                        break;
                    case "2":
                        UiMenuValues.instance.excuteToggleReady(false, true);
                        break;
                }
                break;
            case "CNRDY":
                switch (aData[1])
                {
                    case "1":
                        UiMenuValues.instance.excuteToggleReady(true, false);
                        break;
                    case "2":
                        UiMenuValues.instance.excuteToggleReady(false, false);
                        break;
                }

                break;
            case "CSTRT":
                //MenuChat menuchat = GameObject.Find("MenuChat").GetComponent<MenuChat>();
                this.countDownPanel = GameObject.Find("Canvas").transform.FindChild("countDownPanel").gameObject;
                this.countDownPanel.SetActive(true);
                this.countDownLabel = countDownPanel.transform.GetComponentInChildren<Text>();
                countDownTime = 3f;
                isGameStarted = false;
                startCountDown = true;

                break;

            case "CLFT":
                print("SLFT");
                if (int.Parse(aData[1]) == 2)
                {
                    print("we entered SLFT Case ");
                    if (isHost)
                    {
                        print("we entered SLFT Case isHost ");

                        UiMenuValues.instance.clientLeftWaitingRoomInfo();
                        Rooms.instance.SetNotReadyForRoom(UiMenuValues.instance.activeRoomId);
                    }
                    else
                    {
                        m_UNETSnake c = FindObjectOfType<m_UNETSnake>();
                        ShutDownConnection();
                        ShutDownRoom();
                        Destroy(c.gameObject);
                    }

                }
                else if (int.Parse(aData[1]) == 1)
                {
                    if (!isHost)
                    {
                        UiAnimationManager.instance.showOnlinesGamesMenuFromCreatRoomMenu();
                        UiAnimationManager.instance.hostClosedRoom();
                    }
                    ShutDownConnection();
                    ShutDownRoom();
                    Destroy(this.gameObject);
                }
                break;
            //case "STRC":
            //    Debug.Log("Turn sent to clients " + aData[1]);
            //    myManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            //    myPlayerControl = myManager.GetComponent<PlayerControl>();
            //    myPlayerControl.playerTurn = int.Parse(aData[1]);
            //    break;
            case "SRNDR":
                //TODO check if player 1 who surrendered or player 2
				if(int.Parse(aData[1]) == 1) 
				{
					if(!isHost)
					{
						print("client should win");
						otherSnakePlayerSurrendered();
					}
					//Player 1 surrendered
				}
				else if (int.Parse(aData[1]) == 2)
				{
					//Player 2 Surrendered
					if(isHost)
					{
						print("host should win");
						otherSnakePlayerSurrendered();
					}

				}
                break;
            default:
                Debug.Log("Invalid Message! : " + message);
                break;
        }
    }




    //Server Methods
    private void OnConnection(int cnnId)
    {
        //when the player joins the server, tell him his id
        //Request his name and send the name of the all other players
        string msg = "ASKNAME|";
        UNETSend(msg);
    }

    //Client Methods

    private void OnAskName(string[] data)
    {
        // Send out name to the server
        
        if (isHost)
        {
            int BoardNumberRandom = Random.Range(1, 3);
            UNETSend("NAMEIS|" + currentPlayerName + "|" + Login.instance.successfulLoginUser.data.user.country_code + "|" + Login.instance.successfulLoginUser.data.user.id + "|" + BoardNumberRandom);
            BoardNumber = BoardNumberRandom;
        }
        else
        {
            UNETSend("NAMEIS|" + currentPlayerName + "|" + Login.instance.successfulLoginUser.data.user.country_code + "|" + Login.instance.successfulLoginUser.data.user.id);
        }

    }


    private void OnNameIs(string playerNameCountry)
    {

		UiAnimationManager.instance.hideLoadingPanel();

        string[] aData = playerNameCountry.Split('|');

        //Link the name to the connection ID
        secondPlayerName = aData[0];
        secondPlayerCountry = aData[1];
        secondPlayerID = int.Parse(aData[2]);

        if (!isHost)
        {
            //show lobby layout.
            UiAnimationManager.instance.showWaitingRoomMenu();
            //	ServerManager.Instance.StartGame();      
            UiMenuValues.instance.updateWaitingRoomInfo(secondPlayerName, currentPlayerName, false, false, 2, false);
            BoardNumber = int.Parse(aData[3]);
        }
        else
        {
            UiMenuValues.instance.updateWaitingRoomInfo(currentPlayerName, secondPlayerName, false, false, 2, false);
        }
    }

    public void UNETSend(string message)
    {
        m_Writer.SeekZero();
        m_Writer.Write(message);
        byte error;
        for (int i = 0; i < m_ConnectionIds.Count; ++i)
        {
            NetworkTransport.Send(m_HostId,
                m_ConnectionIds[i], 0, m_Writer.AsArray(), m_Writer.Position, out error);
            Debug.Log("Sending: " + message + "to :" + m_ConnectionIds[i]);
            if ((NetworkError)error != NetworkError.Ok)
                Debug.LogError("Failed to send message: " + (NetworkError)error);
        }
    }



	public void otherSnakePlayerSurrendered()
{		
		SnakeBoard.instance.someoneWon = true;
		GameObject Canvas = GameObject.Find("CanvasPlayer");
		GameObject VictoryFrame = Canvas.transform.FindChild("VictoryFrame").gameObject;
		VictoryFrame.SetActive(true);
		GameObject victoryFrameImage = VictoryFrame.transform.FindChild("VictoryFrameImage").gameObject;
		Text VictoryText = victoryFrameImage.transform.FindChild("WinnerText").GetComponent<Text>();
		VictoryText.text = "THE OTHER PLAYER HAS SURRENDERED!";
		Rooms.instance.EndGameRoom(Login.instance.activeRoomId,Login.instance.successfulLoginUser.data.user.id);
        Social.ReportProgress(TableTopPlayServiceResources.achievement_snakelet, 100.0f, (bool success) =>
        {
            Debug.Log("Achievment Successfully Unlocked");
        });

        PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_rattling, 2, (bool success) =>
        {
            Debug.Log("Achievment Successfully Unlocked");
        });
        PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_cobra, 2, (bool success) =>
        {
            Debug.Log("Achievment Successfully Unlocked");
        });
        PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_anaconda, 2, (bool success) =>
        {
            Debug.Log("Achievment Successfully Unlocked");
        });
	}
}
