﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using GooglePlayGames;
using UnityEngine.UI;
using GooglePlayGames.BasicApi;

public class GoogleServiceManager : MonoBehaviour {
    bool IsConnectedToGoogleServices = false;

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(this.gameObject);
        Init();
        Social.localUser.Authenticate((bool success) =>
        {
            if (success)
            {
                Debug.Log("Successfully Logged in1");
            }
            else
            {
                Debug.Log("Login Failed");

            }
        }); 
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void Init()
    {
        // PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
        //// enables saving game progress.
        //.EnableSavedGames()
        //// Will bring up a prompt for consent.
        //.RequestEmail()
        //// requests a server auth code be generated so it can be passed to an
        ////  associated back end server application and exchanged for an OAuth token.
        //.RequestServerAuthCode(false)
        //// requests an ID token be generated.  This OAuth token can be used to
        ////  identify the player to other services such as Firebase.
        //.RequestIdToken()
        //.Build();

        // PlayGamesPlatform.InitializeInstance(config);

       PlayGamesPlatform.DebugLogEnabled = true;
       PlayGamesPlatform.Activate();


    }

    public bool ConnectToGoogleServices()
    {
        if (!IsConnectedToGoogleServices)
        {
            Social.localUser.Authenticate((bool success) =>
            {
                IsConnectedToGoogleServices = success;
            });
        }
        return IsConnectedToGoogleServices;
    }

    public void ToAchvs()
    {
        if (Social.localUser.authenticated)
        {
            Social.ShowAchievementsUI();
        }
        else
        {
            Debug.Log("Cannot show because not logged in!");
        }
    }

    public void ToLeaderBoard()
    {
        if (Social.localUser.authenticated)
        {
            Social.ShowLeaderboardUI();
        }
        else
        {
           Debug.Log("Cannot show leaderboard because not logged in");
        }
    }
}
