﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChatWindow : MonoBehaviour {
    public bool ChatOpen;
    //Quaternion closeRot =  Quaternion.Euler(new Vector3(0f, 0f, 146f));
    //Quaternion openRot =  Quaternion.Euler(new Vector3(0f, 0f, -30f));
    public Transform CloseTrans;
    public Transform OpenTrans;

    public GameObject ChatWindowObject;
    public bool endedTrans;
    // Use this for initialization
    void Start()
    {
        ChatOpen = false;
        endedTrans = true;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ChatPressed()
    {
        if (!ChatOpen && endedTrans)
        {
            StartCoroutine(RotationRoutine(CloseTrans.position, OpenTrans.position));
            ChatOpen = !ChatOpen;
        }
        else if (ChatOpen && endedTrans)
        {
            StartCoroutine(RotationRoutine(OpenTrans.position, CloseTrans.position));
            ChatOpen = !ChatOpen;
        }
    }

    IEnumerator RotationRoutine(Vector3 startingPosition, Vector3 targetPosition)
    {
        float elapsedTime = 0.0f;
        float time = 0.5f;
        while (elapsedTime < time)
        {
            endedTrans = false;
            elapsedTime += Time.deltaTime; // <- move elapsedTime increment here
            // Rotations
            ChatWindowObject.transform.position = Vector3.Lerp(startingPosition, targetPosition, (elapsedTime / time));
            yield return new WaitForEndOfFrame();
        }
        endedTrans = true;
    }
}
