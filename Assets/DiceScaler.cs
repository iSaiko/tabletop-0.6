﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceScaler : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Vector2 aspectRatio = AspectRatio.GetAspectRatio(Screen.width, Screen.height);
        if (aspectRatio.x == 5 && aspectRatio.y == 4) {
            //Scale 40 40 40 
            //Position 696 13 -235
            this.gameObject.transform.localPosition = new Vector3(696f, 13f, -235f);
            this.gameObject.transform.localScale = new Vector3(40f, 40f, 40f);
        }
        else if (aspectRatio.x == 4 && aspectRatio.y == 3) { 
           //scale 40 40 40 
            //position 688 13 -235
            this.gameObject.transform.localPosition = new Vector3(688f, 13f, -235f);
            this.gameObject.transform.localScale = new Vector3(40f, 40f, 40f);
        }
        else if (aspectRatio.x == 3 && aspectRatio.y == 2) {
            //Scale 35 35 35
            //Position 668.6 13 -235
            this.gameObject.transform.localPosition = new Vector3(668.6f, 13f, -235f);
            this.gameObject.transform.localScale = new Vector3(35f, 35f, 35f);
        }
        else if (aspectRatio.x == 16 && aspectRatio.y == 10) {
            //Scale 35 35 35
            //Position 657 13 -235
            this.gameObject.transform.localPosition = new Vector3(657f, 13f, -235f);
            this.gameObject.transform.localScale = new Vector3(35f, 35f, 35f);
        }
        else if (aspectRatio.x == 16 && aspectRatio.y == 9) {
            //Scale 35 35 35
            //Position 633 13 -235
            this.gameObject.transform.localPosition = new Vector3(633f, 13f, -235f);
            this.gameObject.transform.localScale = new Vector3(35f, 35f, 35f);
        }


	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static class AspectRatio
    {
        public static Vector2 GetAspectRatio(int x, int y)
        {
            float f = (float)x / (float)y;
            int i = 0;
            while (true)
            {
                i++;
                if (System.Math.Round(f * i, 2) == Mathf.RoundToInt(f * i))
                    break;
            }
            return new Vector2((float)System.Math.Round(f * i, 2), i);
        }
        public static Vector2 GetAspectRatio(Vector2 xy)
        {
            float f = xy.x / xy.y;
            int i = 0;
            while (true)
            {
                i++;
                if (System.Math.Round(f * i, 2) == Mathf.RoundToInt(f * i))
                    break;
            }
            return new Vector2((float)System.Math.Round(f * i, 2), i);
        }
        public static Vector2 GetAspectRatio(int x, int y, bool debug)
        {
            float f = (float)x / (float)y;
            int i = 0;
            while (true)
            {
                i++;
                if (System.Math.Round(f * i, 2) == Mathf.RoundToInt(f * i))
                    break;
            }
            if (debug)
                Debug.Log("Aspect ratio is " + f * i + ":" + i + " (Resolution: " + x + "x" + y + ")");
            return new Vector2((float)System.Math.Round(f * i, 2), i);
        }
        public static Vector2 GetAspectRatio(Vector2 xy, bool debug)
        {
            float f = xy.x / xy.y;
            int i = 0;
            while (true)
            {
                i++;
                if (System.Math.Round(f * i, 2) == Mathf.RoundToInt(f * i))
                    break;
            }
            if (debug)
                Debug.Log("Aspect ratio is " + f * i + ":" + i + " (Resolution: " + xy.x + "x" + xy.y + ")");
            return new Vector2((float)System.Math.Round(f * i, 2), i);
        }
    }
}
