﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiAnimationManager : MonoBehaviour {

	public static UiAnimationManager instance ;

//	private const int mainMenu = 0;
//	private const int loginMenu = 1 ;
//	private const int registrationMenu = 2 ;
//	private const int forgetMenu = 3 ;
//	private const int acountSettingViewMenu = 4 ;
//	private const int acountSettingChangeMenu = 5 ;
//	private const int creatGameMenu = 6 ;
//	private const int onlineGamesMenu = 7 ;
//	private const int selectGameMenu = 8 ;
//	private const int waitingRoomMenu = 9 ;
//	private const int profileMenu = 10;


	public GameObject loginMenuPanel;
	public GameObject registrationMenuPanel;
	public GameObject forgetMenuPanel;
	public GameObject accountSettingViewMenuPanel;
	public GameObject changePasswordPanel;
	public GameObject changePhoneNumberPanel;
	public GameObject changeEmailPanel;
	public GameObject creatGameMenuPanel;
	public GameObject onlineGamesMenuPanel;
	public GameObject loginMenuPanelPanel;
	public GameObject selectGameMenuPanel;
	public GameObject waitingRoomMenuPanel;
	public GameObject profileMenuPanel;
	public GameObject flagsPanel;

	public GameObject roomPasswordInputPanel;
	public RoomUiElement roomWaitingForPassword;
//	public GameObject onlineGamesContentPanel;

	public GameObject waitingRoomChatBox;

	public GameObject loadingPanel;
	public GameObject registerationSuccessfulPanel;
	public GameObject loginSuccessfulPanel;
	public GameObject failureMassagePanel;
	public Rooms rooms ;
	public bool isLoginingToGoToProfile= false;


	public List<GameObject> allMenus = new List<GameObject>();




	public Animator CanvasAnimator;
	public bool isMainMenuSettingOpen = false;
	public int currentMenu;
	public GameObject currentMenuPanel ;

	public Button refreshButton;
	public Button searchButton;	

//	public GameObject DustEffect;






	void Start ()
	{

		currentMenuPanel = new GameObject();
		instance = this;
//		currentMenu = mainMenu ;

		allMenus.Add(loginMenuPanel);
		allMenus.Add(registrationMenuPanel);
		allMenus.Add(forgetMenuPanel);
		allMenus.Add(accountSettingViewMenuPanel);
		allMenus.Add(changePasswordPanel);
		allMenus.Add(changePhoneNumberPanel);
		allMenus.Add(changeEmailPanel);
		allMenus.Add(creatGameMenuPanel);
		allMenus.Add(onlineGamesMenuPanel);
		allMenus.Add(loginMenuPanelPanel);
		allMenus.Add(selectGameMenuPanel);
		allMenus.Add(waitingRoomMenuPanel);
		allMenus.Add(profileMenuPanel);
		allMenus.Add(flagsPanel);


	}




	// ------------------------------------------------------------------------------------------------//

	public void toggleSetting()
	{
		if(isMainMenuSettingOpen)
		{
			CanvasAnimator.SetTrigger("closeSetting") ;
			isMainMenuSettingOpen = !isMainMenuSettingOpen ;
		}
		else
		{
			CanvasAnimator.SetTrigger("openSetting");
			isMainMenuSettingOpen = !isMainMenuSettingOpen ;
		}
	}


	// ------------------------------------------------------------------------------------------------//


	public void accountProfileClicked()
	{

		if(!ConnectivityCheck.instance.isConnected)
		{
			StartCoroutine(showFailureMassage("Check your Internet Connection",3f));
			return;
		}

		if(Login.instance.hasLogined)
		{
			hideAllMenusButSelected(profileMenuPanel);
			Login.instance.getProfileDataClick();
            Social.ReportScore(Login.instance.userScores.data.profile.results.chess_wins, TableTopPlayServiceResources.leaderboard_chess, (bool success) =>
            {
                // handle success or failure
            });
            Social.ReportScore(Login.instance.userScores.data.profile.results.domino_wins, TableTopPlayServiceResources.leaderboard_domino, (bool success) =>
            {
                // handle success or failure
            });
            Social.ReportScore(Login.instance.userScores.data.profile.results.snake_wins, TableTopPlayServiceResources.leaderboard_snake_and_ladder, (bool success) =>
            {
                // handle success or failure
            });
			
		}
		else if (!Login.instance.hasLogined)
		{
			isLoginingToGoToProfile  = true;
			CanvasAnimator.SetTrigger("MM fade login slide up"); 
			hideAllMenusButSelected(loginMenuPanel);
			UiMenuValues.instance.loadUserName();
			isMainMenuSettingOpen= false;
		}
	}

	// ------------------------------------------------------------------------------------------------//


	public void playOnlineClicked()
	{
		if(!ConnectivityCheck.instance.isConnected)
		{
			StartCoroutine(showFailureMassage("Check your Internet Connection",3f));
			return;
		}

		if(Login.instance.hasLogined)
		{

			showSelectGameMenuFromMainMenu();

		}
		else if (!Login.instance.hasLogined)
		{
			isLoginingToGoToProfile  = false;	
			CanvasAnimator.SetTrigger("MM fade login slide up");
//			hideAllMenusButSelected(registrationMenuPanel);
			UiMenuValues.instance.loadUserName();
			isMainMenuSettingOpen= false;
		}
	}

	// ------------------------------------------------------------------------------------------------//

	public void ShowLoginMenuFromRegistrationMenu()
	{
		CanvasAnimator.SetTrigger("registration slide up login slide up"); //we canceled animation for now (don't delete)
//		hideAllMenusButSelected(loginMenuPanel);

	}

	// ------------------------------------------------------------------------------------------------//

	public void closeLoginMeneuToMainMenu()
	{
		CanvasAnimator.SetTrigger("login slide down MM fade in");
		isLoginingToGoToProfile = false;

	}
	// ------------------------------------------------------------------------------------------------//
	public void showLoginMenuFromForgetMenu()
	{
		hideAllMenusButSelected(loginMenuPanel);
	}

	// ------------------------------------------------------------------------------------------------//

	public void ShowRegistrationMenuFromLoginMenu()
	{
		CanvasAnimator.SetTrigger("login slide down registration slide down"); //we canceled animation for now (don't delete)
//		hideAllMenusButSelected(registrationMenuPanel) ;
		cleanMenuInPutFields(registrationMenuPanel);
	}
	// ------------------------------------------------------------------------------------------------//



	public void showMainMenuFromProfileMenu()
	{
		profileMenuPanel.SetActive(false);
	}

	// ------------------------------------------------------------------------------------------------//

	public void showAccountSettingViewMenuFromProfileMenu()
	{
		hideAllMenusButSelected(accountSettingViewMenuPanel);
	}	
	public void showAccountSettingMenu()
	{
		hideAllMenusButSelected(accountSettingViewMenuPanel);
	}

	public void fillDataInAccountSettingsMenu(string userName, string country,string email,string phone)
	{
		Text[] accountSettingsLabels = accountSettingViewMenuPanel.GetComponentsInChildren<Text>();
		accountSettingsLabels[0].text = userName;
		accountSettingsLabels[1].text = country.ToUpper();
		accountSettingsLabels[2].text = email;
		accountSettingsLabels[3].text = phone;
	}

	// ------------------------------------------------------------------------------------------------//

	public void showProfileMenuFromAccountSettingViewMenu()
	{
		hideAllMenusButSelected(profileMenuPanel);
	}

	public void showChangePasswordPanel()
	{
		hideAllMenusButSelected(changePasswordPanel);
		cleanMenuInPutFields(changePasswordPanel);
	}

	public void showChangeEmailPanel()
	{
		hideAllMenusButSelected(changeEmailPanel);
		cleanMenuInPutFields(changeEmailPanel);
	}
	public void showChangePhoneNumberPanel()
	{
		hideAllMenusButSelected(changePhoneNumberPanel);
		cleanMenuInPutFields(changePhoneNumberPanel);
	}


	// ------------------------------------------------------------------------------------------------//

	public void showAccountSettingViewMenuFromAccountSettingChangeMenu()
	{
		hideAllMenusButSelected(accountSettingViewMenuPanel);
	}

	// ------------------------------------------------------------------------------------------------//

	public void showSelectGameMenuFromMainMenu()
	{
		
		hideAllMenusButSelected(selectGameMenuPanel);
	}

	// ------------------------------------------------------------------------------------------------//

	public void showMainMenuFromSelectGameMenu()
	{
		selectGameMenuPanel.SetActive(false);
	}

	// ------------------------------------------------------------------------------------------------//

	public void showForgetMenuFromLoginMenu()
	{
		hideAllMenusButSelected(forgetMenuPanel);
	}

	// ------------------------------------------------------------------------------------------------//




	public void showOnlineGamesMenuFromSelectGameMenu()
	{
		hideAllMenusButSelected(onlineGamesMenuPanel);
		cleanMenuInPutFields(onlineGamesMenuPanel);
		rooms.cleanRoomsList();
		rooms.getRoomSearchResult();
	}


	public void showOnlinesGamesMenuFromCreatRoomMenu()
	{
		hideAllMenusButSelected(onlineGamesMenuPanel);
		cleanMenuInPutFields(onlineGamesMenuPanel);
		rooms.cleanRoomsList();
		rooms.getRoomSearchResult();
	}

	public void showOnlinesGamesMenuFromWaitingRoomMenu()
	{
		hideAllMenusButSelected(onlineGamesMenuPanel);
		cleanMenuInPutFields(onlineGamesMenuPanel);
		rooms.cleanRoomsList();
		Invoke("refreshRoomList",2f);
	}


	// ------------------------------------------------------------------------------------------------//
//	public void takeSearchForRoomInput(string roomToSearchFor)
//	{
//		this.roomToSearchFor = roomToSearchFor;
//	}
//
	// ------------------------------------------------------------------------------------------------//
	public void searchForCertainRoom()
	{
		rooms.cleanRoomsList();
		rooms.getRoomSearchResult(UiMenuValues.instance.roomToSearchFor);
		searchButton.interactable = false;

	}
	// ------------------------------------------------------------------------------------------------//
//	public void cleanRoomsList()
//	{
//		if(RoomsUiMananger.instance.transform.childCount != null )
//		{
//			print("before the loop");
//			print(RoomsUiMananger.instance.transform.childCount);
//			for(int i = 0 ;  i < RoomsUiMananger.instance.transform.childCount ; i++)
//			{
//				Destroy( RoomsUiMananger.instance.transform.GetChild(i).gameObject);
//			}
//		}
//	}
		
	// ------------------------------------------------------------------------------------------------//
	public void refreshRoomList()
	{
		rooms.cleanRoomsList();
		rooms.getRoomSearchResult();
		refreshButton.interactable = false ;
	}
	// ------------------------------------------------------------------------------------------------//

	public void showSelectGameMenuFromOnlineGamesMenu()
	{
		hideAllMenusButSelected(selectGameMenuPanel);
	}

	// ------------------------------------------------------------------------------------------------//

	public void showCreatRoomMenuFromOnlinesGamesMenu()
	{
		hideAllMenusButSelected(creatGameMenuPanel);
	}
	// ------------------------------------------------------------------------------------------------//

	public void showWaitingRoomMenuFromCreatRoomMenu()
	{
		hideAllMenusButSelected(waitingRoomMenuPanel);
		clearWaitingRoomChatBox();

	}

    public void showWaitingRoomMenu()
    {
        hideAllMenusButSelected(waitingRoomMenuPanel);
		clearWaitingRoomChatBox();
    }


	// ------------------------------------------------------------------------------------------------//

	public void showCreatRoomMenuFromWaitingRoomMenu()
	{

		hideAllMenusButSelected(creatGameMenuPanel);
	}

	// ------------------------------------------------------------------------------------------------//

	public void showJoinRoomPasswordPanel(RoomUiElement roomWaitingForPassword)
	{
		roomPasswordInputPanel.SetActive(true);
		this.roomWaitingForPassword = roomWaitingForPassword;
	}
	public void hideJoinRoomPasswordPanel()
	{
		roomPasswordInputPanel.SetActive(false);
		this.roomWaitingForPassword = null;
	}
	public void joinRoomWithPassword()
	{
		this.roomWaitingForPassword.joinRoomWithPassword();
	}

	// ------------------------------------------------------------------------------------------------//

	public void closeFlagsPanel()
	{
		flagsPanel.SetActive(false);
	}



	// ------------------------------------------------------------------------------------------------//
	public void userNameInfo()
	{
		StartCoroutine(showFailureMassage("Username must be between 5 and 25 characters begins with letter only small letters and numbers",5f));
	}

	// ------------------------------------------------------------------------------------------------//
	public void emailInfo()
	{
		StartCoroutine(showFailureMassage("Enter a valid email",3f));
	}

	// ------------------------------------------------------------------------------------------------//

	public void countryInfo()
	{
		StartCoroutine(showFailureMassage("Select your country",2f));
	}

	// ------------------------------------------------------------------------------------------------//

	public void phoneInfo()
	{
		StartCoroutine(showFailureMassage("Enter a valid mobile number",3f));
	}
	// ------------------------------------------------------------------------------------------------//

	public void passwordInfo()
	{
		StartCoroutine(showFailureMassage("Enter a password ,More than 6 characters, letters and numbers",4f));
	}
	// ------------------------------------------------------------------------------------------------//
	public void passwordConfirmInfo()
	{
		StartCoroutine(showFailureMassage("Enter the same password as you entered above",3f));
	}
	// ------------------------------------------------------------------------------------------------//
    public void hostClosedRoom()
    {
        StartCoroutine(showFailureMassage("Host Closed Room", 2f));
    }
    public void WrongConfirmationForPassword()
    {
        StartCoroutine(showFailureMassage("Password Entered doesn't Match", 2f));
    }
	// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void PasswordChangedSuccessfully()
    {
        StartCoroutine(showFailureMassage("Password Has Been Changed Successfully", 2f));
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void EmailChangedSuccessfully()
    {
        StartCoroutine(showFailureMassage("E-mail Has Been Changed Successfully, Check you New E-mail For Activation", 6f));
		Invoke("afterChangMail",6.1f);
    }
		private void afterChangMail()
	{
		Login.instance.signOut();
		accountProfileClicked();
//		for(int i = 0 ; i < allMenus.Count ; i++)
//		{
//			
//		}
	}

    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
    public void PhoneChangedSuccessfully()
    {
        StartCoroutine(showFailureMassage("Phone Has Been Changed Successfully", 2f));
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

	public void showRegisterationSuccessfulPanel ()
	{
		registerationSuccessfulPanel.SetActive(true);
		Invoke("waitToDiactivateRegSuccessPanel",1);
	}

	public void waitToDiactivateRegSuccessPanel ()
	{
		registerationSuccessfulPanel.SetActive(false);
		StartCoroutine(showFailureMassage("Check you E-mail for account activation!"));
		Invoke("ShowLoginMenuFromRegistrationMenu",2f);
//		ShowLoginMenuFromRegistrationMenu();
	}

//	public void startDustEffect()
//	{
//		DustEffect.SetActive(true);
//	}
//
//	public void stopDustEffect()
//	{
//		DustEffect.SetActive(false);
//	}
//


	// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//


	public void showLoginSuccessfulPanel ()
	{
		Login.instance.hasLogined = true;
		loginSuccessfulPanel.SetActive(true);
		Invoke("waitToDiactivateLoginSuccessPanel",2);
	}

	public void waitToDiactivateLoginSuccessPanel ()
	{
		loginSuccessfulPanel.SetActive(false);
		if(isLoginingToGoToProfile)
		{
			CanvasAnimator.SetTrigger("login slide down MM fade in");
			accountProfileClicked();
		}
		else
		{
			CanvasAnimator.SetTrigger("login slide down MM fade in");
			playOnlineClicked();
		}
	}

	// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//



	public IEnumerator showFailureMassage(string massage)
	{
		failureMassagePanel.SetActive(true);
		failureMassagePanel.GetComponentInChildren<Text>().text = massage;
		yield return new WaitForSeconds(2f);
		failureMassagePanel.SetActive(false);
	}

	public IEnumerator showFailureMassage(string massage,float duration)
	{
		failureMassagePanel.SetActive(true);
		failureMassagePanel.GetComponentInChildren<Text>().text = massage;
		yield return new WaitForSeconds(duration);
		failureMassagePanel.SetActive(false);
	}
	// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//


	public void showWaitingRoomData(string player1Name , string player2Name,string playersNumber , string gameType ,string roundTime ,string gameState)
	{
		clearWaitingRoomChatBox();
		creatGameMenuPanel.SetActive(false);
		waitingRoomMenuPanel.SetActive(true);
        waitingRoomMenuPanel.transform.GetChild(2).gameObject.SetActive(true);
        waitingRoomMenuPanel.transform.GetChild(3).gameObject.SetActive(false);
		Text[] waitingRoomLabels = waitingRoomMenuPanel.GetComponentsInChildren<Text>(true);
		waitingRoomLabels[0].text = player1Name;
		waitingRoomLabels[1].text = player2Name;
		waitingRoomLabels[4].text = playersNumber;
		waitingRoomLabels[6].text = gameType;
		waitingRoomLabels[7].text = (gameType == "domino") ? "Game Score :" : "Round Time :";
		waitingRoomLabels[8].text = roundTime;
		waitingRoomLabels[10].text = gameState;

		if (Rooms.instance.gameType == "chess")
		{
			m_UNETChess.instance.roundTime =  UiMenuValues.instance.roundTime;
		}

		else if (Rooms.instance.gameType == "domino")
		{
			m_UNETDomino.instance.GameScore = UiMenuValues.instance.roundTime;
		}
	}


	public void resetWaitingRoomUI()
	{

		UiMenuValues.instance.player2ReadyStateImageWR.color =  Color.clear;

		Image hostLabels = waitingRoomMenuPanel.transform.GetChild(2).GetChild(1).GetComponent<Image>();
		hostLabels.sprite = UiMenuValues.instance.buttonReadySprite;
		hostLabels.gameObject.GetComponent<Button>().interactable =false;

		waitingRoomMenuPanel.transform.GetChild(2).GetChild(0).GetComponent<Button>().interactable = true;
		waitingRoomMenuPanel.transform.GetChild(2).GetChild(1).GetComponent<Button>().interactable = false;
		waitingRoomMenuPanel.transform.GetChild(2).GetChild(2).GetComponent<Button>().interactable = false;


		Image clientLabel = waitingRoomMenuPanel.transform.GetChild(3).GetChild(1).GetComponent<Image>();
		clientLabel.sprite = UiMenuValues.instance.buttonReadySprite;

		waitingRoomMenuPanel.transform.GetChild(3).GetChild(0).GetComponent<Button>().interactable = true;
		waitingRoomMenuPanel.transform.GetChild(3).GetChild(1).GetComponent<Button>().interactable = true;

		waitingRoomMenuPanel.transform.GetChild(2).gameObject.SetActive(false);
		waitingRoomMenuPanel.transform.GetChild(3).gameObject.SetActive(true);

		clearWaitingRoomChatBox();

	}


	public void showProfileData(string playerUserName , string chessWins,string chessLoses, string SnakeWins ,string Snakeloses ,string dominosWins,string dominosLoses,string lvl,string exp)
	{
		Text[] profileLabels = profileMenuPanel.GetComponentsInChildren<Text>(true);
		profileLabels[0].text = playerUserName;
		profileLabels[1].text = "LV " + lvl; //////
		profileLabels[5].text = chessWins;
		profileLabels[7].text = chessLoses;
		profileLabels[10].text = SnakeWins;
		profileLabels[12].text = Snakeloses;
		profileLabels[15].text = dominosWins;
		profileLabels[17].text = dominosLoses;
		profileMenuPanel.GetComponentInChildren<Slider>().value = (float)int.Parse(exp);

	}
	// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
	public void clearWaitingRoomChatBox()
	{
		for(int i = 0 ; i < waitingRoomChatBox.transform.childCount ; i++)
		{
			Destroy(waitingRoomChatBox.transform.GetChild(i).gameObject);
		}
	}

	// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
	public void showLoadingPanel()
	{
		loadingPanel.SetActive(true);
		Invoke("hideLoadingPanel",10f);
	}
	public void hideLoadingPanel()
	{
		CancelInvoke("hideLoadingPanel");
		loadingPanel.SetActive(false);
	}

	// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

	public void hideAllMenusButSelected(GameObject currentMenuPanel)
	{

		for(int i = 0 ; i < allMenus.Count ; i ++)
		{
			if(allMenus[i] != currentMenuPanel )
			{
				allMenus[i].SetActive(false);
			}
			else
			{
				currentMenuPanel.SetActive(true);
			}
		}

	}



	public void cleanMenuInPutFields(GameObject menu)
	{
		InputField[] inputs = menu.GetComponentsInChildren<InputField>();
		for(int i = 0 ; i < inputs.Length ; i++)
		{
			inputs[i].text = "";
		}
	}


}
