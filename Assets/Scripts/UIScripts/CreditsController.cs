﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsController : MonoBehaviour {


	public GameObject creditsPanel;

	public void DisableCredits()
	{
//		this.transform.parent.gameObject.SetActive(false);
		Destroy(this.transform.parent.gameObject);
	}

	public void createCredits()
	{
		Instantiate(creditsPanel).SetActive(true);
	}

}
