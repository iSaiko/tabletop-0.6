﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomUiElement : MonoBehaviour {

//	public Text roomName;
//	public Text hostName;
//	public Text roundTime;
//	public Text hasPassword;
//
	private int indexInView;
	private int roomId;
    private bool hasPassword;
    private string roomPassword;
    private long match_id;
    private int roundTime;


	public void assignRoomData (string roomName , string hostName,int roundTime ,int hasPassword,int roomId,long match_id,bool colorPick)
	{
        this.roomId = roomId;
        this.match_id = match_id;
		Text[] roomeUiElementLabel = gameObject.GetComponentsInChildren<Text>(true);
		roomeUiElementLabel[0].text = roomName ;
		roomeUiElementLabel[1].text = hostName ;
        roomeUiElementLabel[2].text = roundTime.ToString();
        this.roundTime = roundTime;
		roomeUiElementLabel[3].text = (hasPassword == 1) ? "Yes" : "No" ;
		this.hasPassword = (hasPassword == 1) ? true : false;
		this.transform.GetComponent<Image>().color =  (colorPick) ? new Color(UiMenuValues.instance.color1.r,UiMenuValues.instance.color1.g,UiMenuValues.instance.color1.b,0.8f): new Color(UiMenuValues.instance.color2.r,UiMenuValues.instance.color2.g,UiMenuValues.instance.color2.b,0.8f);
	}

    public void joinRoom()
    {

		if(!hasPassword)
		{
			UiMenuValues.instance.roundTime = this.roundTime;
			UiMenuValues.instance.activeRoomId = this.roomId;
            Debug.Log("Match ID from Join Button" + match_id);
			Rooms.instance.joinRoom(this.roomId,"",match_id);
			Login.instance.activeRoomId = roomId;
			UiAnimationManager.instance.showLoadingPanel();
		}
		else
		{
			UiAnimationManager.instance.showJoinRoomPasswordPanel(this);
		}
    }
	public void joinRoomWithPassword()
	{
		UiMenuValues.instance.roundTime = this.roundTime;
		UiMenuValues.instance.activeRoomId = this.roomId;
		Rooms.instance.joinRoom(this.roomId,UiMenuValues.instance.roomToEnterPassword, match_id);
		Login.instance.activeRoomId = roomId;
		UiAnimationManager.instance.showLoadingPanel();

	}

}
