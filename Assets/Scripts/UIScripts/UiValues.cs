﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;



public class UiValues : MonoBehaviour {

//	private float gameTimef;
//	private int gameSeconds;
//	private int gameMinutes;
//	private int gameHours;

	private float timer;
//	private string gamePlayTimeToDisplay;
	private bool player1Previous ;

	public static UiValues instance;
//	public Text playTimeLabel ;

	public bool isHostCanvas;
	public Text player1TimeLabel;
	public Text player2TimeLabel;
	public Text player1NameLabel;
	public Text player2NameLabel;
	public float timerValue;
	public bool player1Turn;
	private string player1Name;
	private string player2Name;

	private string player1CountryCode;
	private string player2CountryCode;


	public Image player1Flag;
	public Image player2Flag;

	public GameObject flagsLoaderObject;
	public GameObject gameEndCanvas;

//	public Client client ;




	void Start () {
		if((isHostCanvas && !m_UNETChess.instance.isHost) || (!isHostCanvas && m_UNETChess.instance.isHost))
			Destroy(this.gameObject);

	
		instance = this;
		timerValue = m_UNETChess.instance.roundTime * 60;
		timer = timerValue;
		player1Turn = (PlayerControl.instance.playerTurn == 1) ? true : false ; 
		player1Previous = player1Turn ;
		Invoke("getPlayerNames",0.3f);
		Invoke("getPlayerFlags",0.3f);
	}


	void Update () {
	
		// displaying total game time
//		gameTimef += Time.deltaTime;
//		gameSeconds = Mathf.CeilToInt(gameTimef);
//
//
//		if(gameSeconds > 59 )
//		{
//			gameMinutes += 1;
//			gameSeconds = 0 ;
//			gameTimef = 0 ;
//		}
//		if(gameMinutes > 59)
//		{
//			gameHours +=1;
//			gameMinutes = 0;
//		}
//
//		gamePlayTimeToDisplay = gameHours.ToString()+":"+gameMinutes.ToString()+":"+gameSeconds.ToString();
//		playTimeLabel.text = gamePlayTimeToDisplay;
		//********************************************************************

		// displaying remaining turn time for each player

		player1Turn = (PlayerControl.instance.playerTurn == 1) ? true : false ; 
		timer = (player1Turn == player1Previous) ? timer : timerValue ;
		player1Previous = player1Turn ;


		timer -= Time.deltaTime;


		player1TimeLabel.text = ((player1Turn && m_UNETChess.instance.isHost) || (!player1Turn && !m_UNETChess.instance.isHost)) ? ((Mathf.FloorToInt(timer)/60).ToString() + ":" + (Mathf.FloorToInt(timer)%60).ToString()): "00:00";
		player2TimeLabel.text = ((!player1Turn && m_UNETChess.instance.isHost) || (player1Turn && !m_UNETChess.instance.isHost)) ? ((Mathf.FloorToInt(timer)/60).ToString() + ":" + (Mathf.FloorToInt(timer)%60).ToString()): "00:00";



		// check if a player didn't play in his turn >>> if that happen make him lose
		if(Mathf.FloorToInt(timer) <= 0)
		{
			if(player1Turn && m_UNETChess.instance.isHost)
			{
				// host lost the game	
				//			GameOver();
				StartCoroutine(ChessBoard.instance.AnnounceWinner(false));
			}
			else if (!player1Turn && !m_UNETChess.instance.isHost)
			{
				// client lost the game	
				//			GameOver();
				StartCoroutine(ChessBoard.instance.AnnounceWinner(true));
			}
			else if (!player1Turn && m_UNETChess.instance.isHost)
			{
				// host won the game	
				//			GameOver();
				StartCoroutine(ChessBoard.instance.AnnounceWinner(true));
			}
			else if (player1Turn && !m_UNETChess.instance.isHost)
			{
				// client won the game	
				//			GameOver();
				StartCoroutine(ChessBoard.instance.AnnounceWinner(false));
			}

			timer = timerValue;
		}


	}


//	public static void setTurnTimer(int minutes)
//	{
//	}


	public static  void GameOver ()
	{
		Application.Quit();
	}



	public void getPlayerNames()
	{
		player1Name = m_UNETChess.instance.currentPlayerName;
		player2Name = m_UNETChess.instance.secondPlayerName; 
		player1NameLabel.text = player1Name;
		player2NameLabel.text = player2Name;
	}

	public void getPlayerFlags()
	{
		player1CountryCode = m_UNETChess.instance.currentPlayerCountry;
		player2CountryCode = m_UNETChess.instance.secondPlayerCountry;


		CountryDetection countryGetter = flagsLoaderObject.GetComponent<CountryDetection>();


//		player1Flag.sprite =  (m_UNETChess.instance.isHost) ? countryGetter.LoadFlag(player1CountryCode) : countryGetter.LoadFlag(player2CountryCode);
//		player2Flag.sprite = (!m_UNETChess.instance.isHost) ? countryGetter.LoadFlag(player1CountryCode) : countryGetter.LoadFlag(player2CountryCode);
		player1Flag.sprite =  countryGetter.LoadFlag(player1CountryCode);
		player2Flag.sprite =  countryGetter.LoadFlag(player2CountryCode);
		Destroy(flagsLoaderObject,0.2f);
	}

//	public void timeUpMassage(string massage)
//	{
//		gameEndCanvas.SetActive(true);
//		Text temp = gameEndCanvas.transform.GetComponentInChildren<Text>();
//		temp.text = massage;
//	}



}
