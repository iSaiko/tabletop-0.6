using UnityEngine;
using System.Collections.Generic;

[System.Serializable]

public class CountryInfo {

	public string countryCode = "";
	public string countryName = "";
	public string continentName = "";
	public float lng; //default longitute
	public float lat; //default latitude
	public List<string> languages = new List<string>();

	public CountryInfo(string newCountryCode, string newCountryName, string newContinentName) {
		countryCode = newCountryCode;
		countryName = newCountryName;
		continentName = newContinentName;
		lng = 0f;
		lat = 0f;
	}
	
	public CountryInfo(string newCountryCode, string newCountryName, string newContinentName, float newLng, float newLat) {
		countryCode = newCountryCode;
		countryName = newCountryName;
		continentName = newContinentName;
		lng = newLng;
		lat = newLat;
	}
	
	public CountryInfo(string newCountryCode, string newCountryName, string newContinentName, float newLng, float newLat, string[] newLanguages) {
		countryCode = newCountryCode;
		countryName = newCountryName;
		continentName = newContinentName;
		lng = newLng;
		lat = newLat;
		for (int i = 0; i < newLanguages.Length; i++) {
			languages.Add(newLanguages[i]);
		}
	}
	
}
