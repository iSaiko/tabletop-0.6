﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ShowPassInField : MonoBehaviour {
	public InputField gamePasswordField;

	public void toggleGamePasswordState()
	{
		gamePasswordField.contentType = (gamePasswordField.contentType == InputField.ContentType.Standard) ?  InputField.ContentType.Password  :  InputField.ContentType.Standard ;
		gamePasswordField.ForceLabelUpdate();

	}



}
