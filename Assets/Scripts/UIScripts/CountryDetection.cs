///////////////////////////////////////////////
////	   CountryDetection.cs 1.1         ////
////  copyright (c) 2012 by Markus Hofer   ////
////          blackish-games.com           ////
///////////////////////////////////////////////

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent (typeof (CountryData))]
public class CountryDetection : MonoBehaviour {
	
	public CountryData countryData;
	public string geonamesUsername = ""; //Optional! If you have a geonames.org username and want to send it along, enter it here.
	public static CountryDetection instance;

	public bool autoStart = false; //Start Locating automatically?
	public bool saveCountryCode = false; //Save country-code the first time it is detected and reuse that instead of locating again the next time
	
	public string countryCode = "";
	public string countryName = "";
	public string continentName = "";

	public Image flagImage;
	public Text CountryCodeTextField;
	
	public float lng = 0f;
	public float lat = 0f;
	
	public string flagsPath = "flags/"; //path relative to the resource path set in ScreenCtrl
	public Vector2 flagDimensions = new Vector2(56f, 42f); //dimensions of a flag at 100% - this is needed because you might want to compress NPOT flags, which will change their sizes
	public Sprite flagTexture = null;


	public string statusMessage = "";
	
	public bool done = false;
	public bool success = false;
	
		
#if UNITY_EDITOR	
	public bool debugUseDefaultLanLat = false;
	public string debugUseThisIP = "";
#endif
	
	
	void Awake() {
		if(countryData == null) countryData = gameObject.GetComponent<CountryData>();
		if(autoStart) StartCoroutine(Locate());
	}

	void Start()
	{
		instance = this;
	}
	
	
	public void LocateNow() { StartCoroutine(Locate("", true)); }
	public IEnumerator Locate() { yield return StartCoroutine(Locate("", true)); }
	public IEnumerator Locate(string useCountryCode) { yield return StartCoroutine(Locate(useCountryCode, false)); }
	public IEnumerator Locate(string useCountryCode, bool loadSavedData) {
		
		Debug.Log ("Locate: " + (string.IsNullOrEmpty(this.countryCode) ? "Current Location" : this.countryCode) + ", " + (loadSavedData ? "allowed to load saved location" : "ignoring saved data"));
		
		statusMessage = "Locating...";
		countryCode = useCountryCode;
		countryName = "";
		continentName = "";
		flagTexture = null;
		statusMessage = "";
		done = false;
		success = false;
				
		
		if(!string.IsNullOrEmpty(countryCode)) { //country code given? get default lng, lat from CountryData
			if(countryCode == "UK") countryCode = "GB";
			int cid = countryData.GetCountryID(countryCode);
			if(cid >= 0) {
				lng = countryData.countryInfo[cid].lng;
				lat = countryData.countryInfo[cid].lat;
			}
		}
		
		
		if(!loadSavedData || !LoadSavedCountryCode() || string.IsNullOrEmpty(useCountryCode)) { //no country-code given and either not allowed to load saved data or no saved data found? use GPS!
		
			//Try getting it via GPS/Web!
//			if(countryCode == "") {
//				yield return StartCoroutine(GetCountryCodeViaGPS());
//			}
			
			if (countryCode == "" || countryCode == "XX") {
				yield return StartCoroutine(GetCountryCodeViaIP());	
			}
			
			//Still no countryCode! Damnit!
			if(countryCode == "" || countryCode == "XX") {
				statusMessage = "Could not determine Location";
				countryCode = "XX";
			}

		}

		success = LoadData(countryCode); //Load country name, contintent name and flag for the given country code. returns true if everything worked
		done = true;
		
		Debug.Log ("CountryDetection.Locate: " + (success ? "Located!" : "Could not locate.") + " - " + countryCode + ", " + countryName + ", " + continentName);
		
		//SAVE COUNTRYCODE!
		if(success && saveCountryCode) {
			PlayerPrefs.SetString("CountryCode", countryCode);
		}
	}

	
	public bool LoadSavedCountryCode() {
		if(!saveCountryCode) return false; // abort if not allowed to save/load countrycode
		if(PlayerPrefs.HasKey("CountryCode")) {
			countryCode = PlayerPrefs.GetString("CountryCode");
			return true; //Found!
		}
		return false; //Nothing found
	}
	
	
	public bool LoadData(string useCountryCode) {
		//Get continent-code and country-name
		countryCode = useCountryCode;
	 	continentName = countryData.GetContinentName(useCountryCode);
		countryName = countryData.GetCountryName(useCountryCode);
		
		flagTexture = LoadFlag(useCountryCode);
		flagImage.sprite = flagTexture;
		CountryCodeTextField.text = countryName.ToString();

		if(flagTexture == null) flagTexture = LoadFlag("XX");
		
		if(continentName != "" && countryName != "" && countryCode != "" && countryCode != "XX") return true;
		else return false;
	}


	public void LoadDataManually(string useCountryCode) {
		//Get continent-code and country-name
		countryCode = useCountryCode;
		continentName = countryData.GetContinentName(useCountryCode);
		countryName = countryData.GetCountryName(useCountryCode);

		flagTexture = LoadFlag(useCountryCode);
		CountryCodeTextField.text = countryName.ToString();

		if(flagTexture == null) flagTexture = LoadFlag("XX");
		flagImage.sprite = flagTexture;


	}


	
		
	public Sprite LoadFlag(string cc) {
		
		
		return Resources.Load( flagsPath + cc, typeof(Sprite)) as Sprite;

	}
	
	
	IEnumerator GetCountryCodeViaGPS() {
	
	    // First, check if user has location service enabled - if not: abort
	    if (!Input.location.isEnabledByUser) yield break;
	
	    // Start service before querying location
	    Input.location.Start();
	
	    // Wait until service initializes
	    int maxWait = 20;
	    while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0) {
	        yield return new WaitForSeconds (1f);
	        maxWait--;
	    }
	
	    // Service didn't initialize in 20 seconds
	    if (maxWait < 1) {
	        Debug.Log("LocationService timed out");
	        yield break;
	    }
	
	    // Connection has failed
	    if (Input.location.status == LocationServiceStatus.Failed) {
	        Debug.Log ("Unable to determine device location");
	        yield break;
	    } else { // Access granted and location value could be retrieved
			Debug.Log ("Location found: " + Input.location.lastData.latitude + " " +
	               Input.location.lastData.longitude + " " +
	               Input.location.lastData.altitude + " " +
	               Input.location.lastData.horizontalAccuracy + " " +
	               Input.location.lastData.timestamp);
			#if UNITY_EDITOR
			if(!debugUseDefaultLanLat) {
			#endif
			lng = Input.location.lastData.longitude;
			lat = Input.location.lastData.latitude;
			#if UNITY_EDITOR
			}			
			#endif
	    }
	
	    // Stop service if there is no need to query location updates continuously
	    Input.location.Stop ();		
			
		if(!(lng == 0f && lat == 0f)) { //do make sure they are zero by default if you want this to work!
			Debug.Log("lng: " + lng + ", lat: " + lat);
			
			// Query the geonames webservice to get the country-code for the current location
			string gpsURL = "http://ws.geonames.org/countryCode?lat="+Mathf.Clamp(lat, -90f, 90f)+"&lng="+Mathf.Clamp(lng, -180f, 180f);
			if(geonamesUsername.Length > 0) gpsURL += "&username=" + geonamesUsername;
			Debug.Log (gpsURL);
			WWW gpswww = new WWW (gpsURL);
			float gpswwwStartTime = Time.time;
			
			// Wait for download to complete
			while(!gpswww.isDone) {
				if(gpswww.error != null || Time.time - gpswwwStartTime > 8.0f) break; 
				yield return new WaitForSeconds(0.2f);
			}
		
			if (gpswww.error != null) { Debug.Log(gpswww.error); }
			
			// IT WORKED!
			if(gpswww.isDone && gpswww.error == null && gpswww.text != null) { 
				if(gpswww.text.Length >= 2) {
					countryCode = gpswww.text.Substring(0,2); //Result OK
					if(countryCode == "<!") { //seems we got an error
						Debug.Log (gpswww.text);
					}
				}
				else countryCode = "XX"; //Result not OK
			} else { //Didn't work
				countryCode = "XX";
			}
		}
	}	
	
	
	public IEnumerator GetCountryCodeViaIP() {
		
		string url = "http://www.geoplugin.net/json.gp";
		#if UNITY_EDITOR
		if(debugUseThisIP != "") url += "?ip=" + debugUseThisIP;
		#endif
		WWW www = new WWW(url);
		float startTime = Time.time;
		
		// Wait for download to complete
		while(!www.isDone) {
			if(www.error != null || Time.time - startTime > 8.0f) break; 
			yield return new WaitForSeconds(0.2f);
		}
	
		if (www.error != null) { Debug.Log(www.error); }
		
		// IT WORKED!
		if(www.isDone && www.error == null && www.text != null) { 
				//Debug.Log(www.text);
			
			//Extract CountryCode and lng, lat from the result
			countryCode = www.text.Substring(www.text.IndexOf("countryCode") + 11 + 3, 2);
			Debug.Log ("CountryCode from IP: " + countryCode);
			int lpos = www.text.IndexOf("longitude") + 9 + 3;
			int llen = www.text.IndexOf(',', lpos) - 1;
			if(!float.TryParse(www.text.Substring(lpos, llen - lpos), System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out lng)) lng = 0f;
				//Debug.Log("lng: "+www.text.Substring(lpos, llen - lpos) + ": " + lngTxt + " >> " + lng);
			lpos = www.text.IndexOf("latitude") + 8 + 3;
			llen = www.text.IndexOf(',', lpos) - 1;
			string latTxt = www.text.Substring(lpos, llen - lpos);
			if(!float.TryParse(latTxt, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out lat)) lat = 0f;
				//Debug.Log("lat: "+www.text.Substring(lpos, llen - lpos) + ": " + latTxt + " >> " + lat);
				//Debug.Log("#######" + lng + ", " + lat);
		} else { //Didn't work
			countryCode = "XX";
		}		
	}
		
}
