﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;




public class FlagsFetcher : MonoBehaviour {
	

	public static FlagsFetcher instance ;

	public GameObject flag ;
	public GameObject flagsContentPanel;
	public GameObject flagsScrollPanel;
	public string flagsPath = "flags/";
	public bool isLoaded ;



	private GameObject temp ;

	// Use this for initialization
	void Start () {
		instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public Sprite LoadFlag(string cc) 
	{

		return Resources.Load( flagsPath + cc, typeof(Sprite)) as Sprite;
	}



	public void loadAllFlags()
	{
		if(!isLoaded)
		{
//			UiAnimationManager.instance.showLoadingPanel();
			flagsScrollPanel.SetActive(true);
			StartCoroutine(loadAllFlagsRountine());
//			for(int i = 0 ;i < CountryData.instance.countryInfo.Length;i++)
//			{
//				temp =  Instantiate(flag);
//				temp.transform.SetParent(flagsContentPanel.transform);
//				temp.transform.localScale = new Vector3 (1,1,1);
//				temp.GetComponent<Flag>().initFlagData(CountryData.instance.countryInfo[i].countryName,((LoadFlag(CountryData.instance.countryInfo[i].countryCode)  != null) ? LoadFlag(CountryData.instance.countryInfo[i].countryCode) : LoadFlag ("XX")),CountryData.instance.countryInfo[i].countryCode.ToString());
//			}
//			isLoaded = true;
//			UiAnimationManager.instance.hideLoadingPanel();
//
		}
		else
		{
			flagsScrollPanel.SetActive(true);
		}
	}


	IEnumerator loadAllFlagsRountine()
	{
		for(int i = 0 ;i < CountryData.instance.countryInfo.Length;i++)
		{
			temp =  Instantiate(flag);
			temp.transform.SetParent(flagsContentPanel.transform);
			temp.transform.localScale = new Vector3 (1,1,1);
			temp.GetComponent<Flag>().initFlagData(CountryData.instance.countryInfo[i].countryName,((LoadFlag(CountryData.instance.countryInfo[i].countryCode)  != null) ? LoadFlag(CountryData.instance.countryInfo[i].countryCode) : LoadFlag ("XX")),CountryData.instance.countryInfo[i].countryCode.ToString());
			if(i == CountryData.instance.countryInfo.Length-1)
			{
				isLoaded = true;
				StopCoroutine(loadAllFlagsRountine());
//				UiAnimationManager.instance.hideLoadingPanel();
			}
			yield return null;
				
		}


	}




//	IEnumerator showLoadingPanelRoutine ()
//	{
//		
//	}




}
