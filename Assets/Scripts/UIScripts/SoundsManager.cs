﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SoundsManager : MonoBehaviour//, IPointerClickHandler
{

	public static SoundsManager instance;

	public AudioClip mainMenuButtonClickSound;
	public AudioClip settingButtonSlideSound;
	public AudioClip backGroundMusic;

	public AudioClip chessPieceMoveSound;

	public AudioClip dominoThrowSound;
	public AudioClip dominoDrawSound;
	public AudioClip dominoShuffleSound;

	public AudioClip snakePieceSound;

	public AudioSource audioSource;


	public bool isMusicMuted;
	public bool isSFXMuted;

	public bool hasSceneLoadedBefore ;


	void Awake()
	{
//		if (instance == null)
//		{
//			DontDestroyOnLoad(this.gameObject);
//			instance = this;
//		}
//		else if (instance != this)
//		{
////			SoundsManager previousInstance = SoundsManager.instance ;
//	
//			this.isMusicMuted = SoundsManager.instance.isMusicMuted;
//			this.isSFXMuted = SoundsManager.instance.isSFXMuted;
//			Destroy(SoundsManager.instance.gameObject);
//			SoundsManager.instance = null;
//			SoundsManager.instance = this;
//			DontDestroyOnLoad(this.gameObject);
//		}



		if (instance == null)
		{
			 DontDestroyOnLoad(this.gameObject);
			instance = this;
		} else if (instance != this)
		{
			DontDestroyOnLoad(this.gameObject);

			if(!hasSceneLoadedBefore)
			{
				this.isMusicMuted = PlayerPrefs.GetInt("isMusicMuted") == 1 ? true : false ;
				this.isSFXMuted = PlayerPrefs.GetInt("isSFXMuted") == 1 ? true : false ;
			}
			else
			{
				this.isMusicMuted = SoundsManager.instance.isMusicMuted;
				this.isSFXMuted = SoundsManager.instance.isSFXMuted;
				this.hasSceneLoadedBefore = SoundsManager.instance.hasSceneLoadedBefore;
			}
		
			Destroy(SoundsManager.instance.gameObject);
			SoundsManager.instance = this;
		}

	}


	public void playBackGroundMusic()
	{
		audioSource.mute = false;
		audioSource.clip = backGroundMusic ;
		audioSource.loop = true;
		audioSource.Play();
	}

	public void stopBackGroundMusic()
	{
		audioSource.clip = null ;
		audioSource.loop = true;
		audioSource.Stop();
	}


	public void muteBackGroundMusic()
	{
		audioSource.Stop();
	}


	public void playMainMenuButtonClickSound()
	{
		if(isSFXMuted)
			return;

		AudioSource.PlayClipAtPoint(mainMenuButtonClickSound,(Camera.main != null) ? Camera.main.transform.position : Camera.current.transform.position);
	}


	public void playMainMenuSettingButtonClickSound()
	{
		if(isSFXMuted)
			return;

		AudioSource.PlayClipAtPoint(settingButtonSlideSound,(Camera.main != null) ? Camera.main.transform.position : Camera.current.transform.position);
	}



		
	public void toggleMuteMusic(bool musicState)
	{
		isMusicMuted = musicState;
		if(isMusicMuted)
		{
//			isMusicMuted = true;
			audioSource.mute = true;
			audioSource.Pause();
		}
		else if(!isMusicMuted)
		{
			if(audioSource.clip != null)
			{
//				isMusicMuted = false;
				audioSource.mute = false;
				audioSource.Play();
			}
			else
			{
				playBackGroundMusic();
//				isMusicMuted = false;
			}
		}
		PlayerPrefs.SetInt("isMusicMuted",isMusicMuted ? 1:0);
	}

	public void toggleMuteSFX(bool sfxState)
	{
		isSFXMuted = sfxState;
		PlayerPrefs.SetInt("isSFXMuted",isSFXMuted ? 1:0);
	}




	//************************************************************************//
	public void playButtonSoundInChess()
	{
		if(isSFXMuted)
			return;

		AudioSource.PlayClipAtPoint(mainMenuButtonClickSound,Camera.allCameras[0].transform.position);
	}
	public void playChessMovementSound()
	{
		if(isSFXMuted)
			return;

		AudioSource.PlayClipAtPoint(chessPieceMoveSound,Camera.allCameras[0].transform.position,0.35f);
	}

	//************************************************************************//

	public void playDominoThrowSound()
	{
		if(isSFXMuted)
			return;
		
		AudioSource.PlayClipAtPoint(dominoThrowSound,Camera.main.transform.position);
	}

	public void playDominoDrawSound()
	{
		if(isSFXMuted)
			return;

		AudioSource.PlayClipAtPoint(dominoDrawSound,Camera.main.transform.position,0.35f);
	}

	public void playDominoShuffleSound()
	{
		if(isSFXMuted)
			return;
		
		AudioSource.PlayClipAtPoint(dominoShuffleSound,Camera.main.transform.position);
	}

	//************************************************************************//

	public void playSnakePieceMovementSound()
	{
		if(isSFXMuted)
			return;
		
		AudioSource.PlayClipAtPoint(snakePieceSound,Camera.main.transform.position,0.15f);
	}

	//************************************************************************//
	void OnEnable()
	{
		SceneManager.sceneLoaded += sceneLoaded;
	}

	private void OnDisable()
	{
		SceneManager.sceneLoaded -= sceneLoaded;
	}


	void sceneLoaded(Scene scene, LoadSceneMode mode)
	{
		if (scene.name == "main_menu_scene")
		{
			if(!hasSceneLoadedBefore)
			{
				GameObject.Find("music Toggle").GetComponent<Toggle>().isOn = PlayerPrefs.GetInt("isMusicMuted") == 1 ? true : false ;
				GameObject.Find("sfx Toggle").GetComponent<Toggle>().isOn = PlayerPrefs.GetInt("isSFXMuted") == 1 ? true : false ;

				backGroundMusic = Resources.Load("Sounds/GameMusic",typeof(AudioClip)) as AudioClip;
				chessPieceMoveSound = null;
				dominoThrowSound = null;
				dominoDrawSound = null;
				dominoShuffleSound = null;
				snakePieceSound = null;

			}
			else
			{
				GameObject.Find("music Toggle").GetComponent<Toggle>().isOn = SoundsManager.instance.isMusicMuted;
				GameObject.Find("sfx Toggle").GetComponent<Toggle>().isOn = SoundsManager.instance.isSFXMuted;

				backGroundMusic = Resources.Load("Sounds/GameMusic",typeof(AudioClip)) as AudioClip;
				chessPieceMoveSound = null;
				dominoThrowSound = null;
				dominoDrawSound = null;
				dominoShuffleSound = null;
				snakePieceSound = null;
//				backGroundMusic = Resources.Load("Sounds/GameMusic",typeof(AudioClip)) as AudioClip;

			}
			hasSceneLoadedBefore = true;

			if(!isMusicMuted)
			{
				Invoke("playBackGroundMusic",3f);
			}
		}
		else if(scene.name == "chessScene")
		{
//
//			if (instance == null)
//			{
//				DontDestroyOnLoad(this.gameObject);
//				instance = this;
//			}
//			else if (instance != this)
//			{
//				//SoundsManager previousInstance = SoundsManager.instance ;
//				this.isMusicMuted = SoundsManager.instance.isMusicMuted;
//				this.isSFXMuted = SoundsManager.instance.isSFXMuted;
//				Destroy(SoundsManager.instance.gameObject);
//				SoundsManager.instance = null;
//				SoundsManager.instance = this;
//				DontDestroyOnLoad(this.gameObject);
//			}

			stopBackGroundMusic();
//			GameObject.Find("soundMuteToggle").GetComponent<Toggle>().isOn = PlayerPrefs.GetInt("isSFXMuted") == 1 ? true : false;
			GameObject.Find("soundMuteToggle").GetComponent<Toggle>().isOn = SoundsManager.instance.isSFXMuted;

			backGroundMusic = null;
			chessPieceMoveSound = Resources.Load("Sounds/Chess/Chess Move",typeof(AudioClip)) as AudioClip;
			dominoThrowSound = null;
			dominoDrawSound = null;
			dominoShuffleSound = null;
			snakePieceSound = null;

//			toggleMuteSFX();
		}
		else if(scene.name == "Domino")
		{
			stopBackGroundMusic();
//			GameObject.Find("soundMuteToggle").GetComponent<Toggle>().isOn = PlayerPrefs.GetInt("isSFXMuted") == 1 ? true : false ;
			GameObject.Find("soundMuteToggle").GetComponent<Toggle>().isOn = SoundsManager.instance.isSFXMuted;
//			toggleMuteSFX();

			backGroundMusic = null;
			chessPieceMoveSound =null;
			dominoThrowSound =  Resources.Load("Sounds/Domino/Axe Chop Tree Sound Effect - Royalty Free from AudioBlocks",typeof(AudioClip)) as AudioClip;;
			dominoDrawSound =  Resources.Load("Sounds/Domino/draw sound",typeof(AudioClip)) as AudioClip;;
			dominoShuffleSound = null;
			snakePieceSound = null;
		}
		else if(scene.name == "Snake")
		{	
			stopBackGroundMusic();
//			GameObject.Find("soundMuteToggle").GetComponent<Toggle>().isOn = PlayerPrefs.GetInt("isSFXMuted") == 1 ? true : false ;
			GameObject.Find("soundMuteToggle").GetComponent<Toggle>().isOn = SoundsManager.instance.isSFXMuted;
//			toggleMuteSFX();

			backGroundMusic = null;
			chessPieceMoveSound =null;
			dominoThrowSound = null;
			dominoDrawSound =  null;
			dominoShuffleSound = null;
			snakePieceSound = Resources.Load("Sounds/Snake/S&L",typeof(AudioClip)) as AudioClip;

		}

	}






}
