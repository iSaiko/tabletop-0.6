﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HideCreateRoomPassword : MonoBehaviour {


	public GameObject passwordObject;
	public GameObject showPasswordButtonObject;


	public void togglePasswordInputHide(bool passwordInputState)
	{
		if(passwordInputState)
		{
			passwordObject.SetActive(true);
			showPasswordButtonObject.SetActive(true);
		}
		else
		{
			passwordObject.SetActive(false);
			showPasswordButtonObject.SetActive(false);
		}

	}

	void Start()
	{
		passwordObject.SetActive(false);
		showPasswordButtonObject.SetActive(false);
	}

}
