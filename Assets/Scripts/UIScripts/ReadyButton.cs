﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReadyButton : MonoBehaviour {

    public bool isReady;
	void Start () {
		
	}
	
	void Update () {
		
	}

    public void toggleReady()
    {
        if (!isReady)
        {
            isReady = true;
            this.gameObject.GetComponentInChildren<Text>().text = "Not Ready";
            UiMenuValues.instance.PressReady();
        }
        else
        {
            isReady = false;
            this.gameObject.GetComponentInChildren<Text>().text = "Ready";

        }
    
    }

}
