﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiMenuValues : MonoBehaviour {

	public static UiMenuValues instance ;

	public Color color1 ;
	public Color color2 ;

	public string userName ;
	public string password ;
	public InputField loginUserName;
	public InputField loginPassword;
	//****************************************************//
	public string registrationUserName;
	public string registrationEmail;
	public string registrationPhone;
	public string registrationPassword;
	public string registrationPasswordConfirm;
	//****************************************************//
	public string gameType = "Chess"; // just for now >>> will be dynamic later
	public string roomNameToBeCreated;
	public string creatRoomPassword;
	public bool creatRoomWithPassword;

	public int roundTime;
	public Slider RoundTimeSlider;
	public Slider dominoTotalGameScoreSlider;
	//****************************************************//
	public string roomToSearchFor;
	//****************************************************//
	public Text CreateRoomGameTypeLabel;
	//****************************************************//
	public string oldPassword;
	public string NewPassword;
	public string confirmNewPassword;


	public string newPhoneNumber; 
	public string newEmail;
	public string confirmPassword;

	//****************************************************//
	// waiting room variables  
	public Text player1NameLabelWR;
	public Text player2NameLabelWR;
	public Image player1ReadyStateImageWR ;
	public Image player2ReadyStateImageWR ;

	public Text waitingRoomPlayersNumber;
	public Text waitingRoomGameType;
	public Text WaitingRoomTimeOrScoreLabel;
	public Text WaitingRoomRoundTime;
	public Text WaitingRoomGameState;
    public Button hostReadyButton;

    public bool isHostReady;
    public bool isClientReady;


	public Sprite readySprite;
	public Sprite notReadySprite;

	public Sprite buttonReadySprite;
	public Sprite buttonNotReadySprite;

    public int activeRoomId;
	public string roomToEnterPassword;


	public Text onlineGameRoundTimeOrGameScore;

	//******************************************************************************************************************//



	void Start () {
		instance = this;
	}


	//******************************************************************************************************************//

	public void takeUserNameInput(string userName)
	{
		this.userName = userName;
	}

	public void takePasswordInput(string password)
	{
		this.password = password ;
	}

	//****************************************************//


	public void takeRegistrationUserNameInput(string registrationUserName)
	{
		this.registrationUserName = registrationUserName;
	}

	public void takeRegistrationEmailInput(string registrationEmail)
	{
		this.registrationEmail = registrationEmail;
	}

	public void takeRegistrationPhoneInput(string registrationPhone)
	{
		this.registrationPhone = registrationPhone;
	}

	public void takeRegistrationPasswordInput(string registrationPassword)
	{
		this.registrationPassword = registrationPassword;
	}
	public void takeRegistrationPasswordConfirmInput(string registrationPasswordConfirm)
	{
		this.registrationPasswordConfirm = registrationPasswordConfirm;
	}

	public bool isPasswordsMatching()
	{
		if(registrationPassword == registrationPasswordConfirm)
		{
			return true;	
		}
		else
		{
			return false;	
		}
	}



	//****************************************************//


	public void takeCreateRoomName(string roomNameToBeCreated)
	{
		this.roomNameToBeCreated = roomNameToBeCreated;
	}


	public void takeRoundTimeSliderValue(float value)
	{
		RoundTimeSlider.value = ((int)value / 5)*5 + Mathf.Round((value % 5)/10) * 5; 
		roundTime = (int)RoundTimeSlider.value ;
	}

	public void takeDominoTotalGameScoreValue(float value)
	{
		dominoTotalGameScoreSlider.value = ((int)value / 50)*50 + Mathf.Round((value % 50)/50) * 50; 
		roundTime = (int)dominoTotalGameScoreSlider.value ;
	}

	public void checkIfRoomWithPassword(bool usePassword)
	{
		creatRoomWithPassword = usePassword;
	}

	public void takeCreatRoomPassword(string creatRoomPassword)
	{
		this.creatRoomPassword = creatRoomPassword;
	}

	//****************************************************//
	public void takeOldPassword(string oldPassword)
	{
		this.oldPassword = oldPassword ;
	}


	public void takeNewPassword(string NewPassword)
	{
		this.NewPassword = NewPassword ;
	}


	public void takeConfirmNewPassword(string confirmNewPassword)
	{
		this.confirmNewPassword = confirmNewPassword ;
	}

	public void takeNewPhoneNumber(string newPhoneNumber)
	{
		this.newPhoneNumber = newPhoneNumber;
	}
		
	public void takeNewEmail(string newEmail)
	{
		this.newEmail = newEmail;
	}
	public void takeConfirmPassword(string confirmPassword)
	{
		this.confirmPassword  = confirmPassword ;
	}

	//****************************************************//


	public void takeRoomToSearchFor(string roomToSearchFor)
	{

		this.roomToSearchFor = roomToSearchFor;
	}

	public void takeRoomToEnterPassword(string roomToEnterPassword)
	{
		this.roomToEnterPassword = roomToEnterPassword;
	}

	public void putRegistrationDataInLoginMenu()
	{
		loginUserName.text = registrationUserName ;
		loginPassword.text = registrationPassword;
	}

	public void loadUserName()
	{
		print("user name should be loaded");
			loginUserName.text = SavingManager.instance.getUserName();
	}


	public void saveUserName()
	{
		print(userName);
		print(loginUserName.text);
		SavingManager.instance.userName = userName;
		SavingManager.instance.Save();
		print("user name saved successfully");

	}


	public void selectGameChess()
	{
		gameType = "chess";
        Rooms.instance.gameType = gameType;
		CreateRoomGameTypeLabel.text = gameType;
		RoundTimeSlider.gameObject.SetActive(true);
		dominoTotalGameScoreSlider.gameObject.SetActive(false);
		roundTime = 5;
		onlineGameRoundTimeOrGameScore.text = "Round Time";
	}

	public void selectGameSnake()
	{
		gameType = "snake";
        Rooms.instance.gameType = gameType;
		CreateRoomGameTypeLabel.text = gameType;
		RoundTimeSlider.gameObject.SetActive(false);
		dominoTotalGameScoreSlider.gameObject.SetActive(false);
		roundTime = 10;
		onlineGameRoundTimeOrGameScore.text = "Round Time";

	}

	public void selectGameDominos()
	{
		gameType = "domino";
        Rooms.instance.gameType = gameType;
		CreateRoomGameTypeLabel.text = gameType;
		RoundTimeSlider.gameObject.SetActive(false);
		dominoTotalGameScoreSlider.gameObject.SetActive(true);
		roundTime = 50;
		onlineGameRoundTimeOrGameScore.text = "Game Score";

	}






	public void updateWaitingRoomInfo(string player1Name , string player2Name,bool isPlayer1Ready , bool isPlayer2Ready ,int waitingRoomPlayersNumber ,bool GameState)
	{
		UiAnimationManager.instance.clearWaitingRoomChatBox();
		player1NameLabelWR.text = player1Name;
		player2NameLabelWR.text = player2Name;
		player1ReadyStateImageWR.sprite = isPlayer1Ready ? readySprite : notReadySprite ;
		player2ReadyStateImageWR.color =  Color.white;
		player2ReadyStateImageWR.sprite = isPlayer2Ready ? readySprite : notReadySprite ;
		this.waitingRoomPlayersNumber.text = waitingRoomPlayersNumber.ToString();
		waitingRoomGameType.text = this.gameType;
		WaitingRoomTimeOrScoreLabel.text = (gameType == "domino") ? "Game score :" : "Round time :";
        WaitingRoomRoundTime.text = this.roundTime.ToString();
		WaitingRoomGameState.text = GameState ? "Ready" : "Not Ready" ;
		WaitingRoomGameState.color = GameState ? Color.green : Color.red ;
        if (Rooms.instance.gameType == "chess")
        {
//            hostReadyButton.interactable = (m_UNETChess.instance.m_ConnectionIds.Count + 1 == 2) ? true : false;
			hostReadyButton.interactable = true;
			m_UNETChess.instance.roundTime =  this.roundTime;
        }
		else if (Rooms.instance.gameType == "snake")
        {
//            hostReadyButton.interactable = (m_UNETSnake.instance.m_ConnectionIds.Count + 1 == 2) ? true : false;
			hostReadyButton.interactable = true;

        }
		else if (Rooms.instance.gameType == "domino")
        {
//            hostReadyButton.interactable = (m_UNETDomino.instance.m_ConnectionIds.Count + 1 == 2) ? true : false;
			hostReadyButton.interactable = true;
			m_UNETDomino.instance.GameScore = this.roundTime;
        }

	}


    public void clientLeftWaitingRoomInfo()
    {
        player2NameLabelWR.text = "";
		player2ReadyStateImageWR.color =  Color.clear;
        this.waitingRoomPlayersNumber.text = "1";
        WaitingRoomGameState.text = "Not Ready";
        WaitingRoomGameState.color =  Color.red;
        hostReadyButton.interactable = false;
        excuteToggleReady(true, false);

        GameObject hostButtonsGroup = UiAnimationManager.instance.waitingRoomMenuPanel.transform.FindChild("host Buttons").gameObject;
        Button[] hostButtons = hostButtonsGroup.GetComponentsInChildren<Button>();
        hostButtons[2].interactable = false;
    }

    public void updateWaitingRoomInfo(bool GameState)
    {

        WaitingRoomGameState.text = GameState ? "Ready" : "Not Ready";
        WaitingRoomGameState.color = GameState ? Color.green : Color.red;

    }



    public void PressReady()
    {
	
		UiAnimationManager.instance.showLoadingPanel();

        if (gameType == "chess")
        {
            if (!isHostReady && m_UNETChess.instance.isHost)
            {
                //m_UNETChess.instance.Send("CRDY|" + (m_UNETChess.instance.isHost ? 1 : 2).ToString());
                Rooms.instance.SetReadyForRoom(activeRoomId);
            }
            else if (isHostReady && m_UNETChess.instance.isHost)
            {
                //m_UNETChess.instance.Send("CNRDY|" + (m_UNETChess.instance.isHost ? 1 : 2).ToString());
                Rooms.instance.SetNotReadyForRoom(activeRoomId);
            }
            else if (isClientReady && !m_UNETChess.instance.isHost)
            {
                Rooms.instance.SetNotReadyForRoom(activeRoomId);
                //m_UNETChess.instance.Send("CNRDY|" + (m_UNETChess.instance.isHost ? 1 : 2).ToString());
            }
            else if (!isClientReady && !m_UNETChess.instance.isHost)
            {
                //m_UNETChess.instance.Send("CRDY|" + (m_UNETChess.instance.isHost ? 1 : 2).ToString());
                Rooms.instance.SetReadyForRoom(activeRoomId);
            }
        }
        else if (gameType == "snake")
        {
            if (!isHostReady && m_UNETSnake.instance.isHost)
            {

                //m_UNETChess.instance.Send("CRDY|" + (m_UNETChess.instance.isHost ? 1 : 2).ToString());
                Rooms.instance.SetReadyForRoom(activeRoomId);
            }
            else if (isHostReady && m_UNETSnake.instance.isHost)
            {
                //m_UNETChess.instance.Send("CNRDY|" + (m_UNETChess.instance.isHost ? 1 : 2).ToString());
                Rooms.instance.SetNotReadyForRoom(activeRoomId);
            }
            else if (isClientReady && !m_UNETSnake.instance.isHost)
            {
                Rooms.instance.SetNotReadyForRoom(activeRoomId);
                //m_UNETChess.instance.Send("CNRDY|" + (m_UNETChess.instance.isHost ? 1 : 2).ToString());
            }
            else if (!isClientReady && !m_UNETSnake.instance.isHost)
            {
                //m_UNETChess.instance.Send("CRDY|" + (m_UNETChess.instance.isHost ? 1 : 2).ToString());
                Rooms.instance.SetReadyForRoom(activeRoomId);
            }
        }
        else if (gameType == "domino")
        {
            if (!isHostReady && m_UNETDomino.instance.isHost)
            {

                //m_UNETChess.instance.Send("CRDY|" + (m_UNETChess.instance.isHost ? 1 : 2).ToString());
                Rooms.instance.SetReadyForRoom(activeRoomId);
            }
            else if (isHostReady && m_UNETDomino.instance.isHost)
            {
                //m_UNETChess.instance.Send("CNRDY|" + (m_UNETChess.instance.isHost ? 1 : 2).ToString());
                Rooms.instance.SetNotReadyForRoom(activeRoomId);
            }
            else if (isClientReady && !m_UNETDomino.instance.isHost)
            {
                Rooms.instance.SetNotReadyForRoom(activeRoomId);
                //m_UNETChess.instance.Send("CNRDY|" + (m_UNETChess.instance.isHost ? 1 : 2).ToString());
            }
            else if (!isClientReady && !m_UNETDomino.instance.isHost)
            {
                //m_UNETChess.instance.Send("CRDY|" + (m_UNETChess.instance.isHost ? 1 : 2).ToString());
                Rooms.instance.SetReadyForRoom(activeRoomId);
            }
        }
    }
       


    public void excuteToggleReady(bool isHost , bool state)
    {

        if (isHost)
        {
			player1ReadyStateImageWR.sprite = (state) ? readySprite : notReadySprite;
            isHostReady = state;
            GameObject hostButtonsGroup = UiAnimationManager.instance.waitingRoomMenuPanel.transform.FindChild("host Buttons").gameObject;
			Image[] hostLabels = hostButtonsGroup.GetComponentsInChildren<Image>();
			hostLabels[1].sprite = (!state) ? buttonReadySprite : buttonNotReadySprite;
			hostLabels[0].gameObject.GetComponent<Button>().interactable = (state) ? false : true;

        }
        else
        {
			player2ReadyStateImageWR.sprite = (state) ? readySprite : notReadySprite;
            isClientReady = state;
            GameObject clientButtonsGroup = UiAnimationManager.instance.waitingRoomMenuPanel.transform.FindChild("client Buttons").gameObject;
			Image[] clientLabels = clientButtonsGroup.GetComponentsInChildren<Image>();
			clientLabels[1].sprite = (!state) ? buttonReadySprite : buttonNotReadySprite ;
			clientLabels[0].gameObject.GetComponent<Button>().interactable = (state) ? false : true;

        }

        if (isHostReady && isClientReady)
        {
            updateWaitingRoomInfo(true);
            GameObject hostButtonsGroup = UiAnimationManager.instance.waitingRoomMenuPanel.transform.FindChild("host Buttons").gameObject;
            Button[] hostButtons = hostButtonsGroup.GetComponentsInChildren<Button>();
            hostButtons[2].interactable = true;
        }
        else
        {
            updateWaitingRoomInfo(false);
            GameObject hostButtonsGroup = UiAnimationManager.instance.waitingRoomMenuPanel.transform.FindChild("host Buttons").gameObject;
            Button[] hostButtons = hostButtonsGroup.GetComponentsInChildren<Button>();
            hostButtons[2].interactable = false;
        }
		UiAnimationManager.instance.hideLoadingPanel();
    }

    public void startGame()
    {
        Rooms.instance.startRoom(activeRoomId);
		UiAnimationManager.instance.showLoadingPanel();

    }

    //Change Account Information 

    public void ChangePasswordButton()
    {
        if (NewPassword == "")
            return;
        if (NewPassword == confirmNewPassword)
        {
            Debug.Log("Changing Password from button function");
            Login.instance.ChangePassword(oldPassword, NewPassword);

        }
        else
        {
            
            UiAnimationManager.instance.WrongConfirmationForPassword();
        }
    }

    public void ChangeMailButton()
    {
        Login.instance.ChangeMail(newEmail, confirmPassword);
    }

    public void ChangePhoneButton()
    {
		print("in chang phone button method");
        Login.instance.ChangePhoneNumber(int.Parse(newPhoneNumber), confirmPassword);
    }

	public void logOut()
	{
		print("we are in logOut method");
		if(Login.instance.hasLogined)
		{
			print("yes the user is logged in");
			Login.instance.signOut();
			StartCoroutine(UiAnimationManager.instance.showFailureMassage("Logout Successful"));
		}
		else 
		{
			StartCoroutine(UiAnimationManager.instance.showFailureMassage("You are already Logged out"));
		}
	}



}
