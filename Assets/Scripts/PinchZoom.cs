﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PinchZoom : MonoBehaviour
{
	public float zoomSpeedTouch = 0.35f;        // The rate of change of the field of view in perspective mode.
	public float zoomSpeedScroll = 3f;        // The rate of change of the field of view in perspective mode.

	public Transform startPoint;
	public Transform End1Point;
	public Transform End2Point;

	public	bool doAutoZoom = false;

	public float initialPosY;
	public static float dominoCameraDeltaPosY;


	void Start()
	{
		
		initialPosY =  this.transform.position.y;

	}

	void Update()
	{
//		#if UNITY_STANDALONE || UNITY_EDITOR || UNITY_STANDALONE_WIN
//		Debug.DrawLine(startPoint.position,this.transform.position,Color.red);
//		Debug.DrawLine(startPoint.position,End1Point.position,Color.green);
//		Debug.DrawLine(this.transform.position,End1Point.position,Color.blue);

//		print(Vector3.Angle((new Vector3 (0,transform.position.y,0) - new Vector3 (0,0,0)),(new Vector3 (0,transform.position.y,0)- End1Point.position)));

		if(doAutoZoom)
		{
			if(Vector3.Angle((new Vector3 (0,transform.position.y,0) - new Vector3 (0,0,0)),(new Vector3 (0,transform.position.y,0)- End1Point.position))  > 27f || 
				Vector3.Angle((new Vector3 (0,transform.position.y,0) - new Vector3 (0,0,0)),(new Vector3 (0,transform.position.y,0)- End2Point.position))  > 27f)
			{
				this.transform.position = new Vector3(0,this.transform.position.y +.25f ,0);
				dominoCameraDeltaPosY += 0.25f;
			}
			else
			{
				doAutoZoom = false ;
			}
		}



		//*********************************************************************//



		if (Input.GetAxis("Mouse ScrollWheel") < 0 ) // && !DominoGround.instance.isDraggingPiece
		{
			float yPos = Mathf.Clamp(transform.position.y + zoomSpeedScroll ,30f ,110);
			transform.position = new Vector3(transform.position.x , yPos,transform.position.z);
			dominoCameraDeltaPosY = transform.position.y - initialPosY ;
		}
		if (Input.GetAxis("Mouse ScrollWheel") > 0 ) // && !DominoGround.instance.isDraggingPiece
		{
			float yPos = Mathf.Clamp(transform.position.y - zoomSpeedScroll ,30f ,110);
			transform.position = new Vector3(transform.position.x , yPos,transform.position.z);		
			dominoCameraDeltaPosY = transform.position.y - initialPosY ;

		}

//		#else

		// If there are two touches on the device...
		if (Input.touchCount == 2)
		{
			// Store both touches.
			Touch touchZero = Input.GetTouch(0);
			Touch touchOne = Input.GetTouch(1);

			// Find the position in the previous frame of each touch.
			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

			// Find the magnitude of the vector (the distance) between the touches in each frame.
			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

			// Find the difference in the distances between each frame.
			float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

			float yPos = Mathf.Clamp(transform.position.y + deltaMagnitudeDiff * zoomSpeedTouch ,30f ,100);
			transform.position = new Vector3(transform.position.x , yPos,transform.position.z);

		}
//		#endif
	}


	public void autoZoom()
	{
		doAutoZoom  = true ;
	}






}