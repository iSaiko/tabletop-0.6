﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationSettings : MonoBehaviour {

	static ApplicationSettings instance ;

	void Awake()
	{
		if(instance == null)
		{
			instance = this;
			DontDestroyOnLoad(this.gameObject);
		}
		else if (instance != this)
		{
			Destroy(this);
		}

	}


	void Start () {

		Screen.sleepTimeout = SleepTimeout.NeverSleep;
//		Application.targetFrameRate = 45;
	}
	

}
