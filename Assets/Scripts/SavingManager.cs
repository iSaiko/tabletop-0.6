﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SavingManager : MonoBehaviour
{
    public static SavingManager instance;

	public string userName;
	public bool isSavedBefore = false;

    void Awake()
    {
		if (instance == null)
        {
            // DontDestroyOnLoad(gameObject);
			instance = this;
		} else if (instance != this)
        {
            Destroy(gameObject);
        }


        Load();
    }
    void Start() { }
	
    public void Save()
    {
		if(isSavedBefore)
			return;

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");
        PlayerData data = new PlayerData();
     
		data.userName = userName;
		data.isSavedBefore = true;
      
        bf.Serialize(file, data);
        file.Close();
    }
	public void Save(string userName)
	{

		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");
		PlayerData data = new PlayerData();

		data.userName = userName;
		data.isSavedBefore = true;

		bf.Serialize(file, data);
		file.Close();
	}


    public void Load()
    {
		if(!isSavedBefore)
			return;
		
        if (File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {
            
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
           
            PlayerData data = (PlayerData)bf.Deserialize(file);
            file.Close();

			userName = data.userName;
			isSavedBefore = data.isSavedBefore ;
        }
    }

    public void nullifySavedData()
    {
		userName = "";
		isSavedBefore = false;
        Save();
    }
	
	public string getUserName()
	{
		
		if (File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {
            
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
           
            PlayerData data = (PlayerData)bf.Deserialize(file);
            file.Close();

			return userName = data.userName;
        }
		return "";
		
	}

}



[Serializable]
class PlayerData
{
    
    public string userName;
	public bool isSavedBefore;


}

