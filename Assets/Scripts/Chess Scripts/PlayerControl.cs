﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using ArabicSupport;
public class PlayerControl : MonoBehaviour {



	private Camera player1Cam;
	private Camera player2Cam;
	private GameManager myGameManager;
	public GameObject heldPiece;
	public bool isAttacking = false;
	public GameObject player1PromotionCanvas;
	public GameObject player2PromotionCanvas;
	public RaycastHit globalRayHit;
	//public GameObject player1Promoter;
	//public GameObject player2Promoter;
    public GameObject Promoter;
	public static PlayerControl instance;


	private int player1PromotedToQueenIndex = 0;
	private int player1PromotedToBishopIndex = 0;
	private int player1PromotedToKnightIndex = 0;
	private int player1PromotedToRookIndex = 0;

	private int player2PromotedToQueenIndex = 0;
	private int player2PromotedToBishopIndex = 0;
	private int player2PromotedToKnightIndex = 0;
	private int player2PromotedToRookIndex = 0;

	public int playerTurn = 2;
    public bool playerHost;

    public List<GameObject> team1;
    public List<GameObject> team2;
    public List<GameObject> chessPieces = new List<GameObject>();

    public  GameObject currentClient;
    private m_UNETChess client;

    public Transform chatMessageContainer;
    public GameObject messagePrefab;

	private bool stopPlay;
	// Use this for initialization
	void Start () {
		instance = this;
		player1Cam = GameObject.Find("Player1Camera").GetComponent<Camera>();
		player2Cam = GameObject.Find("Player2Camera").GetComponent<Camera>();
        client = FindObjectOfType<m_UNETChess>();
		myGameManager = gameObject.GetComponent<GameManager> ();
        //team1 = ChessBoard.instance.team1;
       // team2 = ChessBoard.instance.team2;
		//player1Promoter = GameObject.Find("Player1PromotablePieces");
		//player2Promoter = GameObject.Find("Player2PromotablePieces");

        for (int i = 0; i < team1.Count; i++)
        {
            team1[i].GetComponent<Piece>().pieceIndex = i;
            chessPieces.Add(team1[i]);
        }
        for (int i = 0; i < team2.Count; i++)
        {
            team2[i].GetComponent<Piece>().pieceIndex = 16 + i;
            chessPieces.Add(team2[i]);
        }
		//team1 = null ;
		//team2 = null ;

        currentClient = GameObject.Find("UNETChessObject");
        playerHost = currentClient.GetComponent<m_UNETChess>().isHost;
        if(playerHost)
        {
            player1Cam.gameObject.SetActive(true);
            player2Cam.gameObject.SetActive(false);
            //Debug.Log("Deactivating player 2 and activating player 1");
        }
        else
        {
            player2Cam.gameObject.SetActive(true);
            player1Cam.gameObject.SetActive(false);
        }

        
	}
    //public void player1PromoteToQueen(){

    //    player1Promoter.GetComponent<Promoter>().queensArr[player1PromotedToQueenIndex].SetActive(true);
    //    player1Promoter.GetComponent<Promoter>().queensArr[player1PromotedToQueenIndex].transform.position 
    //        = new Vector3(heldPiece.transform.position.x, heldPiece.transform.position.y,heldPiece.transform.position.z);
    //    int pieceIndexToBeSent = player1Promoter.GetComponent<Promoter>().queensArr[player1PromotedToQueenIndex].GetComponent<Piece>().pieceIndex;
    //    player1Promoter.GetComponent<Promoter>().queensArr[player1PromotedToQueenIndex].GetComponent<Queen>().queenRow
    //        = heldPiece.transform.GetComponent<Piece>().row;
    //    player1Promoter.GetComponent<Promoter>().queensArr[player1PromotedToQueenIndex].GetComponent<Queen>().queenColum
    //        = heldPiece.transform.GetComponent<Piece>().column;
    //    heldPiece.GetComponent<Piece>().updateAfterAttack();
    //    print("Sending from promotion button to server");
    //    SendPromotionToServer("CPROM|", heldPiece.GetComponent<Piece>().pieceIndex, pieceIndexToBeSent, 2);
    //    //client.ClientChess("CPROM|", heldPiece.GetComponent<Piece>().pieceIndex, pieceIndexToBeSent, 2)
    //    heldPiece.SetActive(false);
    //    player1PromotionCanvas.SetActive(false);
    //    player1PromotedToQueenIndex++;
    //}

    //public void player1PromoteToRook(){
		
    //    player1Promoter.GetComponent<Promoter>().rooksArr[player1PromotedToRookIndex].SetActive(true);
    //    player1Promoter.GetComponent<Promoter>().rooksArr[player1PromotedToRookIndex].transform.position 
    //        = new Vector3(heldPiece.transform.position.x, heldPiece.transform.position.y,heldPiece.transform.position.z);
    //    int pieceIndexToBeSent = player1Promoter.GetComponent<Promoter>().rooksArr[player1PromotedToRookIndex].GetComponent<Piece>().pieceIndex;
    //    player1Promoter.GetComponent<Promoter>().rooksArr[player1PromotedToRookIndex].GetComponent<Rook>().rookRow
    //        = heldPiece.transform.GetComponent<Piece>().row;
    //    player1Promoter.GetComponent<Promoter>().rooksArr[player1PromotedToRookIndex].GetComponent<Rook>().rookColum
    //        = heldPiece.transform.GetComponent<Piece>().column;
    //    heldPiece.GetComponent<Piece>().updateAfterAttack();
    //    SendPromotionToServer("CPROM|", heldPiece.GetComponent<Piece>().pieceIndex, pieceIndexToBeSent, 2);
    //    heldPiece.SetActive(false);
    //    heldPiece.SetActive(false);
    //    player1PromotionCanvas.SetActive(false);
    //    player1PromotedToRookIndex++;
    //}

    //public void player1PromoteToBishop(){
		
    //    player1Promoter.GetComponent<Promoter>().bishopsArr[player1PromotedToBishopIndex].SetActive(true);
    //    player1Promoter.GetComponent<Promoter>().bishopsArr[player1PromotedToBishopIndex].transform.position 
    //        = new Vector3(heldPiece.transform.position.x, heldPiece.transform.position.y,heldPiece.transform.position.z);
    //    int pieceIndexToBeSent = player1Promoter.GetComponent<Promoter>().bishopsArr[player1PromotedToBishopIndex].GetComponent<Piece>().pieceIndex;

    //    player1Promoter.GetComponent<Promoter>().bishopsArr[player1PromotedToBishopIndex].GetComponent<Bishop>().bishopRow
    //        = heldPiece.transform.GetComponent<Piece>().row;
    //    player1Promoter.GetComponent<Promoter>().bishopsArr[player1PromotedToBishopIndex].GetComponent<Bishop>().bishopColum
    //        = heldPiece.transform.GetComponent<Piece>().column;
    //    heldPiece.GetComponent<Piece>().updateAfterAttack();
    //    SendPromotionToServer("CPROM|", heldPiece.GetComponent<Piece>().pieceIndex, pieceIndexToBeSent, 2);
    //    heldPiece.SetActive(false);
    //    player1PromotionCanvas.SetActive(false);
    //    player1PromotedToBishopIndex++;
    //}

    //public void player1PromoteToKnight(){
		
    //    player1Promoter.GetComponent<Promoter>().knightsArr[player1PromotedToKnightIndex].SetActive(true);
    //    player1Promoter.GetComponent<Promoter>().knightsArr[player1PromotedToKnightIndex].transform.position 
    //        = new Vector3(heldPiece.transform.position.x, heldPiece.transform.position.y,heldPiece.transform.position.z);
    //    int pieceIndexToBeSent = player1Promoter.GetComponent<Promoter>().knightsArr[player1PromotedToKnightIndex].GetComponent<Piece>().pieceIndex;
    //    player1Promoter.GetComponent<Promoter>().knightsArr[player1PromotedToKnightIndex].GetComponent<Knight>().knightRow
    //        = heldPiece.transform.GetComponent<Piece>().row;
    //    player1Promoter.GetComponent<Promoter>().knightsArr[player1PromotedToKnightIndex].GetComponent<Knight>().knightColum
    //        = heldPiece.transform.GetComponent<Piece>().column;
    //    heldPiece.GetComponent<Piece>().updateAfterAttack();
    //    SendPromotionToServer("CPROM|", heldPiece.GetComponent<Piece>().pieceIndex, pieceIndexToBeSent, 2);
    //    heldPiece.SetActive(false);
    //    player1PromotionCanvas.SetActive(false);
    //    player1PromotedToKnightIndex++;
    //}

    //public void player2PromoteToQueen(){
		
    //    player2Promoter.GetComponent<Promoter>().queensArr[player2PromotedToQueenIndex].SetActive(true);
    //    player2Promoter.GetComponent<Promoter>().queensArr[player2PromotedToQueenIndex].transform.position 
    //        = new Vector3(heldPiece.transform.position.x, heldPiece.transform.position.y,heldPiece.transform.position.z);
    //    int pieceIndexToBeSent = player2Promoter.GetComponent<Promoter>().queensArr[player2PromotedToQueenIndex].GetComponent<Piece>().pieceIndex;
    //    player2Promoter.GetComponent<Promoter>().queensArr[player2PromotedToQueenIndex].GetComponent<Queen>().queenRow
    //        = heldPiece.transform.GetComponent<Piece>().row;
    //    player2Promoter.GetComponent<Promoter>().queensArr[player2PromotedToQueenIndex].GetComponent<Queen>().queenColum
    //        = heldPiece.transform.GetComponent<Piece>().column;
    //    heldPiece.GetComponent<Piece>().updateAfterAttack();
    //    SendPromotionToServer("CPROM|", heldPiece.GetComponent<Piece>().pieceIndex, pieceIndexToBeSent, 1);
    //    heldPiece.SetActive(false);
    //    player2PromotionCanvas.SetActive(false);
    //    player2PromotedToQueenIndex++;
    //}
	
    //public void player2PromoteToRook(){
		
    //    player2Promoter.GetComponent<Promoter>().rooksArr[player2PromotedToRookIndex].SetActive(true);
    //    player2Promoter.GetComponent<Promoter>().rooksArr[player2PromotedToRookIndex].transform.position 
    //        = new Vector3(heldPiece.transform.position.x, heldPiece.transform.position.y,heldPiece.transform.position.z);
    //    int pieceIndexToBeSent = player2Promoter.GetComponent<Promoter>().rooksArr[player2PromotedToRookIndex].GetComponent<Piece>().pieceIndex;
    //    player2Promoter.GetComponent<Promoter>().rooksArr[player2PromotedToRookIndex].GetComponent<Rook>().rookRow
    //        = heldPiece.transform.GetComponent<Piece>().row;
    //    player2Promoter.GetComponent<Promoter>().rooksArr[player2PromotedToRookIndex].GetComponent<Rook>().rookColum
    //        = heldPiece.transform.GetComponent<Piece>().column;
    //    heldPiece.GetComponent<Piece>().updateAfterAttack();
    //    SendPromotionToServer("CPROM|", heldPiece.GetComponent<Piece>().pieceIndex, pieceIndexToBeSent, 1);
    //    heldPiece.SetActive(false);
    //    player2PromotionCanvas.SetActive(false);
    //    player2PromotedToRookIndex++;
    //}
	
    //public void player2PromoteToBishop(){
		
    //    player2Promoter.GetComponent<Promoter>().bishopsArr[player2PromotedToBishopIndex].SetActive(true);
    //    player2Promoter.GetComponent<Promoter>().bishopsArr[player2PromotedToBishopIndex].transform.position 
    //        = new Vector3(heldPiece.transform.position.x, heldPiece.transform.position.y,heldPiece.transform.position.z);
    //    int pieceIndexToBeSent = player2Promoter.GetComponent<Promoter>().bishopsArr[player2PromotedToBishopIndex].GetComponent<Piece>().pieceIndex;
    //    player2Promoter.GetComponent<Promoter>().bishopsArr[player2PromotedToBishopIndex].GetComponent<Bishop>().bishopRow
    //        = heldPiece.transform.GetComponent<Piece>().row;
    //    player2Promoter.GetComponent<Promoter>().bishopsArr[player2PromotedToBishopIndex].GetComponent<Bishop>().bishopColum
    //        = heldPiece.transform.GetComponent<Piece>().column;
    //    heldPiece.GetComponent<Piece>().updateAfterAttack();
    //    SendPromotionToServer("CPROM|", heldPiece.GetComponent<Piece>().pieceIndex, pieceIndexToBeSent, 1);
    //    heldPiece.SetActive(false);
    //    player2PromotionCanvas.SetActive(false);
    //    player2PromotedToBishopIndex++;
    //}
	
    //public void player2PromoteToKnight(){
		
    //    player2Promoter.GetComponent<Promoter>().knightsArr[player2PromotedToKnightIndex].SetActive(true);
    //    player2Promoter.GetComponent<Promoter>().knightsArr[player2PromotedToKnightIndex].transform.position 
    //        = new Vector3(heldPiece.transform.position.x, heldPiece.transform.position.y,heldPiece.transform.position.z);
    //    int pieceIndexToBeSent = player2Promoter.GetComponent<Promoter>().knightsArr[player2PromotedToKnightIndex].GetComponent<Piece>().pieceIndex;
    //    player2Promoter.GetComponent<Promoter>().knightsArr[player2PromotedToKnightIndex].GetComponent<Knight>().knightRow
    //        = heldPiece.transform.GetComponent<Piece>().row;
    //    player2Promoter.GetComponent<Promoter>().knightsArr[player2PromotedToKnightIndex].GetComponent<Knight>().knightColum
    //        = heldPiece.transform.GetComponent<Piece>().column;
    //    heldPiece.GetComponent<Piece>().updateAfterAttack();
    //    SendPromotionToServer("CPROM|", heldPiece.GetComponent<Piece>().pieceIndex, pieceIndexToBeSent, 1);
    //    heldPiece.SetActive(false);
    //    player2PromotionCanvas.SetActive(false);
    //    player2PromotedToKnightIndex++;
    //}

	// Update is called once per frame
	void Update () {

		if(stopPlay)
		{
			//print("player control should be stopped");
			return;
		}
		else
		{
			//print("allow play");

		}

		

		getMouseInputs ();
		//int i = 0;
		//Debug.Log ("Update " + i++);
		//Debug.Log ( heldPiece.GetComponent<Piece>().row );
        //Debug.Log(team1.Length.ToString() + "  " + team2.Length.ToString());
	}


	public void getMouseInputs(){
		Ray ray1;
        Ray ray2;
		RaycastHit rayHit;
		if(playerTurn == 1 && playerHost){
			//player1Cam.gameObject.SetActive(true);
			//player2Cam.gameObject.SetActive(false);
			if (Input.GetMouseButtonDown (0)) {
				ray1 = player1Cam.ScreenPointToRay (Input.mousePosition);
				if (Physics.Raycast (ray1, out rayHit)) // ,10000f,LayerMask.NameToLayer("Default")
				{
					if(rayHit.collider.gameObject.layer ==  10){ return;}

					//Debug.Log(rayHit.transform.name +" and "+rayHit.transform.parent);
					if (rayHit.collider.gameObject.tag == "PiecePlayer1" && !myGameManager.pieceIsMoving){
						//rayHit.collider.gameObject.transform.GetComponent<Piece>().highLight();
						heldPiece = rayHit.collider.gameObject;
                        ChessBoard.instance.AllUnattackable();
						heldPiece.transform.GetComponent<Piece>().highLight();
						myGameManager.GetComponent<ChessBoard>().AllUnavailable();
						//check every piece availability
						if(rayHit.collider.gameObject.name.Contains("Queen")){
							//Debug.Log ("Queen Pressed");
							int cellColumn = rayHit.collider.gameObject.GetComponent<Queen>().queenColum ;
							int cellRow = rayHit.collider.gameObject.GetComponent<Queen>().queenRow;
							string team = rayHit.collider.gameObject.transform.tag.ToString();
							myGameManager.GetComponent<ChessBoard>().QueenRegion(cellRow, cellColumn,team);

						}
						//check if piece is bishop
						if(rayHit.collider.gameObject.name.Contains("Bishop")){
							//Debug.Log ("Bishop is Pressed");
							int cellColumn = rayHit.collider.gameObject.GetComponent<Bishop>().bishopColum ;
							int cellRow = rayHit.collider.gameObject.GetComponent<Bishop>().bishopRow;
							string team = rayHit.collider.gameObject.transform.tag.ToString();
							myGameManager.GetComponent<ChessBoard>().BishopRegion(cellRow, cellColumn,team);
							
						}
						//check if piece is knight
						if(rayHit.collider.gameObject.name.Contains("Knight")){
							//Debug.Log ("Knight is Pressed");
							int cellColumn = rayHit.collider.gameObject.GetComponent<Knight>().knightColum ;
							int cellRow = rayHit.collider.gameObject.GetComponent<Knight>().knightRow;
							string team = rayHit.collider.gameObject.transform.tag.ToString();
							myGameManager.GetComponent<ChessBoard>().KnightRegion(cellRow, cellColumn,team);
							
						}
						//check if piece is rook
						if(rayHit.collider.gameObject.name.Contains("Rook")){
							//Debug.Log ("Rook is Pressed");
							int cellColumn = rayHit.collider.gameObject.GetComponent<Rook>().rookColum ;
							int cellRow = rayHit.collider.gameObject.GetComponent<Rook>().rookRow;
							string team = rayHit.collider.gameObject.transform.tag.ToString();
							myGameManager.GetComponent<ChessBoard>().RookRegion(cellRow, cellColumn,team);
							
						}
						//check if piece is pawn
						if(rayHit.collider.gameObject.name.Contains("Pawn")){
							//Debug.Log ("Pawn is Pressed");
							int cellColumn = rayHit.collider.gameObject.GetComponent<Pawn>().pawnColum ;
							int cellRow = rayHit.collider.gameObject.GetComponent<Pawn>().pawnRow;
							//string team = rayHit.collider.gameObject.transform.tag.ToString();
							myGameManager.GetComponent<ChessBoard>().PawnRegion(cellRow, cellColumn,
							                                                    rayHit.collider.gameObject,
							                                                    rayHit.collider.gameObject.transform.tag);
							
						}
						//check if piece is king
						if(rayHit.collider.gameObject.name.Contains("King")){
							//Debug.Log ("King is Pressed");
							int cellColumn = rayHit.collider.gameObject.GetComponent<King>().kingColum ;
							int cellRow = rayHit.collider.gameObject.GetComponent<King>().kingRow;
							string team = rayHit.collider.gameObject.transform.tag.ToString();
							myGameManager.GetComponent<ChessBoard>().KingRegion(cellRow, cellColumn,team);
						}
						myGameManager.SelectPiece (rayHit.collider.gameObject);
						myGameManager.changeState (1);
					}
				}
			}

			if(myGameManager.GameState == 1){
				Vector3 selectedCoord;
				if(Input.GetMouseButtonDown(0)){
					ray1 = player1Cam.ScreenPointToRay(Input.mousePosition);
					if(Physics.Raycast(ray1, out rayHit)){
						if(rayHit.collider.gameObject.layer ==  10){ return;}



						if(isAttacking && heldPiece.transform.tag == "PiecePlayer1"){
							if(rayHit.collider.gameObject.tag == "PiecePlayer2"){
								//check if attacked is pawn
								if(rayHit.transform.name.Contains("Pawn")){
									rayHit.collider.gameObject.GetComponent<Pawn>().checkAttackable();
									if(rayHit.collider.gameObject.GetComponent<Pawn>().pieceAttackable){
										selectedCoord = new Vector3(rayHit.collider.transform.position.x, 
																		heldPiece.transform.position.y
								                     		       ,rayHit.collider.transform.position.z);

										myGameManager.MovePiece(selectedCoord);
	//									//Debug.Log ( heldPiece.GetComponent<Piece>().row + "   " + rayHit.collider.GetComponent<Piece>().row );
										heldPiece.GetComponent<Piece>().row = rayHit.collider.GetComponent<Piece>().row;
										heldPiece.GetComponent<Piece>().column = rayHit.collider.GetComponent<Piece>().column;


										//Debug.Log ( heldPiece.GetComponent<Piece>().row );
										heldPiece.GetComponent<Piece>().updateAfterAttack();

										//Debug.Log ("I got Here");
										myGameManager.changeState(0);
										//rayHit.collider.gameObject.SetActive(false);
                                        SendAttackDataToServer("CATK|", heldPiece.GetComponent<Piece>().pieceIndex, rayHit.collider.GetComponent<Piece>().pieceIndex, 2);
										isAttacking = false;
										myGameManager.GetComponent<ChessBoard>().AllUnavailable();
								}
							}
								//check if attacked is queen
								if(rayHit.transform.name.Contains("Queen")){	
									rayHit.collider.gameObject.GetComponent<Queen>().checkAttackable();
									if(rayHit.collider.gameObject.GetComponent<Queen>().pieceAttackable){
										selectedCoord = new Vector3(rayHit.collider.transform.position.x, 
																	heldPiece.transform.position.y
										                            ,rayHit.collider.transform.position.z);
										myGameManager.MovePiece(selectedCoord);
										heldPiece.GetComponent<Piece>().row = rayHit.collider.GetComponent<Piece>().row;
										heldPiece.GetComponent<Piece>().column = rayHit.collider.GetComponent<Piece>().column;
										heldPiece.GetComponent<Piece>().updateAfterAttack();
										//Debug.Log ("I got Here");
										myGameManager.changeState(0);
                                        //rayHit.collider.gameObject.SetActive(false);
                                        SendAttackDataToServer("CATK|", heldPiece.GetComponent<Piece>().pieceIndex, rayHit.collider.GetComponent<Piece>().pieceIndex, 2);
										isAttacking = false;
										myGameManager.GetComponent<ChessBoard>().AllUnavailable();
									}
								}
								//check if attacked is king
								if(rayHit.transform.name.Contains("King")){
									rayHit.collider.gameObject.GetComponent<King>().checkAttackable();
									if(rayHit.collider.gameObject.GetComponent<King>().pieceAttackable){
										selectedCoord = new Vector3(rayHit.collider.transform.position.x, 
																		heldPiece.transform.position.y
										                            ,rayHit.collider.transform.position.z);
										myGameManager.MovePiece(selectedCoord);
										heldPiece.GetComponent<Piece>().row = rayHit.collider.GetComponent<Piece>().row;
										heldPiece.GetComponent<Piece>().column = rayHit.collider.GetComponent<Piece>().column;
										heldPiece.GetComponent<Piece>().updateAfterAttack();

										//Debug.Log ("I got Here");
										myGameManager.changeState(0);
                                        //rayHit.collider.gameObject.SetActive(false);
                                        SendAttackDataToServer("CATK|", heldPiece.GetComponent<Piece>().pieceIndex, rayHit.collider.GetComponent<Piece>().pieceIndex, 2);
										isAttacking = false;
										myGameManager.GetComponent<ChessBoard>().AllUnavailable();
									}
								}
								//check if attacked is rook
								if(rayHit.transform.name.Contains("Rook")){
									rayHit.collider.gameObject.GetComponent<Rook>().checkAttackable();
									if(rayHit.collider.gameObject.GetComponent<Rook>().pieceAttackable){
										selectedCoord = new Vector3(rayHit.collider.transform.position.x, 
																		heldPiece.transform.position.y
										                            ,rayHit.collider.transform.position.z);
										myGameManager.MovePiece(selectedCoord);
										heldPiece.GetComponent<Piece>().row = rayHit.collider.GetComponent<Piece>().row;
										heldPiece.GetComponent<Piece>().column = rayHit.collider.GetComponent<Piece>().column;
										heldPiece.GetComponent<Piece>().updateAfterAttack();

										//Debug.Log ("I got Here");
										myGameManager.changeState(0);
                                        //rayHit.collider.gameObject.SetActive(false);
                                        SendAttackDataToServer("CATK|", heldPiece.GetComponent<Piece>().pieceIndex, rayHit.collider.GetComponent<Piece>().pieceIndex, 2);
										isAttacking = false;
										myGameManager.GetComponent<ChessBoard>().AllUnavailable();
									}
								}
								//check if attakced is knight
								if(rayHit.transform.name.Contains("Knight")){
									rayHit.collider.gameObject.GetComponent<Knight>().checkAttackable();
									if(rayHit.collider.gameObject.GetComponent<Knight>().pieceAttackable){
										selectedCoord = new Vector3(rayHit.collider.transform.position.x, 
																	heldPiece.transform.position.y
										                            ,rayHit.collider.transform.position.z);
										myGameManager.MovePiece(selectedCoord);
										heldPiece.GetComponent<Piece>().row = rayHit.collider.GetComponent<Piece>().row;
										heldPiece.GetComponent<Piece>().column = rayHit.collider.GetComponent<Piece>().column;
										heldPiece.GetComponent<Piece>().updateAfterAttack();

										//Debug.Log ("I got Here");
										myGameManager.changeState(0);
                                        //rayHit.collider.gameObject.SetActive(false);
                                        SendAttackDataToServer("CATK|", heldPiece.GetComponent<Piece>().pieceIndex, rayHit.collider.GetComponent<Piece>().pieceIndex, 2);
										isAttacking = false;
										myGameManager.GetComponent<ChessBoard>().AllUnavailable();
									}
								}
								//check if attacked is bishop
								if(rayHit.transform.name.Contains("Bishop")){
									rayHit.collider.gameObject.GetComponent<Bishop>().checkAttackable();
									if(rayHit.collider.gameObject.GetComponent<Bishop>().pieceAttackable){
										selectedCoord = new Vector3(rayHit.collider.transform.position.x, 
																	heldPiece.transform.position.y
										                            ,rayHit.collider.transform.position.z);
										myGameManager.MovePiece(selectedCoord);
										heldPiece.GetComponent<Piece>().row = rayHit.collider.GetComponent<Piece>().row;
										heldPiece.GetComponent<Piece>().column = rayHit.collider.GetComponent<Piece>().column;
										heldPiece.GetComponent<Piece>().updateAfterAttack();

										//Debug.Log ("I got Here");
										myGameManager.changeState(0);
                                        //rayHit.collider.gameObject.SetActive(false);
                                        SendAttackDataToServer("CATK|", heldPiece.GetComponent<Piece>().pieceIndex, rayHit.collider.GetComponent<Piece>().pieceIndex, 2);
										isAttacking = false;
										myGameManager.GetComponent<ChessBoard>().AllUnavailable();
									}
								}
						}
						}
						if(rayHit.collider.gameObject.tag == "Cell"/* and check if it is a piece of player 2*/){
							globalRayHit = rayHit;
							//Debug.Log (	rayHit.collider.GetComponent<Tile>().isOccupied);

							//Debug.Log ("Current Row = " + cellRow + "Current Column = "+cellColumn);
								if(rayHit.collider.gameObject.GetComponent<Tile>().isAvailable)
								{
								heldPiece.transform.GetComponent<Piece>().UnhighLight();

									selectedCoord = new Vector3(rayHit.collider.transform.position.x, 
																	heldPiece.transform.position.y
									                            ,rayHit.collider.transform.position.z);
									/*Debug.Log ("X ="+rayHit.collider.transform.position.x + "and Y ="+
									           rayHit.collider.transform.position.y);*/
									myGameManager.MovePiece(selectedCoord);
									myGameManager.changeState(0);
//									myGameManager.GetComponent<ChessBoard>().AllUnavailable();
								//queen placement
									if(heldPiece.transform.name.Contains("Queen"))
								   {
									heldPiece.transform.GetComponent<Queen>().unOccupyTile();
									heldPiece.transform.GetComponent<Queen>().queenRow = 
										int.Parse(rayHit.collider.gameObject.transform.parent.name.ToString());
									heldPiece.transform.GetComponent<Queen>().queenColum = 
										int.Parse(rayHit.collider.gameObject.name.ToString());
                                    heldPiece.transform.GetComponent<Queen>().OccupyTile(); 
                                        SendMovementDataToServer("CMOV|", heldPiece.GetComponent<Piece>().pieceIndex, selectedCoord.x, selectedCoord.y, selectedCoord.z,
                                         heldPiece.transform.GetComponent<Queen>().queenRow, heldPiece.transform.GetComponent<Queen>().queenColum,2);
									myGameManager.GetComponent<ChessBoard>().AllUnavailable();

								}
								//bishop placement
								if(heldPiece.transform.name.Contains("Bishop"))
								{
									heldPiece.transform.GetComponent<Bishop>().unOccupyTile();
									heldPiece.transform.GetComponent<Bishop>().bishopRow = 
										int.Parse(rayHit.collider.gameObject.transform.parent.name.ToString());
									heldPiece.transform.GetComponent<Bishop>().bishopColum = 
										int.Parse(rayHit.collider.gameObject.name.ToString());
                                    heldPiece.transform.GetComponent<Bishop>().OccupyTile();
                                    SendMovementDataToServer("CMOV|", heldPiece.GetComponent<Piece>().pieceIndex, selectedCoord.x, selectedCoord.y, selectedCoord.z,
                                     heldPiece.transform.GetComponent<Bishop>().bishopRow, heldPiece.transform.GetComponent<Bishop>().bishopColum,2);
									myGameManager.GetComponent<ChessBoard>().AllUnavailable();

								}
								//knight placement
								if(heldPiece.transform.name.Contains("Knight"))
								{
									heldPiece.transform.GetComponent<Knight>().unOccupyTile();
									heldPiece.transform.GetComponent<Knight>().knightRow = 
										int.Parse(rayHit.collider.gameObject.transform.parent.name.ToString());
									heldPiece.transform.GetComponent<Knight>().knightColum = 
										int.Parse(rayHit.collider.gameObject.name.ToString());
                                    heldPiece.transform.GetComponent<Knight>().OccupyTile();
                                    SendMovementDataToServer("CMOV|", heldPiece.GetComponent<Piece>().pieceIndex, selectedCoord.x, selectedCoord.y, selectedCoord.z,
                                     heldPiece.transform.GetComponent<Knight>().knightRow, heldPiece.transform.GetComponent<Knight>().knightColum,2);
									myGameManager.GetComponent<ChessBoard>().AllUnavailable();

								}
								//rook placement
								if(heldPiece.transform.name.Contains("Rook"))
								{
									heldPiece.transform.GetComponent<Rook>().unOccupyTile();
									heldPiece.transform.GetComponent<Rook>().rookRow = 
										int.Parse(rayHit.collider.gameObject.transform.parent.name.ToString());
									heldPiece.transform.GetComponent<Rook>().rookColum = 
										int.Parse(rayHit.collider.gameObject.name.ToString());
                                    heldPiece.transform.GetComponent<Rook>().OccupyTile();
                                    SendMovementDataToServer("CMOV|", heldPiece.GetComponent<Piece>().pieceIndex, selectedCoord.x, selectedCoord.y, selectedCoord.z,
                                     heldPiece.transform.GetComponent<Rook>().rookRow, heldPiece.transform.GetComponent<Rook>().rookColum,2);
									myGameManager.GetComponent<ChessBoard>().AllUnavailable();

								}
								//pawn placement
								if(heldPiece.transform.name.Contains("Pawn"))
								{
									heldPiece.transform.GetComponent<Pawn>().unOccupyTile();
									heldPiece.transform.GetComponent<Pawn>().pawnRow = 
										int.Parse(rayHit.collider.gameObject.transform.parent.name.ToString());
									heldPiece.transform.GetComponent<Pawn>().pawnColum = 
										int.Parse(rayHit.collider.gameObject.name.ToString());
									heldPiece.transform.GetComponent<Pawn>().OccupyTile();
									heldPiece.transform.GetComponent<Pawn>().firstMove = false;
									if(heldPiece.GetComponent<Pawn>().pawnRow == 1){
                                        SendMovementDataToServer("CMOV|", heldPiece.GetComponent<Piece>().pieceIndex, selectedCoord.x, selectedCoord.y, selectedCoord.z,
                                          heldPiece.transform.GetComponent<Pawn>().pawnRow, heldPiece.transform.GetComponent<Pawn>().pawnColum, 1);
                                        //Debug.Log ("Promotion");
										player1PromotionCanvas.SetActive(true);
									}
                                    if (heldPiece.GetComponent<Pawn>().pawnRow != 1)
                                    {
                                        SendMovementDataToServer("CMOV|", heldPiece.GetComponent<Piece>().pieceIndex, selectedCoord.x, selectedCoord.y, selectedCoord.z,
                                          heldPiece.transform.GetComponent<Pawn>().pawnRow, heldPiece.transform.GetComponent<Pawn>().pawnColum, 2);
                                    }
									myGameManager.GetComponent<ChessBoard>().AllUnavailable();

								}
								//king placement
								if(heldPiece.transform.name.Contains("King"))
								{
									
									if(!rayHit.collider.gameObject.GetComponent<Tile>().readyForCastling)
									{
									print(rayHit.collider.gameObject.GetComponent<Tile>().readyForCastling);
									heldPiece.transform.GetComponent<King>().unOccupyTile();
									heldPiece.transform.GetComponent<King>().kingRow = 
									int.Parse(rayHit.collider.gameObject.transform.parent.name.ToString());
									heldPiece.transform.GetComponent<King>().kingColum = 
									int.Parse(rayHit.collider.gameObject.name.ToString());
                                    heldPiece.transform.GetComponent<King>().OccupyTile();
                                    SendMovementDataToServer("CMOV|", heldPiece.GetComponent<Piece>().pieceIndex, selectedCoord.x, selectedCoord.y, selectedCoord.z,
                                     heldPiece.transform.GetComponent<King>().kingRow, heldPiece.transform.GetComponent<King>().kingColum,2);
									myGameManager.GetComponent<ChessBoard>().AllUnavailable();

									}
									
									else if(rayHit.collider.gameObject.GetComponent<Tile>().readyForCastling)
									{
										int tileRow = int.Parse(rayHit.collider.gameObject.transform.parent.name.ToString());
										int tileColum = int.Parse(rayHit.collider.gameObject.name.ToString());
										string team = heldPiece.transform.GetComponent<King>().team ;
										ChessBoard board = myGameManager.GetComponent<ChessBoard>();


										heldPiece.transform.GetComponent<King>().unOccupyTile();
										heldPiece.transform.GetComponent<King>().kingRow = tileRow;
										heldPiece.transform.GetComponent<King>().kingColum = tileColum;
										heldPiece.transform.GetComponent<King>().OccupyTile();

									

										if(team == "PiecePlayer1" &&  tileRow ==  8 && tileColum == 2 )
										{

											Rook castlingRook = board.getWantedRook(team ,8,1);
											castlingRook.unOccupyTile();
											castlingRook.rookRow = 8;
											castlingRook.rookColum = 3;
											castlingRook.OccupyTile();
											Transform tileTransform = board.rows[7].tiles[2].GetComponent<Transform>();
											Vector3 selectedCoordR = new Vector3(tileTransform.position.x,castlingRook.transform.position.y,castlingRook.transform.position.z);
//											myGameManager.MovePiece(castlingRook,selectedCoordR);

											SendTabyeetDataToServer("CTAB|", heldPiece.GetComponent<Piece>().pieceIndex, selectedCoord.x, selectedCoord.y, selectedCoord.z,
												heldPiece.transform.GetComponent<King>().kingRow, heldPiece.transform.GetComponent<King>().kingColum,
												castlingRook.transform.GetComponent<Piece>().pieceIndex, selectedCoordR.x, selectedCoordR.y, selectedCoordR.z,
												castlingRook.transform.GetComponent<Rook>().rookRow,castlingRook.transform.GetComponent<Rook>().rookColum,2);



										

										}
										else if(team == "PiecePlayer1" &&  tileRow == 8 && tileColum == 6 )
										{
											Rook castlingRook = board.getWantedRook(team,8,8);
											castlingRook.unOccupyTile();
											castlingRook.rookRow = 8;
											castlingRook.rookColum = 5;
											castlingRook.OccupyTile();
											Transform tileTransform = board.rows[7].tiles[4].GetComponent<Transform>();
											Vector3 selectedCoordR  = new Vector3(tileTransform.position.x,castlingRook.transform.position.y,castlingRook.transform.position.z);
//											myGameManager.MovePiece(castlingRook,selectedCoordR);

											SendTabyeetDataToServer("CTAB|", heldPiece.GetComponent<Piece>().pieceIndex, selectedCoord.x, selectedCoord.y, selectedCoord.z,
												heldPiece.transform.GetComponent<King>().kingRow, heldPiece.transform.GetComponent<King>().kingColum,
												castlingRook.transform.GetComponent<Piece>().pieceIndex, selectedCoordR.x, selectedCoordR.y, selectedCoordR.z,
												castlingRook.transform.GetComponent<Rook>().rookRow,castlingRook.transform.GetComponent<Rook>().rookColum,2);




										}
									
										myGameManager.GetComponent<ChessBoard>().AllUnavailable();
									}

								}
								//Debug.Log ("Queen New Row" + heldPiece.transform.GetComponent<Queen>().queenRow + "Queen New Colum" + 
								     //      heldPiece.transform.GetComponent<Queen>().queenColum);
								//Debug.Log (isAttacking);
							}else{
								//Debug.Log ("Tile is not Available");
								myGameManager.GetComponent<ChessBoard>().AllUnavailable();
							}
                               //playerTurn = 2;
                                
						}

                    }
                    //SendTurnDataToServer("CTRC|", 2);
                }
			}
			//end of if player1 statement
        }
		//start of player 2 turn
		if(playerTurn == 2 && !playerHost){
			//player1Cam.gameObject.SetActive(false);
			//player2Cam.gameObject.SetActive(true);
			if (Input.GetMouseButtonDown (0)) {
				ray2 = player2Cam.ScreenPointToRay (Input.mousePosition);
				if (Physics.Raycast (ray2, out rayHit)) {
					if(rayHit.collider.gameObject.layer ==  10){ return;}

					//Debug.Log(rayHit.transform.name +" and "+rayHit.transform.parent);
                    if (rayHit.collider.gameObject.tag == "PiecePlayer2" && !myGameManager.pieceIsMoving)
                    {
						heldPiece = rayHit.collider.gameObject;
						heldPiece.transform.GetComponent<Piece>().highLight();
						myGameManager.GetComponent<ChessBoard>().AllUnavailable();
                        ChessBoard.instance.AllUnattackable();
						//check every piece availability
						if(rayHit.collider.gameObject.name.Contains("Queen")){
							//Debug.Log ("Queen Pressed");
							int cellColumn = rayHit.collider.gameObject.GetComponent<Queen>().queenColum ;
							int cellRow = rayHit.collider.gameObject.GetComponent<Queen>().queenRow;
							string team = rayHit.collider.gameObject.transform.tag.ToString();
							myGameManager.GetComponent<ChessBoard>().QueenRegion(cellRow, cellColumn,team);
							
						}
						//check if piece is bishop
						if(rayHit.collider.gameObject.name.Contains("Bishop")){
							//Debug.Log ("Bishop is Pressed");
							int cellColumn = rayHit.collider.gameObject.GetComponent<Bishop>().bishopColum ;
							int cellRow = rayHit.collider.gameObject.GetComponent<Bishop>().bishopRow;
							string team = rayHit.collider.gameObject.transform.tag.ToString();
							myGameManager.GetComponent<ChessBoard>().BishopRegion(cellRow, cellColumn,team);
							
						}
						//check if piece is knight
						if(rayHit.collider.gameObject.name.Contains("Knight")){
							//Debug.Log ("Knight is Pressed");
							int cellColumn = rayHit.collider.gameObject.GetComponent<Knight>().knightColum ;
							int cellRow = rayHit.collider.gameObject.GetComponent<Knight>().knightRow;
							string team = rayHit.collider.gameObject.transform.tag.ToString();
							myGameManager.GetComponent<ChessBoard>().KnightRegion(cellRow, cellColumn,team);
							
						}
						//check if piece is rook
						if(rayHit.collider.gameObject.name.Contains("Rook")){
							//Debug.Log ("Rook is Pressed");
							int cellColumn = rayHit.collider.gameObject.GetComponent<Rook>().rookColum ;
							int cellRow = rayHit.collider.gameObject.GetComponent<Rook>().rookRow;
							string team = rayHit.collider.gameObject.transform.tag.ToString();
							myGameManager.GetComponent<ChessBoard>().RookRegion(cellRow, cellColumn,team);
							
						}
						//check if piece is pawn
						if(rayHit.collider.gameObject.name.Contains("Pawn")){
							//Debug.Log ("Pawn is Pressed");
							int cellColumn = rayHit.collider.gameObject.GetComponent<Pawn>().pawnColum ;
							int cellRow = rayHit.collider.gameObject.GetComponent<Pawn>().pawnRow;
							//string team = rayHit.collider.gameObject.transform.tag.ToString();
							myGameManager.GetComponent<ChessBoard>().PawnRegion(cellRow, cellColumn,
							                                                    rayHit.collider.gameObject,
							                                                    rayHit.collider.gameObject.transform.tag);
							
						}
						//check if piece is king
						if(rayHit.collider.gameObject.name.Contains("King")){
							//Debug.Log ("King is Pressed");
							int cellColumn = rayHit.collider.gameObject.GetComponent<King>().kingColum ;
							int cellRow = rayHit.collider.gameObject.GetComponent<King>().kingRow;
							string team = rayHit.collider.gameObject.transform.tag.ToString();
							myGameManager.GetComponent<ChessBoard>().KingRegion(cellRow, cellColumn,team);
						}
						myGameManager.SelectPiece (rayHit.collider.gameObject);
						myGameManager.changeState (1);
					}
				}
			}
			
			if(myGameManager.GameState == 1){
				Vector3 selectedCoord;
				if(Input.GetMouseButtonDown(0)){
					ray2 = player2Cam.ScreenPointToRay(Input.mousePosition);
					if(Physics.Raycast(ray2, out rayHit)){
						if(rayHit.collider.gameObject.layer ==  10){ return;}


						
						if(isAttacking && heldPiece.transform.tag == "PiecePlayer2"){
							if(rayHit.collider.gameObject.tag == "PiecePlayer1"){
								//check if attacked is pawn
								if(rayHit.transform.name.Contains("Pawn")){
									rayHit.collider.gameObject.GetComponent<Pawn>().checkAttackable();
									if(rayHit.collider.gameObject.GetComponent<Pawn>().pieceAttackable){
										selectedCoord = new Vector3(rayHit.collider.transform.position.x, 
																	heldPiece.transform.position.y
										                            ,rayHit.collider.transform.position.z);
										
										myGameManager.MovePiece(selectedCoord);
										heldPiece.GetComponent<Piece>().row = rayHit.collider.GetComponent<Piece>().row;
										heldPiece.GetComponent<Piece>().column = rayHit.collider.GetComponent<Piece>().column;
										
										
										//Debug.Log ( heldPiece.GetComponent<Piece>().row );
										heldPiece.GetComponent<Piece>().updateAfterAttack();
										
										//Debug.Log ("I got Here");
										myGameManager.changeState(0);
                                        //rayHit.collider.gameObject.SetActive(false);
                                        SendAttackDataToServer("CATK|", heldPiece.GetComponent<Piece>().pieceIndex, rayHit.collider.GetComponent<Piece>().pieceIndex,1);
										isAttacking = false;
										myGameManager.GetComponent<ChessBoard>().AllUnavailable();
									}
								}
								//check if attacked is queen
								if(rayHit.transform.name.Contains("Queen")){	
									rayHit.collider.gameObject.GetComponent<Queen>().checkAttackable();
									if(rayHit.collider.gameObject.GetComponent<Queen>().pieceAttackable){
										selectedCoord = new Vector3(rayHit.collider.transform.position.x, 
																	heldPiece.transform.position.y
										                            ,rayHit.collider.transform.position.z);
										myGameManager.MovePiece(selectedCoord);
										heldPiece.GetComponent<Piece>().row = rayHit.collider.GetComponent<Piece>().row;
										heldPiece.GetComponent<Piece>().column = rayHit.collider.GetComponent<Piece>().column;
										heldPiece.GetComponent<Piece>().updateAfterAttack();
										//Debug.Log ("I got Here");
										myGameManager.changeState(0);
                                        //rayHit.collider.gameObject.SetActive(false);
                                        SendAttackDataToServer("CATK|", heldPiece.GetComponent<Piece>().pieceIndex, rayHit.collider.GetComponent<Piece>().pieceIndex, 1);
										isAttacking = false;
										myGameManager.GetComponent<ChessBoard>().AllUnavailable();
									}
								}
								//check if attacked is king
								if(rayHit.transform.name.Contains("King")){
									rayHit.collider.gameObject.GetComponent<King>().checkAttackable();
									if(rayHit.collider.gameObject.GetComponent<King>().pieceAttackable){
										selectedCoord = new Vector3(rayHit.collider.transform.position.x, 
																	heldPiece.transform.position.y
										                            ,rayHit.collider.transform.position.z);
										myGameManager.MovePiece(selectedCoord);
										heldPiece.GetComponent<Piece>().row = rayHit.collider.GetComponent<Piece>().row;
										heldPiece.GetComponent<Piece>().column = rayHit.collider.GetComponent<Piece>().column;
										heldPiece.GetComponent<Piece>().updateAfterAttack();
										
										//Debug.Log ("I got Here");
										myGameManager.changeState(0);
                                        //rayHit.collider.gameObject.SetActive(false);
                                        SendAttackDataToServer("CATK|", heldPiece.GetComponent<Piece>().pieceIndex, rayHit.collider.GetComponent<Piece>().pieceIndex,1);
										isAttacking = false;
										myGameManager.GetComponent<ChessBoard>().AllUnavailable();
									}
								}
								//check if attacked is rook
								if(rayHit.transform.name.Contains("Rook")){
									rayHit.collider.gameObject.GetComponent<Rook>().checkAttackable();
									if(rayHit.collider.gameObject.GetComponent<Rook>().pieceAttackable){
										selectedCoord = new Vector3(rayHit.collider.transform.position.x, 
																		heldPiece.transform.position.y
										                            ,rayHit.collider.transform.position.z);
										myGameManager.MovePiece(selectedCoord);
										heldPiece.GetComponent<Piece>().row = rayHit.collider.GetComponent<Piece>().row;
										heldPiece.GetComponent<Piece>().column = rayHit.collider.GetComponent<Piece>().column;
										heldPiece.GetComponent<Piece>().updateAfterAttack();
										
										//Debug.Log ("I got Here");
										myGameManager.changeState(0);
                                        //rayHit.collider.gameObject.SetActive(false);
                                        SendAttackDataToServer("CATK|", heldPiece.GetComponent<Piece>().pieceIndex, rayHit.collider.GetComponent<Piece>().pieceIndex, 1);
										isAttacking = false;
										myGameManager.GetComponent<ChessBoard>().AllUnavailable();
									}
								}
								//check if attakced is knight
								if(rayHit.transform.name.Contains("Knight")){
									rayHit.collider.gameObject.GetComponent<Knight>().checkAttackable();
									if(rayHit.collider.gameObject.GetComponent<Knight>().pieceAttackable){
										selectedCoord = new Vector3(rayHit.collider.transform.position.x, 
																	heldPiece.transform.position.y
										                            ,rayHit.collider.transform.position.z);
										myGameManager.MovePiece(selectedCoord);
										heldPiece.GetComponent<Piece>().row = rayHit.collider.GetComponent<Piece>().row;
										heldPiece.GetComponent<Piece>().column = rayHit.collider.GetComponent<Piece>().column;
										heldPiece.GetComponent<Piece>().updateAfterAttack();
										
										//Debug.Log ("I got Here");
										myGameManager.changeState(0);
                                        //rayHit.collider.gameObject.SetActive(false);
                                        SendAttackDataToServer("CATK|", heldPiece.GetComponent<Piece>().pieceIndex, rayHit.collider.GetComponent<Piece>().pieceIndex, 1);
										isAttacking = false;
										myGameManager.GetComponent<ChessBoard>().AllUnavailable();
									}
								}
								//check if attacked is bishop
								if(rayHit.transform.name.Contains("Bishop")){
									rayHit.collider.gameObject.GetComponent<Bishop>().checkAttackable();
									if(rayHit.collider.gameObject.GetComponent<Bishop>().pieceAttackable){
										selectedCoord = new Vector3(rayHit.collider.transform.position.x, 
																	heldPiece.transform.position.y
										                            ,rayHit.collider.transform.position.z);
										myGameManager.MovePiece(selectedCoord);
										heldPiece.GetComponent<Piece>().row = rayHit.collider.GetComponent<Piece>().row;
										heldPiece.GetComponent<Piece>().column = rayHit.collider.GetComponent<Piece>().column;
										heldPiece.GetComponent<Piece>().updateAfterAttack();
										
										//Debug.Log ("I got Here");
										myGameManager.changeState(0);
                                        //rayHit.collider.gameObject.SetActive(false);
                                        SendAttackDataToServer("CATK|", heldPiece.GetComponent<Piece>().pieceIndex, rayHit.collider.GetComponent<Piece>().pieceIndex, 1);
										isAttacking = false;
										myGameManager.GetComponent<ChessBoard>().AllUnavailable();

									}
								}
							}
						}
						if(rayHit.collider.gameObject.tag == "Cell"/* and check if it is a piece of player 2*/){
							globalRayHit = rayHit;
							//Debug.Log (	rayHit.collider.GetComponent<Tile>().isOccupied);
							
							//Debug.Log ("Current Row = " + cellRow + "Current Column = "+cellColumn);
							if(rayHit.collider.gameObject.GetComponent<Tile>().isAvailable)
							{
								heldPiece.transform.GetComponent<Piece>().UnhighLight();

								selectedCoord = new Vector3(rayHit.collider.transform.position.x, 
															heldPiece.transform.position.y
								                            ,rayHit.collider.transform.position.z);
								/*Debug.Log ("X ="+rayHit.collider.transform.position.x + "and Y ="+
									           rayHit.collider.transform.position.y);*/
								myGameManager.MovePiece(selectedCoord);
								myGameManager.changeState(0);
								//queen placement
								if(heldPiece.transform.name.Contains("Queen"))
								{
									heldPiece.transform.GetComponent<Queen>().unOccupyTile();
									heldPiece.transform.GetComponent<Queen>().queenRow = 
										int.Parse(rayHit.collider.gameObject.transform.parent.name.ToString());
									heldPiece.transform.GetComponent<Queen>().queenColum = 
										int.Parse(rayHit.collider.gameObject.name.ToString());
                                    heldPiece.transform.GetComponent<Queen>().OccupyTile();
                                    SendMovementDataToServer("CMOV|", heldPiece.GetComponent<Piece>().pieceIndex, selectedCoord.x, selectedCoord.y, selectedCoord.z,
                                     heldPiece.transform.GetComponent<Queen>().queenRow, heldPiece.transform.GetComponent<Queen>().queenColum,1);
									myGameManager.GetComponent<ChessBoard>().AllUnavailable();

									
								}
								//bishop placement
								if(heldPiece.transform.name.Contains("Bishop"))
								{
									heldPiece.transform.GetComponent<Bishop>().unOccupyTile();
									heldPiece.transform.GetComponent<Bishop>().bishopRow = 
										int.Parse(rayHit.collider.gameObject.transform.parent.name.ToString());
									heldPiece.transform.GetComponent<Bishop>().bishopColum = 
										int.Parse(rayHit.collider.gameObject.name.ToString());
                                    heldPiece.transform.GetComponent<Bishop>().OccupyTile();
                                    SendMovementDataToServer("CMOV|", heldPiece.GetComponent<Piece>().pieceIndex, selectedCoord.x, selectedCoord.y, selectedCoord.z,
                                     heldPiece.transform.GetComponent<Bishop>().bishopRow, heldPiece.transform.GetComponent<Bishop>().bishopColum,1);
									myGameManager.GetComponent<ChessBoard>().AllUnavailable();

									
								}
								//knight placement
								if(heldPiece.transform.name.Contains("Knight"))
								{
									heldPiece.transform.GetComponent<Knight>().unOccupyTile();
									heldPiece.transform.GetComponent<Knight>().knightRow = 
										int.Parse(rayHit.collider.gameObject.transform.parent.name.ToString());
									heldPiece.transform.GetComponent<Knight>().knightColum = 
										int.Parse(rayHit.collider.gameObject.name.ToString());
                                    heldPiece.transform.GetComponent<Knight>().OccupyTile();
                                    SendMovementDataToServer("CMOV|", heldPiece.GetComponent<Piece>().pieceIndex, selectedCoord.x, selectedCoord.y, selectedCoord.z,
                                     heldPiece.transform.GetComponent<Knight>().knightRow, heldPiece.transform.GetComponent<Knight>().knightColum,1);
									myGameManager.GetComponent<ChessBoard>().AllUnavailable();

								}
								//rook placement
								if(heldPiece.transform.name.Contains("Rook"))
								{
									heldPiece.transform.GetComponent<Rook>().unOccupyTile();
									heldPiece.transform.GetComponent<Rook>().rookRow = 
										int.Parse(rayHit.collider.gameObject.transform.parent.name.ToString());
									heldPiece.transform.GetComponent<Rook>().rookColum = 
										int.Parse(rayHit.collider.gameObject.name.ToString());
                                    heldPiece.transform.GetComponent<Rook>().OccupyTile();
                                    SendMovementDataToServer("CMOV|", heldPiece.GetComponent<Piece>().pieceIndex, selectedCoord.x, selectedCoord.y, selectedCoord.z,
                                     heldPiece.transform.GetComponent<Rook>().rookRow, heldPiece.transform.GetComponent<Rook>().rookColum,1);
									myGameManager.GetComponent<ChessBoard>().AllUnavailable();

									
								}
								//pawn placement
								if(heldPiece.transform.name.Contains("Pawn"))
								{
									heldPiece.transform.GetComponent<Pawn>().unOccupyTile();
									heldPiece.transform.GetComponent<Pawn>().pawnRow = 
										int.Parse(rayHit.collider.gameObject.transform.parent.name.ToString());
									heldPiece.transform.GetComponent<Pawn>().pawnColum = 
										int.Parse(rayHit.collider.gameObject.name.ToString());
									heldPiece.transform.GetComponent<Pawn>().OccupyTile();
									heldPiece.transform.GetComponent<Pawn>().firstMove = false;
									if(heldPiece.GetComponent<Pawn>().pawnRow == 1){
										//Debug.Log ("Promotion");
										player1PromotionCanvas.SetActive(true);

									}

                                    SendMovementDataToServer("CMOV|", heldPiece.GetComponent<Piece>().pieceIndex, selectedCoord.x, selectedCoord.y, selectedCoord.z,
                                     heldPiece.transform.GetComponent<Pawn>().pawnRow, heldPiece.transform.GetComponent<Pawn>().pawnColum,1);
									myGameManager.GetComponent<ChessBoard>().AllUnavailable();

								}
								//king placement
								if(heldPiece.transform.name.Contains("King"))
								{

									if(!rayHit.collider.gameObject.GetComponent<Tile>().readyForCastling)
									{
										heldPiece.transform.GetComponent<King>().unOccupyTile();
										heldPiece.transform.GetComponent<King>().kingRow = 
										int.Parse(rayHit.collider.gameObject.transform.parent.name.ToString());
										heldPiece.transform.GetComponent<King>().kingColum = 
										int.Parse(rayHit.collider.gameObject.name.ToString());
										heldPiece.transform.GetComponent<King>().OccupyTile();
										SendMovementDataToServer("CMOV|", heldPiece.GetComponent<Piece>().pieceIndex, selectedCoord.x, selectedCoord.y, selectedCoord.z,
										heldPiece.transform.GetComponent<King>().kingRow, heldPiece.transform.GetComponent<King>().kingColum,1);
										myGameManager.GetComponent<ChessBoard>().AllUnavailable();

									}

									else if(rayHit.collider.gameObject.GetComponent<Tile>().readyForCastling) 
									{
										int tileRow = int.Parse(rayHit.collider.gameObject.transform.parent.name.ToString());
										int tileColum = int.Parse(rayHit.collider.gameObject.name.ToString());
										string team = heldPiece.transform.GetComponent<King>().team ;
										ChessBoard board = myGameManager.GetComponent<ChessBoard>();


										heldPiece.transform.GetComponent<King>().unOccupyTile();
										heldPiece.transform.GetComponent<King>().kingRow = tileRow;
										heldPiece.transform.GetComponent<King>().kingColum = tileColum;
										heldPiece.transform.GetComponent<King>().OccupyTile();
	


										if(team == "PiecePlayer2" && tileRow == 1 && tileColum == 6 )
										{
											Rook castlingRook = board.getWantedRook(team ,1,8);
											castlingRook.unOccupyTile();
											castlingRook.rookRow = 1;
											castlingRook.rookColum = 5;
											castlingRook.OccupyTile();
											Transform tileTransform = board.rows[0].tiles[4].GetComponent<Transform>();
											Vector3 selectedCoordR = new Vector3(tileTransform.position.x,castlingRook.transform.position.y,castlingRook.transform.position.z);


											SendTabyeetDataToServer("CTAB|", heldPiece.GetComponent<Piece>().pieceIndex, selectedCoord.x, selectedCoord.y, selectedCoord.z,
												heldPiece.transform.GetComponent<King>().kingRow, heldPiece.transform.GetComponent<King>().kingColum,
												castlingRook.transform.GetComponent<Piece>().pieceIndex, selectedCoordR.x, selectedCoordR.y, selectedCoordR.z,
												castlingRook.transform.GetComponent<Rook>().rookRow,castlingRook.transform.GetComponent<Rook>().rookColum,1);
											
										}
										else if(team == "PiecePlayer2" && tileRow == 1 && tileColum == 2)
										{
											Rook castlingRook = board.getWantedRook(team ,1,1);

											castlingRook.unOccupyTile();
											castlingRook.rookRow = 1;
											castlingRook.rookColum = 3;
											castlingRook.OccupyTile();
											Transform tileTransform = board.rows[0].tiles[2].GetComponent<Transform>();
											Vector3	selectedCoordR = new Vector3(tileTransform.position.x,castlingRook.transform.position.y,castlingRook.transform.position.z);

											SendTabyeetDataToServer("CTAB|", heldPiece.GetComponent<Piece>().pieceIndex, selectedCoord.x, selectedCoord.y, selectedCoord.z,
											heldPiece.transform.GetComponent<King>().kingRow, heldPiece.transform.GetComponent<King>().kingColum,
											castlingRook.transform.GetComponent<Piece>().pieceIndex, selectedCoordR.x, selectedCoordR.y, selectedCoordR.z,
											castlingRook.transform.GetComponent<Rook>().rookRow,castlingRook.transform.GetComponent<Rook>().rookColum,1);

										}
										myGameManager.GetComponent<ChessBoard>().AllUnavailable();

									}

								}
								//Debug.Log ("Queen New Row" + heldPiece.transform.GetComponent<Queen>().queenRow + "Queen New Colum" + 
								//      heldPiece.transform.GetComponent<Queen>().queenColum);
								//Debug.Log (isAttacking);
							}else{
								//Debug.Log ("Tile is not Available");
								myGameManager.GetComponent<ChessBoard>().AllUnavailable();
							}

                           // playerTurn = 1;
                            //SendTurnDataToServer("CTRC|", 1);
						}
					}
                }
			}
			//end of if player2 statement
		}
	}

    public void SendMovementDataToServer(string cmnd, int pieceIndex, float newPositionX, float newPositionY,float newPositionZ, int newRow, int newColum, int turn)
    {
        string msg = cmnd;
        msg += pieceIndex.ToString() + "|";
        msg += newPositionX.ToString() + "|";
        msg += newPositionY.ToString() + "|";
        msg += newPositionZ.ToString() + "|";
        msg += newRow.ToString() + "|";
        msg += newColum.ToString() + "|";
        msg += turn.ToString();

        client.UNETSend(msg);
        client.ClientChess(msg);
    }

    public void SendTabyeetDataToServer(string cmnd, int piece1Index,float x1, float y1, float z1, int row1, int column1, int piece2Index, float x2, float y2, float z2, int row2, int column2, int turn)
    {
        string msg = cmnd;
        msg += piece1Index + "|";
        msg += x1 + "|";
        msg += y1 + "|";
        msg += z1 + "|";
        msg += row1 + "|";
        msg += column1 + "|";
        msg += piece2Index + "|";
        msg += x2 + "|";
        msg += y2 + "|";
        msg += z2 + "|";
        msg += row2 + "|";
        msg += column2 + "|";
        msg += turn;

        client.UNETSend(msg);
        client.ClientChess(msg);
    }
    public void SendAttackDataToServer(string cmnd, int attackingPieceIndex, int attackedPieceIndex, int turn)
    {
        string msg = cmnd;
        msg += attackingPieceIndex.ToString() + "|";
        msg += attackedPieceIndex.ToString() + "|";
        msg += turn.ToString();

        client.UNETSend(msg);
        client.ClientChess(msg);

    }

    public void SendPromotionToServer(string cmnd, int playerNum, int pawnIndex, int pieceTypeToPromoteTo)
    {
        string msg = cmnd;
        msg += playerNum + "|";
        msg += pawnIndex + "|";
        msg += pieceTypeToPromoteTo;
        Debug.Log("Sending Promotion to server" + msg.ToString());
        client.UNETSend(msg); 
        //client.ClientChess(msg);

    }

    public void ChatMessage(string msg)
    {
        string messageToShow = "";
        GameObject go = Instantiate(messagePrefab) as GameObject;
        string[] chatmsg = msg.Split(':');
        //Debug.Log(chatmsg[0]+chatmsg[1]);
        //messageToShow = "<color=red>" + chatmsg[0] + "</color>" + chatmsg[1];
        //Debug.Log(UiMenuValues.instance.player1NameLabelWR.text);
        if (chatmsg[0].Equals(m_UNETChess.instance.currentPlayerName))
            messageToShow = "<color=blue><b>" + chatmsg[0] + "</b></color>" + " : " + ArabicFixer.Fix(chatmsg[1], true, true);
        else if (chatmsg[0].Equals(m_UNETChess.instance.secondPlayerName))
            messageToShow = "<color=red><b>" + chatmsg[0] + "</b></color>" + " : " + ArabicFixer.Fix(chatmsg[1], true, true);
        go.transform.SetParent(chatMessageContainer);
        go.transform.localScale = new Vector3(1f, 1.2f, 1f);
        go.GetComponentInChildren<Text>().text = messageToShow;
         
    }

    public void SendChatMessage()
    {
        InputField i = GameObject.Find("MessageInput").GetComponent<InputField>();
        if (i.text == "")
            return;


        client.UNETSend("CMSG|" + m_UNETGameManager.instance.currentPlayerName + ":" + i.text);
        client.ClientChess("CMSG|" + m_UNETGameManager.instance.currentPlayerName + ":" + i.text);
         
        i.text = "";
    }
        //public void SendTurnDataToServer(string cmnd, int turn)
        //{
        //    string msg = cmnd;
        //    msg += turn.ToString();
        //    //Debug.Log("Turn Changed " + msg);
        //    client.Send(msg);
        //}
    public void SendSurrender()
    {
        int playerNum = 0;
        if (client.isHost) playerNum = 1;
        else if (!client.isHost) playerNum = 2;

        string msg = "SRNDR|" + playerNum;
        client.UNETSend(msg);
    }

	public void stopPlaying()
	{
		  stopPlay = true;
//        client.UNETSend("CSTOP|");
//        client.ClientChess("CSTOP|");
	}
}
