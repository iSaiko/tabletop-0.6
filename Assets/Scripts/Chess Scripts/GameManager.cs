﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	public GameObject selectedObject;
	public int GameState = 0;
	public GameObject SelectedPiece;
	public ChessBoard  myChessBoard;
    public bool pieceIsMoving;
	//variables for smooth movement
	private float moveSpeed = 100f;
	private float t;
	private float gridSize = 40f;

	public void SelectPiece(GameObject PieceToSelect){
		SelectedPiece = PieceToSelect;
		//Debug.Log (SelectedPiece);
	}

	public void MovePiece(Vector3 coordToMove){
		StartCoroutine (smoothMove (SelectedPiece.transform.position, coordToMove));
		//SelectedPiece.transform.position = coordToMove;
		//SelectedPiece = null;
	}

	// overload for moving castling rook
	public void MovePiece(Rook castlingRook,Vector3 coordToMove){
		StartCoroutine (smoothMove (castlingRook.transform,castlingRook.transform.position, coordToMove));
	}

	public void changeState(int newState){
		GameState = newState;
		//Debug.Log ("Game state is" + newState);
	}
	// Use this for initialization
	void Start () {
        pieceIsMoving = false;
	}

	public IEnumerator smoothMove(Vector3 startPosition, Vector3 endPosition)
	{
		SoundsManager.instance.playChessMovementSound();
		t = 0;
        pieceIsMoving = true;
		while (t < 1f) {
			t += Time.deltaTime * (moveSpeed / gridSize) * 1;
			
			SelectedPiece.transform.position = Vector3.Lerp (startPosition, endPosition, t);
			if(SelectedPiece.transform.position == endPosition)
				SelectedPiece = null;
			//Debug.Log (t);

			yield return null;
		}
		myChessBoard.cleanBoard();
		myChessBoard.checkIfPieceThreatKing();
        pieceIsMoving = false;
		yield return 0;

	}


	// overload for moving castling rook
	public IEnumerator smoothMove(Transform castlingRookTransform ,Vector3 startPosition, Vector3 endPosition)
	{
		t = 0;
		while (t < 1f) {
			t += Time.deltaTime * (moveSpeed / gridSize) * 1;
			castlingRookTransform.position = Vector3.Lerp (startPosition, endPosition, t);
			yield return null;
		}
		yield return 0;

	}
	
	void Update()
	{
		/*Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit = new RaycastHit();
		if(Input.GetMouseButton(0)) {
			if(Physics.Raycast(ray, out hit)) {
				print(hit.transform.name);
				selectedObject = hit.transform.gameObject;

			}
		}*/

	}

}
