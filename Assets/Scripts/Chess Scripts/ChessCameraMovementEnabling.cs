﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChessCameraMovementEnabling : MonoBehaviour {


	public Image CameraToggleButtonImage;
	public Sprite camera3DSprite;
	public Sprite camera2DSprite;
	public bool isPlayer1Canvas;


	public static bool isFreeMovement;


	public bool isCamera3D;

	void Start () {
		isCamera3D = true ;
		isFreeMovement = true;
	}
	
	void Update () {
		
	}




	public void toggleCamera()
	{
		//change camera view from 3d to 2d 
		if(isCamera3D)
		{
			isCamera3D = false ;
			CameraToggleButtonImage.sprite = camera2DSprite ;
			isFreeMovement = false ;
			if(isPlayer1Canvas)
			{
				CameraPerspective.instance.player1CameraTop();
			}
			else
			{
				CameraPerspective.instance.player2CameraTop();
			}

		}
		//change camera view from 3d to 2d 
		else
		{
			isCamera3D = true ;
			CameraToggleButtonImage.sprite = camera3DSprite ;
			isFreeMovement = true;
			if(isPlayer1Canvas)
			{
				CameraPerspective.instance.player1CameraNormal();
			}
			else
			{
				CameraPerspective.instance.player2CameraNormal();
			}

		}

	}


	public void toggleChatBox()
	{



	}



}
