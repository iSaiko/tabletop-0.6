﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour {



	public Material availableMaterial ;
	public Material dangerMaterial;
//	private Material oldMaterial ;
	public bool isAvailable ;
	public bool isOccupied = false;
	public bool isAttackable = false ;
	public string occupyingTeam = "";
	public bool holdingKing = false ;
	public bool readyForCastling ;


	private Renderer myRenderer ;
	private Color intialColor;


	void Start () {
	
		myRenderer = GetComponent<Renderer> ();
//		oldMaterial = myRenderer.material;
//		intialColor = myRenderer.material.color;
		readyForCastling = false ;
	}

	public void MakeAvailable () {
		myRenderer.enabled = true;
		myRenderer.material = availableMaterial;
		isAvailable = true;
		//		myRenderer.material.color = Color.green;

	}
		
	public void MakeUnAvailable () {
//		myRenderer.material = oldMaterial;
		myRenderer.enabled = false;
		isAvailable = false;
		//myRenderer.material.color  = intialColor;

	}

	public void makeDanger()
	{
//		myRenderer.material.color = Color.red;
	}
	public void showThreatPath()
	{
		myRenderer.enabled = true;
		myRenderer.material = dangerMaterial;
//		myRenderer.material.color = Color.red;
	}

	public void showIntersectionTile()
	{
//			myRenderer.material.color = Color.blue;
	}

	public void cleanTile()
	{
//		myRenderer.material.color = intialColor;
		myRenderer.enabled = false;
	}







	
}
