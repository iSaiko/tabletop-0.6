﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChessCameraPanel : MonoBehaviour {



	private bool isPanelOpened ;
	private bool isAnimationDone  = true;
	public Animator chessCameraPanelAnimator ;

	public void toggleChessCameraPanel()
	{
		if(!isAnimationDone)
			return;

		isAnimationDone  = false ;
		chessCameraPanelAnimator.SetTrigger( (!isPanelOpened) ? "openChessCameraPanel" : "closeChessCameraPanel");
		isPanelOpened = !isPanelOpened ;
	}


	public void toggleChessSettingPanel()
	{
		
		chessCameraPanelAnimator.SetTrigger( (!isPanelOpened) ? "openChessSettingPanel" : "closeChessSettingPanel");
		isPanelOpened = !isPanelOpened ;

	}


	public void lockStateTillAnimationDone()
	{
		isAnimationDone  = true;
	}
		





}
