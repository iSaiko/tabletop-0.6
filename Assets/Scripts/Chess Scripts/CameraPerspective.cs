﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CameraPerspective : MonoBehaviour {
	// initialize 3 positions vectors for the camera
	private Vector3 player1TopCoord;
	private Quaternion player1TopRot;
	private Vector3 player1SideCoord;
	private Quaternion player1SideRot;
	private Vector3 player1NormalCoord;
	private Quaternion player1NormalRot;

	private Vector3 player2TopCoord;
	private Quaternion player2TopRot;
	private Vector3 player2SideCoord;
	private Quaternion player2SideRot;
	private Vector3 player2NormalCoord;
	private Quaternion player2NormalRot;

	private GameObject player1Camera;
	private GameObject player2Camera;

	public bool persClickable;
	
	private float moveSpeed = 45f;
	private float t;
	private float gridSize = 40f;



	public Button cameraPerspectiveButton1;
	public Button cameraPerspectiveButton2;

	public static CameraPerspective instance;

	void Start(){

		instance = this;

		player1Camera = GameObject.Find("Player1Camera");
		player2Camera = GameObject.Find ("Player2Camera");

		player1TopCoord = new Vector3 (-25f, 55f, -24f);
		//player1SideCoord = new Vector3 (58f, 57f, -36f);
		player1NormalCoord = new Vector3 (-26f, 47f, -70.8f);
		
		player1TopRot = Quaternion.Euler(82f, 0f, 0f);
		//player1SideRot = Quaternion.Euler (44f, -90f, 0f);
		player1NormalRot = Quaternion.Euler (50f, 0f, 0f);

		player2TopCoord = new Vector3 (-25f, 55f, -7f);
		//player2SideCoord = new Vector3 (-55.9f, 47.5f, -39.2f);
		player2NormalCoord = new Vector3 (-24f, 47f, 41.4f);
		
		player2TopRot = Quaternion.Euler(85f, 180f, 0f);
		//player2SideRot = Quaternion.Euler (48.1f, 86.1f, -4.24f);
		player2NormalRot = Quaternion.Euler (50f, 180f, 0f);

		persClickable = true;

	}

	public void player1CameraTop(){
		//change position
		if(persClickable)
			StartCoroutine (camera1SmoothMove (player1TopRot, player1TopCoord));
	}
    //public void player1CameraSide(){
    //    //change position
    //    if(persClickable)
    //        StartCoroutine (camera1SmoothMove (player1SideRot, player1SideCoord));


    //}

	public void player1CameraNormal(){
		//change position
		if(persClickable)
			StartCoroutine (camera1SmoothMove (player1NormalRot, player1NormalCoord));
	}

	public void player2CameraTop(){
		//change position

		if(persClickable)
			StartCoroutine (camera2SmoothMove (player2TopRot, player2TopCoord));

	}
    //public void player2CameraSide(){
    //    //change position

    //    if(persClickable)
    //        StartCoroutine (camera2SmoothMove (player2SideRot, player2SideCoord));

		
		
    //}
	
	public void player2CameraNormal(){
		//change position

		if(persClickable)
			StartCoroutine (camera2SmoothMove (player2NormalRot, player2NormalCoord));

		
		
	}

	public IEnumerator camera1SmoothMove(Quaternion endRotation, Vector3 endPosition)
	{
		cameraPerspectiveButton1.interactable = false;
		t = 0;
		while (t < 1f) {
			t += Time.deltaTime * (moveSpeed / gridSize) * 1;
			
			player1Camera.transform.localPosition = Vector3.Lerp (player1Camera.transform.localPosition, endPosition, t);
			player1Camera.transform.localRotation = Quaternion.Slerp(player1Camera.transform.localRotation, endRotation, t);
			//Debug.Log (t);
			persClickable = false;
			yield return null;
		}
		persClickable = true;
		cameraPerspectiveButton1.interactable = true;

		yield return 0;
		
	}

	public IEnumerator camera2SmoothMove(Quaternion endRotation, Vector3 endPosition)
	{
		cameraPerspectiveButton2.interactable = false;

		t = 0;
		while (t < 1f) {
			t += Time.deltaTime * (moveSpeed / gridSize) * 1;
			
			player2Camera.transform.localPosition = Vector3.Lerp (player2Camera.transform.localPosition, endPosition, t);
			//Debug.Log (player2Camera.transform.position);
			player2Camera.transform.localRotation = Quaternion.Slerp(player2Camera.transform.localRotation, endRotation, t);
			//Debug.Log (t);
			persClickable = false;
			yield return null;
		}
		persClickable = true;
		cameraPerspectiveButton2.interactable = true;
		yield return 0;
		
	}
}
