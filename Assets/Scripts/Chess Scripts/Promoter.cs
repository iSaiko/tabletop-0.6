﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Promoter : MonoBehaviour {
	public GameObject[] queensArr;
	public GameObject[] rooksArr;
	public GameObject[] knightsArr;
	public GameObject[] bishopsArr;

	public GameObject queen;
	public GameObject rook;
	public GameObject knight;
	public GameObject bishop;

   public List<GameObject> promotionPieces = new List<GameObject>();


	// Use this for initialization
	void Start () {
		queensArr = new GameObject[9];
		for(int i=0; i<=8 ; i++)
		{
			GameObject go = 
				Instantiate(queen, new Vector3(20f, 20f, -20f), Quaternion.identity) as GameObject;
			go.transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
            go.GetComponent<Piece>().pieceIndex = i+32;
            go.transform.parent = this.transform; 
            promotionPieces.Add(go);
			queensArr[i] = go;
			queensArr[i].SetActive(false);
		}
		rooksArr = new GameObject[9];
		for(int i=0; i<=8 ; i++)
		{
			GameObject go = 
				Instantiate(rook, new Vector3(20f, 20f, -20f), Quaternion.identity) as GameObject;
			go.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
            go.GetComponent<Piece>().pieceIndex = i + 9+32;
			go.transform.parent = this.transform;
            promotionPieces.Add(go);
			rooksArr[i] = go;
			rooksArr[i].SetActive(false);
		}
		knightsArr = new GameObject[9];
		for(int i=0; i<=8 ; i++)
		{
			GameObject go = 
				Instantiate(knight, new Vector3(20f, 20f, -20f), Quaternion.identity) as GameObject;
			go.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
			go.transform.eulerAngles = new Vector3(0f, 270f, 0f);
            go.GetComponent<Piece>().pieceIndex = i + 18+32;
			go.transform.parent = this.transform;
            promotionPieces.Add(go);
			knightsArr[i] = go;
			knightsArr[i].SetActive(false);
		}
		bishopsArr = new GameObject[9];
		for(int i=0; i<=8 ; i++)
		{GameObject go = 
			Instantiate(bishop, new Vector3(20f, 20f, -20f), Quaternion.identity) as GameObject;
			go.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
            go.GetComponent<Piece>().pieceIndex = i + 27+32;
			go.transform.parent = this.transform;
            promotionPieces.Add(go);
			bishopsArr[i] = go;
			bishopsArr[i].SetActive(false);
		}
	}

	public GameObject grabQueen(int queenIndex){

			return queensArr[queenIndex];
	}
	public GameObject grabKnight(int knightIndex){

			return knightsArr[knightIndex];

	}
	public GameObject grabRook(int rookIndex){

			return rooksArr[rookIndex];
	}
	public GameObject grabBishop(int bishopIndex){

			return bishopsArr[bishopIndex];
	}

	// Update is called once per frame
	void Update () {
	
	}
}
