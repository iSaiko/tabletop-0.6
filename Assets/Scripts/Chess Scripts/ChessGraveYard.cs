﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChessGraveYard : MonoBehaviour {

	public bool isPlayer1Grave;

	public static Transform player1GraveAnchor;
	public static Transform player2GraveAnchor;

	public Transform graveTransform;

	void Start () {

		if(isPlayer1Grave)
		{
			player1GraveAnchor = graveTransform ;
		}
		else
		{
			player2GraveAnchor = graveTransform;
		}
	}

}
