using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GooglePlayGames;

public class ChessBoard : MonoBehaviour {

	public Row[] rows ;
	public List<GameObject> team1;
	public List<GameObject> team2;
	
	public int[,] aroundKing1 = new int[3,8];
	public int[,] aroundKing2 = new int[3,8];
	public int dangerTilesAroundKing1;
	public int TilesAroundKing1;
	public int dangerTilesAroundKing2;
	public int TilesAroundKing2;


	private int [,] king1CastlingTiles = {{7,7,7,7,7},{1,2,4,5,6},{1,1,1,1,1}};
	private int [,] king2CastlingTiles = {{0,0,0,0,0},{1,2,4,5,6},{1,1,1,1,1}};


	public List<GameObject> threatToTeam1 = new List<GameObject>(); 
	public List<GameObject> threatToTeam2 = new List<GameObject>();
	public List<int[,]> threatingPathsToTeam2 = new List<int[,]>();
	public List<int[,]> threatingPathsToTeam1 = new List<int[,]>();
	public List<int> intersectionRow = new List<int> ();
	public List<int> intersectionColum = new List<int> ();


	public GameObject chessEndGameCanvas;

	public static ChessBoard instance;
//	public ThreatingPieces threatingPieces ;
	

	GameManager myGameManager;


	void Start () {
		instance = this;
		//myGameManager.GetComponent<PlayerControl>().isAttacking = true;
		myGameManager = gameObject.GetComponent<GameManager> ();
		dangerTilesAroundKing1 = 0 ;
		TilesAroundKing1 = 0 ;
		dangerTilesAroundKing2 = 0 ;
		TilesAroundKing2 = 0 ;
		assignAroundKing();

//		threatingPieces = new ThreatingPieces();
//		threatingPieces.threatToTeam1;
//		InvokeRepeating("checkMate",3,3);



	}
	

	public void AllUnavailable()
	{
		for (int i = 0; i < rows.Length; i++) {
			
			for (int  k = 0; k < rows[i].tiles.Length; k++) {
				
				rows[i].tiles[k].MakeUnAvailable();
				rows[i].tiles[k].readyForCastling = false;
				
			}
			
		}
		
	}


	public void AllUnattackable()
	{
		for (int i = 0; i < rows.Length; i++) {
			
			for (int  k = 0; k < rows[i].tiles.Length; k++) {
				
				rows[i].tiles[k].isAttackable = false ;
			}
			
		}
		
	}

	public void QueenRegion(int currentRow , int currentColum , string team){
		currentRow--;
		currentColum--;
		string enemyTeam ;
		int sum = currentRow + currentColum;
		if(team == "PiecePlayer1"){
			enemyTeam  =  "PiecePlayer2";
		}
		else {
			
			enemyTeam  =  "PiecePlayer1";
		}
		
		
		for (int i = currentRow +1; i < rows.Length; i++) {
			
			for (int  k = currentColum+1; k < rows[i].tiles.Length; k++) {
				
				if(currentRow - i == currentColum - k){	
					if(rows[i].tiles[k].isOccupied)
					{
						if(rows[i].tiles[k].occupyingTeam == enemyTeam){
							rows[i].tiles[k].MakeAvailable();
							rows[i].tiles[k].isAttackable = true;

							myGameManager.GetComponent<PlayerControl>().isAttacking = true;}
						
						goto j1;
					}
					
					rows[i].tiles[k].MakeAvailable();
				}
			}
		}
	j1:
			
		for (int i = currentRow+ 1; i < rows.Length; i++) {
			
			for (int  k = currentColum -1 ; k >= 0; k--) {
				
				if(i==sum-k){
					if(rows[i].tiles[k].isOccupied){
						
						if(rows[i].tiles[k].occupyingTeam == enemyTeam){
							rows[i].tiles[k].MakeAvailable();
							myGameManager.GetComponent<PlayerControl>().isAttacking = true;
							rows[i].tiles[k].isAttackable = true;
						}
						
						goto j2;
						
					}
					
					rows[i].tiles[k].MakeAvailable(); 
				}
				
			}
			
		}
		
		
	j2:
		for (int i = currentRow -1 ; i >= 0; i--) {
			
			for (int  k = currentColum +1; k < rows[i].tiles.Length; k++){
				
				if(i==sum-k){
					if(rows[i].tiles[k].isOccupied){
						if(rows[i].tiles[k].occupyingTeam == enemyTeam){
							rows[i].tiles[k].MakeAvailable();
							myGameManager.GetComponent<PlayerControl>().isAttacking = true;
							rows[i].tiles[k].isAttackable = true;

						}

						goto j3;
					}
					
					rows[i].tiles[k].MakeAvailable();
				}
				
			}
			
		}
		
		
	j3:
			
		for (int i = currentRow-1; i >= 0; i--) {

			for (int  k = currentColum-1; k >= 0; k--){
				
				if(currentRow - i ==currentColum - k){
					
					if(rows[i].tiles[k].isOccupied){
						if(rows[i].tiles[k].occupyingTeam == enemyTeam){
							rows[i].tiles[k].MakeAvailable();
							myGameManager.GetComponent<PlayerControl>().isAttacking = true;
							rows[i].tiles[k].isAttackable = true;
						}
						goto j4;
					}
					rows[i].tiles[k].MakeAvailable();
				}
				
				
			}
			
			
		}
		
		
		
	j4:
			
			
			
			
		for (int  k = currentColum +1; k < rows.Length ; k++) {
			if( rows[currentRow].tiles[k].isOccupied== true)
			{
				if(rows[currentRow].tiles[k].occupyingTeam ==  enemyTeam){ 
					rows[currentRow].tiles[k].MakeAvailable();
					myGameManager.GetComponent<PlayerControl>().isAttacking = true;
					rows[currentRow].tiles[k].isAttackable = true ;

				}
				goto r2;
			}
			rows[currentRow].tiles[k].MakeAvailable();
			
		}
		
	r2:
			
			
		for (int i = currentRow -1; i >= 0  ; i--) {
			
			if( rows[i].tiles[currentColum].isOccupied== true)
			{
				if(rows[i].tiles[currentColum].occupyingTeam ==  enemyTeam){
					rows[i].tiles[currentColum].MakeAvailable();
					myGameManager.GetComponent<PlayerControl>().isAttacking = true;
					rows[i].tiles[currentColum].isAttackable = true ;

				}
				goto r3 ;
			}
			rows[i].tiles[currentColum].MakeAvailable();
			
		}
		
	r3:
			
		
		for (int i = currentRow +1 ; i < rows.Length; i++) {
			
			
			if( rows[i].tiles[currentColum].isOccupied)
			{
				if(rows[i].tiles[currentColum].occupyingTeam ==  enemyTeam)
				{
					rows[i].tiles[currentColum].MakeAvailable();
					myGameManager.GetComponent<PlayerControl>().isAttacking = true;
					rows[i].tiles[currentColum].isAttackable = true ;
				}
				goto r1;
			}
			rows[i].tiles[currentColum].MakeAvailable();
		}
		
	r1:
			
			
		for (int  k = currentColum - 1; k >= 0; k--) {

			if( rows[currentRow].tiles[k].isOccupied)
			{
				if(rows[currentRow].tiles[k].occupyingTeam ==  enemyTeam)
				{
					rows[currentRow].tiles[k].MakeAvailable();
					myGameManager.GetComponent<PlayerControl>().isAttacking = true;
					rows[currentRow].tiles[k].isAttackable = true ;

				}

				goto r4;
			}
			rows[currentRow].tiles[k].MakeAvailable();
		}
		

	r4:

		if(true){}

	}

	public void BishopRegion (int currentRow , int currentColum, string team){
		
		currentRow--;
		currentColum--;
		string enemyTeam ;
		int sum = currentRow + currentColum;

		if(team == "PiecePlayer1"){
			enemyTeam  =  "PiecePlayer2";
		}
		else {
			
			enemyTeam  =  "PiecePlayer1";
		}
		
		for (int i = currentRow +1; i < rows.Length; i++) {
			
			for (int  k = currentColum+1; k < rows[i].tiles.Length; k++) {
				
				if(currentRow - i == currentColum - k){	
					if(rows[i].tiles[k].isOccupied)
					{
						if(rows[i].tiles[k].occupyingTeam == enemyTeam)
						{
							rows[i].tiles[k].MakeAvailable();
							myGameManager.GetComponent<PlayerControl>().isAttacking = true;
							rows[i].tiles[k].isAttackable = true; 
						}

						goto j1;
					}
					
					rows[i].tiles[k].MakeAvailable();
				}
			}
		}
		
	j1:
			
		for (int i = currentRow+ 1; i < rows.Length; i++) {
			
			for (int  k = currentColum -1 ; k >= 0; k--) {
				
				if(i==sum-k){
					if(rows[i].tiles[k].isOccupied){
					
						if(rows[i].tiles[k].occupyingTeam == enemyTeam){

							rows[i].tiles[k].MakeAvailable();
							myGameManager.GetComponent<PlayerControl>().isAttacking = true;
							rows[i].tiles[k].isAttackable = true ;
						}

						goto j2;
					
					}
					
					rows[i].tiles[k].MakeAvailable(); 
				}
				
			}
			
		}
		
		
	j2:
		for (int i = currentRow -1 ; i >= 0; i--) {
			
			for (int  k = currentColum +1; k < rows[i].tiles.Length; k++){
				
				if(i==sum-k){
					if(rows[i].tiles[k].isOccupied){
						if(rows[i].tiles[k].occupyingTeam == enemyTeam)
						{
							rows[i].tiles[k].MakeAvailable();
							myGameManager.GetComponent<PlayerControl>().isAttacking = true;
							rows[i].tiles[k].isAttackable = true ;
						}
						goto j3;
					}
					
					rows[i].tiles[k].MakeAvailable();
				}
				
			}
			
		}
		
		
	j3:
			
		for (int i = currentRow-1; i >= 0; i--) {
			
			for (int  k = currentColum-1; k >= 0; k--){
				
				if(currentRow - i ==currentColum - k){
					
					if(rows[i].tiles[k].isOccupied)
					{
						if(rows[i].tiles[k].occupyingTeam == enemyTeam){
							rows[i].tiles[k].MakeAvailable();
							myGameManager.GetComponent<PlayerControl>().isAttacking = true;
							rows[i].tiles[k].isAttackable = true ;
						}
						goto j4;
					}
					rows[i].tiles[k].MakeAvailable();
				}
				
			}

		}
		
		
		
	j4:
			
		if (true) {
			
		}
		
	}

	public void KnightRegion(int currentRow , int currentColum,string team)
	{
		currentRow--;
		currentColum--;

		string enemyTeam ;
		if(team == "PiecePlayer1"){
			enemyTeam  =  "PiecePlayer2";
		}
		else {
			enemyTeam  =  "PiecePlayer1";
		}


		if(currentRow +2 <= rows.Length-1 && currentColum + 1 <=rows[currentRow].tiles.Length-1 )
		   
		{
				if((!rows [currentRow+2].tiles [currentColum + 1].isOccupied) || (rows [currentRow+2].tiles [currentColum + 1].isOccupied && rows [currentRow+2].tiles [currentColum + 1].occupyingTeam == enemyTeam ))
					rows [currentRow+2].tiles [currentColum + 1].MakeAvailable ();
					myGameManager.GetComponent<PlayerControl>().isAttacking = true;
					rows [currentRow+2].tiles [currentColum + 1].isAttackable = true ;
		}

		
		 if(currentRow +1 <=rows.Length-1 && currentColum + 2 <=rows[currentRow].tiles.Length-1 ) 
		{
			if((!rows [currentRow+1].tiles [currentColum+2].isOccupied) ||(rows [currentRow+1].tiles [currentColum+2].isOccupied && rows [currentRow+1].tiles [currentColum+2].occupyingTeam == enemyTeam ))
					rows [currentRow+1].tiles [currentColum+2].MakeAvailable ();
					myGameManager.GetComponent<PlayerControl>().isAttacking = true;
					rows [currentRow+1].tiles [currentColum+2].isAttackable = true ;
		}


		 if(currentRow -1 >= 0  && currentColum + 2 <=rows[currentRow].tiles.Length-1 )  
		{
			if((!rows [currentRow-1].tiles [currentColum+2].isOccupied) || (rows [currentRow-1].tiles [currentColum+2].isOccupied && rows [currentRow-1].tiles [currentColum+2].occupyingTeam == enemyTeam ))
					rows [currentRow-1].tiles [currentColum+2].MakeAvailable ();
					myGameManager.GetComponent<PlayerControl>().isAttacking = true;
					rows [currentRow-1].tiles [currentColum+2].isAttackable = true ;
		}


		
		 if(currentRow -2 >= 0 && currentColum + 1 <=rows[currentRow].tiles.Length-1 )
		{
			if((rows [currentRow-2].tiles [currentColum+1].isOccupied == false ) ||(rows [currentRow-2].tiles [currentColum+1].isOccupied && rows [currentRow-2].tiles [currentColum+1].occupyingTeam == enemyTeam ))
					rows [currentRow-2].tiles [currentColum+1].MakeAvailable ();
					myGameManager.GetComponent<PlayerControl>().isAttacking = true;
					rows [currentRow-2].tiles [currentColum+1].isAttackable =true ;
		}


		 if(currentRow -1 >= 0 && currentColum - 2 >=0 ) 
		{
			if((!rows [currentRow-1].tiles [currentColum-2].isOccupied) || (rows [currentRow-1].tiles [currentColum-2].isOccupied && rows [currentRow-1].tiles [currentColum-2].occupyingTeam ==  enemyTeam )) 
					rows [currentRow-1].tiles [currentColum-2].MakeAvailable ();
					myGameManager.GetComponent<PlayerControl>().isAttacking = true;
					rows [currentRow-1].tiles [currentColum-2].isAttackable = true ;
		}


		 if(currentRow -2 >= 0 && currentColum-1 >= 0 )
		{
			if((!rows [currentRow-2].tiles [currentColum-1].isOccupied)||(rows [currentRow-2].tiles [currentColum-1].isOccupied && rows [currentRow-2].tiles [currentColum-1].occupyingTeam == enemyTeam ))
					rows [currentRow-2].tiles [currentColum-1].MakeAvailable ();
					myGameManager.GetComponent<PlayerControl>().isAttacking = true;
					rows [currentRow-2].tiles [currentColum-1].isAttackable = true ;

		}

		if(currentRow +1 <=rows.Length-1  && currentColum-2 >=0 ) 
		{
			if ((!rows [currentRow+1].tiles [currentColum-2].isOccupied) || (rows [currentRow+1].tiles [currentColum-2].isOccupied && rows [currentRow+1].tiles [currentColum-2].occupyingTeam == enemyTeam ))
					rows [currentRow+1].tiles [currentColum-2].MakeAvailable ();
					myGameManager.GetComponent<PlayerControl>().isAttacking = true;
					rows [currentRow+1].tiles [currentColum-2].isAttackable = true;
		}


		 if(currentRow +2 <= rows.Length-1  && currentColum-1 >= 0  )

		{
			if((!rows [currentRow+2].tiles [currentColum-1].isOccupied) || (rows [currentRow+2].tiles [currentColum-1].isOccupied && rows [currentRow+2].tiles [currentColum-1].occupyingTeam == enemyTeam ))
					rows [currentRow+2].tiles [currentColum-1].MakeAvailable ();
					myGameManager.GetComponent<PlayerControl>().isAttacking = true;
					rows [currentRow+2].tiles [currentColum-1].isAttackable = true ;
		}



	}


	public void RookRegion (int currentRow , int currentColum,string team)
	{
		currentRow--;
		currentColum--;
		string enemyTeam ;


		if(team == "PiecePlayer1"){
			enemyTeam  =  "PiecePlayer2";
		}
		else {

			enemyTeam  =  "PiecePlayer1";
		}
		
		for (int  k = currentColum +1; k < rows.Length ; k++) {
			if( rows[currentRow].tiles[k].isOccupied== true)
			{
				if(rows[currentRow].tiles[k].occupyingTeam ==  enemyTeam)
				{ 
					rows[currentRow].tiles[k].MakeAvailable();
					myGameManager.GetComponent<PlayerControl>().isAttacking = true;
					rows[currentRow].tiles[k].isAttackable = true ;
				}
//				else if(rows[currentRow].tiles[k].occupyingTeam ==  team && rows[currentRow].tiles[k].holdingKing)
//				{ 
//					rows[currentRow].tiles[k].MakeAvailable();
//				}
				goto r2;
			}
			rows[currentRow].tiles[k].MakeAvailable();
		}
		
	r2:
			
			
		for (int i = currentRow -1; i >= 0  ; i--) {

			if( rows[i].tiles[currentColum].isOccupied== true)
			{
				if(rows[i].tiles[currentColum].occupyingTeam ==  enemyTeam)
				{
					rows[i].tiles[currentColum].MakeAvailable();
					myGameManager.GetComponent<PlayerControl>().isAttacking = true;
					rows[i].tiles[currentColum].isAttackable = true ;
				}
//				else if(rows[i].tiles[currentColum].occupyingTeam ==  team && rows[i].tiles[currentColum].holdingKing)
//				{ 
//					rows[i].tiles[currentColum].MakeAvailable();
//				}


				goto r3 ;
			}
			rows[i].tiles[currentColum].MakeAvailable();
		}
		
	r3:
			
			
			
			
		for (int i = currentRow +1 ; i < rows.Length; i++) {
			
			if( rows[i].tiles[currentColum].isOccupied)
			{
				if(rows[i].tiles[currentColum].occupyingTeam ==  enemyTeam)
				{
					rows[i].tiles[currentColum].MakeAvailable();
					myGameManager.GetComponent<PlayerControl>().isAttacking = true;
					rows[i].tiles[currentColum].isAttackable = true ;
				}
//				else if(rows[i].tiles[currentColum].occupyingTeam ==  team && rows[i].tiles[currentColum].holdingKing)
//				{ 
//					rows[i].tiles[currentColum].MakeAvailable();
//				}
//
				goto r1;
			}
			rows[i].tiles[currentColum].MakeAvailable();
		}
		
	r1:
			
			
		for (int  k = currentColum - 1; k >= 0; k--) {

			if( rows[currentRow].tiles[k].isOccupied)
			{
				if(rows[currentRow].tiles[k].occupyingTeam ==  enemyTeam)
				{
					rows[currentRow].tiles[k].MakeAvailable();
					myGameManager.GetComponent<PlayerControl>().isAttacking = true;
					rows[currentRow].tiles[k].isAttackable = true ; 

				}
//				else if(rows[currentRow].tiles[k].occupyingTeam ==  team && rows[currentRow].tiles[k].holdingKing)
//				{ 
//					rows[currentRow].tiles[k].MakeAvailable();
//				}
				goto r4;
			}
			rows[currentRow].tiles[k].MakeAvailable();
		}
		
		
		
	r4:
		if(true){}

	}
		
	public void PawnRegion(int currentRow , int currentColum ,GameObject passedPawn,string tagName)
	{
		bool firstMove = passedPawn.transform.GetComponent<Pawn> ().firstMove;
		currentRow--;
		currentColum--;
		
		if (tagName == "PiecePlayer2") {
			string enemyTeam = "PiecePlayer1";
			if (firstMove) {
				if(!rows [currentRow + 1].tiles [currentColum].isOccupied)
					rows [currentRow + 1].tiles [currentColum].MakeAvailable ();
				if(!rows [currentRow + 2].tiles [currentColum].isOccupied && !rows [currentRow + 1].tiles [currentColum].isOccupied)
					rows [currentRow + 2].tiles [currentColum].MakeAvailable ();
			} else {
				if(!rows [currentRow + 1].tiles [currentColum].isOccupied)
					rows [currentRow + 1].tiles [currentColum].MakeAvailable ();
			}

			if(currentColum + 1 <=rows[currentRow].tiles.Length-1 &&( rows [currentRow + 1].tiles [currentColum+1].isOccupied && rows [currentRow + 1].tiles [currentColum+1].occupyingTeam == enemyTeam ))
			{
				rows [currentRow + 1].tiles [currentColum+1].MakeAvailable();
				myGameManager.GetComponent<PlayerControl>().isAttacking = true;
				rows [currentRow + 1].tiles [currentColum+1].isAttackable = true ;

			}
			if(currentColum-1>=0   &&( rows [currentRow + 1].tiles [currentColum-1].isOccupied && rows [currentRow + 1].tiles [currentColum-1].occupyingTeam == enemyTeam ))
			{
				rows [currentRow + 1].tiles [currentColum-1].MakeAvailable();
				myGameManager.GetComponent<PlayerControl>().isAttacking = true;
				rows [currentRow + 1].tiles [currentColum-1].isAttackable = true ;

			}

		}
		else if (tagName == "PiecePlayer1")
		{
			string enemyTeam = "PiecePlayer2";

			if (firstMove) {
				if(!rows [currentRow - 1].tiles [currentColum].isOccupied)
					rows [currentRow - 1].tiles [currentColum].MakeAvailable ();
				if(!rows [currentRow - 2].tiles [currentColum].isOccupied && !rows [currentRow - 1].tiles [currentColum].isOccupied)
				{rows [currentRow - 2].tiles [currentColum].MakeAvailable (); }
			} else {
				if(!rows [currentRow - 1].tiles [currentColum].isOccupied)
					rows [currentRow - 1].tiles [currentColum].MakeAvailable ();
			}

			if(currentColum + 1 <=rows[currentRow].tiles.Length-1 &&( rows [currentRow - 1].tiles [currentColum+1].isOccupied && rows [currentRow - 1].tiles [currentColum+1].occupyingTeam == enemyTeam ))
			{
				rows [currentRow - 1].tiles [currentColum+1].MakeAvailable();
				myGameManager.GetComponent<PlayerControl>().isAttacking = true;
				rows [currentRow - 1].tiles [currentColum+1].isAttackable = true ;

			}
			if(currentColum-1>=0   &&( rows [currentRow - 1].tiles [currentColum-1].isOccupied && rows [currentRow - 1].tiles [currentColum-1].occupyingTeam == enemyTeam ))
			{
				rows [currentRow - 1].tiles [currentColum-1].MakeAvailable();
				myGameManager.GetComponent<PlayerControl>().isAttacking = true;
				rows [currentRow - 1].tiles [currentColum-1].isAttackable = true ;

			}
			
		}

		
		
	}

	public void KingRegion (int currentRow , int currentColum ,string team )
	{
		currentRow--;
		currentColum--;
		string enemyTeam ;
		if(team == "PiecePlayer1"){
			enemyTeam  =  "PiecePlayer2";
		}
		else {
			
			enemyTeam  =  "PiecePlayer1";
		}

		if(currentColum + 1 <=rows[currentRow].tiles.Length-1 && 
		   ((!rows [currentRow].tiles [currentColum + 1].isOccupied || (rows [currentRow].tiles [currentColum + 1].isOccupied && rows [currentRow].tiles [currentColum + 1].occupyingTeam == enemyTeam) ) ))
		{
			rows [currentRow].tiles [currentColum + 1].MakeAvailable ();
			myGameManager.GetComponent<PlayerControl>().isAttacking = true;
			rows [currentRow].tiles [currentColum + 1].isAttackable = true ;
		}
		
		if(currentColum-1>=0 && 
		   ((!rows [currentRow].tiles [currentColum-1].isOccupied || (rows [currentRow].tiles [currentColum-1].isOccupied && rows [currentRow].tiles [currentColum-1].occupyingTeam == enemyTeam ) )))
		{
			rows [currentRow].tiles [currentColum-1].MakeAvailable ();
			myGameManager.GetComponent<PlayerControl>().isAttacking = true;
			rows [currentRow].tiles [currentColum-1].isAttackable = true ;
		}
		
		if(currentRow+1<=rows.Length-1  &&
		   ((!rows [currentRow+1].tiles [currentColum].isOccupied || (rows [currentRow+1].tiles [currentColum].isOccupied && rows [currentRow+1].tiles [currentColum].occupyingTeam == enemyTeam ) )))
		{
			rows [currentRow+1].tiles [currentColum].MakeAvailable ();
			myGameManager.GetComponent<PlayerControl>().isAttacking = true;
			rows [currentRow+1].tiles [currentColum].isAttackable = true ;
		}
		
		if(currentRow+1<=rows.Length-1 && currentColum + 1 <=rows[currentRow].tiles.Length-1 &&
		   ((!rows [currentRow+1].tiles [currentColum+1].isOccupied) || (rows [currentRow+1].tiles [currentColum+1].isOccupied && rows [currentRow+1].tiles [currentColum+1].occupyingTeam == enemyTeam )) )
		{
			rows [currentRow+1].tiles [currentColum+1].MakeAvailable ();
			myGameManager.GetComponent<PlayerControl>().isAttacking = true;
			rows [currentRow+1].tiles [currentColum+1].isAttackable = true;
		}
		
		if(currentRow+1<=rows.Length-1 && currentColum-1>=0  && 
		   (( !rows [currentRow+1].tiles [currentColum-1].isOccupied || (rows [currentRow+1].tiles [currentColum-1].isOccupied && rows [currentRow+1].tiles [currentColum-1].occupyingTeam ==enemyTeam  ))))
		{
			rows [currentRow+1].tiles [currentColum-1].MakeAvailable ();
			myGameManager.GetComponent<PlayerControl>().isAttacking = true;
			rows [currentRow+1].tiles [currentColum-1].isAttackable = true ;
		}
		
		if(currentRow-1 >= 0 && currentColum-1 >= 0 &&
		   ((!rows [currentRow-1].tiles [currentColum-1].isOccupied || (rows [currentRow-1].tiles [currentColum-1].isOccupied && rows [currentRow-1].tiles [currentColum-1].occupyingTeam == enemyTeam) )))
		{
			rows [currentRow-1].tiles [currentColum-1].MakeAvailable ();
			myGameManager.GetComponent<PlayerControl>().isAttacking = true;
			rows [currentRow-1].tiles [currentColum-1].isAttackable = true ;
		}
		
		if(currentRow-1 >= 0 && currentColum + 1 <=rows[currentRow].tiles.Length-1 &&
		   ((!rows [currentRow-1].tiles [currentColum+1].isOccupied || (rows [currentRow-1].tiles [currentColum+1].isOccupied && rows [currentRow-1].tiles [currentColum+1].occupyingTeam == enemyTeam ) )))
		{
			rows [currentRow-1].tiles [currentColum+1].MakeAvailable ();
			myGameManager.GetComponent<PlayerControl>().isAttacking = true;
			rows [currentRow-1].tiles [currentColum+1].isAttackable = true;
		}
		
		
		if(currentRow-1 >= 0 && 
		   ((!rows [currentRow-1].tiles [currentColum].isOccupied ||(rows [currentRow-1].tiles [currentColum].isOccupied && rows [currentRow-1].tiles [currentColum].occupyingTeam == enemyTeam) )))
			
		{
			rows [currentRow-1].tiles [currentColum].MakeAvailable ();
			myGameManager.GetComponent<PlayerControl>().isAttacking = true; 
			rows [currentRow-1].tiles [currentColum].isAttackable = true ;
		}

		//checking the Castling availabilty for both kings in both directions

		//checking player 1 king Castling

		//checking if player 1 king hasn't move and still in the base row

		if(team == "PiecePlayer1" && currentRow == 7 && currentColum == 3  &&  isKingNotMoved(team))
		{
			//checking if the path between king 1 and the rook to the right isn't occupied && checking if rook hasn't move and still in the base row
			if(!rows [7].tiles [2].isOccupied && !rows [7].tiles [1].isOccupied  && isRookNotMoved(7,0))
			{
				// checking that the king isn't checkmated by any piece
				if(threatToTeam1.Count>0)
					goto p3;

				//resetting castling tiles states before checking them
				resetCastlingTile();
				//checking that the path between  king 1 and the right rook isn't controlled by an enemy piece &&&&& the king doesn't end up in check mate

				if(checkCastlingTiles(team,"right")) 
				{
				rows[7].tiles[1].MakeAvailable();
				rows[7].tiles[1].readyForCastling = true;
				}

			}

			//checking if the path between king 1 and the rook to the left isn't occupied
			if(!rows [7].tiles [4].isOccupied && !rows [7].tiles [5].isOccupied && !rows [7].tiles [6].isOccupied  && isRookNotMoved(7,7) )
			{
				

				// checking that the king isn't checkmated by any piece
				if(threatToTeam1.Count>0)
					goto p3;

				//resetting castling tiles states before checking them
				resetCastlingTile();
				//checking that the path between  king 1 and the right rook isn't controlled by an enemy piece &&&&& the king doesn't end up in check mate

				if(checkCastlingTiles(team,"left")) 
				{
					rows[7].tiles[5].MakeAvailable();
					rows[7].tiles[5].readyForCastling = true ;

				}


			}

			p3: 
			if(true){}
		}
		else if (team == "PiecePlayer2" && currentRow == 0 && currentColum == 3  &&  isKingNotMoved(team))
		{
			if(!rows [0].tiles [4].isOccupied && !rows [0].tiles [5].isOccupied && !rows [0].tiles [6].isOccupied  && isRookNotMoved(0,7) )
			{


				// checking that the king isn't checkmated by any piece
				if(threatToTeam2.Count>0)
					goto p3;
				//resetting castling tiles states before checking them
				resetCastlingTile();
				//checking that the path between  king 1 and the right rook isn't controlled by an enemy piece &&&&& the king doesn't end up in check mate
				if(checkCastlingTiles(team,"right")) 
				{
					rows[0].tiles[5].MakeAvailable();
					rows[0].tiles[5].readyForCastling = true ;
				}


			}
			 
			if(!rows [0].tiles [2].isOccupied && !rows [0].tiles [1].isOccupied  && isRookNotMoved(0,0))
			{
				
				// checking that the king isn't checkmated by any piece
				if(threatToTeam2.Count>0)
					goto p3;
				//resetting castling tiles states before checking them
				resetCastlingTile();
				//checking that the path between  king 1 and the right rook isn't controlled by an enemy piece &&&&& the king doesn't end up in check mate
				if(checkCastlingTiles(team,"left")) 
				{
					rows[0].tiles[1].MakeAvailable();
					rows[0].tiles[1].readyForCastling = true ;
				}


			}
		
			p3: 
			if(true){}


		}
		
		
//		
	}







	//checking if rook threantening the king 
	public void RookVSKing (int currentRow , int currentColum,string team ,GameObject piece )
	{
		currentRow--;
		currentColum--;
		string enemyTeam ;

		if(team == "PiecePlayer1"){
			enemyTeam  =  "PiecePlayer2";
		}
		else {
			enemyTeam  =  "PiecePlayer1";
		}
		
		for (int  k = currentColum +1; k < rows.Length ; k++) {
			if( rows[currentRow].tiles[k].isOccupied== true)
			{
				if(rows[currentRow].tiles[k].occupyingTeam ==  enemyTeam)
				{ 
					if(rows[currentRow].tiles[k].holdingKing)
					{						
						addToThreatList(piece,enemyTeam);
						DetermineRookThreatPath(team,currentRow,currentColum,currentRow,k);
						rows[currentRow].tiles[currentColum].showThreatPath();
						rows[currentRow].tiles[k].showThreatPath();

					}
					
				}
				goto r2;
			}

		}
		
	r2:
			
			
		for (int i = currentRow -1; i >= 0  ; i--) {
			
			if( rows[i].tiles[currentColum].isOccupied== true)
			{
				if(rows[i].tiles[currentColum].occupyingTeam ==  enemyTeam)
				{
					if(rows[i].tiles[currentColum].holdingKing)
					{
						addToThreatList(piece,enemyTeam);
						DetermineRookThreatPath(team,currentRow,currentColum,i,currentColum);
						rows[currentRow].tiles[currentColum].showThreatPath();
						rows[i].tiles[currentColum].showThreatPath();


					}
				}
				goto r3 ;
			}

		}
		
	r3:
			
			
			
			
		for (int i = currentRow +1 ; i < rows.Length; i++) {
			
			
			if( rows[i].tiles[currentColum].isOccupied)
			{
				if(rows[i].tiles[currentColum].occupyingTeam ==  enemyTeam)
				{
					if(rows[i].tiles[currentColum].holdingKing)
					{
						addToThreatList(piece,enemyTeam);	
						DetermineRookThreatPath(team,currentRow,currentColum,i,currentColum);
						rows[currentRow].tiles[currentColum].showThreatPath();
						rows[i].tiles[currentColum].showThreatPath();

					}
					
				}
				goto r1;
			}
		}
		
	r1:
			
			
		for (int  k = currentColum - 1; k >= 0; k--) {


			if( rows[currentRow].tiles[k].isOccupied)
			{
				if(rows[currentRow].tiles[k].occupyingTeam ==  enemyTeam)
				{
					if(rows[currentRow].tiles[k].holdingKing)
					{
						addToThreatList(piece,enemyTeam);
						DetermineRookThreatPath(team,currentRow,currentColum,currentRow,k);
						rows[currentRow].tiles[currentColum].showThreatPath();
						rows[currentRow].tiles[k].showThreatPath();
					}
					
				}
				goto r4;
			}
		}
		
		
		
	r4:
			
		if(true){}
		
	}




	// checking if a bishop threats the king
	public void BishopVSKing (int currentRow , int currentColum, string team, GameObject piece){
		
		currentRow--;
		currentColum--;
		string enemyTeam ;
		int sum = currentRow + currentColum;
		
		if(team == "PiecePlayer1"){
			enemyTeam  =  "PiecePlayer2";
		}
		else {
			
			enemyTeam  =  "PiecePlayer1";
		}
		
		for (int i = currentRow +1; i < rows.Length; i++) {
			
			
			for (int  k = currentColum+1; k < rows[i].tiles.Length; k++) {
				
				
				if(currentRow - i == currentColum - k){	
					if(rows[i].tiles[k].isOccupied)
					{
						if(rows[i].tiles[k].occupyingTeam == enemyTeam)
						{
							if(rows[i].tiles[k].holdingKing)
							{
								addToThreatList(piece,enemyTeam);
								DetermineBishopThreatPath(team,currentRow,currentColum,i,k);
								rows[i].tiles[k].showThreatPath();
								rows[currentRow].tiles[currentColum].showThreatPath();
							}
						}
						
						goto j1;
					}
					
				}
			}
		}
		
	j1:
			
		for (int i = currentRow+ 1; i < rows.Length; i++) {
			
			
			for (int  k = currentColum -1 ; k >= 0; k--) {
				
				if(i==sum-k){
					if(rows[i].tiles[k].isOccupied){
						
						if(rows[i].tiles[k].occupyingTeam == enemyTeam){
							
							if(rows[i].tiles[k].holdingKing)
							{
								addToThreatList(piece,enemyTeam);
								DetermineBishopThreatPath(team,currentRow,currentColum,i,k);
								rows[i].tiles[k].showThreatPath();
								rows[currentRow].tiles[currentColum].showThreatPath();
							}
						}
						
						goto j2;
						
					}
					
				}
				
			}
			
		}
		
		
	j2:
		for (int i = currentRow -1 ; i >= 0; i--) {
			
			for (int  k = currentColum +1; k < rows[i].tiles.Length; k++){
				
				if(i==sum-k){
					if(rows[i].tiles[k].isOccupied){
						if(rows[i].tiles[k].occupyingTeam == enemyTeam)
						{
							if(rows[i].tiles[k].holdingKing)
							{
								addToThreatList(piece,enemyTeam);
								DetermineBishopThreatPath(team,currentRow,currentColum,i,k);
								rows[i].tiles[k].showThreatPath();
								rows[currentRow].tiles[currentColum].showThreatPath();
							}
						}
						goto j3;
					}
					
				}
				
			}
			
		}
		
		
	j3:
			
		for (int i = currentRow-1; i >= 0; i--) {
			
			
			for (int  k = currentColum-1; k >= 0; k--){
				
				if(currentRow - i ==currentColum - k){
					
					if(rows[i].tiles[k].isOccupied)
					{
						if(rows[i].tiles[k].occupyingTeam == enemyTeam){
							if(rows[i].tiles[k].holdingKing)
							{
								addToThreatList(piece,enemyTeam);
								DetermineBishopThreatPath(team,currentRow,currentColum,i,k);
								rows[i].tiles[k].showThreatPath();
								rows[currentRow].tiles[currentColum].showThreatPath();
							}
						}
						goto j4;
					}
				}
				
				
			}
			
			
		}
		
		
		
	j4:
			
		if (true) {
			
		}
		
	}



	// check if a queen threats the king
	public void QueenVSKing(int currentRow , int currentColum , string team , GameObject piece){
		currentRow--;
		currentColum--;
		string enemyTeam ;
		int sum = currentRow + currentColum;
		if(team == "PiecePlayer1"){
			enemyTeam  =  "PiecePlayer2";
		}
		else {
			
			enemyTeam  =  "PiecePlayer1";
		}
		
		for (int  k = currentColum +1; k < rows.Length ; k++) {
			if( rows[currentRow].tiles[k].isOccupied== true)
			{
				if(rows[currentRow].tiles[k].occupyingTeam ==  enemyTeam)
				{ 
					if(rows[currentRow].tiles[k].holdingKing)
					{
						addToThreatList(piece,enemyTeam);
						DetermineQueenThreatPath(team,currentRow,currentColum,currentRow,k);
						rows[currentRow].tiles[currentColum].showThreatPath();
						rows[currentRow].tiles[k].showThreatPath();
					}
					
				}
				goto r2;
			}
			
		}
		
	r2:
			
			
		for (int i = currentRow -1; i >= 0  ; i--) {
			
			if( rows[i].tiles[currentColum].isOccupied== true)
			{
				if(rows[i].tiles[currentColum].occupyingTeam ==  enemyTeam)
				{
					if(rows[i].tiles[currentColum].holdingKing)
					{
						addToThreatList(piece,enemyTeam);
						DetermineQueenThreatPath(team,currentRow,currentColum,i,currentColum);
						rows[currentRow].tiles[currentColum].showThreatPath();
						rows[i].tiles[currentColum].showThreatPath();
					}
				}
				goto r3 ;
			}
			
		}
		
	r3:
			
			
			
			
		for (int i = currentRow +1 ; i < rows.Length; i++) {
			
			
			if( rows[i].tiles[currentColum].isOccupied)
			{
				if(rows[i].tiles[currentColum].occupyingTeam ==  enemyTeam)
				{
					if(rows[i].tiles[currentColum].holdingKing)
					{
						addToThreatList(piece,enemyTeam);
						DetermineQueenThreatPath(team,currentRow,currentColum,i,currentColum);
						rows[currentRow].tiles[currentColum].showThreatPath();
						rows[i].tiles[currentColum].showThreatPath();

					}
					
				}
				goto r1;
			}
		}
		
	r1:
			
			
		for (int  k = currentColum - 1; k >= 0; k--) {
			
			
			if( rows[currentRow].tiles[k].isOccupied)
			{
				if(rows[currentRow].tiles[k].occupyingTeam ==  enemyTeam)
				{
					if(rows[currentRow].tiles[k].holdingKing)
					{
						addToThreatList(piece,enemyTeam);
						DetermineQueenThreatPath(team,currentRow,currentColum,currentRow,k);
						rows[currentRow].tiles[currentColum].showThreatPath();
						rows[currentRow].tiles[k].showThreatPath();

					}
					
				}
				goto r4;
			}
		}
		
		
		
	r4:
			
	

		for (int i = currentRow +1; i < rows.Length; i++) {
			
			
			for (int  k = currentColum+1; k < rows[i].tiles.Length; k++) {
				
				
				if(currentRow - i == currentColum - k){	
					if(rows[i].tiles[k].isOccupied)
					{
						if(rows[i].tiles[k].occupyingTeam == enemyTeam)
						{
							if(rows[i].tiles[k].holdingKing)
							{
								addToThreatList(piece,enemyTeam);
								DetermineQueenThreatPath(team,currentRow,currentColum,i,k);
								rows[currentRow].tiles[currentColum].showThreatPath();
								rows[i].tiles[k].showThreatPath();
							}
						}
						
						goto j1;
					}
					
				}
			}
		}
		
	j1:
			
		for (int i = currentRow+ 1; i < rows.Length; i++) {
			
			
			for (int  k = currentColum -1 ; k >= 0; k--) {
				
				if(i==sum-k){
					if(rows[i].tiles[k].isOccupied){
						
						if(rows[i].tiles[k].occupyingTeam == enemyTeam){
							
							if(rows[i].tiles[k].holdingKing)
							{
								addToThreatList(piece,enemyTeam);
								DetermineQueenThreatPath(team,currentRow,currentColum,i,k);
								rows[currentRow].tiles[currentColum].showThreatPath();
								rows[i].tiles[k].showThreatPath();
							}
						}
						
						goto j2;
						
					}
					
				}
				
			}
			
		}
		
		
	j2:
		for (int i = currentRow -1 ; i >= 0; i--) {
			
			for (int  k = currentColum +1; k < rows[i].tiles.Length; k++){
				
				if(i==sum-k){
					if(rows[i].tiles[k].isOccupied){
						if(rows[i].tiles[k].occupyingTeam == enemyTeam)
						{
							if(rows[i].tiles[k].holdingKing)
							{
								addToThreatList(piece,enemyTeam);
								DetermineQueenThreatPath(team,currentRow,currentColum,i,k);
								rows[currentRow].tiles[currentColum].showThreatPath();
								rows[i].tiles[k].showThreatPath();
							}
						}
						goto j3;
					}
					
				}
				
			}
			
		}
		
		
	j3:
			
		for (int i = currentRow-1; i >= 0; i--) {
			
			
			for (int  k = currentColum-1; k >= 0; k--){
				
				if(currentRow - i ==currentColum - k){
					
					if(rows[i].tiles[k].isOccupied)
					{
						if(rows[i].tiles[k].occupyingTeam == enemyTeam){
							if(rows[i].tiles[k].holdingKing)
							{
								addToThreatList(piece,enemyTeam);
								DetermineQueenThreatPath(team,currentRow,currentColum,i,k);
								rows[currentRow].tiles[currentColum].showThreatPath();
								rows[i].tiles[k].showThreatPath();
							}
						}
						goto j4;
					}
				}
				
				
			}
			
			
		}
		
		
		
	j4:
			
		if (true) {
			
		}



		
	}


	//check if the a knight threats the king
	public void KnightVSKing(int currentRow , int currentColum,string team , GameObject piece)
	{
		currentRow--;
		currentColum--;
		
		string enemyTeam ;
		if(team == "PiecePlayer1"){
			enemyTeam  =  "PiecePlayer2";
		}
		else {
			enemyTeam  =  "PiecePlayer1";
		}
		
		
		if(currentRow +2 <= rows.Length-1 && currentColum + 1 <=rows[currentRow].tiles.Length-1 )
			
		{
			if(rows [currentRow+2].tiles [currentColum + 1].isOccupied && rows [currentRow+2].tiles [currentColum + 1].occupyingTeam == enemyTeam && rows [currentRow+2].tiles [currentColum + 1].holdingKing )
			{
				addToThreatList(piece,enemyTeam);
				rows[currentRow].tiles[currentColum].showThreatPath();
				rows[currentRow+2].tiles[currentColum+1].showThreatPath();
			}
			
		}
		
		
		if(currentRow +1 <=rows.Length-1 && currentColum + 2 <=rows[currentRow].tiles.Length-1 ) 
		{
			if(rows [currentRow+1].tiles [currentColum+2].isOccupied && rows [currentRow+1].tiles [currentColum+2].occupyingTeam == enemyTeam  && rows [currentRow+1].tiles [currentColum+2].holdingKing )
			{
				addToThreatList(piece,enemyTeam);
				rows[currentRow].tiles[currentColum].showThreatPath();
				rows[currentRow+1].tiles[currentColum+2].showThreatPath();

			}

			
		}
		
		
		if(currentRow -1 >= 0  && currentColum + 2 <=rows[currentRow].tiles.Length-1 )  
		{
			if (rows [currentRow-1].tiles [currentColum+2].isOccupied && rows [currentRow-1].tiles [currentColum+2].occupyingTeam == enemyTeam && rows [currentRow-1].tiles [currentColum+2].holdingKing )
			{
				addToThreatList(piece,enemyTeam);
				rows[currentRow].tiles[currentColum].showThreatPath();
				rows[currentRow-1].tiles[currentColum+2].showThreatPath();
			}
			
		}

		
		if(currentRow -2 >= 0 && currentColum + 1 <=rows[currentRow].tiles.Length-1 )
		{
			if(rows [currentRow-2].tiles [currentColum+1].isOccupied && rows [currentRow-2].tiles [currentColum+1].occupyingTeam == enemyTeam  && rows [currentRow-2].tiles [currentColum+1].holdingKing)
			{
				addToThreatList(piece,enemyTeam);
				rows[currentRow].tiles[currentColum].showThreatPath();
				rows[currentRow-2].tiles[currentColum+1].showThreatPath();
			}
			
			
		}
		
		
		if(currentRow -1 >= 0 && currentColum - 2 >=0 ) 
		{
			if (rows [currentRow-1].tiles [currentColum-2].isOccupied && rows [currentRow-1].tiles [currentColum-2].occupyingTeam ==  enemyTeam  && rows [currentRow-1].tiles [currentColum-2].holdingKing)
			{

				addToThreatList(piece,enemyTeam);
				rows[currentRow].tiles[currentColum].showThreatPath();
				rows[currentRow-1].tiles[currentColum-2].showThreatPath();
			}
			
			
		}
		
		
		if(currentRow -2 >= 0 && currentColum-1 >= 0 )
		{
			if(rows [currentRow-2].tiles [currentColum-1].isOccupied && rows [currentRow-2].tiles [currentColum-1].occupyingTeam == enemyTeam && rows [currentRow-2].tiles [currentColum-1].holdingKing)
			{
				currentRow++;
				currentColum++;
				addToThreatList(piece,enemyTeam);
				rows[currentRow].tiles[currentColum].showThreatPath();
				rows[currentRow-2].tiles[currentColum-1].showThreatPath();
			}
			
			
		}
		
		if(currentRow +1 <=rows.Length-1  && currentColum-2 >=0 ) 
		{
			if  (rows [currentRow+1].tiles [currentColum-2].isOccupied && rows [currentRow+1].tiles [currentColum-2].occupyingTeam == enemyTeam && rows [currentRow+1].tiles [currentColum-2].holdingKing )
			{

				addToThreatList(piece,enemyTeam);
				rows[currentRow].tiles[currentColum].showThreatPath();
				rows[currentRow+1].tiles[currentColum-2].showThreatPath();
			}
			
			
		}
		
		
		if(currentRow +2 <= rows.Length-1  && currentColum-1 >= 0  )
			
		{
			if (rows [currentRow+2].tiles [currentColum-1].isOccupied && rows [currentRow+2].tiles [currentColum-1].occupyingTeam == enemyTeam  && rows [currentRow+2].tiles [currentColum-1].holdingKing)
			{
				addToThreatList(piece,enemyTeam);
				rows[currentRow].tiles[currentColum].showThreatPath();
				rows[currentRow+2].tiles[currentColum-1].showThreatPath();
			}
		}
		
		
		
		
	}

	// check if a pawn threats the king
	public void PawnVSKing(int currentRow , int currentColum ,string tagName , GameObject piece)
	{
		currentRow--;
		currentColum--;


		if (tagName == "PiecePlayer2") {
			string enemyTeam = "PiecePlayer1";

			if((currentColum + 1 <=rows[currentRow].tiles.Length-1 &&  currentRow + 1 < rows.Length) &&
			   ( rows [currentRow + 1].tiles [currentColum+1].isOccupied && rows [currentRow + 1].tiles [currentColum+1].occupyingTeam == enemyTeam && rows [currentRow + 1].tiles [currentColum+1]. holdingKing))
			{
				addToThreatList(piece,enemyTeam);
				rows[currentRow].tiles[currentColum].showThreatPath();
				rows[currentRow + 1].tiles[currentColum + 1].showThreatPath();
			}
			if((currentColum-1>=0  && currentRow + 1< rows.Length)  &&
			   ( rows [currentRow + 1].tiles [currentColum-1].isOccupied && rows [currentRow + 1].tiles [currentColum-1].occupyingTeam == enemyTeam && rows [currentRow + 1].tiles [currentColum-1].holdingKing ))
			{
				addToThreatList(piece,enemyTeam);
				rows[currentRow].tiles[currentColum].showThreatPath();
				rows[currentRow + 1].tiles[currentColum - 1].showThreatPath();
			}
			
		}
		else if (tagName == "PiecePlayer1")
		{
			string enemyTeam = "PiecePlayer2";

			if((currentColum + 1 <=rows[currentRow].tiles.Length-1 && currentRow - 1 >= 0 )&&
			   ( rows [currentRow - 1].tiles [currentColum + 1].isOccupied && rows [currentRow - 1].tiles [currentColum+1].occupyingTeam == enemyTeam && rows [currentRow - 1].tiles [currentColum+1].holdingKing ))
			{
				addToThreatList(piece,enemyTeam);
				rows[currentRow].tiles[currentColum].showThreatPath();
				rows[currentRow - 1].tiles[currentColum + 1].showThreatPath();
			}
			if((currentColum-1>=0 && currentRow - 1 >= 0 )  &&
			   ( rows [currentRow - 1].tiles [currentColum-1].isOccupied && rows [currentRow - 1].tiles [currentColum-1].occupyingTeam == enemyTeam  && rows [currentRow - 1].tiles [currentColum-1].holdingKing))
			{
				addToThreatList(piece,enemyTeam);
				rows[currentRow].tiles[currentColum].showThreatPath();
				rows[currentRow - 1].tiles[currentColum - 1].showThreatPath();
			}
			
		}
		
		
		
	}



	// check if a rook can attack any threating piece >> then remove it from threatlist
	public void RookVSThreatingPiece (int currentRow , int currentColum,string team ,GameObject piece ,GameObject threatingPiece)
	{
		currentRow--;
		currentColum--;
		string enemyTeam ;
		
		int row = threatingPiece.transform.GetComponent<Piece>().row;
		int colum = threatingPiece.transform.GetComponent<Piece>().column;
		row--; 
		colum--;


		if(team == "PiecePlayer1"){
			enemyTeam  =  "PiecePlayer2";
		}
		else {
			
			enemyTeam  =  "PiecePlayer1";
		}
		
		for (int  k = currentColum +1; k < rows.Length ; k++) {

			if( rows[currentRow].tiles[k].isOccupied== true)
			{
				if(rows[currentRow].tiles[k].occupyingTeam ==  enemyTeam)
				{
//					print ("here is an enemy piece");
					if( currentRow == row && k == colum  )
					{	
						removeFromThreatList(threatingPiece,enemyTeam);
//						removeFromThreatList(threatingPiece,enemyTeam);

					}
					
				}
				goto r2;
			}
			
		}
		
	r2:
			
			
		for (int i = currentRow -1; i >= 0  ; i--) {
			
			if( rows[i].tiles[currentColum].isOccupied== true)
			{
				if(rows[i].tiles[currentColum].occupyingTeam ==  enemyTeam)
				{
//					print ("here is an enemy piece");
//					rows[i].tiles[currentColum] == rows[row].tiles[colum]
					if(i == row && currentColum == colum)
					{
						removeFromThreatList(threatingPiece,enemyTeam);
//						removeFromThreatList(threatingPiece,enemyTeam);
					}
				}
				goto r3 ;
			}
			
		}
		
	r3:
			
			
			
			
		for (int i = currentRow +1 ; i < rows.Length; i++) {
			
			
			if( rows[i].tiles[currentColum].isOccupied)
			{
				if(rows[i].tiles[currentColum].occupyingTeam ==  enemyTeam)
				{
//					print ("here is an enemy piece");

					if(i == row && currentColum == colum)
					{
						removeFromThreatList(threatingPiece,enemyTeam);
//						removeFromThreatList(threatingPiece,enemyTeam);
					}
					
				}
				goto r1;
			}
		}
		
	r1:
			
			
		for (int  k = currentColum - 1; k >= 0; k--) {
			
			
			if( rows[currentRow].tiles[k].isOccupied)
			{
				if(rows[currentRow].tiles[k].occupyingTeam ==  enemyTeam)
				{
//					print ("here is an enemy piece");
//					rows[currentRow].tiles[k] == rows[row].tiles[colum]
					if(currentRow == row && k == colum )
					{
						removeFromThreatList(threatingPiece,enemyTeam);
//						removeFromThreatList(threatingPiece,enemyTeam);
					}
					
				}
				goto r4;
			}
		}
		
		
		
	r4:
			
		if(true){}
		
	}







	public void BishopVSThreatingPiece (int currentRow , int currentColum, string team, GameObject piece,GameObject threatingPiece){
		
		currentRow--;
		currentColum--;
		string enemyTeam ;
		int sum = currentRow + currentColum;

		int row = threatingPiece.transform.GetComponent<Piece>().row;
		int colum = threatingPiece.transform.GetComponent<Piece>().column;
		row--;
		colum--;


		
		if(team == "PiecePlayer1"){
			enemyTeam  =  "PiecePlayer2";
		}
		else {
			
			enemyTeam  =  "PiecePlayer1";
		}
		
		for (int i = currentRow +1; i < rows.Length; i++) {
			
			
			for (int  k = currentColum+1; k < rows[i].tiles.Length; k++) {
				
				
				if(currentRow - i == currentColum - k){	
					if(rows[i].tiles[k].isOccupied)
					{
						if(rows[i].tiles[k].occupyingTeam == enemyTeam)
						{
							if(i == row && k == colum)
							{
								removeFromThreatList(threatingPiece,enemyTeam);
//								removeFromThreatList(threatingPiece,enemyTeam);
							}
						}
						
						goto j1;
					}
					
				}
			}
		}
		
	j1:
			
		for (int i = currentRow+ 1; i < rows.Length; i++) {
			
			
			for (int  k = currentColum -1 ; k >= 0; k--) {
				
				if(i==sum-k){
					if(rows[i].tiles[k].isOccupied){
						
						if(rows[i].tiles[k].occupyingTeam == enemyTeam){
							
							if(i == row && k == colum)
							{
								removeFromThreatList(threatingPiece,enemyTeam);
//								removeFromThreatList(threatingPiece,enemyTeam);
							}
						}
						
						goto j2;
						
					}
					
				}
				
			}
			
		}
		
		
	j2:
		for (int i = currentRow -1 ; i >= 0; i--) {
			
			for (int  k = currentColum +1; k < rows[i].tiles.Length; k++){
				
				if(i==sum-k){
					if(rows[i].tiles[k].isOccupied){
						if(rows[i].tiles[k].occupyingTeam == enemyTeam)
						{
							if(i == row && k == colum)
							{
								removeFromThreatList(threatingPiece,enemyTeam);
//								removeFromThreatList(threatingPiece,enemyTeam);
							}
						}
						goto j3;
					}
					
				}
				
			}
			
		}
		
		
	j3:
			
		for (int i = currentRow-1; i >= 0; i--) {
			
			
			for (int  k = currentColum-1; k >= 0; k--){
				
				if(currentRow - i ==currentColum - k){
					
					if(rows[i].tiles[k].isOccupied)
					{
						if(rows[i].tiles[k].occupyingTeam == enemyTeam){
							if(i == row && k == colum)
							{
								removeFromThreatList(threatingPiece,enemyTeam);
//								removeFromThreatList(threatingPiece,enemyTeam);
							}
						}
						goto j4;
					}
				}
				
				
			}
			
			
		}
		
		
		
	j4:
			
		if (true) {
			
		}
		
	}



	public void  QueenVSThreatingPiece (int currentRow , int currentColum,string team ,GameObject piece ,GameObject threatingPiece)
	{
		currentRow--;
		currentColum--;
		string enemyTeam ;
		int sum = currentRow + currentColum;

		
		int row = threatingPiece.transform.GetComponent<Piece>().row;
		int colum = threatingPiece.transform.GetComponent<Piece>().column;
		row--;
		colum--;
		
		
		
		if(team == "PiecePlayer1"){
			enemyTeam  =  "PiecePlayer2";
		}
		else {
			
			enemyTeam  =  "PiecePlayer1";
		}
		
		for (int  k = currentColum +1; k < rows.Length ; k++) {
			
			if( rows[currentRow].tiles[k].isOccupied== true)
			{
				if(rows[currentRow].tiles[k].occupyingTeam ==  enemyTeam)
				{
					if( currentRow == row && k == colum  )
					{	
						removeFromThreatList(threatingPiece,enemyTeam);
//						removeFromThreatList(threatingPiece,enemyTeam);
					}
					
				}
				goto r2;
			}
			
		}
		
	r2:
			
			
		for (int i = currentRow -1; i >= 0  ; i--) {
			
			if( rows[i].tiles[currentColum].isOccupied== true)
			{
				if(rows[i].tiles[currentColum].occupyingTeam ==  enemyTeam)
				{
					if(i == row && currentColum == colum)
					{
						removeFromThreatList(threatingPiece,enemyTeam);
//						removeFromThreatList(threatingPiece,enemyTeam);
					}
				}
				goto r3 ;
			}
			
		}
		
	r3:
			
			
			
			
		for (int i = currentRow +1 ; i < rows.Length; i++) {
			
			
			if( rows[i].tiles[currentColum].isOccupied)
			{
				if(rows[i].tiles[currentColum].occupyingTeam ==  enemyTeam)
				{
					if(i == row && currentColum == colum)
					{
						removeFromThreatList(threatingPiece,enemyTeam);
//						removeFromThreatList(threatingPiece,enemyTeam);
					}
					
				}
				goto r1;
			}
		}
		
	r1:
			
			
		for (int  k = currentColum - 1; k >= 0; k--) {
			
			
			if( rows[currentRow].tiles[k].isOccupied)
			{
				if(rows[currentRow].tiles[k].occupyingTeam ==  enemyTeam)
				{
					if(currentRow == row && k == colum )
					{
						removeFromThreatList(threatingPiece,enemyTeam);
//						removeFromThreatList(threatingPiece,enemyTeam);
					}
					
				}
				goto r4;
			}
		}
		
		
		
	r4:

		for (int i = currentRow +1; i < rows.Length; i++) {
			
			
			for (int  k = currentColum+1; k < rows[i].tiles.Length; k++) {
				
				
				if(currentRow - i == currentColum - k){	
					if(rows[i].tiles[k].isOccupied)
					{
						if(rows[i].tiles[k].occupyingTeam == enemyTeam)
						{
							if(i == row && k == colum)
							{
								removeFromThreatList(threatingPiece,enemyTeam);
//								removeFromThreatList(threatingPiece,enemyTeam);					
							}
						}
						
						goto j1;
					}
					
				}
			}
		}
		
	j1:
			
		for (int i = currentRow+ 1; i < rows.Length; i++) {
			
			
			for (int  k = currentColum -1 ; k >= 0; k--) {
				
				if(i==sum-k){
					if(rows[i].tiles[k].isOccupied){
						
						if(rows[i].tiles[k].occupyingTeam == enemyTeam){
							
							if(i == row && k == colum)
							{
								removeFromThreatList(threatingPiece,enemyTeam);
//								removeFromThreatList(threatingPiece,enemyTeam);
							}
						}
						
						goto j2;
						
					}
					
				}
				
			}
			
		}
		
		
	j2:
		for (int i = currentRow -1 ; i >= 0; i--) {
			
			for (int  k = currentColum +1; k < rows[i].tiles.Length; k++){
				
				if(i==sum-k){
					if(rows[i].tiles[k].isOccupied){
						if(rows[i].tiles[k].occupyingTeam == enemyTeam)
						{
							if(i == row && k == colum)
							{
								removeFromThreatList(threatingPiece,enemyTeam);
//								removeFromThreatList(threatingPiece,enemyTeam);
							}
						}
						goto j3;
					}
					
				}
				
			}
			
		}
		
		
	j3:
			
		for (int i = currentRow-1; i >= 0; i--) {
			
			
			for (int  k = currentColum-1; k >= 0; k--){
				
				if(currentRow - i ==currentColum - k){
					
					if(rows[i].tiles[k].isOccupied)
					{
						if(rows[i].tiles[k].occupyingTeam == enemyTeam){
							if(i == row && k == colum)
							{
								removeFromThreatList(threatingPiece,enemyTeam);
//								removeFromThreatList(threatingPiece,enemyTeam);
							}
						}
						goto j4;
					}
				}
				
				
			}
			
			
		}
		
		
		
	j4:
			
		if (true) {
			
		}
			
		
	}





	public void KnightVSThreatingPiece(int currentRow , int currentColum,string team, GameObject piece , GameObject threatingPiece)
	{
		currentRow--;
		currentColum--;


		int row = threatingPiece.transform.GetComponent<Piece>().row;
		int colum = threatingPiece.transform.GetComponent<Piece>().column;
		row--;
		colum--;

		string enemyTeam ;
		if(team == "PiecePlayer1"){
			enemyTeam  =  "PiecePlayer2";
		}
		else {
			enemyTeam  =  "PiecePlayer1";
		}
		
		
		if(currentRow +2 <= rows.Length-1 && currentColum + 1 <=rows[currentRow].tiles.Length-1 )
			
		{
			if(rows [currentRow+2].tiles [currentColum + 1].isOccupied && rows [currentRow+2].tiles [currentColum + 1].occupyingTeam == enemyTeam &&  currentRow+2 == row  && currentColum + 1 == colum )
			{
				removeFromThreatList(threatingPiece,enemyTeam);
//				removeFromThreatList(threatingPiece,enemyTeam);
			}
			
		}
		
		
		if(currentRow +1 <=rows.Length-1 && currentColum + 2 <=rows[currentRow].tiles.Length-1 ) 
		{
			if(rows [currentRow+1].tiles [currentColum+2].isOccupied && rows [currentRow+1].tiles [currentColum+2].occupyingTeam == enemyTeam  && currentRow+1 == row  && currentColum + 2 == colum  )
			{
				removeFromThreatList(threatingPiece,enemyTeam);
//				removeFromThreatList(threatingPiece,enemyTeam);
			}
			
			
		}
		
		
		if(currentRow -1 >= 0  && currentColum + 2 <=rows[currentRow].tiles.Length-1 )  
		{
			if (rows [currentRow-1].tiles [currentColum+2].isOccupied && rows [currentRow-1].tiles [currentColum+2].occupyingTeam == enemyTeam && currentRow-1 == row  && currentColum + 2 == colum )
			{
				removeFromThreatList(threatingPiece,enemyTeam);
//				removeFromThreatList(threatingPiece,enemyTeam);
			}
			
		}
		
		
		
		if(currentRow -2 >= 0 && currentColum + 1 <=rows[currentRow].tiles.Length-1 )
		{
			if(rows [currentRow-2].tiles [currentColum+1].isOccupied && rows [currentRow-2].tiles [currentColum+1].occupyingTeam == enemyTeam && currentRow-2 == row  && currentColum + 1 == colum  )
			{
				removeFromThreatList(threatingPiece,enemyTeam);
//				removeFromThreatList(threatingPiece,enemyTeam);
			}
			
			
		}
		
		
		if(currentRow -1 >= 0 && currentColum - 2 >=0 ) 
		{
			if (rows [currentRow-1].tiles [currentColum-2].isOccupied && rows [currentRow-1].tiles [currentColum-2].occupyingTeam ==  enemyTeam  && currentRow-1 == row  && currentColum -2 == colum )
			{
				removeFromThreatList(threatingPiece,enemyTeam);
//				removeFromThreatList(threatingPiece,enemyTeam);
			}
			
			
		}
		
		
		if(currentRow -2 >= 0 && currentColum-1 >= 0 )
		{
			if(rows [currentRow-2].tiles [currentColum-1].isOccupied && rows [currentRow-2].tiles [currentColum-1].occupyingTeam == enemyTeam && currentRow-2 == row  && currentColum - 1 == colum )
			{
				removeFromThreatList(threatingPiece,enemyTeam);
//				removeFromThreatList(threatingPiece,enemyTeam);
			}
			
			
		}
		
		if(currentRow +1 <=rows.Length-1  && currentColum-2 >=0 ) 
		{
			if  (rows [currentRow+1].tiles [currentColum-2].isOccupied && rows [currentRow+1].tiles [currentColum-2].occupyingTeam == enemyTeam && currentRow+1 == row  && currentColum -2 == colum  )
			{
				removeFromThreatList(threatingPiece,enemyTeam);
//				removeFromThreatList(threatingPiece,enemyTeam);
			}
			
			
		}
		
		
		if(currentRow +2 <= rows.Length-1  && currentColum-1 >= 0  )
			
		{
			if (rows [currentRow+2].tiles [currentColum-1].isOccupied && rows [currentRow+2].tiles [currentColum-1].occupyingTeam == enemyTeam  && currentRow+2 == row  && currentColum - 1 == colum )
			{
				removeFromThreatList(threatingPiece,enemyTeam);
//				removeFromThreatList(threatingPiece,enemyTeam);
			}
		}
		
		
		
		
	}

	public void PawnVSThreatingPiece(int currentRow , int currentColum ,string tagName , GameObject piece, GameObject threatingPiece )
	{
		currentRow--;
		currentColum--;

		int row = threatingPiece.transform.GetComponent<Piece>().row;
		int colum = threatingPiece.transform.GetComponent<Piece>().column;
		row--;
		colum--;
		
		
		if (tagName == "PiecePlayer2") {
			string enemyTeam = "PiecePlayer1";
			
			if((currentColum + 1 <=rows[currentRow].tiles.Length-1 &&  currentRow + 1 < rows.Length) &&
			   ( rows [currentRow + 1].tiles [currentColum+1].isOccupied && rows [currentRow + 1].tiles [currentColum+1].occupyingTeam == enemyTeam &&  currentRow + 1  == row && currentColum+1 == colum ))
			{
				removeFromThreatList(threatingPiece,enemyTeam);
//				removeFromThreatList(threatingPiece,enemyTeam);
			}
			if((currentColum-1>=0  && currentRow + 1< rows.Length)  &&
			   ( rows [currentRow + 1].tiles [currentColum-1].isOccupied && rows [currentRow + 1].tiles [currentColum-1].occupyingTeam == enemyTeam &&  currentRow + 1  == row &&  currentColum-1  == colum ))
			{
				removeFromThreatList(threatingPiece,enemyTeam);
//				removeFromThreatList(threatingPiece,enemyTeam);
			}
			
		}
		else if (tagName == "PiecePlayer1")
		{
			string enemyTeam = "PiecePlayer2";
			
			if((currentColum + 1 <=rows[currentRow].tiles.Length-1 && currentRow - 1 >= 0 )&&
			   ( rows [currentRow - 1].tiles [currentColum+1].isOccupied && rows [currentRow - 1].tiles [currentColum+1].occupyingTeam == enemyTeam &&  currentRow - 1  == row &&  currentColum+1  == colum ))
			{
				removeFromThreatList(threatingPiece,enemyTeam);
//				removeFromThreatList(threatingPiece,enemyTeam);
			}
			if((currentColum-1>=0 && currentRow - 1 >= 0 )  &&
			   ( rows [currentRow - 1].tiles [currentColum-1].isOccupied && rows [currentRow - 1].tiles [currentColum-1].occupyingTeam == enemyTeam  &&  currentRow - 1  == row &&  currentColum-1  == colum))
			{				
				removeFromThreatList(threatingPiece,enemyTeam);
//				removeFromThreatList(threatingPiece,enemyTeam);
			}
			
		}
		
		
		
	}


	public void RookVSAroundKing (int currentRow , int currentColum,string team ,GameObject piece )
	{
		currentRow--;
		currentColum--;
		string enemyTeam ;
		
		int[,] aroundKing; 
		
		
		
		if(team == "PiecePlayer1"){
			enemyTeam  =  "PiecePlayer2";
			aroundKing = aroundKing2;
		}
		else {
			
			enemyTeam  =  "PiecePlayer1";
			aroundKing = aroundKing1;
		}
		
		for (int  k = currentColum +1; k < rows.Length ; k++) {
			
			if( rows[currentRow].tiles[k].isOccupied== true)
			{
				if(rows[currentRow].tiles[k].occupyingTeam ==  team)
				{
					for(int j = 0 ; j < 8 ; j++)
					{
						if( currentRow == aroundKing[0,j] && k == aroundKing[1,j] )
						{	
							aroundKing[2,j] = 1 ;					

						}
					}
				}
				goto r2;
			}
			else
			{
				for(int j = 0 ; j < 8 ; j++)
				{
					if( currentRow == aroundKing[0,j] && k == aroundKing[1,j] )
					{	
						aroundKing[2,j] = 1 ;					
						
					}
				}
			}
			
		}
		
	r2:
			
			
		for (int i = currentRow -1; i >= 0  ; i--) {
			
			if( rows[i].tiles[currentColum].isOccupied== true)
			{
				if(rows[i].tiles[currentColum].occupyingTeam ==  team)
				{
					for(int j = 0 ; j < 8 ; j++)
					{
						if( i == aroundKing[0,j] && currentColum == aroundKing[1,j] )
						{	
							aroundKing[2,j] = 1 ;					
							
						}
					}
				}
				goto r3 ;
			}
			else
			{
				for(int j = 0 ; j < 8 ; j++)
				{
					if( i == aroundKing[0,j] && currentColum == aroundKing[1,j] )
					{	
						aroundKing[2,j] = 1 ;					
						
					}
				}
			}

			
		}
		
	r3:
			
			
			
			
		for (int i = currentRow +1 ; i < rows.Length; i++) {
			
			
			if( rows[i].tiles[currentColum].isOccupied)
			{
				if(rows[i].tiles[currentColum].occupyingTeam ==  team)
				{
					for(int j = 0 ; j < 8 ; j++)
					{
						if( i == aroundKing[0,j] && currentColum == aroundKing[1,j] )
						{	
							aroundKing[2,j] = 1 ;					
							
						}
					}
					
				}
				goto r1;
			}
			else
			{
				for(int j = 0 ; j < 8 ; j++)
				{
					if( i == aroundKing[0,j]  && currentColum == aroundKing[1,j]  )
					{	
						aroundKing[2,j] = 1 ;					
						
					}
				}
			}
		}
		
	r1:
			
			
		for (int  k = currentColum - 1; k >= 0; k--) {
			
			
			if( rows[currentRow].tiles[k].isOccupied)
			{
				if(rows[currentRow].tiles[k].occupyingTeam ==  team)
				{
					for(int j = 0 ; j < 8 ; j++)
					{
						if( currentRow == aroundKing[0,j] && k == aroundKing[1,j] )
						{	
							aroundKing[2,j] = 1 ;					
							
						}
					}
					
				}
				goto r4;
			}
			else
			{
				for(int j = 0 ; j < 8 ; j++)
				{
					if( currentRow == aroundKing[0,j] && k == aroundKing[1,j] )
					{	
						aroundKing[2,j] = 1 ;					
						
					}
				}
			}
		}
		
		
		
	r4:
			
			
		if(team == "PiecePlayer1"){
			 
			aroundKing2= aroundKing;
		}
		else {
			
			aroundKing1 = aroundKing ;
		}

		
	}


	public void BishopVSAroundKing (int currentRow , int currentColum, string team, GameObject piece){
		
		currentRow--;
		currentColum--;
		string enemyTeam ;
		int sum = currentRow + currentColum;

		int[,] aroundKing; 
		

		if(team == "PiecePlayer1"){
			enemyTeam  =  "PiecePlayer2";
			aroundKing = aroundKing2;
		}
		else {
			
			enemyTeam  =  "PiecePlayer1";
			aroundKing = aroundKing1;
			
		}



		
		for (int i = currentRow +1; i < rows.Length; i++) {
			
			
			for (int  k = currentColum+1; k < rows[i].tiles.Length; k++) {

				
				if(currentRow - i == currentColum - k){	
					if(rows[i].tiles[k].isOccupied)
					{
						if(rows[i].tiles[k].occupyingTeam == team)
						{
							for(int j = 0 ; j < 8 ; j++)
							{
								if( i == aroundKing[0,j] && k == aroundKing[1,j] )
								{	
									aroundKing[2,j] = 1 ;					
									
								}
							}
						}
						
						goto j1;
					}
					else
					{
						for(int j = 0 ; j < 8 ; j++)
						{
							if( i == aroundKing[0,j] && k == aroundKing[1,j] )
							{	
								aroundKing[2,j] = 1 ;					
								
							}
						}

					
					}
				}
			}
		}
		
	j1:
			
		for (int i = currentRow+ 1; i < rows.Length; i++) {
			
			
			for (int  k = currentColum -1 ; k >= 0; k--) {
				
				if(i==sum-k){
					if(rows[i].tiles[k].isOccupied){
						
						if(rows[i].tiles[k].occupyingTeam == team){
							
							for(int j = 0 ; j < 8 ; j++)
							{
								if( i == aroundKing[0,j] && k == aroundKing[1,j] )
								{	
									aroundKing[2,j] = 1 ;					
									
								}
							}
						}
						
						goto j2;
						
					}
					else
					{
						for(int j = 0 ; j < 8 ; j++)
						{
							if( i == aroundKing[0,j] && k == aroundKing[1,j] )
							{	
								aroundKing[2,j] = 1 ;					
								
							}
						}
						
						
					}
					
				}
				
			}
			
		}
		
		
	j2:
		for (int i = currentRow -1 ; i >= 0; i--) {
			
			for (int  k = currentColum +1; k < rows[i].tiles.Length; k++){
				
				if(i==sum-k){
					if(rows[i].tiles[k].isOccupied){
						if(rows[i].tiles[k].occupyingTeam == team)
						{
							for(int j = 0 ; j < 8 ; j++)
							{
								if( i == aroundKing[0,j] && k == aroundKing[1,j] )
								{	
									aroundKing[2,j] = 1 ;					
									
								}
							}
						}
						goto j3;
					}
					else
					{
						for(int j = 0 ; j < 8 ; j++)
						{
							if( i == aroundKing[0,j] && k == aroundKing[1,j] )
							{	
								aroundKing[2,j] = 1 ;					
								
							}
						}
						
						
					}
					
				}
				
			}
			
		}
		
		
	j3:
			
		for (int i = currentRow-1; i >= 0; i--) {
			
			
			for (int  k = currentColum-1; k >= 0; k--){
				
				if(currentRow - i ==currentColum - k){
					
					if(rows[i].tiles[k].isOccupied)
					{
						if(rows[i].tiles[k].occupyingTeam == team){
							for(int j = 0 ; j < 8 ; j++)
							{
								if( i == aroundKing[0,j] && k == aroundKing[1,j] )
								{	
									aroundKing[2,j] = 1 ;					
									
								}
							}
						}
						goto j4;
					}
					else
					{
						for(int j = 0 ; j < 8 ; j++)
						{
							if( i == aroundKing[0,j] && k == aroundKing[1,j] )
							{	
								aroundKing[2,j] = 1 ;					
								
							}
						}
						
						
					}
				}
				
				
			}
			
			
		}
		
		
		
	j4:
			
		if(team == "PiecePlayer1"){
			
			aroundKing2= aroundKing;
		}
		else {
			
			aroundKing1 = aroundKing ;
		}
		
	}




	public void  QueenVSAroundKing (int currentRow , int currentColum,string team ,GameObject piece)
	{
		currentRow--;
		currentColum--;
		string enemyTeam ;
		int sum = currentRow + currentColum;
		
		int[,] aroundKing; 
		
		
		if(team == "PiecePlayer1"){
			enemyTeam  =  "PiecePlayer2";
			aroundKing = aroundKing2;
		}
		else {
			
			enemyTeam  =  "PiecePlayer1";
			aroundKing = aroundKing1;
			
		}

		for (int  k = currentColum +1; k < rows.Length ; k++) {
			
			if( rows[currentRow].tiles[k].isOccupied== true)
			{
				if(rows[currentRow].tiles[k].occupyingTeam ==  team)
				{
					for(int j = 0 ; j < 8 ; j++)
					{
						if( currentRow == aroundKing[0,j] && k == aroundKing[1,j] )
						{	
							aroundKing[2,j] = 1 ;					
							
						}
					}
				}
				goto r2;
			}
			else
			{
				for(int j = 0 ; j < 8 ; j++)
				{
					if( currentRow == aroundKing[0,j] && k == aroundKing[1,j] )
					{	
						aroundKing[2,j] = 1 ;					
						
					}
				}
			}
			
		}
		
	r2:
			
			
		for (int i = currentRow -1; i >= 0  ; i--) {
			
			if( rows[i].tiles[currentColum].isOccupied== true)
			{
				if(rows[i].tiles[currentColum].occupyingTeam ==  team)
				{
					for(int j = 0 ; j < 8 ; j++)
					{
						if( i == aroundKing[0,j] && currentColum == aroundKing[1,j] )
						{	
							aroundKing[2,j] = 1 ;					
							
						}
					}
				}
				goto r3 ;
			}
			else
			{
				for(int j = 0 ; j < 8 ; j++)
				{
					if( i == aroundKing[0,j] && currentColum == aroundKing[1,j] )
					{	
						aroundKing[2,j] = 1 ;					
						
					}
				}
			}
			
			
		}
		
	r3:
			
			
			
			
		for (int i = currentRow +1 ; i < rows.Length; i++) {
			
			
			if( rows[i].tiles[currentColum].isOccupied)
			{
				if(rows[i].tiles[currentColum].occupyingTeam ==  team)
				{
					for(int j = 0 ; j < 8 ; j++)
					{
						if( i == aroundKing[0,j] && currentColum == aroundKing[1,j] )
						{	
							aroundKing[2,j] = 1 ;					
							
						}
					}
					
				}
				goto r1;
			}
			else
			{
				for(int j = 0 ; j < 8 ; j++)
				{
					if( i == aroundKing[0,j]  && currentColum == aroundKing[1,j]  )
					{	
						aroundKing[2,j] = 1 ;					
						
					}
				}
			}
		}
		
	r1:
			
			
		for (int  k = currentColum - 1; k >= 0; k--) {
			
			
			if( rows[currentRow].tiles[k].isOccupied)
			{
				if(rows[currentRow].tiles[k].occupyingTeam ==  team)
				{
					for(int j = 0 ; j < 8 ; j++)
					{
						if( currentRow == aroundKing[0,j] && k == aroundKing[1,j] )
						{	
							aroundKing[2,j] = 1 ;					
							
						}
					}
					
				}
				goto r4;
			}
			else
			{
				for(int j = 0 ; j < 8 ; j++)
				{
					if( currentRow == aroundKing[0,j] && k == aroundKing[1,j] )
					{	
						aroundKing[2,j] = 1 ;					
						
					}
				}
			}
		}
		
		
		
	r4:


			
		for (int i = currentRow +1; i < rows.Length; i++) {
			
			
			for (int  k = currentColum+1; k < rows[i].tiles.Length; k++) {
				
				
				if(currentRow - i == currentColum - k){	
					if(rows[i].tiles[k].isOccupied)
					{
						if(rows[i].tiles[k].occupyingTeam == team)
						{
							for(int j = 0 ; j < 8 ; j++)
							{
								if( i == aroundKing[0,j] && k == aroundKing[1,j] )
								{	
									aroundKing[2,j] = 1 ;					
									
								}
							}
						}
						
						goto j1;
					}
					else
					{
						for(int j = 0 ; j < 8 ; j++)
						{
							if( i == aroundKing[0,j] && k == aroundKing[1,j] )
							{	
								aroundKing[2,j] = 1 ;					
								
							}
						}
						
						
					}
				}
			}
		}
		
	j1:
			
		for (int i = currentRow+ 1; i < rows.Length; i++) {
			
			
			for (int  k = currentColum -1 ; k >= 0; k--) {
				
				if(i==sum-k){
					if(rows[i].tiles[k].isOccupied){
						
						if(rows[i].tiles[k].occupyingTeam == team){
							
							for(int j = 0 ; j < 8 ; j++)
							{
								if( i == aroundKing[0,j] && k == aroundKing[1,j] )
								{	
									aroundKing[2,j] = 1 ;					
									
								}
							}
						}
						
						goto j2;
						
					}
					else
					{
						for(int j = 0 ; j < 8 ; j++)
						{
							if( i == aroundKing[0,j] && k == aroundKing[1,j] )
							{	
								aroundKing[2,j] = 1 ;					
								
							}
						}
						
						
					}
					
				}
				
			}
			
		}
		
		
	j2:
		for (int i = currentRow -1 ; i >= 0; i--) {
			
			for (int  k = currentColum +1; k < rows[i].tiles.Length; k++){
				
				if(i==sum-k){
					if(rows[i].tiles[k].isOccupied){
						if(rows[i].tiles[k].occupyingTeam == team)
						{
							for(int j = 0 ; j < 8 ; j++)
							{
								if( i == aroundKing[0,j] && k == aroundKing[1,j] )
								{	
									aroundKing[2,j] = 1 ;					
									
								}
							}
						}
						goto j3;
					}
					else
					{
						for(int j = 0 ; j < 8 ; j++)
						{
							if( i == aroundKing[0,j] && k == aroundKing[1,j] )
							{	
								aroundKing[2,j] = 1 ;					
								
							}
						}
						
						
					}
					
				}
				
			}
			
		}
		
		
	j3:
			
		for (int i = currentRow-1; i >= 0; i--) {
			
			
			for (int  k = currentColum-1; k >= 0; k--){
				
				if(currentRow - i ==currentColum - k){
					
					if(rows[i].tiles[k].isOccupied)
					{
						if(rows[i].tiles[k].occupyingTeam == team){
							for(int j = 0 ; j < 8 ; j++)
							{
								if( i == aroundKing[0,j] && k == aroundKing[1,j] )
								{	
									aroundKing[2,j] = 1 ;					
									
								}
							}
						}
						goto j4;
					}
					else
					{
						for(int j = 0 ; j < 8 ; j++)
						{
							if( i == aroundKing[0,j] && k == aroundKing[1,j] )
							{	
								aroundKing[2,j] = 1 ;					
								
							}
						}
						
						
					}
				}
				
				
			}
			
			
		}
		
		
		
	j4:
	
		
		
		
	



			
		if(team == "PiecePlayer1"){
			
			aroundKing2= aroundKing;
		}
		else {
			aroundKing1 = aroundKing ;
		}
		
	}


	public void KnightVSAroundKing(int currentRow , int currentColum,string team, GameObject piece )
	{
		currentRow--;
		currentColum--;
		string enemyTeam ;


		int[,] aroundKing; 
		
		
		if(team == "PiecePlayer1"){
			enemyTeam  =  "PiecePlayer2";
			aroundKing = aroundKing2;
		}
		else {
			
			enemyTeam  =  "PiecePlayer1";
			aroundKing = aroundKing1;
		}
		
		
		for(int j = 0 ; j<8 ; j++)
		{
			if(currentRow +2 <= rows.Length-1 && currentColum + 1 <=rows[currentRow].tiles.Length-1 )
			{
				if(rows [currentRow+2].tiles [currentColum + 1].isOccupied && rows [currentRow+2].tiles [currentColum + 1].occupyingTeam == team &&  currentRow+2 == aroundKing[0,j]  && currentColum + 1 == aroundKing[1,j] )
				{
					aroundKing[2,j] = 1 ;
				}
				
			}
			if(currentRow +1 <=rows.Length-1 && currentColum + 2 <=rows[currentRow].tiles.Length-1 ) 
			{
				if(rows [currentRow+1].tiles [currentColum+2].isOccupied && rows [currentRow+1].tiles [currentColum+2].occupyingTeam == team  && currentRow+1 == aroundKing[0,j]  && currentColum + 2 == aroundKing[1,j]  )
				{
					aroundKing[2,j] = 1 ;
				}
			
			}
			if(currentRow -1 >= 0  && currentColum + 2 <=rows[currentRow].tiles.Length-1 )  
			{
				if (rows [currentRow-1].tiles [currentColum+2].isOccupied && rows [currentRow-1].tiles [currentColum+2].occupyingTeam == team && currentRow-1 == aroundKing[0,j]  && currentColum + 2 == aroundKing[1,j] )
				{
					aroundKing[2,j] = 1 ;
				}
				
			}
			
			if(currentRow -2 >= 0 && currentColum + 1 <=rows[currentRow].tiles.Length-1 )
			{
				if(rows [currentRow-2].tiles [currentColum+1].isOccupied && rows [currentRow-2].tiles [currentColum+1].occupyingTeam == team && currentRow-2 == aroundKing[0,j]  && currentColum + 1 == aroundKing[1,j]  )
				{
					aroundKing[2,j] = 1 ;
				}
			}
			if(currentRow -1 >= 0 && currentColum - 2 >=0 ) 
			{
				if (rows [currentRow-1].tiles [currentColum-2].isOccupied && rows [currentRow-1].tiles [currentColum-2].occupyingTeam ==  team  && currentRow-1 == aroundKing[0,j]  && currentColum -2 == aroundKing[1,j] )
				{
					aroundKing[2,j] = 1 ;
				}
			}
			if(currentRow -2 >= 0 && currentColum-1 >= 0 )
			{
				if(rows [currentRow-2].tiles [currentColum-1].isOccupied && rows [currentRow-2].tiles [currentColum-1].occupyingTeam == team && currentRow-2 == aroundKing[0,j]  && currentColum - 1 == aroundKing[1,j] )
				{
					aroundKing[2,j] = 1 ;
				}
				
			}
			if(currentRow +1 <=rows.Length-1  && currentColum-2 >=0 ) 
			{
				if  (rows [currentRow+1].tiles [currentColum-2].isOccupied && rows [currentRow+1].tiles [currentColum-2].occupyingTeam == team && currentRow+1 == aroundKing[0,j]  && currentColum -2 == aroundKing[1,j]  )
				{
					aroundKing[2,j] = 1 ;
				}

			}
			if(currentRow +2 <= rows.Length-1  && currentColum-1 >= 0  )
			{
				if (rows [currentRow+2].tiles [currentColum-1].isOccupied && rows [currentRow+2].tiles [currentColum-1].occupyingTeam == team  && currentRow+2 == aroundKing[0,j]  && currentColum - 1 == aroundKing[1,j] )
				{
					aroundKing[2,j] = 1 ;
				}
			}


			//**************************************************************************************************************************************************************


			if(currentRow +2 <= rows.Length-1 && currentColum + 1 <=rows[currentRow].tiles.Length-1 )
			{
				if(!rows [currentRow+2].tiles [currentColum + 1].isOccupied &&   currentRow+2 == aroundKing[0,j]  && currentColum + 1 == aroundKing[1,j] )
				{
					aroundKing[2,j] = 1 ;
				}
				
			}
			
			
			if(currentRow +1 <=rows.Length-1 && currentColum + 2 <=rows[currentRow].tiles.Length-1 ) 
			{
				if(!rows [currentRow+1].tiles [currentColum+2].isOccupied  && currentRow+1 == aroundKing[0,j]  && currentColum + 2 == aroundKing[1,j]  )
				{
					aroundKing[2,j] = 1 ;

				}
				
			}
			
			
			if(currentRow -1 >= 0  && currentColum + 2 <=rows[currentRow].tiles.Length-1 )  
			{
				if (!rows [currentRow-1].tiles [currentColum+2].isOccupied &&  currentRow-1 == aroundKing[0,j]  && currentColum + 2 == aroundKing[1,j] )
				{
					aroundKing[2,j] = 1 ;
				}
				
			}
			
			
			
			if(currentRow -2 >= 0 && currentColum + 1 <=rows[currentRow].tiles.Length-1 )
			{
				if(!rows [currentRow-2].tiles [currentColum+1].isOccupied && currentRow-2 == aroundKing[0,j]  && currentColum + 1 == aroundKing[1,j]  )
				{
					aroundKing[2,j] = 1 ;
				}
	
			}
			
			
			if(currentRow -1 >= 0 && currentColum - 2 >=0 ) 
			{
				if (!rows [currentRow-1].tiles [currentColum-2].isOccupied  && currentRow-1 == aroundKing[0,j]  && currentColum -2 == aroundKing[1,j] )
				{
					aroundKing[2,j] = 1 ;
				}
		
			}
			
			
			if(currentRow -2 >= 0 && currentColum-1 >= 0 )
			{
				if(!rows [currentRow-2].tiles [currentColum-1].isOccupied && currentRow-2 == aroundKing[0,j]  && currentColum - 1 == aroundKing[1,j] )
				{
					aroundKing[2,j] = 1 ;
				}
		
			}
			
			if(currentRow +1 <=rows.Length-1  && currentColum-2 >=0 ) 
			{
				if  (!rows [currentRow+1].tiles [currentColum-2].isOccupied  && currentRow+1 == aroundKing[0,j]  && currentColum -2 == aroundKing[1,j]  )
				{
					aroundKing[2,j] = 1 ;

				}
			}
			
			
			if(currentRow +2 <= rows.Length-1  && currentColum-1 >= 0  )
			{
				if (!rows [currentRow+2].tiles [currentColum-1].isOccupied   && currentRow+2 == aroundKing[0,j]  && currentColum - 1 == aroundKing[1,j] )
				{
					aroundKing[2,j] = 1 ;
				}
			}



		}




		if(team == "PiecePlayer1"){
			
			aroundKing2= aroundKing;
		}
		else {
			
			aroundKing1 = aroundKing ;
		}
		
		
		
		
	}




	public void PawnVSAroundKing(int currentRow , int currentColum ,string tagName , GameObject piece )
	{
		currentRow--;
		currentColum--;



		
		int[,] aroundKing; 
		
		
		if(tagName == "PiecePlayer1"){
			aroundKing = aroundKing2;
		}
		else {
			
			aroundKing = aroundKing1;
			
		}

		

		if (tagName == "PiecePlayer2") 
		{


			for(int j = 0 ; j<8 ; j++)
			{
				if((currentColum + 1 <=rows[currentRow].tiles.Length-1 &&  currentRow + 1 < rows.Length) &&
				   ( rows [currentRow + 1].tiles [currentColum+1].isOccupied && rows [currentRow + 1].tiles [currentColum+1].occupyingTeam == tagName &&  currentRow + 1  == aroundKing[0,j] && currentColum+1 == aroundKing[1,j]))
				{
					aroundKing[2,j] = 1;
				}
				else if((currentColum + 1 <=rows[currentRow].tiles.Length-1 &&  currentRow + 1 < rows.Length) &&
				   (!rows [currentRow + 1].tiles [currentColum+1].isOccupied  &&  currentRow + 1  == aroundKing[0,j] && currentColum+1 == aroundKing[1,j]))
				{
					aroundKing[2,j] = 1;
				}
			
				if((currentColum-1>=0  && currentRow + 1< rows.Length)  &&
				   ( rows [currentRow + 1].tiles [currentColum-1].isOccupied && rows [currentRow + 1].tiles [currentColum-1].occupyingTeam == tagName &&  currentRow + 1  == aroundKing[0,j] &&  currentColum-1  == aroundKing[1,j] ))
				{
					aroundKing[2,j] = 1;
				}

				else if((currentColum-1>=0  && currentRow + 1< rows.Length)  &&
				   (!rows [currentRow + 1].tiles [currentColum-1].isOccupied  &&  currentRow + 1  == aroundKing[0,j] &&  currentColum-1  == aroundKing[1,j] ))
				{
					aroundKing[2,j] = 1;
				}

			
			
			}
			
		}
		else if (tagName == "PiecePlayer1")
		{
			for(int j = 0 ; j<8 ; j++)
			{
				if((currentColum + 1 <=rows[currentRow].tiles.Length-1 && currentRow - 1 >= 0 )&&
				   ( rows [currentRow - 1].tiles [currentColum+1].isOccupied && rows [currentRow - 1].tiles [currentColum+1].occupyingTeam == tagName &&  currentRow - 1  == aroundKing[0,j] &&  currentColum+1  == aroundKing[1,j] ))
				{
					aroundKing[2,j] = 1;
				}
				else if((currentColum + 1 <=rows[currentRow].tiles.Length-1 && currentRow - 1 >= 0 )&&
				( !rows [currentRow - 1].tiles [currentColum+1].isOccupied &&  currentRow - 1  == aroundKing[0,j] &&  currentColum+1  == aroundKing[1,j] ))
				{
					aroundKing[2,j] = 1;
				}
				if((currentColum-1>=0 && currentRow - 1 >= 0 )  &&
				   ( rows [currentRow - 1].tiles [currentColum-1].isOccupied && rows [currentRow - 1].tiles [currentColum-1].occupyingTeam == tagName  &&  currentRow - 1  == aroundKing[0,j] &&  currentColum-1  == aroundKing[1,j]))
				{				
					aroundKing[2,j] = 1;
				}
				else if((currentColum-1>=0 && currentRow - 1 >= 0 )  &&
				(! rows [currentRow - 1].tiles [currentColum-1].isOccupied  &&  currentRow - 1  == aroundKing[0,j] &&  currentColum-1  == aroundKing[1,j]))
				{				
					aroundKing[2,j] = 1;
				}
			}
			
		}


		
		if(tagName == "PiecePlayer1"){
			
			aroundKing2= aroundKing;
		}
		else {
			
			aroundKing1 = aroundKing ;
		}

		
		
		
	}




	public void DetermineRookThreatPath(string team,int rookRow, int rookColum, int kingRow,int kingColum)
	{
		
		
		if(team == "PiecePlayer1")
		{
			
			if(kingRow == rookRow && kingColum > rookColum)
			{
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingColum-rookColum)-1)]; 
				for(int i = kingColum-1 ; i>rookColum ; i--  )
				{
					if ( j < Mathf.Abs(kingColum-rookColum)-1  )
					{
						container [0,j] = rookRow ; container [1,j] = i;
						rows[rookRow].tiles[i].showThreatPath();
						j++;
						
					}
				}
				threatingPathsToTeam2.Add(container);
				
			}
			else if(kingRow == rookRow && kingColum < rookColum)
			{
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingColum-rookColum)-1)];
				for(int i = kingColum+1 ; i < rookColum ; i++  )
				{
					if ( j < Mathf.Abs(kingColum-rookColum)-1)
					{
						container [0,j] = rookRow ; container [1,j] = i;
						rows[rookRow].tiles[i].showThreatPath();
						j++;
					}
					
				}

				threatingPathsToTeam2.Add(container);
				
			}
			else if(kingColum == rookColum && kingRow > rookRow)
			{
				int j = 0 ;
				int[,] container =  new int[2,(Mathf.Abs(kingRow-rookRow)-1)];
				for(int i = kingRow-1 ; i>rookRow ; i--  )
				{
					if ( j < Mathf.Abs(kingRow-rookRow)-1  )
					{
						container[0,j] = i;   container[1,j] = rookColum;
						rows[i].tiles[rookColum].showThreatPath();
						j++;
					}
					
				}
				threatingPathsToTeam2.Add(container);
				
			}
			else if(kingColum == rookColum && kingRow < rookRow)
			{
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingRow-rookRow)-1)];
				for(int i = kingRow+1 ; i < rookRow ; i++  )
				{
					if ( j < Mathf.Abs(kingRow-rookRow)-1 )
					{
						container [0,j] = i ; container [1,j] = rookColum;
						rows[i].tiles[rookColum].showThreatPath();
						j++;
					}
					
				}
				threatingPathsToTeam2.Add(container);
			}
			
		}
		
		
		
		else 
		{
			if(kingRow == rookRow && kingColum > rookColum)
			{
				int j = 0 ; 
				int[,] container = new int[2,(Mathf.Abs(kingColum-rookColum)-1)]; // new int[2,2]   { { 1, 2 }, { 3, 4 }};
				for(int i = kingColum-1 ; i>rookColum ; i--  )
				{
					if (j < Mathf.Abs(kingColum-rookColum)-1 )
					{
						container [0,j] = rookRow ; container [1,j] = i;
						rows[rookRow].tiles[i].showThreatPath();
						j++;
						
					}
				}
				threatingPathsToTeam1.Add(container);
				
			}
			else if(kingRow == rookRow && kingColum < rookColum)
			{
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingColum-rookColum)-1)];
				for(int i = kingColum+1 ; i < rookColum ; i++  )
				{
					if ( j < Mathf.Abs(kingColum-rookColum)-1 )
					{
						container [0,j] = rookRow ; container [1,j] = i;
						rows[rookRow].tiles[i].showThreatPath();
						j++;
					}
					
				}
				threatingPathsToTeam1.Add(container);
				
			}
			else if(kingColum == rookColum && kingRow > rookRow)
			{
				int j = 0 ;
				int[,] container =  new int[2,(Mathf.Abs(kingRow-rookRow)-1)];
				for(int i = kingRow-1 ; i>rookRow ; i--  )
				{
					if ( j < Mathf.Abs(kingRow-rookRow)-1 )
					{
						container[0,j] = i;   container[1,j] = rookColum;
						rows[i].tiles[rookColum].showThreatPath();
						j++;
					}
					
				}
				threatingPathsToTeam1.Add(container);
				
			}
			else if(kingColum == rookColum && kingRow < rookRow)
			{
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingRow-rookRow)-1)];
				for(int i = kingRow+1 ; i < rookRow ; i++  )
				{
					if ( j < Mathf.Abs(kingRow-rookRow)-1 )
					{
						container [0,j] = i ; container [1,j] = rookColum;
						rows[i].tiles[rookColum].showThreatPath();
						j++;
					}
					
				}
				threatingPathsToTeam1.Add(container);
			}
			
		}
		
		
		
		
		
	}












	
	public void DetermineBishopThreatPath(string team,int bishopRow, int bishopColum, int kingRow,int kingColum)
	{
		int sum = bishopRow + bishopColum;
		
		if(team == "PiecePlayer1")
		{
			//checking if bishop is up right the king
			if(bishopRow > kingRow && bishopColum > kingColum )
			{
				print("from up right");
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingColum-bishopColum)-1)]; 
				
				for(int i = kingRow + 1 ; i< bishopRow ; i++  )
				{
					for (int k = kingColum + 1 ; k < bishopColum ; k++  )
					{
						if(bishopRow - i == bishopColum - k)
						{
							if ( j < Mathf.Abs(kingColum-bishopColum)-1)
							{
								container [0,j] = i ; container [1,j] = k;
								rows[i].tiles[k].showThreatPath();
								j++;
							}
						}
					}
				}
				threatingPathsToTeam2.Add(container);
				
			}
			//checking if bishop is up left the king
			else if(bishopRow > kingRow && bishopColum < kingColum)
			{
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingColum-bishopColum)-1)];
				for(int i = kingRow+1 ; i < bishopRow ; i++  )
				{	
					
					for(int k = kingColum - 1 ; k > bishopColum ; k-- )
					{
						if(i==sum-k)
						{
							if ( j < Mathf.Abs(kingColum-bishopColum)-1)
							{
								container [0,j] = i ; container [1,j] = k;
								rows[i].tiles[k].showThreatPath();
								j++;
							}
						}
					}
				}
				threatingPathsToTeam2.Add(container);
				
			}
			//checking if bishop is down left the king
			else if(bishopRow < kingRow && bishopColum < kingColum)
			{
				int j = 0 ;
				int[,] container =  new int[2,(Mathf.Abs(kingRow-bishopRow)-1)];
				for(int i = kingRow-1 ; i>bishopRow ; i--  )
				{	for(int k = kingColum - 1 ; k > bishopColum ; k--)
					{							
						if(bishopRow - i == bishopColum - k)
						{
							if ( j < Mathf.Abs(kingRow-bishopRow)-1)
							{
								container[0,j] = i;   container[1,j] = k;
								rows[i].tiles[k].showThreatPath();
								j++;
							}
						}
					}
					
				}
				threatingPathsToTeam2.Add(container);
				
			}
			//checking if bishop is down right the king
			else if(bishopRow < kingRow && bishopColum > kingColum)
			{
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingRow-bishopRow)-1)];
				for(int i = kingRow-1 ; i>bishopRow ; i--   )
				{
					for(int k = kingColum + 1 ; k < bishopColum ; k++)
					{
						if(i==sum-k)
						{
							if ( j < Mathf.Abs(kingRow-bishopRow)-1)
							{
								container [0,j] = i ; container [1,j] = k;
								rows[i].tiles[k].showThreatPath();
								j++;
							}
						}
					}
					
				}
				threatingPathsToTeam2.Add(container);
			}
			
		}
		
		
		
		else 
		{
			//checking if bishop is up right the king
			if(bishopRow > kingRow && bishopColum > kingColum )
			{
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingColum-bishopColum)-1)]; 
				
				for(int i = kingRow + 1 ; i< bishopRow ; i++  )
				{
					for (int k = kingColum + 1 ; k < bishopColum ; k++  )
					{
						if(bishopRow - i == bishopColum - k)
						{
							if ( j < Mathf.Abs(kingColum-bishopColum)-1 )
							{
								container [0,j] = i ; container [1,j] = k;
								rows[i].tiles[k].showThreatPath();
								j++;
								
							}
						}
					}
				}
				threatingPathsToTeam1.Add(container);
				
			}
			//checking if bishop is up left the king
			else if(bishopRow > kingRow && bishopColum < kingColum)
			{
				print("from up left");
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingColum-bishopColum)-1)];
				for(int i = kingRow+1 ; i < bishopRow ; i++  )
				{	
					
					for(int k = kingColum - 1 ; k > bishopColum ; k-- )
					{
						if(i==sum-k)
						{
							if ( j < Mathf.Abs(kingColum-bishopColum)-1)
							{
								container [0,j] = i ; container [1,j] = k;
								rows[i].tiles[k].showThreatPath();
								j++;
							}
						}
					}
				}
				threatingPathsToTeam1.Add(container);
				
			}
			//checking if bishop is down left the king
			else if(bishopRow < kingRow && bishopColum < kingColum)
			{
				print("from down left");
				int j = 0 ;
				int[,] container =  new int[2,(Mathf.Abs(kingRow-bishopRow)-1)];
				for(int i = kingRow-1 ; i>bishopRow ; i--  )
				{	for(int k = kingColum - 1 ; k > bishopColum ; k--)
					{							
						if(bishopRow - i == bishopColum - k)
						{
							if ( j < Mathf.Abs(kingRow-bishopRow)-1 )
							{
								container[0,j] = i;   container[1,j] = k;
								rows[i].tiles[k].showThreatPath();
								j++;
							}
						}
					}
					
				}
				threatingPathsToTeam1.Add(container);
				
			}
			//checking if bishop is down right the king
			else if(bishopRow < kingRow && bishopColum > kingColum)
			{
				print("from down right");
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingRow-bishopRow)-1)];
				for(int i = kingRow-1 ; i>bishopRow ; i--   )
				{
					for(int k = kingColum + 1 ; k < bishopColum ; k++)
					{
						if(i==sum-k)
						{
							if ( j < Mathf.Abs(kingRow-bishopRow)-1 )
							{
								container [0,j] = i ; container [1,j] = k;
								rows[i].tiles[k].showThreatPath();
								j++;
							}
						}
					}
					
				}
				threatingPathsToTeam1.Add(container);
			}
			
		}
		
	}



	public void rookVsCastlingTiles (string team,int rookRow, int rookColum)
	{

		rookRow --;
		rookColum --;

		if(team == "PiecePlayer1")
		{
			for(int k = 0 ; k < 5 ; k++)
			{
				if(king2CastlingTiles[1,k] == rookColum )
				{
					for(int i = rookRow -1  ; i >= 0   ; i--)
					{
						if(rows[i].tiles[rookColum].isOccupied)
							goto r1;

						if ( king2CastlingTiles[0,k] == i )
						{
							king2CastlingTiles[2,k] = 0;
						}


					}
				}

			}
			r1:
			if(true){}
		}



		else
		{
			for(int k = 0 ; k < 5 ; k++)
			{
				if(king1CastlingTiles[1,k] == rookColum )
				{
					for(int i = rookRow +1  ; i <= 7   ; i++  )
					{
						if(rows[i].tiles[rookColum].isOccupied)
							goto r2;


						if ( king1CastlingTiles[0,k] == i )
						{
							king1CastlingTiles[2,k] = 0;
						}

					}
				}

			}
			r2:
			if(true){}
		}



	}





	public void bishopVsCastlingTiles(string team,int bishopRow, int bishopColum)
	{
		bishopRow --;
		bishopColum --;
		int sum = bishopRow + bishopColum;

		if(team == "PiecePlayer1")
		{
			for(int j= 0 ; j < 5 ; j++)
			{
				for(int i = bishopRow-1 ; i>=0 ; i-- )
					{	
					for(int k = bishopColum - 1 ; k >=0 ; k--)
						{							
							if(bishopRow - i == bishopColum - k)
							{
								if(rows[i].tiles[k].isOccupied)
								{goto b1;}

								if (i == king2CastlingTiles[0,j] && k == king2CastlingTiles[1,j] )
								{
									king2CastlingTiles[2,j] = 0;
								}
							}
						}

					}
			}

			b1:
				
				for(int j= 0 ; j < 5 ; j++)
				{
					for(int i = bishopRow-1 ; i>=0 ; i-- )
					{	
						for(int k = bishopColum + 1 ; k <= 7 ; k++)
						{	
							if(i==sum-k)
							{
								if(rows[i].tiles[k].isOccupied)
								{goto b2;}

								if (i == king2CastlingTiles[0,j] && k == king2CastlingTiles[1,j] )
								{
									king2CastlingTiles[2,j] = 0;
								}
							}
						}

					}
				}
				
			b2:
			if(true){}
		}

		else 
		{
			for(int j= 0 ; j < 5 ; j++)
			{
				for(int i = bishopRow+1 ; i<=7 ; i++ )
				{	
					for(int k = bishopColum - 1 ; k >=0 ; k--)
					{							
						if(bishopRow - i == bishopColum - k)
						{
							if(rows[i].tiles[k].isOccupied)
							{goto b3;}

							if (i == king1CastlingTiles[0,j] && k == king1CastlingTiles[1,j] )
							{
								king1CastlingTiles[2,j] = 0;
							}
						}
					}

				}
			}

			b3:

			for(int j= 0 ; j < 5 ; j++)
			{
				for(int i = bishopRow-1 ; i>=0 ; i-- )
				{	
					for(int k = bishopColum + 1 ; k <= 7 ; k++)
					{	
						if(i==sum-k)
						{
							if(rows[i].tiles[k].isOccupied)
							{goto b4;}

							if (i == king1CastlingTiles[0,j] && k == king1CastlingTiles[1,j] )
							{
								king1CastlingTiles[2,j] = 0;
							}
						}
					}

				}
			}

			b4:
			if(true){}

		}

	}



	public void queenVsCastlingTiles(string team,int queenRow, int queenColum)
	{
		queenRow --;
		queenColum --;
		int sum = queenRow + queenColum;

		if(team == "PiecePlayer1")
		{
			for(int k = 0 ; k < 5 ; k++)
			{
				if(king2CastlingTiles[1,k] == queenColum )
				{
					for(int i = queenRow -1  ; i >= 0   ; i--)
					{
						if(rows[i].tiles[queenColum].isOccupied)
							goto r1;

						if ( king2CastlingTiles[0,k] == i )
						{
							king2CastlingTiles[2,k] = 0;
						}


					}
				}

			}
			r1:

			for(int j= 0 ; j < 5 ; j++)
			{
				for(int i = queenRow-1 ; i>=0 ; i-- )
				{	
					for(int k = queenColum - 1 ; k >=0 ; k--)
					{							
						if(queenRow - i == queenColum - k)
						{
							if(rows[i].tiles[k].isOccupied)
							{goto b1;}

							if (i == king2CastlingTiles[0,j] && k == king2CastlingTiles[1,j] )
							{
								king2CastlingTiles[2,j] = 0;
							}
						}
					}

				}
			}

			b1:

			for(int j= 0 ; j < 5 ; j++)
			{
				for(int i = queenRow-1 ; i>=0 ; i-- )
				{	
					for(int k = queenColum + 1 ; k <= 7 ; k++)
					{	
						if(i==sum-k)
						{
							if(rows[i].tiles[k].isOccupied)
							{goto b2;}

							if (i == king2CastlingTiles[0,j] && k == king2CastlingTiles[1,j] )
							{
								king2CastlingTiles[2,j] = 0;
							}
						}
					}

				}
			}

			b2:
			if(true){}

		}

		else
		{
			for(int k = 0 ; k < 5 ; k++)
			{
				if(king1CastlingTiles[1,k] == queenColum )
				{
					for(int i = queenRow +1  ; i <= 7   ; i++  )
					{
						if(rows[i].tiles[queenColum].isOccupied)
							goto r2;


						if ( king1CastlingTiles[0,k] == i )
						{
							king1CastlingTiles[2,k] = 0;
						}

					}
				}

			}
			r2:


			for(int j= 0 ; j < 5 ; j++)
			{
				for(int i = queenRow+1 ; i<=7 ; i++ )
				{	
					for(int k = queenColum - 1 ; k >=0 ; k--)
					{							
						if(queenRow - i == queenColum - k)
						{
							if(rows[i].tiles[k].isOccupied)
							{goto b3;}

							if (i == king1CastlingTiles[0,j] && k == king1CastlingTiles[1,j] )
							{
								king1CastlingTiles[2,j] = 0;
							}
						}
					}

				}
			}

			b3:

			for(int j= 0 ; j < 5 ; j++)
			{
				for(int i = queenRow-1 ; i>=0 ; i-- )
				{	
					for(int k = queenColum + 1 ; k <= 7 ; k++)
					{	
						if(i==sum-k)
						{
							if(rows[i].tiles[k].isOccupied)
							{goto b4;}

							if (i == king1CastlingTiles[0,j] && k == king1CastlingTiles[1,j] )
							{
								king1CastlingTiles[2,j] = 0;
							}
						}
					}

				}
			}

			b4:
			if(true){}

		}


	}



	public void knightVsCastlingTiles(string team,int currentRow, int currentColum)
	{
		currentRow--;
		currentColum--;

		if(team == "PiecePlayer1")
		{
			
			for(int j= 0 ; j < 5 ; j++)
			{
	
				if(currentRow +2 <= rows.Length-1 && currentColum + 1 <=rows[currentRow].tiles.Length-1 )

				{
					if(!rows [currentRow+2].tiles [currentColum + 1].isOccupied  && king2CastlingTiles[0,j] == currentRow+2  && king2CastlingTiles[1,j] == currentColum + 1 )
					{
						king2CastlingTiles[2,j] = 0;
					}
				}

				if(currentRow +1 <=rows.Length-1 && currentColum + 2 <=rows[currentRow].tiles.Length-1 ) 
				{
					if(!rows [currentRow+1].tiles [currentColum+2].isOccupied && king2CastlingTiles[0,j] == currentRow+1 && king2CastlingTiles[1,j] == currentColum+2)
					{}
				}


				if(currentRow -1 >= 0  && currentColum + 2 <=rows[currentRow].tiles.Length-1 )  
				{
					if(!rows [currentRow-1].tiles [currentColum+2].isOccupied && king2CastlingTiles[0,j] == currentRow-1 && king2CastlingTiles[1,j] == currentColum+2)
					{
						king2CastlingTiles[2,j] = 0;
					}
			
				}



				if(currentRow -2 >= 0 && currentColum + 1 <=rows[currentRow].tiles.Length-1 )
				{
					if(!rows [currentRow-2].tiles [currentColum+1].isOccupied && king2CastlingTiles[0,j] == currentRow-2 && king2CastlingTiles[1,j] == currentColum+1)
					{
						king2CastlingTiles[2,j] = 0;
					}
			
				}


				if(currentRow -1 >= 0 && currentColum - 2 >=0 ) 
				{
					if(!rows [currentRow-1].tiles [currentColum-2].isOccupied && king2CastlingTiles[0,j] == currentRow-1 && king2CastlingTiles[1,j] == currentColum-2)
					{
						king2CastlingTiles[2,j] = 0;
					}
			
				}


				if(currentRow -2 >= 0 && currentColum-1 >= 0 )
				{
					if(!rows [currentRow-2].tiles [currentColum-1].isOccupied && king2CastlingTiles[0,j] == currentRow-2 && king2CastlingTiles[1,j] == currentColum-1)
					{
						king2CastlingTiles[2,j] = 0;
					}
						

				}

				if(currentRow +1 <=rows.Length-1  && currentColum-2 >=0 ) 
				{
					if (!rows [currentRow+1].tiles [currentColum-2].isOccupied && king2CastlingTiles[0,j] == currentRow+1 && king2CastlingTiles[1,j] == currentColum-2)
					{
						king2CastlingTiles[2,j] = 0;
					}

				}


				if(currentRow +2 <= rows.Length-1  && currentColum-1 >= 0  )

				{
					if(!rows [currentRow+2].tiles [currentColum-1].isOccupied && king2CastlingTiles[0,j] == currentRow+2 && king2CastlingTiles[1,j] == currentColum-1)
					{
						king2CastlingTiles[2,j] = 0;
					}

				}
			}
		}

		else 
		{
			
			for(int j= 0 ; j < 5 ; j++)
			{

				if(currentRow +2 <= rows.Length-1 && currentColum + 1 <=rows[currentRow].tiles.Length-1 )
				{
					if(!rows [currentRow+2].tiles [currentColum + 1].isOccupied  && king1CastlingTiles[0,j] == currentRow+2  && king1CastlingTiles[1,j] == currentColum + 1 )
					{
						king1CastlingTiles[2,j] = 0;
					}
				}

				if(currentRow +1 <=rows.Length-1 && currentColum + 2 <=rows[currentRow].tiles.Length-1 ) 
				{
					if(!rows [currentRow+1].tiles [currentColum+2].isOccupied && king1CastlingTiles[0,j] == currentRow+1 && king1CastlingTiles[1,j] == currentColum+2)
					{
						king1CastlingTiles[2,j] = 0;
					}
				}


				if(currentRow -1 >= 0  && currentColum + 2 <=rows[currentRow].tiles.Length-1 )  
				{
					if(!rows [currentRow-1].tiles [currentColum+2].isOccupied && king1CastlingTiles[0,j] == currentRow-1 && king1CastlingTiles[1,j] == currentColum+2)
					{
						king1CastlingTiles[2,j] = 0;
					}
				}



				if(currentRow -2 >= 0 && currentColum + 1 <=rows[currentRow].tiles.Length-1 )
				{
					if(!rows [currentRow-2].tiles [currentColum+1].isOccupied && king1CastlingTiles[0,j] == currentRow-2 && king1CastlingTiles[1,j] == currentColum+1)
					{
						king1CastlingTiles[2,j] = 0;
					}
				}


				if(currentRow -1 >= 0 && currentColum - 2 >=0 ) 
				{
					if(!rows [currentRow-1].tiles [currentColum-2].isOccupied && king1CastlingTiles[0,j] == currentRow-1 && king1CastlingTiles[1,j] == currentColum-2)
					{
						king1CastlingTiles[2,j] = 0;
					}
				}


				if(currentRow -2 >= 0 && currentColum-1 >= 0 )
				{
					if(!rows [currentRow-2].tiles [currentColum-1].isOccupied && king1CastlingTiles[0,j] == currentRow-2 && king1CastlingTiles[1,j] == currentColum-1)
					{
						king1CastlingTiles[2,j] = 0;
					}
				}

				if(currentRow +1 <=rows.Length-1  && currentColum-2 >=0 ) 
				{
					if (!rows [currentRow+1].tiles [currentColum-2].isOccupied && king1CastlingTiles[0,j] == currentRow+1 && king1CastlingTiles[1,j] == currentColum-2)
					{
						king1CastlingTiles[2,j] = 0;
					}
				}


				if(currentRow +2 <= rows.Length-1  && currentColum-1 >= 0  )

				{
					if(!rows [currentRow+2].tiles [currentColum-1].isOccupied && king1CastlingTiles[0,j] == currentRow+2 && king1CastlingTiles[1,j] == currentColum-1)
					{
						king1CastlingTiles[2,j] = 0;
					}
				}
			}

		}


	}



	public void pawnVsCastlingTiles(string tagName,int currentRow, int currentColum)
	{
		currentRow--;
		currentColum--;

		if (tagName == "PiecePlayer2") 
		{
			for(int j = 0 ; j<5 ; j++)
			{
				if( (currentRow + 1 < rows.Length) &&
					(!rows [currentRow + 1].tiles [currentColum].isOccupied  &&  currentRow + 1  == king1CastlingTiles[0,j] && currentColum == king1CastlingTiles[1,j]))
				{
					king1CastlingTiles[2,j] = 0;
				}
			}
		}
		else 
		{
			for(int j = 0 ; j<5 ; j++)
			{
				if( (currentRow - 1 >= 0) &&
					(!rows [currentRow - 1].tiles [currentColum].isOccupied  &&  currentRow - 1  == king2CastlingTiles[0,j] && currentColum == king2CastlingTiles[1,j]))
				{
					king2CastlingTiles[2,j] = 0;
				}
			}
		}

	}



	public void kingVsCastlingTiles(string team,int currentRow, int currentColum)
	{
		currentRow--;
		currentColum--;

		if (team == "PiecePlayer2") 
		{
			for(int j = 0 ; j<5 ; j++)
			{
				if( (currentRow + 1 < rows.Length) &&
					(!rows [currentRow + 1].tiles [currentColum].isOccupied  &&  currentRow + 1  == king1CastlingTiles[0,j] && currentColum == king1CastlingTiles[1,j]))
				{
					king1CastlingTiles[2,j] = 0;
				}
				else if( (currentRow + 1 < rows.Length && currentColum + 1 < rows.Length) &&
					(!rows [currentRow + 1].tiles [currentColum + 1].isOccupied  &&  currentRow + 1  == king1CastlingTiles[0,j] && currentColum + 1 == king1CastlingTiles[1,j]))
				{
					king1CastlingTiles[2,j] = 0;
				}
				else if( (currentRow + 1 < rows.Length && currentColum - 1 >= 0) &&
					(!rows [currentRow + 1].tiles [currentColum - 1].isOccupied  &&  currentRow + 1  == king1CastlingTiles[0,j] && currentColum - 1 == king1CastlingTiles[1,j]))
				{
					king1CastlingTiles[2,j] = 0;
				}
			}
		}
		else 
		{
			for(int j = 0 ; j<5 ; j++)
			{
				if( (currentRow - 1 >= 0) &&
					(!rows [currentRow - 1].tiles [currentColum].isOccupied  &&  currentRow - 1  == king2CastlingTiles[0,j] && currentColum == king2CastlingTiles[1,j]))
				{
					king2CastlingTiles[2,j] = 0;
				}
				else if( (currentRow - 1  >= 0 && currentColum + 1 < rows.Length) &&
					(!rows [currentRow - 1].tiles [currentColum + 1].isOccupied  &&  currentRow - 1  == king1CastlingTiles[0,j] && currentColum + 1 == king1CastlingTiles[1,j]))
				{
					king2CastlingTiles[2,j] = 0;
				}
				else if( (currentRow - 1  >= 0 && currentColum - 1 >= 0) &&
					(!rows [currentRow - 1].tiles [currentColum - 1].isOccupied  &&  currentRow - 1  == king1CastlingTiles[0,j] && currentColum - 1 == king1CastlingTiles[1,j]))
				{
					king2CastlingTiles[2,j] = 0;
				}
			}
		}

	}



	public void DetermineQueenThreatPath(string team,int queenRow, int queenColum, int kingRow,int kingColum)
	{
		
		int sum = queenRow + queenColum;
		
		if(team == "PiecePlayer1")
		{


			if(kingRow == queenRow && kingColum > queenColum)
			{
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingColum-queenColum)-1)]; 
				for(int i = kingColum-1 ; i>queenColum ; i--  )
				{
					if ( j < Mathf.Abs(kingColum-queenColum)-1)
					{
						container [0,j] = queenRow ; container [1,j] = i;
						rows[queenRow].tiles[i].showThreatPath();
						j++;
					}
				}
				threatingPathsToTeam2.Add(container);
				
			}
			else if(kingRow == queenRow && kingColum < queenColum)
			{
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingColum-queenColum)-1)];
				for(int i = kingColum+1 ; i < queenColum ; i++  )
				{
					if ( j < Mathf.Abs(kingColum-queenColum)-1)
					{
						container [0,j] = queenRow ; container [1,j] = i;
						rows[queenRow].tiles[i].showThreatPath();
						j++;
					}
					
				}
				threatingPathsToTeam2.Add(container);
				
			}
			else if(kingColum == queenColum && kingRow > queenRow)
			{
				int j = 0 ;
				int[,] container =  new int[2,(Mathf.Abs(kingRow-queenRow)-1)];
				for(int i = kingRow-1 ; i>queenRow ; i--  )
				{
					if ( j < Mathf.Abs(kingRow-queenRow)-1 )
					{
						container[0,j] = i;   container[1,j] = queenColum;
						rows[i].tiles[queenColum].showThreatPath();
						j++;
					}
					
				}
				threatingPathsToTeam2.Add(container);
				
			}
			else if(kingColum == queenColum && kingRow < queenRow)
			{
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingRow-queenRow)-1)];
				for(int i = kingRow+1 ; i < queenRow ; i++  )
				{
					if ( j < Mathf.Abs(kingRow-queenRow)-1 )
					{
						container [0,j] = i ; container [1,j] = queenColum;
						rows[i].tiles[queenColum].showThreatPath();
						j++;
					}
					
				}
				threatingPathsToTeam2.Add(container);
			}
			
			
			//checking if queen is up right the king
			else if(queenRow > kingRow && queenColum > kingColum )
			{
				print("from up right");
				int j = 0 ; 
				int[,] container = new int[2,(Mathf.Abs(kingColum-queenColum)-1)]; 
				
				for(int i = kingRow + 1 ; i< queenRow ; i++  )
				{
					for (int k = kingColum + 1 ; k < queenColum ; k++  )
					{
						if(queenRow - i == queenColum - k)
						{
							if (j < Mathf.Abs(kingColum-queenColum)-1)
							{
								container [0,j] = i ; container [1,j] = k;
								rows[i].tiles[k].showThreatPath();
								j++;
							}
						}
					}
				}
				threatingPathsToTeam2.Add(container);
				
			}
			//checking if queen is up left the king
			else if(queenRow > kingRow && queenColum < kingColum)
			{
				print("from up left");
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingColum-queenColum)-1)];
				for(int i = kingRow+1 ; i < queenRow ; i++  )
				{	
					
					for(int k = kingColum - 1 ; k > queenColum ; k-- )
					{
						if(i==sum-k)
						{
							if ( j < Mathf.Abs(kingColum-queenColum)-1 )
							{
								container [0,j] = i ; container [1,j] = k;
								rows[i].tiles[k].showThreatPath();
								j++;
							}
						}
					}
				}
				threatingPathsToTeam2.Add(container);
				
			}
			//checking if queen is down left the king
			else if(queenRow < kingRow && queenColum < kingColum)
			{
				print("from down left");
				int j = 0 ;
				int[,] container =  new int[2,(Mathf.Abs(kingRow-queenRow)-1)];
				for(int i = kingRow-1 ; i>queenRow ; i--  )
				{	for(int k = kingColum - 1 ; k > queenColum ; k--)
					{							
						if(queenRow - i == queenColum - k)
						{
							if( j < Mathf.Abs(kingRow-queenRow)-1)
							{
								container[0,j] = i;   container[1,j] = k;
								rows[i].tiles[k].showThreatPath();
								j++;
							}
						}
					}
					
				}
				threatingPathsToTeam2.Add(container);
				
			}
			//checking if queen is down right the king
			else if(queenRow < kingRow && queenColum > kingColum)
			{
				print("from down right");
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingRow-queenRow)-1)];
				for(int i = kingRow-1 ; i>queenRow ; i--   )
				{
					for(int k = kingColum + 1 ; k < queenColum ; k++)
					{
						if(i==sum-k)
						{
							if ( j < Mathf.Abs(kingRow-queenRow)-1)
							{
								container [0,j] = i ; container [1,j] = k;
								rows[i].tiles[k].showThreatPath();
								j++;
							}
						}
					}
					
				}
				threatingPathsToTeam2.Add(container);
			}
			
		}
		
		
		
		else 
		{
			if(kingRow == queenRow && kingColum > queenColum)
			{
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingColum-queenColum)-1)];
				for(int i = kingColum-1 ; i>queenColum ; i--  )
				{
					if ( j < Mathf.Abs(kingColum-queenColum)-1 )
					{
						container [0,j] = queenRow ; container [1,j] = i;
						rows[queenRow].tiles[i].showThreatPath();
						j++;
						
					}
				}
				threatingPathsToTeam1.Add(container);
				
			}
			else if(kingRow == queenRow && kingColum < queenColum)
			{
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingColum-queenColum)-1)];
				for(int i = kingColum+1 ; i < queenColum ; i++  )
				{
					if ( j < Mathf.Abs(kingColum-queenColum)-1 )
					{
						container [0,j] = queenRow ; container [1,j] = i;
						rows[queenRow].tiles[i].showThreatPath();
						j++;
					}
					
				}
				threatingPathsToTeam1.Add(container);
				
			}
			else if(kingColum == queenColum && kingRow > queenRow)
			{
				int j = 0 ;
				int[,] container =  new int[2,(Mathf.Abs(kingRow-queenRow)-1)];
				for(int i = kingRow-1 ; i>queenRow ; i--  )
				{
					if ( j < Mathf.Abs(kingRow-queenRow)-1)
					{
						container[0,j] = i;   container[1,j] = queenColum;
						rows[i].tiles[queenColum].showThreatPath();
						j++;
					}
					
				}
				threatingPathsToTeam1.Add(container);
				
			}
			else if(kingColum == queenColum && kingRow < queenRow)
			{
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingRow-queenRow)-1)];
				for(int i = kingRow+1 ; i < queenRow ; i++  )
				{
					if ( j < Mathf.Abs(kingRow-queenRow)-1 )
					{
						container [0,j] = i ; container [1,j] = queenColum;
						rows[i].tiles[queenColum].showThreatPath();
						j++;
					}
					
				}
				threatingPathsToTeam1.Add(container);
			}
			
			//checking if queen is up right the king
			else if(queenRow > kingRow && queenColum > kingColum )
			{
				print("from up right");
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingColum-queenColum)-1)]; 
				
				for(int i = kingRow + 1 ; i< queenRow ; i++  )
				{
					for (int k = kingColum + 1 ; k < queenColum ; k++  )
					{
						if(queenRow - i == queenColum - k)
						{
							if ( j < Mathf.Abs(kingColum-queenColum)-1)
							{
								container [0,j] = i ; container [1,j] = k;
								rows[i].tiles[k].showThreatPath();
								j++;
								
							}
						}
					}
				}
				threatingPathsToTeam1.Add(container);
				
			}
			//checking if queen is up left the king
			else if(queenRow > kingRow && queenColum < kingColum)
			{
				print("from up left");
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingColum-queenColum)-1)];
				for(int i = kingRow+1 ; i < queenRow ; i++  )
				{	
					
					for(int k = kingColum - 1 ; k > queenColum ; k-- )
					{
						if(i==sum-k)
						{
							if ( j < Mathf.Abs(kingColum-queenColum)-1)
							{
								container [0,j] = i ; container [1,j] = k;
								rows[i].tiles[k].showThreatPath();
								j++;
							}
						}
					}
				}
				threatingPathsToTeam1.Add(container);
				
			}
			//checking if queen is down left the king
			else if(queenRow < kingRow && queenColum < kingColum)
			{
				print("from down left");
				int j = 0 ; 
				int[,] container =  new int[2,(Mathf.Abs(kingRow-queenRow)-1)];
				for(int i = kingRow-1 ; i>queenRow ; i--  )
				{	for(int k = kingColum - 1 ; k > queenColum ; k--)
					{							
						if(queenRow - i == queenColum - k)
						{
							if(j < Mathf.Abs(kingRow-queenRow)-1)
							{
								container[0,j] = i;   container[1,j] = k;
								rows[i].tiles[k].showThreatPath();
								j++;
							}
						}
					}
					
				}
				threatingPathsToTeam1.Add(container);
				
			}
			//checking if queen is down right the king
			else if(queenRow < kingRow && queenColum > kingColum)
			{
				print("from down right");
				int j = 0 ;
				int[,] container = new int[2,(Mathf.Abs(kingRow-queenRow)-1)];
				for(int i = kingRow-1 ; i>queenRow ; i--   )
				{
					for(int k = kingColum + 1 ; k < queenColum ; k++)
					{
						if(i==sum-k)
						{
							if ( j < Mathf.Abs(kingRow-queenRow)-1)
							{
								container [0,j] = i ; container [1,j] = k;
								rows[i].tiles[k].showThreatPath();
								j++;
							}
						}
					}
					
				}
				threatingPathsToTeam1.Add(container);
			}
			
		}
		
		
	}













public void rookVSThreatingPath (int currentRow,int currentColum,string team)
	{
		currentRow--;
		currentColum--;

		if(team == "PiecePlayer1")
		{
			for(int j = 0 ; j <threatingPathsToTeam1.Count;j++)
			{

				for(int m = 0 ; m <threatingPathsToTeam1[j].Length/2;m++)
				{

					for (int  k = currentColum +1; k < rows.Length ; k++)
				{

					if(rows[currentRow].tiles[k].isOccupied){goto r2; }
					if(threatingPathsToTeam1[j][0,m] == currentRow && threatingPathsToTeam1[j][1,m] == k )
					{

							rows[currentRow].tiles[k].showIntersectionTile();
							intersectionRow.Add(currentRow) ; intersectionColum.Add(k);
							threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
							threatToTeam1.Remove(threatToTeam1[j]);
							goto x1;
						}
				}
				
			r2:
					
					
				for (int i = currentRow -1; i >= 0  ; i--) 
				{
						if(rows[i].tiles[currentColum].isOccupied){goto r3;}
						if(threatingPathsToTeam1[j][0,m] == i && threatingPathsToTeam1[j][1,m] == currentColum)
						{
							rows[i].tiles[currentColum].showIntersectionTile();
							intersectionRow.Add(i) ; intersectionColum.Add(currentColum);
							threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
							threatToTeam1.Remove(threatToTeam1[j]);
							goto x1;
						}
				}
				
			r3:
					
					
					
					
				for (int i = currentRow +1 ; i < rows.Length; i++) 
				{
						if(rows[i].tiles[currentColum].isOccupied){goto r1;}
						if(threatingPathsToTeam1[j][0,m] == i && threatingPathsToTeam1[j][1,m] == currentColum)
						{						
							rows[i].tiles[currentColum].showIntersectionTile();
							intersectionRow.Add(i) ; intersectionColum.Add(currentColum);
							threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
							threatToTeam1.Remove(threatToTeam1[j]);
							goto x1;
						}
				}
				
			r1:
					
					
				for (int  k = currentColum - 1; k >= 0; k--) 
				{
						if(rows[currentRow].tiles[k].isOccupied){goto r4; }
						if( threatingPathsToTeam1[j][0,m] == currentRow && threatingPathsToTeam1[j][1,m] == k)
						{					
							rows[currentRow].tiles[k].showIntersectionTile();
							intersectionRow.Add(currentRow) ; intersectionColum.Add(k);
							threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
							threatToTeam1.Remove(threatToTeam1[j]);
							goto x1;
						}
				}
				
				
				
			r4:
				if(true){}
				
			}
			}

		x1:
			if(true){}
		}

		else 
		{

			for(int j = 0 ; j <threatingPathsToTeam2.Count;j++)
			{

//				print(" j :" + j);
			

				for(int m = 0 ; m <threatingPathsToTeam2[j].Length/2;m++)
				{

//					print("number of tiles in this threating path  " + threatingPathsToTeam2[j].Length/2);
//					print("m : " + m);
//					print(container[0,m]); 
//					print(container[1,m]);

					for (int  k = currentColum +1; k < rows.Length ; k++)
					{
						
						if(rows[currentRow].tiles[k].isOccupied){goto r2; }
						if( threatingPathsToTeam2[j][0,m] == currentRow && threatingPathsToTeam2[j][1,m] == k )
						{					
							print("the condtion has been meet");
							rows[currentRow].tiles[k].showIntersectionTile();
							intersectionRow.Add(currentRow) ; intersectionColum.Add(k);
							threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
							threatToTeam2.Remove(threatToTeam2[j]);
							goto x2;							
						}
					}
					
				r2:
						
						
					for (int i = currentRow -1; i >= 0  ; i--) 
					{
						if(rows[i].tiles[currentColum].isOccupied){goto r3;}
						if(threatingPathsToTeam2[j][0,m] == i && threatingPathsToTeam2[j][1,m] == currentColum)
						{
							rows[i].tiles[currentColum].showIntersectionTile();
							intersectionRow.Add(i) ; intersectionColum.Add(currentColum);
							threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
							threatToTeam2.Remove(threatToTeam2[j]);
							goto x2;
						}
					}
					
				r3:
						
						
						
						
					for (int i = currentRow +1 ; i < rows.Length; i++) {

						if(rows[i].tiles[currentColum].isOccupied){goto r1;}
						if( threatingPathsToTeam2[j][0,m] == i && threatingPathsToTeam2[j][1,m] == currentColum)
						{						
							rows[i].tiles[currentColum].showIntersectionTile();
							intersectionRow.Add(i) ; intersectionColum.Add(currentColum);
							threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
//							print("threatToTeam2   :  " +threatToTeam2.Count);
							threatToTeam2.Remove(threatToTeam2[j]);
//							print("threatToTeam2   :  " +threatToTeam2.Count);
							goto x2;
							
						}
					}
					
				r1:
						
						
					for (int  k = currentColum - 1; k >= 0; k--) {
						if(rows[currentRow].tiles[k].isOccupied){goto r4; }
						if( threatingPathsToTeam2[j][0,m] == currentRow && threatingPathsToTeam2[j][1,m] == k)
						{						

							rows[currentRow].tiles[k].showIntersectionTile();
							intersectionRow.Add(currentRow) ; intersectionColum.Add(k);
							threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
							threatToTeam2.Remove(threatToTeam2[j]);
							goto x2;
						}
					}
										
				r4:
					if(true){}
				}
				
			}



		x2:
			if(true){}
		}



		



	}


	public void bishopVSThreatingPath (int currentRow,int currentColum,string team)
	{
		currentRow--;
		currentColum--;
		int sum = currentRow + currentColum;

		
		
		if(team == "PiecePlayer1")
		{
			for(int j = 0 ; j <threatingPathsToTeam1.Count;j++)
			{

				for(int m = 0 ; m <threatingPathsToTeam1[j].Length/2;m++)
				{
					for (int i = currentRow +1; i < rows.Length; i++) {
						
						for (int  k = currentColum+1; k < rows[i].tiles.Length; k++) {
							
							if(currentRow - i == currentColum - k)
							{	
									if(rows[i].tiles[k].isOccupied ){goto j1;}
								  if( threatingPathsToTeam1[j][0,m] == i && threatingPathsToTeam1[j][1,m] == k)
								{
								
									rows[i].tiles[k].showIntersectionTile();
									intersectionRow.Add(i) ; intersectionColum.Add(k);
									print("bishop intersected threat path");
									threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
									threatToTeam1.Remove(threatToTeam1[j]);
									goto x1;
								}
								
							}
						}
					}
					
				j1:
						
					for (int i = currentRow+ 1; i < rows.Length; i++) {
						
						for (int  k = currentColum -1 ; k >= 0; k--) {
							
							if(i==sum-k)
							{
									if(rows[i].tiles[k].isOccupied ){goto j2;}
								 if(  threatingPathsToTeam1[j][0,m] == i && threatingPathsToTeam1[j][1,m] == k)
								{

										rows[i].tiles[k].showIntersectionTile();
										intersectionRow.Add(i) ; intersectionColum.Add(k);
										print("bishop intersected threat path");
										threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
										threatToTeam1.Remove(threatToTeam1[j]);

									goto x1;

								}
								
							}
							
						}
						
					}
					
					
				j2:
					for (int i = currentRow -1 ; i >= 0; i--) {
						
						for (int  k = currentColum +1; k < rows[i].tiles.Length; k++){
							
							if(i==sum-k)
							{
									if(rows[i].tiles[k].isOccupied ){goto j3;}
								 if( threatingPathsToTeam1[j][0,m] == i && threatingPathsToTeam1[j][1,m] == k)
								{

									rows[i].tiles[k].showIntersectionTile();
									intersectionRow.Add(i) ; intersectionColum.Add(k);
									threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
									threatToTeam1.Remove(threatToTeam1[j]);
									goto x1;
								}
								
							}
							
						}
						
					}
					
					
				j3:
						
					for (int i = currentRow-1; i >= 0; i--) {
						
						for (int  k = currentColum-1; k >= 0; k--){
							
							if(currentRow - i ==currentColum - k)
							{
									if(rows[i].tiles[k].isOccupied ){goto j4;}
								 if( threatingPathsToTeam1[j][0,m] == i && threatingPathsToTeam1[j][1,m] == k)
								{
									rows[i].tiles[k].showIntersectionTile();
									intersectionRow.Add(i) ; intersectionColum.Add(k);
									print("bishop intersected threat path");
									threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
									threatToTeam1.Remove(threatToTeam1[j]);
									goto x1;
								}
							}
							
						}
						
					}
					
					
					
				j4:
					if(true){}
				}

			}
		x1:
			if (true) {}

		}


		else 

		{

			for(int j = 0 ; j <threatingPathsToTeam2.Count;j++)
			{
			
				for(int m = 0 ; m <threatingPathsToTeam2[j].Length/2;m++)
				{
					for (int i = currentRow +1; i < rows.Length; i++) {
						
						for (int  k = currentColum+1; k < rows[i].tiles.Length; k++) {
							
							if(currentRow - i == currentColum - k)
							{
									if(rows[i].tiles[k].isOccupied ){goto j1;}
								 if(threatingPathsToTeam2[j][0,m] == i && threatingPathsToTeam2[j][1,m] == k)
								{
									
									rows[i].tiles[k].showIntersectionTile();
									intersectionRow.Add(i) ; intersectionColum.Add(k);
									print("bishop intersected threat path");
									threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
									threatToTeam2.Remove(threatToTeam2[j]);
									goto x2;
								}
								
							}
						}
					}
					
					j1:
					
					for (int i = currentRow+ 1; i < rows.Length; i++) {
						
						for (int  k = currentColum -1 ; k >= 0; k--) {
							
							if(i==sum-k)
							{
									if(rows[i].tiles[k].isOccupied ){goto j2;}
								 if( threatingPathsToTeam2[j][0,m] == i && threatingPathsToTeam2[j][1,m] == k)
								{
									
									rows[i].tiles[k].showIntersectionTile();
									intersectionRow.Add(i) ; intersectionColum.Add(k);
									print("bishop intersected threat path");
									threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
									threatToTeam2.Remove(threatToTeam2[j]);
									goto x2;

								}
								
							}
							
						}
						
					}
					
					
					j2:
					for (int i = currentRow -1 ; i >= 0; i--) {
						
						for (int  k = currentColum +1; k < rows[i].tiles.Length; k++){
							
							if(i==sum-k)
							{
									if(rows[i].tiles[k].isOccupied ){goto j3;}
								 if( threatingPathsToTeam2[j][0,m] == i && threatingPathsToTeam2[j][1,m] == k){
									
									rows[i].tiles[k].showIntersectionTile();
									intersectionRow.Add(i) ; intersectionColum.Add(k);
									print("bishop intersected threat path");
									threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
									threatToTeam2.Remove(threatToTeam2[j]);
									goto x2;
								}
								
							}
							
						}
						
					}
					
					
					j3:
					
					for (int i = currentRow-1; i >= 0; i--) {
						
						for (int  k = currentColum-1; k >= 0; k--){
							
							if(currentRow - i ==currentColum - k)
							{
									if(rows[i].tiles[k].isOccupied ){goto j4;}	
								 if( threatingPathsToTeam2[j][0,m] == i && threatingPathsToTeam2[j][1,m] == k)
								{
									rows[i].tiles[k].showIntersectionTile();
									intersectionRow.Add(i) ; intersectionColum.Add(k);
									print("bishop intersected threat path");
									threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
									threatToTeam2.Remove(threatToTeam2[j]);
									goto x2;
								}
							}
							
						}
						
					}
					
					
					
				j4:
					
					if (true) {
						
					}
				}
			
			}

		x2:
			if (true) {}

		}



	}






	public void queenVSThreatingPath (int currentRow,int currentColum,string team)
	{
		currentRow--;
		currentColum--;
		int sum = currentRow + currentColum;


		if(team == "PiecePlayer1")
		{
			for(int j = 0 ; j <threatingPathsToTeam1.Count;j++)
			{
				
				for(int m = 0 ; m <threatingPathsToTeam1[j].Length/2;m++)
				{
					
					for (int  k = currentColum +1; k < rows.Length ; k++)
					{
						
						if(rows[currentRow].tiles[k].isOccupied){goto r2; }
						if(threatingPathsToTeam1[j][0,m] == currentRow && threatingPathsToTeam1[j][1,m] == k )
						{
							rows[currentRow].tiles[k].showIntersectionTile();
							intersectionRow.Add(currentRow) ; intersectionColum.Add(k);
							threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
							threatToTeam1.Remove(threatToTeam1[j]);
							goto x1;
						}
					}
					
				r2:
						
						
						for (int i = currentRow -1; i >= 0  ; i--) 
					{
						if(rows[i].tiles[currentColum].isOccupied){goto r3;}
						if(threatingPathsToTeam1[j][0,m] == i && threatingPathsToTeam1[j][1,m] == currentColum)
						{
							rows[i].tiles[currentColum].showIntersectionTile();
							intersectionRow.Add(i) ; intersectionColum.Add(currentColum);
							threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
							threatToTeam1.Remove(threatToTeam1[j]);
							goto x1;
						}
					}
					
				r3:
						
						
						
						
						for (int i = currentRow +1 ; i < rows.Length; i++) 
					{
						if(rows[i].tiles[currentColum].isOccupied){goto r1;}
						if(threatingPathsToTeam1[j][0,m] == i && threatingPathsToTeam1[j][1,m] == currentColum)
						{						
							rows[i].tiles[currentColum].showIntersectionTile();
							intersectionRow.Add(i) ; intersectionColum.Add(currentColum);
							threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
							threatToTeam1.Remove(threatToTeam1[j]);
							goto x1;
						}
					}
					
				r1:
						
						
						for (int  k = currentColum - 1; k >= 0; k--) 
					{
						if(rows[currentRow].tiles[k].isOccupied){goto r4; }
						if( threatingPathsToTeam1[j][0,m] == currentRow && threatingPathsToTeam1[j][1,m] == k)
						{					
							rows[currentRow].tiles[k].showIntersectionTile();
							intersectionRow.Add(currentRow) ; intersectionColum.Add(k);
							threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
							threatToTeam1.Remove(threatToTeam1[j]);
							goto x1;
						}
					}
					
					
					
				r4:

					for (int i = currentRow +1; i < rows.Length; i++) {
						
						for (int  k = currentColum+1; k < rows[i].tiles.Length; k++) {
							
							if(currentRow - i == currentColum - k)
							{	
								if(rows[i].tiles[k].isOccupied ){goto j1;}
								if( threatingPathsToTeam1[j][0,m] == i && threatingPathsToTeam1[j][1,m] == k)
								{
									
									rows[i].tiles[k].showIntersectionTile();
									intersectionRow.Add(i) ; intersectionColum.Add(k);
									print("queen intersected threat path");
									threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
									threatToTeam1.Remove(threatToTeam1[j]);
									goto x1;
								}
								
							}
						}
					}
					
				j1:
						
					for (int i = currentRow+ 1; i < rows.Length; i++) {
						
						for (int  k = currentColum -1 ; k >= 0; k--) {
							
							if(i==sum-k)
							{
								if(rows[i].tiles[k].isOccupied ){goto j2;}
								if(  threatingPathsToTeam1[j][0,m] == i && threatingPathsToTeam1[j][1,m] == k)
								{
									
									rows[i].tiles[k].showIntersectionTile();
									intersectionRow.Add(i) ; intersectionColum.Add(k);
									print("queen intersected threat path");
									threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
									threatToTeam1.Remove(threatToTeam1[j]);
									
									goto x1;
									
								}
								
							}
							
						}
						
					}
					
					
				j2:
					for (int i = currentRow -1 ; i >= 0; i--) {
						
						for (int  k = currentColum +1; k < rows[i].tiles.Length; k++){
							
							if(i==sum-k)
							{
								if(rows[i].tiles[k].isOccupied ){goto j3;}
								if( threatingPathsToTeam1[j][0,m] == i && threatingPathsToTeam1[j][1,m] == k)
								{
									
									rows[i].tiles[k].showIntersectionTile();
									intersectionRow.Add(i) ; intersectionColum.Add(k);
									threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
									threatToTeam1.Remove(threatToTeam1[j]);
									goto x1;
								}
								
							}
							
						}
						
					}
					
					
				j3:
						
					for (int i = currentRow-1; i >= 0; i--) {
						
						for (int  k = currentColum-1; k >= 0; k--){
							
							if(currentRow - i ==currentColum - k)
							{
								if(rows[i].tiles[k].isOccupied ){goto j4;}
								if( threatingPathsToTeam1[j][0,m] == i && threatingPathsToTeam1[j][1,m] == k)
								{
									rows[i].tiles[k].showIntersectionTile();
									intersectionRow.Add(i) ; intersectionColum.Add(k);
									print("queen intersected threat path");
									threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
									threatToTeam1.Remove(threatToTeam1[j]);
									goto x1;
								}
							}
							
						}
						
					}
					
					
					
				j4:
					if(true){}

					
				}
			}
			
		x1:
			if(true){}
		}
		
		else 
		{
			
			for(int j = 0 ; j <threatingPathsToTeam2.Count;j++)
			{
				for(int m = 0 ; m <threatingPathsToTeam2[j].Length/2;m++)
				{
		
					for (int  k = currentColum +1; k < rows.Length ; k++)
					{
						
						if(rows[currentRow].tiles[k].isOccupied){goto r2; }
						if( threatingPathsToTeam2[j][0,m] == currentRow && threatingPathsToTeam2[j][1,m] == k )
						{					
							print("the condtion has been meet");
							rows[currentRow].tiles[k].showIntersectionTile();
							intersectionRow.Add(currentRow) ; intersectionColum.Add(k);
							threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
							threatToTeam2.Remove(threatToTeam2[j]);
							goto x2;							
						}
					}
					
				r2:
						
						for (int i = currentRow -1; i >= 0  ; i--) 
					{
						if(rows[i].tiles[currentColum].isOccupied){goto r3;}
						if(threatingPathsToTeam2[j][0,m] == i && threatingPathsToTeam2[j][1,m] == currentColum)
						{
							rows[i].tiles[currentColum].showIntersectionTile();
							intersectionRow.Add(i) ; intersectionColum.Add(currentColum);
							threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
							threatToTeam2.Remove(threatToTeam2[j]);
							goto x2;
						}
					}
					
				r3:
						
						
						
						
					for (int i = currentRow +1 ; i < rows.Length; i++) {
						
						if(rows[i].tiles[currentColum].isOccupied){goto r1;}
						if( threatingPathsToTeam2[j][0,m] == i && threatingPathsToTeam2[j][1,m] == currentColum)
						{						
							rows[i].tiles[currentColum].showIntersectionTile();
							intersectionRow.Add(i) ; intersectionColum.Add(currentColum);
							threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
							threatToTeam2.Remove(threatToTeam2[j]);
							goto x2;
							
						}
					}
					
				r1:
						
						
					for (int  k = currentColum - 1; k >= 0; k--) {
						if(rows[currentRow].tiles[k].isOccupied){goto r4; }
						if( threatingPathsToTeam2[j][0,m] == currentRow && threatingPathsToTeam2[j][1,m] == k)
						{						
							
							rows[currentRow].tiles[k].showIntersectionTile();
							intersectionRow.Add(currentRow) ; intersectionColum.Add(k);
							threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
							threatToTeam2.Remove(threatToTeam2[j]);
							goto x2;
						}
					}
					
				r4:

					for (int i = currentRow +1; i < rows.Length; i++) {
						
						for (int  k = currentColum+1; k < rows[i].tiles.Length; k++) {
							
							if(currentRow - i == currentColum - k)
							{
								if(rows[i].tiles[k].isOccupied ){goto j1;}
								if(threatingPathsToTeam2[j][0,m] == i && threatingPathsToTeam2[j][1,m] == k)
								{
									
									rows[i].tiles[k].showIntersectionTile();
									intersectionRow.Add(i) ; intersectionColum.Add(k);
									print("queen intersected threat path");
									threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
									threatToTeam2.Remove(threatToTeam2[j]);
									goto x2;
								}
								
							}
						}
					}
					
				j1:
						
					for (int i = currentRow+ 1; i < rows.Length; i++) {
						
						for (int  k = currentColum -1 ; k >= 0; k--) {
							
							if(i==sum-k)
							{
								if(rows[i].tiles[k].isOccupied ){goto j2;}
								if( threatingPathsToTeam2[j][0,m] == i && threatingPathsToTeam2[j][1,m] == k)
								{
									
									rows[i].tiles[k].showIntersectionTile();
									intersectionRow.Add(i) ; intersectionColum.Add(k);
									print("queen intersected threat path");
									threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
									threatToTeam2.Remove(threatToTeam2[j]);
									goto x2;
									
								}
								
							}
							
						}
						
					}
					
					
				j2:
					for (int i = currentRow -1 ; i >= 0; i--) {
						
						for (int  k = currentColum +1; k < rows[i].tiles.Length; k++){
							
							if(i==sum-k)
							{
								if(rows[i].tiles[k].isOccupied ){goto j3;}
								if( threatingPathsToTeam2[j][0,m] == i && threatingPathsToTeam2[j][1,m] == k){
									
									rows[i].tiles[k].showIntersectionTile();
									intersectionRow.Add(i) ; intersectionColum.Add(k);
									print("queen intersected threat path");
									threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
									threatToTeam2.Remove(threatToTeam2[j]);
									goto x2;
								}
								
							}
							
						}
						
					}
					
					
				j3:
						
					for (int i = currentRow-1; i >= 0; i--) {
						
						for (int  k = currentColum-1; k >= 0; k--){
							
							if(currentRow - i ==currentColum - k)
							{
								if(rows[i].tiles[k].isOccupied ){goto j4;}	
								if( threatingPathsToTeam2[j][0,m] == i && threatingPathsToTeam2[j][1,m] == k)
								{
									rows[i].tiles[k].showIntersectionTile();
									intersectionRow.Add(i) ; intersectionColum.Add(k);
									print("queen intersected threat path");
									threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
									threatToTeam2.Remove(threatToTeam2[j]);
									goto x2;
								}
							}
							
						}
						
					}
					
					
					
				j4:
						
					if (true) {
						
					}


				}
				
			}
			
			
			
		x2:
			if(true){}
		}




		
		
		
	}










	public void KnightVSThreatingPath(int currentRow , int currentColum,string team)
	{
		currentRow--;
		currentColum--;
		
		

		
		if(team == "PiecePlayer1"){

			for(int j = 0 ; j <(threatingPathsToTeam1.Count) ;j++)
			{
				for(int m = 0 ; m <(threatingPathsToTeam1[j].Length/2) ;m++)
				{
					if(threatingPathsToTeam1[j] != null)
					{
					if(currentRow +2 <= rows.Length-1 && currentColum + 1 <=rows[currentRow].tiles.Length-1 )
						
					{
						if(!rows [currentRow+2].tiles [currentColum + 1].isOccupied &&   currentRow+2 == threatingPathsToTeam1[j][0,m]  && currentColum + 1 == threatingPathsToTeam1[j][1,m] )
						{
							rows[currentRow+2].tiles[currentColum + 1].showIntersectionTile();
							intersectionRow.Add(currentRow+2) ; intersectionColum.Add(currentColum + 1);
							print("knight intersected threat path");
							threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
							threatToTeam1.Remove(threatToTeam1[j]);
							goto z1;
						}
						
					}
					
					
					if(currentRow +1 <=rows.Length-1 && currentColum + 2 <=rows[currentRow].tiles.Length-1 ) 
					{
						if(!rows [currentRow+1].tiles [currentColum+2].isOccupied && currentRow+1 == threatingPathsToTeam1[j][0,m]  && currentColum + 2 == threatingPathsToTeam1[j][1,m]  )
						{
							rows[currentRow+1].tiles[currentColum + 2].showIntersectionTile();
								intersectionRow.Add(currentRow+1) ; intersectionColum.Add(currentColum + 2);
							print("knight intersected threat path");
							threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
							threatToTeam1.Remove(threatToTeam1[j]);
							goto z1;
						}
						
						
					}
					
					
					if(currentRow -1 >= 0  && currentColum + 2 <=rows[currentRow].tiles.Length-1 )  
					{
						if (!rows [currentRow-1].tiles [currentColum+2].isOccupied &&  currentRow-1 == threatingPathsToTeam1[j][0,m]  && currentColum + 2 == threatingPathsToTeam1[j][1,m] )
						{
							rows[currentRow-1].tiles[currentColum + 2].showIntersectionTile();
							intersectionRow.Add(currentRow-1) ; intersectionColum.Add(currentColum + 2);
							print("knight intersected threat path");
							threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
							threatToTeam1.Remove(threatToTeam1[j]);
							goto z1;
						}
						
					}
					
					
					
					if(currentRow -2 >= 0 && currentColum + 1 <=rows[currentRow].tiles.Length-1 )
					{
						if(!rows [currentRow-2].tiles [currentColum+1].isOccupied && currentRow-2 == threatingPathsToTeam1[j][0,m]  && currentColum + 1 == threatingPathsToTeam1[j][1,m]  )
						{
							rows[currentRow-2].tiles[currentColum + 1].showIntersectionTile();
							intersectionRow.Add(currentRow-2) ; intersectionColum.Add(currentColum + 1);
							print("knight intersected threat path");
							threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
							threatToTeam1.Remove(threatToTeam1[j]);
							goto z1;
						}
						
						
					}
					
					
					if(currentRow -1 >= 0 && currentColum - 2 >=0 ) 
					{
						if (!rows [currentRow-1].tiles [currentColum-2].isOccupied && currentRow-1 == threatingPathsToTeam1[j][0,m]  && currentColum -2 == threatingPathsToTeam1[j][1,m] )
						{
							rows[currentRow-1].tiles[currentColum -2].showIntersectionTile();
							intersectionRow.Add(currentRow-1) ; intersectionColum.Add(currentColum -2);
							print("knight intersected threat path");
							threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
							threatToTeam1.Remove(threatToTeam1[j]);
								goto z1;
						}
						
						
					}
					
					
					if(currentRow -2 >= 0 && currentColum-1 >= 0 )
					{
						if(!rows [currentRow-2].tiles [currentColum-1].isOccupied && currentRow-2 == threatingPathsToTeam1[j][0,m]  && currentColum - 1 == threatingPathsToTeam1[j][1,m] )
						{
							rows[currentRow-2].tiles[currentColum - 1].showIntersectionTile();
							intersectionRow.Add(currentRow-2) ; intersectionColum.Add(currentColum -1 );
							print("knight intersected threat path");
							threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
							threatToTeam1.Remove(threatToTeam1[j]);
								goto z1;
						}
						
						
					}
					
					if(currentRow +1 <=rows.Length-1  && currentColum-2 >=0 ) 
					{
						if(!rows [currentRow+1].tiles [currentColum-2].isOccupied && currentRow+1 == threatingPathsToTeam1[j][0,m]  && currentColum -2 == threatingPathsToTeam1[j][1,m]  )
						{
							rows[currentRow+1].tiles[currentColum -2].showIntersectionTile();
							intersectionRow.Add(currentRow+1) ; intersectionColum.Add(currentColum -2);
							print("knight intersected threat path");
							threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
							threatToTeam1.Remove(threatToTeam1[j]);
							goto z1;

						}
						
						
					}
					
					
					if(currentRow +2 <= rows.Length-1  && currentColum-1 >= 0  )
						
					{
						if( !rows [currentRow+2].tiles [currentColum-1].isOccupied && currentRow+2 == threatingPathsToTeam1[j][0,m]  && currentColum - 1 == threatingPathsToTeam1[j][1,m] )
						{
							rows[currentRow+2].tiles[currentColum -1 ].showIntersectionTile();
							intersectionRow.Add(currentRow+2) ; intersectionColum.Add(currentColum -1);
							print("knight intersected threat path");
							threatingPathsToTeam1.Remove(threatingPathsToTeam2[j]);
							threatToTeam1.Remove(threatToTeam2[j]);
							goto z1;
						}
					}
					
					
				}
				}
				
			}

		z1:
			if(true){}



		}
		else {
			for(int j = 0 ; j <threatingPathsToTeam2.Count;j++)
			{
				for(int m = 0 ; m <threatingPathsToTeam2[j].Length/2;m++)
				{
					
					
					if(currentRow +2 <= rows.Length-1 && currentColum + 1 <=rows[currentRow].tiles.Length-1 )
						
					{
						if(!rows [currentRow+2].tiles [currentColum + 1].isOccupied &&   currentRow+2 == threatingPathsToTeam2[j][0,m]  && currentColum + 1 == threatingPathsToTeam2[j][1,m] )
						{
							rows[currentRow+2].tiles[currentColum + 1].showIntersectionTile();
							intersectionRow.Add(currentRow+2) ; intersectionColum.Add(currentColum + 1);
							print("knight intersected threat path");
							threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
							threatToTeam2.Remove(threatToTeam2[j]);
							goto z2;
						}
						
					}
					
					
					if(currentRow +1 <=rows.Length-1 && currentColum + 2 <=rows[currentRow].tiles.Length-1 ) 
					{
						if(!rows [currentRow+1].tiles [currentColum+2].isOccupied && currentRow+1 == threatingPathsToTeam2[j][0,m]  && currentColum + 2 == threatingPathsToTeam2[j][1,m]  )
						{
							rows[currentRow+1].tiles[currentColum + 2].showIntersectionTile();
							intersectionRow.Add(currentRow+1) ; intersectionColum.Add(currentColum + 2);
							print("knight intersected threat path");
							threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
							threatToTeam2.Remove(threatToTeam2[j]);
							goto z2;
						}

					}
					
					
					if(currentRow -1 >= 0  && currentColum + 2 <=rows[currentRow].tiles.Length-1 )  
					{
						if (!rows [currentRow-1].tiles [currentColum+2].isOccupied &&  currentRow-1 == threatingPathsToTeam2[j][0,m]  && currentColum + 2 == threatingPathsToTeam2[j][1,m] )
						{
							rows[currentRow-1].tiles[currentColum + 2].showIntersectionTile();
							intersectionRow.Add(currentRow-1) ; intersectionColum.Add(currentColum + 2);
							print("knight intersected threat path");
							threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
							threatToTeam2.Remove(threatToTeam2[j]);
							goto z2;
						}
						
					}
					
					
					
					if(currentRow -2 >= 0 && currentColum + 1 <=rows[currentRow].tiles.Length-1 )
					{
						if(!rows [currentRow-2].tiles [currentColum+1].isOccupied && currentRow-2 == threatingPathsToTeam2[j][0,m]  && currentColum + 1 == threatingPathsToTeam2[j][1,m]  )
						{
							rows[currentRow-2].tiles[currentColum + 1].showIntersectionTile();
							intersectionRow.Add(currentRow-2) ; intersectionColum.Add(currentColum + 1);
							print("knight intersected threat path");
							threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
							threatToTeam2.Remove(threatToTeam2[j]);
							goto z2;
						}

					}
					
					
					if(currentRow -1 >= 0 && currentColum - 2 >=0 ) 
					{
						if (!rows [currentRow-1].tiles [currentColum-2].isOccupied && currentRow-1 == threatingPathsToTeam2[j][0,m]  && currentColum -2 == threatingPathsToTeam2[j][1,m] )
						{
							rows[currentRow-1].tiles[currentColum -2].showIntersectionTile();
							intersectionRow.Add(currentRow-1) ; intersectionColum.Add(currentColum -2);
							print("knight intersected threat path");
							threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
							threatToTeam2.Remove(threatToTeam2[j]);
							goto z2;
						}

					}
					
					
					if(currentRow -2 >= 0 && currentColum-1 >= 0 )
					{
						if(!rows [currentRow-2].tiles [currentColum-1].isOccupied && currentRow-2 == threatingPathsToTeam2[j][0,m]  && currentColum - 1 == threatingPathsToTeam2[j][1,m] )
						{
							rows[currentRow-2].tiles[currentColum - 1].showIntersectionTile();
							intersectionRow.Add(currentRow-2) ; intersectionColum.Add(currentColum - 1);
							print("knight intersected threat path");
							threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
							threatToTeam2.Remove(threatToTeam2[j]);
							goto z2;
						}

					}
					
					if(currentRow +1 <=rows.Length-1  && currentColum-2 >=0 ) 
					{
						if(!rows [currentRow+1].tiles [currentColum-2].isOccupied && currentRow+1 == threatingPathsToTeam2[j][0,m]  && currentColum -2 == threatingPathsToTeam2[j][1,m]  )
						{
							rows[currentRow+1].tiles[currentColum -2].showIntersectionTile();
							intersectionRow.Add(currentRow+1) ; intersectionColum.Add(currentColum-2);
							print("knight intersected threat path");
							threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
							threatToTeam2.Remove(threatToTeam2[j]);
							goto z2;
						}

					}
					
					
					if(currentRow +2 <= rows.Length-1  && currentColum-1 >= 0  )
						
					{

						if(!rows [currentRow+2].tiles [currentColum-1].isOccupied &&  currentRow+2 == threatingPathsToTeam2[j][0,m]  && currentColum - 1 == threatingPathsToTeam2[j][1,m] )
						{
							rows[currentRow+2].tiles[currentColum -1].showIntersectionTile();
							intersectionRow.Add(currentRow+2) ; intersectionColum.Add(currentColum -1 );
							print("knight intersected threat path");
							threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
							threatToTeam2.Remove(threatToTeam2[j]);
							goto z2;

						}
					}
					
					
					
				}
				
			}

		}
		z2:
		if(true){}


		
	}






	public void pawnVSThreatingPath(int currentRow , int currentColum ,GameObject passedPawn,string tagName)
	{
		bool firstMove = passedPawn.transform.GetComponent<Pawn> ().firstMove;
		currentRow--;
		currentColum--;
		
		if (tagName == "PiecePlayer2")
		{
			for(int j = 0 ; j <threatingPathsToTeam2.Count;j++)
			{
				for(int m = 0 ; m <threatingPathsToTeam2[j].Length/2;m++)
				{
									
					if (firstMove) 
					{
						if(!rows [currentRow + 1].tiles [currentColum].isOccupied && currentRow+1 == threatingPathsToTeam2[j][0,m] && currentColum  == threatingPathsToTeam2[j][1,m] )
						{
							rows[currentRow+1].tiles[currentColum ].showIntersectionTile();
							intersectionRow.Add(currentRow+1) ; intersectionColum.Add(currentColum);
							print("pawn intersected threat path");
							threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
							threatToTeam2.Remove(threatToTeam2[j]);
							goto c1;
						}
						if(!rows [currentRow + 2].tiles [currentColum].isOccupied && !rows [currentRow + 1].tiles [currentColum].isOccupied && currentRow+2 == threatingPathsToTeam2[j][0,m] && currentColum  == threatingPathsToTeam2[j][1,m])
						{	
								
							rows[currentRow+2].tiles[currentColum].showIntersectionTile();
							intersectionRow.Add(currentRow+2) ; intersectionColum.Add(currentColum);
							print("pawn intersected threat path");
							threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
							threatToTeam2.Remove(threatToTeam2[j]);
							goto c1;

						}
					} 
					else 
					{
						if(!rows [currentRow + 1].tiles [currentColum].isOccupied && currentRow+1 == threatingPathsToTeam2[j][0,m] && currentColum  == threatingPathsToTeam2[j][1,m])
						{
							rows[currentRow+1].tiles[currentColum ].showIntersectionTile();
							intersectionRow.Add(currentRow+1) ; intersectionColum.Add(currentColum);
							print("pawn intersected threat path");
							threatingPathsToTeam2.Remove(threatingPathsToTeam2[j]);
							threatToTeam2.Remove(threatToTeam2[j]);
							goto c1;

						}
					

						
					}
				}
			}
	
		c1: 
			if(true){}
		}

		else if (tagName == "PiecePlayer1")
		{
			for(int j = 0 ; j <threatingPathsToTeam1.Count;j++)
			{
				for(int m = 0 ; m <threatingPathsToTeam1[j].Length/2;m++)
				{
				
					if (firstMove) 
					{
						if(!rows [currentRow - 1].tiles [currentColum].isOccupied && currentRow-1 == threatingPathsToTeam1[j][0,m] && currentColum  == threatingPathsToTeam1[j][1,m] )
						{
							rows[currentRow-1].tiles[currentColum ].showIntersectionTile();
							intersectionRow.Add(currentRow-1) ; intersectionColum.Add(currentColum);
							print("pawn intersected threat path");
							threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
							threatToTeam1.Remove(threatToTeam1[j]);
							goto c2;
						}
						if(!rows [currentRow - 2].tiles [currentColum].isOccupied && !rows [currentRow - 1].tiles [currentColum].isOccupied && currentRow-2 == threatingPathsToTeam1[j][0,m] && currentColum  == threatingPathsToTeam1[j][1,m])
						{	
							
							rows[currentRow-2].tiles[currentColum].showIntersectionTile();
							intersectionRow.Add(currentRow-2) ; intersectionColum.Add(currentColum);
							print("pawn intersected threat path");
							threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
							threatToTeam1.Remove(threatToTeam1[j]);
							goto c2;
							
						}
					} 
					else 
					{
						if(!rows [currentRow - 1].tiles [currentColum].isOccupied && currentRow-1 == threatingPathsToTeam1[j][0,m] && currentColum  == threatingPathsToTeam1[j][1,m])
						{
							rows[currentRow-1].tiles[currentColum ].showIntersectionTile();
							intersectionRow.Add(currentRow-1) ; intersectionColum.Add(currentColum);
							print("pawn  intersected threat path");
							threatingPathsToTeam1.Remove(threatingPathsToTeam1[j]);
							threatToTeam1.Remove(threatToTeam1[j]);
							goto c2;
							
						}
						
						
						
					}

				}
			}
			
		c2:
			if(true){}
		
		
		
	}



	}






	
//	
//	void OnGUI() {
//		if (GUI.Button(new Rect(Screen.width/2 +350, Screen.height/2 +200, 50, 25), "C M"))
//		{checkIfPieceThreatKing();}
//		if (GUI.Button(new Rect(Screen.width/2 +400, Screen.height/2 +200, 50, 25), "clean"))
//		{cleanBoard(); }
//		if (GUI.Button(new Rect(Screen.width/2 +400, Screen.height/2 +175, 50, 25), "print"))
//		{highlightTilesStates(); }
//		if (GUI.Button(new Rect(Screen.width/2 +350, Screen.height/2 +175, 50, 25), "around"))
//		{assignAroundKing(); }
//
//	}




	public void checkIfPieceThreatKing()
	{
		// checking if any piece threating the king >> then storing them in a list 

		threatToTeam1.Clear();
		threatToTeam2.Clear();
		threatingPathsToTeam1.Clear();
		threatingPathsToTeam2.Clear();
		intersectionRow.Clear();
		intersectionColum.Clear();
		cleanBoard();
	
		bool isPlayer1WhoJustPlayed = (PlayerControl.instance.playerTurn == 2) ? true : false;
	
		for(int i = 0 ; i<team1.Count ; i++)
		{
			if(team1[i].transform.GetComponent<Bishop>())
			{		
				team1[i].transform.GetComponent<Bishop>().checkthreatInChessBoard();
			}
			else if(team1[i].transform.GetComponent<Queen>())
			{		
				team1[i].transform.GetComponent<Queen>().checkthreatInChessBoard();
			}
			else if(team1[i].transform.GetComponent<Knight>())
			{		
				team1[i].transform.GetComponent<Knight>().checkthreatInChessBoard();
			}
			else if(team1[i].transform.GetComponent<Pawn>())
			{		
				team1[i].transform.GetComponent<Pawn>().checkthreatInChessBoard();
			}
			if(team1[i].transform.GetComponent<Rook>())
			{		
				team1[i].transform.GetComponent<Rook>().checkthreatInChessBoard();
			}

		} 
		
		for(int i = 0 ; i<team2.Count ; i++)
		{
			

			if(team2[i].transform.GetComponent<Bishop>())
			{		
				team2[i].transform.GetComponent<Bishop>().checkthreatInChessBoard();
			}
			else if(team2[i].transform.GetComponent<Queen>())
			{		
				team2[i].transform.GetComponent<Queen>().checkthreatInChessBoard();
			}
			else if(team2[i].transform.GetComponent<Knight>())
			{		
				team2[i].transform.GetComponent<Knight>().checkthreatInChessBoard();
			}
			else if(team2[i].transform.GetComponent<Pawn>())
			{		
				team2[i].transform.GetComponent<Pawn>().checkthreatInChessBoard();
			}
			else if(team2[i].transform.GetComponent<Rook>())
			{		
				team2[i].transform.GetComponent<Rook>().checkthreatInChessBoard();
			}
			
		} 

		if(threatToTeam1.Count>0 && isPlayer1WhoJustPlayed) // ||threatToTeam2.Count>0
		{
			print("player1 has lost");
			PlayerControl.instance.stopPlaying();
			StartCoroutine(AnnounceWinner(false));
		}
		else if(threatToTeam2.Count>0 && !isPlayer1WhoJustPlayed)
		{
			print("player2 has lost");
			PlayerControl.instance.stopPlaying();
			StartCoroutine(AnnounceWinner(true));
		}	
		else
		{
			Invoke("CheckIfPiecesThreatingKingCanBeAttacked",0.01f);
		}


	}

	public void CheckIfPiecesThreatingKingCanBeAttacked()
	{
		// checking if there are pieces that can attack the threating pieces 
		for(int i = 0 ; i<team2.Count ; i++ )
		{
			if(threatToTeam2.Count>0)
			{
				for(int j = 0 ; j < threatToTeam2.Count ; j++)
				{
					if(team2[i].transform.name.Contains ("Rook"))
					{		
						team2[i].transform.GetComponent<Rook>().destroyThreat(threatToTeam2[j]);
					}
					if(team2[i].transform.name.Contains ("Bishop"))
					{		
						team2[i].transform.GetComponent<Bishop>().destroyThreat(threatToTeam2[j]);
					}
					else if(team2[i].transform.name.Contains ("Queen"))
					{		
						team2[i].transform.GetComponent<Queen>().destroyThreat(threatToTeam2[j]);
					}
					else if(team2[i].transform.name.Contains ("Knight"))
					{		
						team2[i].transform.GetComponent<Knight>().destroyThreat(threatToTeam2[j]);
					}
					else if(team2[i].transform.name.Contains ("Pawn"))
					{		
						team2[i].transform.GetComponent<Pawn>().destroyThreat(threatToTeam2[j]);
					}

				}
				
			}
			
			
		}
		
		
		for(int i = 0 ; i<team1.Count ; i++ )
		{
			if(threatToTeam1.Count>0)
			{

				for(int j = 0 ; j < threatToTeam1.Count ; j++)
				{
					if(team1[i].transform.name.Contains ("Rook"))
					{		
						team1[i].transform.GetComponent<Rook>().destroyThreat(threatToTeam1[j]);
					}
					if(team1[i].transform.name.Contains ("Bishop"))
					{		
						team1[i].transform.GetComponent<Bishop>().destroyThreat(threatToTeam1[j]);
					}
					else if(team1[i].transform.name.Contains ("Queen"))
					{		
						team1[i].transform.GetComponent<Queen>().destroyThreat(threatToTeam1[j]);
					}
					else if(team1[i].transform.name.Contains ("Knight"))
					{		
						team1[i].transform.GetComponent<Knight>().destroyThreat(threatToTeam1[j]);
					}
					else if(team1[i].transform.name.Contains ("Pawn"))
					{		
						team1[i].transform.GetComponent<Pawn>().destroyThreat(threatToTeam1[j]);
					}
		
				}
				
			}

		}


		if(threatToTeam1.Count>0 ||threatToTeam2.Count>0)
		{
			Invoke("CheckIfPiecesCanIntersectThreatPath",0.01f);
		}

	}

	public void CheckIfPiecesCanIntersectThreatPath()
	{
		//check if a friend piece can intersect the path threating the king
		for(int i = 0 ; i<team1.Count ; i++)
		{
			if(threatingPathsToTeam1.Count>0)
			{
				if(team1[i].transform.name.Contains ("Rook"))
				{		
					team1[i].transform.GetComponent<Rook>().intersectEnemy();
				}
				else if(team1[i].transform.name.Contains ("Bishop"))
				{		
					team1[i].transform.GetComponent<Bishop>().intersectEnemy();
				}
				else if(team1[i].transform.name.Contains ("Queen"))
				{		
					team1[i].transform.GetComponent<Queen>().intersectEnemy();
				}
				else if(team1[i].transform.name.Contains ("Knight"))
				{		
					team1[i].transform.GetComponent<Knight>().intersectEnemy();
				}
				else if(team1[i].transform.name.Contains ("Pawn"))
				{		
					team1[i].transform.GetComponent<Pawn>().intersectEnemy();
				}

			}
			
			
		} 
		
		for(int i = 0 ; i<team2.Count ; i++)
		{
			if(threatingPathsToTeam2.Count>0)
			{
				if(team2[i].transform.name.Contains ("Rook"))
				{		
					team2[i].transform.GetComponent<Rook>().intersectEnemy();
				}
				else if(team2[i].transform.name.Contains ("Bishop"))
				{		
					team2[i].transform.GetComponent<Bishop>().intersectEnemy();
				}
				else if(team2[i].transform.name.Contains ("Queen"))
				{		
					team2[i].transform.GetComponent<Queen>().intersectEnemy();
				}
				else if(team2[i].transform.name.Contains ("Knight"))
				{		
					team2[i].transform.GetComponent<Knight>().intersectEnemy();
				}
				else if(team2[i].transform.name.Contains ("Pawn"))
				{		
					team2[i].transform.GetComponent<Pawn>().intersectEnemy();
				}
			}
			
		}

		if(threatToTeam1.Count>0 ||threatToTeam2.Count>0)
		{
			Invoke("checkMate",0.01f);
		}

	}

	//check mate function
	public void checkMate()
	{
		
		dangerTilesAroundKing1 = 0;
		dangerTilesAroundKing2 = 0;
		
		
		//this function determine the area around each king
		assignAroundKing();
		
		
		//this function determine the threatened tiles around the king which he can't move to
		for(int i = 0 ; i<team1.Count ; i++)
		{
			
			if(team1[i].transform.name.Contains ("Rook"))
			{		
				team1[i].transform.GetComponent<Rook>().threatAroundKing();
			}
			else if(team1[i].transform.name.Contains ("Bishop"))
			{		
				team1[i].transform.GetComponent<Bishop>().threatAroundKing();
			}
			else if(team1[i].transform.name.Contains ("Queen"))
			{		
				team1[i].transform.GetComponent<Queen>().threatAroundKing();
			}
			
			else if(team1[i].transform.name.Contains ("Knight"))
			{		
				team1[i].transform.GetComponent<Knight>().threatAroundKing();
			}
			
			else if(team1[i].transform.name.Contains ("Pawn"))
			{		
				team1[i].transform.GetComponent<Pawn>().threatAroundKing();
			}

		} 
		
		for(int i = 0 ; i<team2.Count ; i++)
		{
			
			if(team2[i].transform.name.Contains ("Rook"))
			{		
				team2[i].transform.GetComponent<Rook>().threatAroundKing();
			}
			else if(team2[i].transform.name.Contains ("Bishop"))
			{		
				team2[i].transform.GetComponent<Bishop>().threatAroundKing();
			}
			else if(team2[i].transform.name.Contains ("Queen"))
			{		
				team2[i].transform.GetComponent<Queen>().threatAroundKing();
			}
						
			else if(team2[i].transform.name.Contains ("Knight"))
			{		
				team2[i].transform.GetComponent<Knight>().threatAroundKing();
			}
						
			else if(team2[i].transform.name.Contains ("Pawn"))
			{		
				team2[i].transform.GetComponent<Pawn>().threatAroundKing();
			}
		} 

		
		//highlighting the danger tiles around each king ,, these tiles king can't move to it
		for(int k = 0 ;k<8; k++)
		{
			if(!(aroundKing1[0,k] < 8 && aroundKing1[0,k] >= 0 && aroundKing1[1,k] < 8 && aroundKing1[1,k] >=0))
				continue;

			if(aroundKing1[2,k]==1 || aroundKing1[2,k]==2)
			{
				rows[aroundKing1[0,k]].tiles[aroundKing1[1,k]].makeDanger();
				dangerTilesAroundKing1++;
			}
		}

		for(int k = 0 ;k<8; k++)
		{
			if(!(aroundKing2[0,k] < 8 && aroundKing2[0,k] >= 0 && aroundKing2[1,k] < 8 && aroundKing2[1,k] >=0))
				continue;

			if(aroundKing2[2,k]==1 || aroundKing2[2,k]==2)
			{
				rows[aroundKing2[0,k]].tiles[aroundKing2[1,k]].makeDanger();
				dangerTilesAroundKing2++;
			}
		}

		if(dangerTilesAroundKing1>=TilesAroundKing1 && threatToTeam1.Count>0)
		{
			print("team1 has lost");
			PlayerControl.instance.stopPlaying();
			StartCoroutine(AnnounceWinner(false));
		}
		if(dangerTilesAroundKing2>=TilesAroundKing2&& threatToTeam2.Count>0)
		{
			print("team2 has lost");
			PlayerControl.instance.stopPlaying();
			StartCoroutine(AnnounceWinner(true));
		}
//		printThreatList();

	}
	
	
	
	
	public void addToThreatList(GameObject piece,string threatingPiece)
	{
		if(threatingPiece == "PiecePlayer2") 
		{
			
			threatToTeam2.Add(piece);
			//			print("object name from threat list: " + threatToTeam2[threatToTeam2.Count-1].transform.name);
			//			print("the list capacity is:  " + threatToTeam2.Capacity);
			//			print("the list last game object number  is : " + threatToTeam2.Count);
			//			print("object name from pass gameobject: "+ piece.name.ToString());
			
			
		}
		else if(threatingPiece == "PiecePlayer1")
		{
			
			
			threatToTeam1.Add(piece);
			//			print("object name from threat list: " + threatToTeam1[threatToTeam1.Count-1].transform.name);
			//			print("the list capacity is:  " + threatToTeam1.Capacity);
			//			print("the list last game object number  is : " + threatToTeam1.Count);
			//			print("object name from pass gameobject: "+ piece.name.ToString());
		}
		
	}
	
	public void removeFromThreatList(GameObject piece,string enemyTeam)
	{
		
		if(enemyTeam == "PiecePlayer1")
		{
			threatToTeam2.Remove(piece);
			
			//			print("object name from threat list: " + threatToTeam2[threatToTeam2.Count-1].transform.name);
			//			print("the list capacity is:  " + threatToTeam2.Capacity);
			//			print("the list last game object number  is : " + threatToTeam2.Count);
			//			print("object name from pass gameobject: "+ piece.name.ToString());
			
		}
		else{
			
			threatToTeam1.Remove(piece);
			//			print("object name from threat list: " + threatToTeam1[threatToTeam1.Count-1].transform.name);
			//			print("the list capacity is:  " + threatToTeam1.Capacity);
			//			print("the list last game object number  is : " + threatToTeam1.Count);
			//			print("object name from pass gameobject: "+ piece.name.ToString());
			
		}
	}
	
	
	public void printThreatList()
	{
		for(int i = 0 ; i<threatToTeam2.Count ; i++)
		{
			print("from threat to team 2   : "+ threatToTeam2[i].transform.name.ToString());
		}
		for(int i = 0 ; i<threatToTeam1.Count ; i++)
		{
			print("from threat to team 1   : "+ threatToTeam1[i].transform.name.ToString());
		}
		
	}
	
	public void assignAroundKing()
	{
		TilesAroundKing1 = 0 ;
		TilesAroundKing2 = 0 ;
		for(int i = 0 ; i<team2.Count ; i++ )
		{
			
			if(team2[i].transform.name.Contains ("King"))
			{		
				int row = team2[i].transform.GetComponent<Piece>().row;
				int colum = team2[i].transform.GetComponent<Piece>().column;
				row--;
				colum--;
				
				aroundKing2[0,0] = (row) ; aroundKing2[1,0] = (colum +1);					//rows[aroundKing2[0,0]].tiles[aroundKing2[1,0]].makeDanger();
				aroundKing2[0,1] = (row ) ; aroundKing2[1,1] = (colum - 1);					//rows[aroundKing2[0,1]].tiles[aroundKing2[1,1]].makeDanger();
				aroundKing2[0,2] = (row + 1) ; aroundKing2[1,2] = (colum);					//rows[aroundKing2[0,2]].tiles[aroundKing2[1,2]].makeDanger();
				aroundKing2[0,3] = (row - 1) ; aroundKing2[1,3] = (colum );					//rows[aroundKing2[0,3]].tiles[aroundKing2[1,3]].makeDanger();
				aroundKing2[0,4] = (row +1) ; aroundKing2[1,4] = (colum +1);				//rows[aroundKing2[0,4]].tiles[aroundKing2[1,4]].makeDanger();
				aroundKing2[0,5] = (row +1) ; aroundKing2[1,5] = (colum -1);				//rows[aroundKing2[0,5]].tiles[aroundKing2[1,5]].makeDanger();	
				aroundKing2[0,6] = (row -1) ; aroundKing2[1,6] = (colum +1);				//rows[aroundKing2[0,6]].tiles[aroundKing2[1,6]].makeDanger();
				aroundKing2[0,7] = (row -1) ; aroundKing2[1,7] = (colum -1);				//rows[aroundKing2[0,7]].tiles[aroundKing2[1,7]].makeDanger();


				for(int j = 0 ; j < 8 ;j++)
				{
					if(aroundKing2[0,j]<=rows[row].tiles.Length-1 && aroundKing2[0,j]>=0 && aroundKing2[1,j]<=rows.Length-1  && aroundKing2[1,j]>=0 )
					{
						TilesAroundKing2++;
						if ( rows [aroundKing2[0,j]].tiles [aroundKing2[1,j]].isOccupied && rows [aroundKing2[0,j]].tiles [aroundKing2[1,j]].occupyingTeam == "PiecePlayer2" )
						{
							aroundKing2[2,j]=2;
						}
					}
				}
			}
		}
		
		for(int i = 0 ; i<team1.Count ; i++ )
		{
			
			if(team1[i].transform.name.Contains ("King"))
			{		
				int row = team1[i].transform.GetComponent<Piece>().row;
				int colum = team1[i].transform.GetComponent<Piece>().column;
				row--;
				colum--;
				
				aroundKing1[0,0] = (row) 		; aroundKing1[1,0] = (colum +1);				//rows[aroundKing2[0,0]].tiles[aroundKing2[1,0]].makeDanger();
				aroundKing1[0,1] = (row ) 		; aroundKing1[1,1] = (colum - 1);				//rows[aroundKing2[0,1]].tiles[aroundKing2[1,1]].makeDanger();
				aroundKing1[0,2] = (row + 1) 	; aroundKing1[1,2] = (colum);					//rows[aroundKing2[0,2]].tiles[aroundKing2[1,2]].makeDanger();
				aroundKing1[0,3] = (row - 1)	; aroundKing1[1,3] = (colum );					//rows[aroundKing2[0,3]].tiles[aroundKing2[1,3]].makeDanger();
				aroundKing1[0,4] = (row +1) 	; aroundKing1[1,4] = (colum +1);				//rows[aroundKing2[0,4]].tiles[aroundKing2[1,4]].makeDanger();
				aroundKing1[0,5] = (row +1) 	; aroundKing1[1,5] = (colum -1);				//rows[aroundKing2[0,5]].tiles[aroundKing2[1,5]].makeDanger();	
				aroundKing1[0,6] = (row -1) 	; aroundKing1[1,6] = (colum +1);				//rows[aroundKing2[0,6]].tiles[aroundKing2[1,6]].makeDanger();
				aroundKing1[0,7] = (row -1) 	; aroundKing1[1,7] = (colum -1);				//rows[aroundKing2[0,7]].tiles[aroundKing2[1,7]].makeDanger();

				for(int j = 0 ; j < 8 ;j++)
				{
					if(aroundKing1[0,j]<=rows[row].tiles.Length-1 && aroundKing1[0,j]>=0 && aroundKing1[1,j]<=rows.Length-1  && aroundKing1[1,j]>=0 )
					{
						TilesAroundKing1++;
					  	if ( rows [aroundKing1[0,j]].tiles [aroundKing1[1,j]].isOccupied && rows [aroundKing1[0,j]].tiles [aroundKing1[1,j]].occupyingTeam == "PiecePlayer1" )
						{
							aroundKing1[2,j]=2;
						}
					}
				}
			}
		}
//		print ("TilesAroundKing1 : "  + TilesAroundKing1);
//		print ("TilesAroundKing2 : "  + TilesAroundKing2);
	}



	public void cleanBoard()
	{
		for (int i = 0 ; i < rows.Length; i++) 
		{
			for (int  k = 0; k < rows[i].tiles.Length; k++) 
			{
				rows[i].tiles[k].cleanTile();
			}
		}
	}



	public void highlightTilesStates()
	{
//		for(int k = 0 ;k<8; k++)
//		{
//			
//			if(aroundKing2[2,k]==1 || aroundKing2[2,k]==2)
//			{
//				rows[aroundKing2[0,k]].tiles[aroundKing2[1,k]].makeDanger();
//			}
//			if(aroundKing1[2,k]==1 || aroundKing1[2,k]==2)
//			{
//				rows[aroundKing1[0,k]].tiles[aroundKing1[1,k]].makeDanger();
//			}
//		}

		for(int j = 0 ; j <threatingPathsToTeam1.Count;j++)
		{
			for(int m = 0 ; m <threatingPathsToTeam1[j].Length/2;m++)
			{
				rows[threatingPathsToTeam1[j][0,m]].tiles[threatingPathsToTeam1[j][1,m]].showThreatPath();
			}
		}

//		for(int i = 0 ; i < intersectionRow.Count ; i++)
//		{
//
//			rows[intersectionRow[i]].tiles[intersectionColum[i]].showIntersectionTile();
//		}

	}


	public bool isKingNotMoved(string team)
	{
		if(team == "PiecePlayer1")
		{
			for(int i = 0 ; i<team1.Count ; i++)
			{
				if(team1[i].transform.name.Contains("King"))
				{
					return team1[i].GetComponent<King>().notMoved;
				}
			}
		}
		else
		{
			for(int i = 0 ; i<team2.Count ; i++)
			{
				if(team2[i].transform.name.Contains("King"))
				{
					return team2[i].GetComponent<King>().notMoved;
				}
			}
		}

		return false ;
	}


	public bool isRookNotMoved(int row, int colum)
	{
		for(int i = 0 ; i<team2.Count ; i++)
		{
			if(team2[i].transform.name.Contains("Rook") && team2[i].transform.GetComponent<Rook>().rookRow-1 == row &&  team2[i].transform.GetComponent<Rook>().rookColum-1 == colum )
			{
				return team2[i].GetComponent<Rook>().notMoved;
			}
		}

		for(int i = 0 ; i<team1.Count ; i++)
		{
			if(team1[i].transform.name.Contains("Rook") && team1[i].transform.GetComponent<Rook>().rookRow-1 == row &&  team1[i].transform.GetComponent<Rook>().rookColum-1 == colum )
			{
				return team1[i].GetComponent<Rook>().notMoved;
			}
		}

		return false;
	}


	public bool checkCastlingTiles(string team ,string direction)
	{
		if(team == "PiecePlayer1" )
		{
			for(int i = 0 ; i<team2.Count ; i++)
			{
				if(team2[i].transform.name.Contains ("Rook"))
				{		
					team2[i].transform.GetComponent<Rook>().intersectCastling();
				}
				else if(team2[i].transform.name.Contains ("Bishop"))
				{		
					team2[i].transform.GetComponent<Bishop>().intersectCastling();
				}
				else if(team2[i].transform.name.Contains ("Queen"))
				{		
					team2[i].transform.GetComponent<Queen>().intersectCastling();
				}
				else if(team2[i].transform.name.Contains ("Knight"))
				{		
					team2[i].transform.GetComponent<Knight>().intersectCastling();
				}
				else if(team2[i].transform.name.Contains ("Pawn"))
				{		
					team2[i].transform.GetComponent<Pawn>().intersectCastling();
				}
				else if (team2[i].transform.name.Contains ("King"))
				{
					team2[i].transform.GetComponent<King>().intersectCastling();
				}

			}

				if(direction == "right")
				{
					for(int j = 0 ; j < 5 ; j++)
					{
						if(king1CastlingTiles[0,j] == 7 && king1CastlingTiles[1,j] == 1)
						{
							if(king1CastlingTiles[2,j] == 0)
								return false;
						}
						else if(king1CastlingTiles[0,j] == 7 && king1CastlingTiles[1,j] == 2)
						{
							if(king1CastlingTiles[2,j] == 0)
								return false;
						}

 					}
				return true;
				}
				else
				{
					for(int j = 0 ; j < 5 ; j++)
					{
						if(king1CastlingTiles[0,j] == 7 && king1CastlingTiles[1,j] == 4)
						{
							if(king1CastlingTiles[2,j] == 0)
								return false;
						}
						else if(king1CastlingTiles[0,j] == 7 && king1CastlingTiles[1,j] == 5)
						{
							if(king1CastlingTiles[2,j] == 0)
								return false;
						}
						else if(king1CastlingTiles[0,j] == 7 && king1CastlingTiles[1,j] == 6)
						{
							if(king1CastlingTiles[2,j] == 0)
								return false;
						}
					}
				return true;

				}
		}
		else
		{
			for(int i = 0 ; i<team1.Count ; i++)
			{

				if(team1[i].transform.name.Contains ("Rook"))
				{		
					team1[i].transform.GetComponent<Rook>().intersectCastling();
				}
				else if(team1[i].transform.name.Contains ("Bishop"))
				{		
					team1[i].transform.GetComponent<Bishop>().intersectCastling();
				}
				else if(team1[i].transform.name.Contains ("Queen"))
				{		
					team1[i].transform.GetComponent<Queen>().intersectCastling();
				}
				else if(team1[i].transform.name.Contains ("Knight"))
				{		
					team1[i].transform.GetComponent<Knight>().intersectCastling();
				}
				else if(team1[i].transform.name.Contains ("Pawn"))
				{		
					team1[i].transform.GetComponent<Pawn>().intersectCastling();
				}
				else if (team1[i].transform.name.Contains ("King"))
				{
					team1[i].transform.GetComponent<King>().intersectCastling();
				}

			} 
			

		if(direction == "right")
		{
			for(int j = 0 ; j < 5 ; j++)
			{
				if(king2CastlingTiles[0,j] == 0 && king2CastlingTiles[1,j] == 4)
				{
					if(king2CastlingTiles[2,j] == 0)
						return false;
				}
				else if(king2CastlingTiles[0,j] == 0 && king2CastlingTiles[1,j] == 5)
				{
					if(king2CastlingTiles[2,j] == 0)
						return false;
				}
				else if(king2CastlingTiles[0,j] == 0 && king2CastlingTiles[1,j] == 6)
				{
					if(king2CastlingTiles[2,j] == 0)
						return false;
				}
			}
			return true;
		}
		else
		{
			for(int j = 0 ; j < 5 ; j++)
			{
				if(king2CastlingTiles[0,j] == 0 && king2CastlingTiles[1,j] == 1)
				{
					if(king2CastlingTiles[2,j] == 0)
						return false;
				}
				else if(king2CastlingTiles[0,j] == 0 && king2CastlingTiles[1,j] == 2)
				{
					if(king2CastlingTiles[2,j] == 0)
						return false;
				}
			}
			return true;
		}

		}

	}

	public void resetCastlingTile()
	{
		for (int i = 0 ; i < 5 ; i++)
		{
			king1CastlingTiles[2,i] = 1;
			king2CastlingTiles[2,i] = 1;
		}
	}

	public Rook getWantedRook(string team, int row , int colum)
	{
	
		if(team == "PiecePlayer1")
		{
			for(int i = 0 ; i<team1.Count ; i++)
			{
				if(team1[i].transform.name.Contains("Rook") && team1[i].transform.GetComponent<Rook>().rookRow == row &&  team1[i].transform.GetComponent<Rook>().rookColum == colum )
				{
					print ("you got the right rook1");
					return team1[i].transform.GetComponent<Rook>();
				}
			}
		}
		else
		{
			for(int i = 0 ; i<team2.Count ; i++)
			{
				if(team2[i].transform.name.Contains("Rook") && team2[i].transform.GetComponent<Rook>().rookRow == row &&  team2[i].transform.GetComponent<Rook>().rookColum == colum )
				{
					print ("you got the right rook2");
					return	team2[i].transform.GetComponent<Rook>();
				}
			}
		}
		print("you didn't get the right rook");
		return new Rook();
	}


	public IEnumerator AnnounceWinner(bool isHostWhoWon)
	{

		print("we entered annoucne winner method");
		
		yield return new WaitForSeconds(3f);
		chessEndGameCanvas.SetActive(true);
		if((m_UNETChess.instance.isHost && isHostWhoWon) || (!m_UNETChess.instance.isHost && !isHostWhoWon))
		{
			chessEndGameCanvas.GetComponentInChildren<Text>().text = " You have won the game" ;
			print(Login.instance.activeRoomId);
			print(Login.instance.successfulLoginUser.data.user.id);
			Rooms.instance.EndGameRoom(Login.instance.activeRoomId,Login.instance.successfulLoginUser.data.user.id);
			print("after end game room data base");
            Social.ReportProgress(TableTopPlayServiceResources.achievement_pawn, 100.0f, (bool success) =>
            {
                Debug.Log("Achievment Successfully Unlocked");
            });

            PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_knight, 2, (bool success) =>
            {
                Debug.Log("Achievment Successfully Unlocked");
            });
            PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_queen, 2, (bool success) =>
            {
                Debug.Log("Achievment Successfully Unlocked");
            });
            PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_king, 2, (bool success) =>
            {
                Debug.Log("Achievment Successfully Unlocked");
            });

		}
		else
		{
			chessEndGameCanvas.GetComponentInChildren<Text>().text = " You have lost the game" ;
		}
		StartCoroutine(afterAnnouncingWinner());
	}


	public IEnumerator otherChessPlayerSurrendered()
	{		
		chessEndGameCanvas.SetActive(true);
		chessEndGameCanvas.GetComponentInChildren<Text>().text = "You have won the game \n the other player has surrendered" ;

        Social.ReportProgress(TableTopPlayServiceResources.achievement_pawn, 100.0f, (bool success) =>
        {
            Debug.Log("Achievment Successfully Unlocked");
        });

        PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_knight, 2, (bool success) =>
        {
            Debug.Log("Achievment Successfully Unlocked");
        });
        PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_queen, 2, (bool success) =>
        {
            Debug.Log("Achievment Successfully Unlocked");
        });
        PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_king, 2, (bool success) =>
        {
            Debug.Log("Achievment Successfully Unlocked");
        });
		Rooms.instance.EndGameRoom(Login.instance.activeRoomId,Login.instance.successfulLoginUser.data.user.id);
		yield return new WaitForSeconds(1f);
		StartCoroutine(afterAnnouncingWinner());
	}

	public void iSurrender()
	{
		chessEndGameCanvas.SetActive(true);
		chessEndGameCanvas.GetComponentInChildren<Text>().text = "You have lost the game" ;
		StartCoroutine(afterAnnouncingWinner());
	}


	IEnumerator afterAnnouncingWinner()
	{
		m_UNETGameManager go = GameObject.FindObjectOfType<m_UNETGameManager>();
		m_UNETChess go2 = GameObject.FindObjectOfType<m_UNETChess>();
		//		go2.ShutDownRoom();
		go2.ShutDownConnection();

		yield return new WaitForSeconds(3f);
		Destroy(Rooms.instance.gameObject);

			Destroy(go.gameObject);
			Destroy(go2.gameObject);

	//	chessEndGameCanvas.SetActive(true);
		Resources.UnloadUnusedAssets();
		SceneManager.LoadScene("main_menu_scene");

		//we should be going to main menu Or to the available rooms menu 
	}

	public void removeFromTeam1Array(GameObject piece)
	{
		for(int i = 0 ; i < team1.Count; i++)
		{
////			if(team1[i] == piece)
//			team1.RemoveAt(i);
//			Array.rem
		}
	}



}




