﻿using UnityEngine;
using System.Collections;

public class Knight : MonoBehaviour {
	
	public int knightRow ; 
	public int knightColum ;
	private static ChessBoard chessBoard ;
	private static GameObject boardGameObject ;
	public string team ;
	public bool  pieceAttackable = false ;

	private Piece pieceScript;
	
	
	void Update(){
		
		pieceScript.row = knightRow ;
		pieceScript.column = knightColum;
		
	}

	void Awake(){
		
		boardGameObject = GameObject.Find ("GameManager");
		chessBoard = boardGameObject.GetComponent<ChessBoard>();

	}
	
	void Start(){

		Invoke ( "OccupyTile", .001f);
		pieceScript = gameObject.GetComponent<Piece>();
//		InvokeRepeating("checkthreatInChessBoard",5f,5f);

	}
	
	public void OccupyTile(){


		team = gameObject.transform.tag.ToString();
		chessBoard.rows [knightRow-1].tiles [knightColum-1].isOccupied = true;
		chessBoard.rows [knightRow-1].tiles [knightColum-1].occupyingTeam = team ;

		
		
	}
	public void unOccupyTile(){
		
		chessBoard.rows [knightRow-1].tiles [knightColum-1].isOccupied = false;
		chessBoard.rows [knightRow-1].tiles [knightColum-1].occupyingTeam = "" ;

		
	}
	public void checkAttackable ()
	{
		pieceAttackable = chessBoard.rows [knightRow-1].tiles [knightColum-1].isAttackable ;
	}

	public void checkthreatInChessBoard()
	{
		chessBoard.KnightVSKing(knightRow ,knightColum , team , gameObject);
	}


	public void destroyThreat(GameObject enemyPiece)
	{
		//		print("function destroyThreat from Rook script has been activated");
		chessBoard.KnightVSThreatingPiece( knightRow , knightColum , team ,gameObject,enemyPiece);
	}

	public void threatAroundKing()
	{
		chessBoard.KnightVSAroundKing(knightRow , knightColum , team ,gameObject);
	}

	public void intersectEnemy()
	{
		chessBoard.KnightVSThreatingPath(knightRow,knightColum,team);
	}

	public void intersectCastling()
	{
		chessBoard.knightVsCastlingTiles(team,knightRow,knightColum);


	}



}
