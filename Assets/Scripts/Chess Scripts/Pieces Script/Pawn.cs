﻿using UnityEngine;
using System.Collections;

public class Pawn : MonoBehaviour {
	public int pawnRow; 
	public int pawnColum;
	public bool firstMove;
	private static ChessBoard chessBoard ;
	private static GameObject boardGameObject ;
	public string team ;
	public bool  pieceAttackable = false ;

	private Piece pieceScript;
	
	
	void Update(){
		pieceScript.row = pawnRow ;
		pieceScript.column = pawnColum;
	}

	void Awake(){
		
		boardGameObject = GameObject.Find ("GameManager");
		chessBoard = boardGameObject.GetComponent<ChessBoard>();
		firstMove = true;
		
	}
	
	void Start () {
		
		Invoke ( "OccupyTile", .001f);
		pieceScript = gameObject.GetComponent<Piece>();
//		InvokeRepeating("checkthreatInChessBoard",5f,5f);
	}



	public void OccupyTile(){
		team = gameObject.transform.tag.ToString();
		chessBoard.rows [pawnRow-1].tiles [pawnColum-1].isOccupied = true;
		chessBoard.rows [pawnRow-1].tiles [pawnColum-1].occupyingTeam = team ;

	}
	public void unOccupyTile(){

		chessBoard.rows [pawnRow-1].tiles [pawnColum-1].isOccupied = false;
		chessBoard.rows [pawnRow-1].tiles [pawnColum-1].occupyingTeam = "" ;

		
		
	}
	
	public void checkAttackable ()
	{
		pieceAttackable = chessBoard.rows [pawnRow-1].tiles [pawnColum-1].isAttackable ;
	}


	public void checkthreatInChessBoard()
	{
		chessBoard.PawnVSKing(pawnRow ,pawnColum , team , gameObject);
	}



	public void destroyThreat(GameObject enemyPiece)
	{
		chessBoard.PawnVSThreatingPiece( pawnRow , pawnColum , team ,gameObject,enemyPiece);
	}

	public void threatAroundKing()
	{
		chessBoard.PawnVSAroundKing(pawnRow , pawnColum , team ,gameObject);
	}


	public void intersectEnemy()
	{
		chessBoard.pawnVSThreatingPath(pawnRow,pawnColum,gameObject,team);
	}


	public void intersectCastling()
	{
		chessBoard.pawnVsCastlingTiles(team,pawnRow,pawnColum);
	}

}
