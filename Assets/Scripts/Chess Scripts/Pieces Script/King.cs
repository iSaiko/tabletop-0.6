﻿using UnityEngine;
using System.Collections;

public class King : MonoBehaviour {
	
	
	public int kingRow ; 
	public int kingColum ;
	private static ChessBoard chessBoard ;
	private static GameObject boardGameObject ;
	public string team ;
	public bool  pieceAttackable = false ;
	private Piece pieceScript; 
	public bool notMoved = true;
	
	
	void Update(){
		pieceScript.row = kingRow ;
		pieceScript.column = kingColum;
		
	}

	void Awake(){
		
		boardGameObject = GameObject.Find ("GameManager");
		chessBoard = boardGameObject.GetComponent<ChessBoard>();
		
	}
	void Start(){
		
		Invoke ( "OccupyTile", .001f);
		pieceScript = gameObject.GetComponent<Piece>();
	}

	public void OccupyTile(){
		
		team = gameObject.transform.tag.ToString();
		chessBoard.rows [kingRow-1].tiles [kingColum-1].isOccupied = true;
		chessBoard.rows [kingRow-1].tiles [kingColum-1].occupyingTeam = gameObject.transform.tag.ToString(); 
		chessBoard.rows [kingRow-1].tiles [kingColum-1].holdingKing = true;


	
	}
	public void unOccupyTile(){

		chessBoard.rows [kingRow-1].tiles [kingColum-1].holdingKing = false;
		chessBoard.rows [kingRow-1].tiles [kingColum-1].isOccupied = false;
		chessBoard.rows [kingRow-1].tiles [kingColum-1].occupyingTeam = "" ;
		notMoved = false;

		
		
	}
	public void checkAttackable ()
	{
		pieceAttackable = chessBoard.rows [kingRow-1].tiles [kingColum-1].isAttackable ;
	
	}


	public void intersectCastling()
	{
		chessBoard.kingVsCastlingTiles(gameObject.transform.tag.ToString(),kingRow,kingColum);
	}

	 
}

