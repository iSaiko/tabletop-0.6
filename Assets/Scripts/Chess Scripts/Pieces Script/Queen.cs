﻿using UnityEngine;
using System.Collections;

public class Queen : MonoBehaviour {

	public int queenRow; 
	public int queenColum;
	private static ChessBoard chessBoard ;
	private static GameObject boardGameObject ;
	public string team ;
	public bool  pieceAttackable = false ;

	private Piece pieceScript;
	
	
	void Update(){
		pieceScript.row = queenRow ;
		pieceScript.column = queenColum;
		
	}
	void Awake(){
	
		boardGameObject = GameObject.Find ("GameManager");
		chessBoard = boardGameObject.GetComponent<ChessBoard>();
		

	}

	void Start () {

		Invoke ( "OccupyTile", .001f);
		pieceScript = gameObject.GetComponent<Piece>();
//		InvokeRepeating("checkthreatInChessBoard",5f,5f);

	}
	

	public void OccupyTile(){
		team = gameObject.transform.tag.ToString();

		chessBoard.rows [queenRow-1].tiles [queenColum-1].isOccupied = true;
		chessBoard.rows [queenRow-1].tiles [queenColum-1].occupyingTeam = team ;

		
		
	}
	public void unOccupyTile(){
		
		chessBoard.rows [queenRow-1].tiles [queenColum-1].isOccupied = false;
		chessBoard.rows [queenRow-1].tiles [queenColum-1].occupyingTeam = "" ;

		
		
	}

	
	public void checkAttackable ()
	{
		pieceAttackable = chessBoard.rows [queenRow-1].tiles [queenColum-1].isAttackable ;


	}
	
	public void checkthreatInChessBoard()
	{
		chessBoard.QueenVSKing( queenRow , queenColum , team , gameObject);
		
	}

	public void destroyThreat(GameObject enemyPiece)
	{
		//		print("function destroyThreat from Rook script has been activated");
		chessBoard.QueenVSThreatingPiece( queenRow , queenColum , team ,gameObject,enemyPiece);
	}


	public void threatAroundKing()
	{
		chessBoard.QueenVSAroundKing(queenRow , queenColum , team ,gameObject);
	}


	public void intersectEnemy()
	{
		chessBoard.queenVSThreatingPath(queenRow,queenColum,team);
	}


	public void intersectCastling()
	{
		chessBoard.queenVsCastlingTiles(team,queenRow,queenColum);
	}












}
