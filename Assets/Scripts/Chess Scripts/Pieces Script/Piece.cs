﻿using UnityEngine;
using System.Collections;

public class Piece : MonoBehaviour {
	public int row;
	public int column;
	public GameObject onPiece;
	public GameObject player1PromotionCanvas;
	public GameObject player2PromotionCanvas;
    public int pieceIndex;
	public bool isPiecePlayer1;

	public Material team1Highlight;
	public Material team2Highlight;

	private Renderer myRenderer;
	private bool isHighlighted;
	private Material normalMaterial;

	public Transform Team1GraveYardDestination;
	public Transform Team2GraveYardDestination;

	private float animationDuration = 0.75f;

	void Start () {
		onPiece = this.gameObject;
//		myRenderer =  GetComponent<Renderer>();
//		normalMaterial = myRenderer.material;
		assignToTeam();

	}
	

	public void updateAfterAttack()
	{
		if(onPiece.transform.name.Contains("Pawn")){
			onPiece.GetComponent<Pawn>().unOccupyTile();
			onPiece.GetComponent<Pawn>().pawnRow = row;
			onPiece.GetComponent<Pawn>().pawnColum = column;
			onPiece.GetComponent<Pawn>().OccupyTile();
			onPiece.GetComponent<Pawn>().firstMove = false;
			if(onPiece.transform.tag == "PiecePlayer1" && row == 1){
				Debug.Log ("Promotion");
				player1PromotionCanvas.SetActive(true);
			}
			if(onPiece.transform.tag == "PiecePlayer2" && row == 8){
				player2PromotionCanvas.SetActive(true);
			}
			//Debug.Log ("I got here to the pawn updating after Attack");
		}
		if(onPiece.transform.name.Contains("Queen")){
			onPiece.GetComponent<Queen>().unOccupyTile();
			onPiece.GetComponent<Queen>().queenRow = row;
			onPiece.GetComponent<Queen>().queenColum = column;
			onPiece.GetComponent<Queen>().OccupyTile();

		}
		if(onPiece.transform.name.Contains("Bishop")){
			onPiece.GetComponent<Bishop>().unOccupyTile();
			onPiece.GetComponent<Bishop>().bishopRow = row;
			onPiece.GetComponent<Bishop>().bishopColum = column;
			onPiece.GetComponent<Bishop>().OccupyTile();
		}
		if(onPiece.transform.name.Contains("Knight")){	
			onPiece.GetComponent<Knight>().unOccupyTile();
			onPiece.GetComponent<Knight>().knightRow = row;
			onPiece.GetComponent<Knight>().knightColum = column;
			onPiece.GetComponent<Knight>().OccupyTile();

		}
		if(onPiece.transform.name.Contains("Rook")){
			onPiece.GetComponent<Rook>().unOccupyTile();
			onPiece.GetComponent<Rook>().rookRow = row;
			onPiece.GetComponent<Rook>().rookColum = column;
			onPiece.GetComponent<Rook>().OccupyTile();

		}
		if(onPiece.transform.name.Contains("King")){
			onPiece.GetComponent<King>().unOccupyTile();
			onPiece.GetComponent<King>().kingRow = row;
			onPiece.GetComponent<King>().kingColum = column;
			onPiece.GetComponent<King>().OccupyTile();

		}
	}

	void assignToTeam()
	{
		isPiecePlayer1 = (transform.tag.ToString() == "PiecePlayer1" ) ? true : false ;
	}


	public void highLight()
	{
//		print("highlight function detected"); 
//		print(isPiecePlayer1);
//		myRenderer.material = (isPiecePlayer1 == true) ?  team1Highlight : team2Highlight ;
	}
	public void UnhighLight()
	{
//		myRenderer.material = normalMaterial;
	}





//	void OnMouseDown()
//	{
//
//		if(isHighlighted)
//		{
//			UnhighLight();
//			isHighlighted = !isHighlighted ;
//		}
//		else
//		{
//			highLight();
//			isHighlighted = !isHighlighted ;
//		}
//
//	}




	public void jump()
	{

		Animation animation = this.transform.gameObject.AddComponent<Animation>();
		AnimationClip clip ;
		clip = new AnimationClip();
		clip.legacy = true;

		Vector3 jumpDestination = isPiecePlayer1 ? ChessGraveYard.player1GraveAnchor.localPosition : ChessGraveYard.player2GraveAnchor.localPosition  ;

		print(jumpDestination.x + " " + jumpDestination.y + " " +jumpDestination.z + " ");
		print(transform.position.x + " " + transform.position.y + " " +transform.position.z + " ");
		transform.parent = null;


		// animating postion from current postion to destination

		AnimationCurve curve = AnimationCurve.Linear(0, transform.position.x, animationDuration,jumpDestination.x);
		clip.SetCurve("", typeof(Transform), "localPosition.x", curve);


		curve = AnimationCurve.Linear(0, transform.position.y, animationDuration,jumpDestination.y);
		curve.AddKey(animationDuration / 2 , jumpDestination.y +10f);
		clip.SetCurve("", typeof(Transform), "localPosition.y", curve);
			

		curve = AnimationCurve.Linear(0, transform.position.z, animationDuration,(jumpDestination.z + Random.Range(-12f,12f)));
		clip.SetCurve("", typeof(Transform), "localPosition.z", curve);



		// animating scale 


//		curve = AnimationCurve.Linear(0, transform.lossyScale.x, animationDuration, transform.lossyScale.x);
//		curve.AddKey(animationDuration / 2, (float)(transform.lossyScale.x + 2));
//		clip.SetCurve("", typeof(Transform), "lossyScale.z", curve);
//
//		curve = AnimationCurve.Linear(0, transform.lossyScale.y, animationDuration, transform.lossyScale.y);
//		curve.AddKey(animationDuration / 2, (float)(transform.lossyScale.y + 2));
//		clip.SetCurve("", typeof(Transform), "lossyScale.y", curve);
//
//
//		curve = AnimationCurve.Linear(0, transform.lossyScale.z, animationDuration, transform.lossyScale.z);
//		curve.AddKey(animationDuration / 2, (float)(transform.lossyScale.z + 2));
//		clip.SetCurve("", typeof(Transform), "lossyScale.z", curve);
//

		animation.AddClip(clip, "die");
		animation.Play("die");
		StartCoroutine(addPhysicsToDefeatedPiece(animationDuration));

	}


	IEnumerator addPhysicsToDefeatedPiece(float AnimationDuration)
	{

		yield return new WaitForSeconds(animationDuration);
		Destroy(this.transform.GetComponent<Animation>());
		BoxCollider[] tempBoxes =  this.transform.GetComponents<BoxCollider>();
		for(int i = 0 ; i < tempBoxes.Length ; i++)
		{
			Destroy(tempBoxes[i]);
		}
		Rigidbody tempRigid = this.gameObject.AddComponent<Rigidbody>();
		CapsuleCollider tempCap =  this.gameObject.AddComponent<CapsuleCollider>();
		tempCap.radius = 5;
		tempCap.height = 24;
		this.gameObject.layer = 10 ;
		Destroy(tempRigid,3f);
		Destroy(this,3f);
	}



}

