﻿using UnityEngine;
using System.Collections;

public class Rook : MonoBehaviour {
	
	
	public int rookRow ; 
	public int rookColum ;
	private static ChessBoard chessBoard ;
	private static GameObject boardGameObject ;
	public string team ;
	public bool  pieceAttackable = false ;
	public bool notMoved = true;


	private Piece pieceScript;
	
	
	void Update(){
		pieceScript.row = rookRow ;
		pieceScript.column = rookColum;
		
	}
	
	void Awake(){
		
		boardGameObject = GameObject.Find ("GameManager");
		chessBoard = boardGameObject.GetComponent<ChessBoard>();
		
	}
	void Start(){
		
		Invoke ( "OccupyTile", .001f);
		pieceScript = gameObject.GetComponent<Piece>();

//		InvokeRepeating("checkthreatInChessBoard",5f,5f);
	}

	
	public void OccupyTile(){

		team = gameObject.transform.tag.ToString();
		chessBoard.rows [rookRow-1].tiles [rookColum-1].isOccupied = true;
		chessBoard.rows [rookRow-1].tiles [rookColum-1].occupyingTeam = team ;

	}
	public void unOccupyTile(){
		
		chessBoard.rows [rookRow-1].tiles [rookColum-1].isOccupied = false;
		chessBoard.rows [rookRow-1].tiles [rookColum-1].occupyingTeam = "" ;
		notMoved = false;


	}

	public void checkAttackable ()
	{
		pieceAttackable = chessBoard.rows [rookRow-1].tiles [rookColum-1].isAttackable ;
	}


	public void checkthreatInChessBoard()
	{
//		print("checkthreatInChessBoard function in Rook script has been activated");
		chessBoard.RookVSKing( rookRow , rookColum , team ,gameObject );
	}

	public void destroyThreat(GameObject enemyPiece)
	{
//		print("function destroyThreat from Rook script has been activated");
		chessBoard.RookVSThreatingPiece( rookRow , rookColum , team ,gameObject,enemyPiece);
	}

	public void threatAroundKing()
	{
		chessBoard.RookVSAroundKing(rookRow , rookColum , team ,gameObject);
	}


	public void intersectEnemy()
	{
		chessBoard.rookVSThreatingPath(rookRow,rookColum,team);
	}

	public void intersectCastling()
	{
		chessBoard.rookVsCastlingTiles(team,rookRow,rookColum);
	}



}

