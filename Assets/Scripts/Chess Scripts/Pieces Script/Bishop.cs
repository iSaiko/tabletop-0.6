﻿using UnityEngine;
using System.Collections;

public class Bishop : MonoBehaviour {
	
	
	public int bishopRow; 
	public int bishopColum ;
	private static ChessBoard chessBoard ;
	private static GameObject boardGameObject ;
	public string team ;
	public bool  pieceAttackable = false ;

	private Piece pieceScript;

	
	void Update(){
		pieceScript.row = bishopRow ;
		pieceScript.column = bishopColum;
		
	}

	void Awake(){
		
		boardGameObject = GameObject.Find ("GameManager");
		chessBoard = boardGameObject.GetComponent<ChessBoard>();

	}  
	
	void Start(){
		
		Invoke ( "OccupyTile", .001f);
		pieceScript = gameObject.GetComponent<Piece>();
//		InvokeRepeating("checkthreatInChessBoard",5f,5f);

	}


	public void OccupyTile(){


		team = gameObject.transform.tag.ToString();
		chessBoard.rows [bishopRow-1].tiles [bishopColum-1].isOccupied = true;
		chessBoard.rows [bishopRow-1].tiles [bishopColum-1].occupyingTeam = team ;

		
	}
	
	public void unOccupyTile(){
		
		chessBoard.rows [bishopRow-1].tiles [bishopColum-1].isOccupied = false;
		chessBoard.rows [bishopRow-1].tiles [bishopColum-1].occupyingTeam = "" ;

		
		
	}

	public void checkAttackable ()
	{
		pieceAttackable = chessBoard.rows [bishopRow-1].tiles [bishopColum-1].isAttackable ;
		
		
	}

	public void checkthreatInChessBoard()
	{

		chessBoard.BishopVSKing( bishopRow , bishopColum , team , gameObject);
		
	}



	public void destroyThreat(GameObject enemyPiece)
	{
//		print("function destroyThreat from bishop script has been activated");
		chessBoard.BishopVSThreatingPiece( bishopRow , bishopColum , team ,gameObject,enemyPiece);
	}

	public void threatAroundKing()
	{
		chessBoard.BishopVSAroundKing(bishopRow , bishopColum , team ,gameObject);
	}

	public void intersectEnemy()
	{
		chessBoard.bishopVSThreatingPath(bishopRow,bishopColum,team);
	}

	public void intersectCastling()
	{
		chessBoard.bishopVsCastlingTiles(team,bishopRow,bishopColum);
	}
		

}
