﻿using System.Collections;
using UnityEngine;

class PawnMovement : MonoBehaviour {
	private float moveSpeed = 30f;
	private float gridSize = 10f;

	//pieces' constraints variables
	//private bool firstMove = true;
	//this one to be accessed by the game engine object and modified depending on the positions of the other pieces
	public bool enemyDiagonal = false; 


	//movement constraints on the Chess Board
	private float maxX = 30f;
	private float minX = -40;
	private float maxZ = 0f;
	private float minZ = -70f;


	private enum Orientation {
		Horizontal,
		Vertical
	};
	private Orientation gridOrientation = Orientation.Horizontal;
	private bool allowDiagonals = false;
	private bool correctDiagonalSpeed = true;
	private Vector2 input;
	private bool isMoving = false;
	private Vector3 startPosition;
   	private Vector3 endPosition;
	private float t;
	private float factor;
	
	public void Update() {

		if (!isMoving) {
			input = new Vector2 (Input.GetAxis ("Horizontal"), Input.GetAxis ("Vertical"));
			if (!allowDiagonals) {
				if (Mathf.Abs (input.x) > Mathf.Abs (input.y)) {
					input.y = 0;
				} else {
					input.x = 0;
				}
			}
			
			if (input != Vector2.zero) {
				//if (Input.GetKey (KeyCode.W) && !Input.GetKey(KeyCode.D) && !Input.GetKey (KeyCode.A)) {
				//if(Input.GetKey (KeyCode.W)){
				StartCoroutine (move (transform));
				//}
				/*if(enemyDiagonal){
					if((Input.GetKey(KeyCode.W)&&Input.GetKey(KeyCode.D)))
					{
						StartCoroutine(move(transform));
					}
					if(Input.GetKey (KeyCode.W) && Input.GetKey (KeyCode.A)){
					}
				}
			}
				/*
				if (allowDiagonals) {
					if (Input.GetKey (KeyCode.D) && Input.GetKey (KeyCode.A)) {
						StartCoroutine (move (transform));
					}
				}
			}*/
			}
		}
	}
	
	public IEnumerator move(Transform transform) {
		isMoving = true;
		startPosition = transform.position;
		t = 0;

		//constraints for the pawn to be changed when the script is applied to another pieces
		/*if (firstMove) {
			gridSize = 20;
		} else {
			gridSize = 10;
		}*/
		allowDiagonals = enemyDiagonal;


		if(gridOrientation == Orientation.Horizontal) {
			endPosition = new Vector3(startPosition.x + System.Math.Sign(input.x) * gridSize,
			                          startPosition.y, startPosition.z + System.Math.Sign(input.y) * gridSize);
		} else {
			endPosition = new Vector3(startPosition.x + System.Math.Sign(input.x) * gridSize,
			                          startPosition.y + System.Math.Sign(input.y) * gridSize, startPosition.z);
		}
		
		if(allowDiagonals && correctDiagonalSpeed && input.x != 0 && input.y != 0) {
			factor = 0.7071f;
		} else {
			factor = 1f;
		}
		
		while (t < 1f) {
			t += Time.deltaTime * (moveSpeed/gridSize) * factor;

			transform.position = Vector3.Lerp(startPosition, endPosition, t);

			//constraining pieces' movement by board limits
			transform.position = new Vector3(Mathf.Clamp (transform.position.x, minX, maxX)
			                                 , 0
			                                 ,Mathf.Clamp (transform.position.z, minZ, maxZ));

			//firstMove = false;
			//Debug.Log(firstMove);
			yield return null;
		}

		if (endPosition.z < startPosition.z)
			isMoving = false;

		isMoving = false;
		yield return 0;
	}
}