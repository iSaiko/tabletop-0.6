﻿using UnityEngine;
using System.Collections;

public class Row : MonoBehaviour {


	public GameObject[] Tiles;
	internal Tile [] tiles ;

	void Start () {

		Tile temp;
		tiles = new Tile[Tiles.Length];


		for (int i = 0; i < Tiles.Length; i++) {
			temp = Tiles [i].GetComponent<Tile>();
			tiles.SetValue(temp,i);
	
		}

	
	}
	
	void Update () {
	
	}
}
