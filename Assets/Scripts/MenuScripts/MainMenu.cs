﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    public InputField regUsernameInput;
    public InputField regPasswordInput;
    public InputField regEmailInput;
    public Text registerState;

    public InputField logUsernameInput;
    public InputField logPasswordInput;
    public Text loginState;

    public GameObject ServerManager;
    public GameObject LoginForm;
    public GameObject mainPlayCanvas;
    public GameObject IntroCanvas;

    public Text NameLabel;

    public bool forceLogoutDone = false;
    //Database Handling Variables

    // Main URL with a slash in the end
    protected string url = "http://test.4mobily.net/games/";
    // Register user URL :
    protected string registerUrl = "saveUser.php";
    // Login user URL :
    protected string logUrl = "logUser.php";
    // Forgot Login user URL :
    protected string forgotLoginUrl = "forgotLogin.php";
    // Logout user URL :
    protected string logoutUrl = "logoutUser.php";
    // Change user profil URL :
    protected string changeProfilUrl = "changeProfil.php";
    // Register new game URL :
    protected string registerGameUrl = "saveGame.php";
    // Update game parameters URL :
    protected string updateGameUrl = "updateGame.php";
    // Search game URL :
    protected string searchGameUrl = "searchGame.php";
    // Backlist user URL :
    protected string blacklistUrl = "backlistUser.php";
    // Friendlist user URL :
    protected string friendlistUrl = "friendlist.php";
    // Exit game URL :
    protected string exitGameUrl = "exitGame.php";

    // User data
    public string userName;
    public string userMail;
    public string userPass;
    public string userId;
    public string userPublicIP;
    public string loginKey;
    public string userGameStatus;


	void Start () {
        DontDestroyOnLoad(gameObject);

	}
	
	void Update () {
		
	}

    public void AssignRegisterValuesAndRegister()
    {
        userName = regUsernameInput.text;
        userPass = regPasswordInput.text;
        userMail = regEmailInput.text;

        StartCoroutine(RegisterUser(userName, userMail, userPass, true));
    }

    public void LoginButton() {
        userName = logUsernameInput.text;
        userPass = logPasswordInput.text;

        StartCoroutine(LogUser(userName, userPass, 60));

    }

    // Save a new user on the dataBase
    public IEnumerator RegisterUser(string userName, string mail, string pass, bool sendMail)
    {
        string mailParam = "";
        if (sendMail)
        {
            mailParam = "&sendMail=1";
        }
        WWW www = new WWW(url + registerUrl + "?userName=" + System.Uri.EscapeUriString(userName)
            + "&mail=" + System.Uri.EscapeUriString(mail) + "&pass=" + System.Uri.EscapeUriString(pass)
            + "&privateIP=" + System.Uri.EscapeUriString(Network.player.ipAddress) + mailParam);
        yield return www;
        if (www.isDone)
        {
            string trimText = www.text.Trim();
            if (trimText == "Success")
            {
                registerState.color = Color.green;
                registerState.text = "Registeration Succesfull!";
                Debug.Log("Register Successful!");
            }
            else
            {

                registerState.color = Color.red;
                registerState.text = "Registeration Failed!";
            }
        }
    }//CreateAccount

    public IEnumerator LogUser(string userName, string pass, int callInterval)
    {
        WWW www = new WWW(url + logUrl + "?login=1&userName=" + System.Uri.EscapeUriString(userName)
            + "&pass=" + System.Uri.EscapeUriString(pass) + "&privateIP=" + System.Uri.EscapeUriString(Network.player.ipAddress)
            + "&callInterval=" + System.Uri.EscapeDataString(callInterval.ToString()));
        yield return www;
        if (www.isDone)
        {
            string trimText = www.text.Trim();
            string[] text = trimText.Split('|');
            if (text[0] == "Success")
            {
                this.userId = text[1];
                this.userName = text[2];
                this.userMail = text[3];
                this.userPublicIP = text[4];
                this.loginKey = text[5];

                loginState.color = Color.green;
                loginState.text = "Login Succesfull";

                ServerManager.GetComponent<m_UNETGameManager>().currentPlayerName = userName;
                NameLabel.text = userName;
                LoginForm.SetActive(false);
                mainPlayCanvas.SetActive(true);
            }
            else
            {
                loginState.color = Color.red;
                loginState.text = "Login Failed";
            }
        }
    }//LogUser

    // Logout user on the dataBase
    public IEnumerator LogOut()
    {
        WWW www = new WWW(url + logoutUrl + "?id=" + System.Uri.EscapeUriString(userId)
            + "&loginKey=" + System.Uri.EscapeUriString(loginKey));
        yield return www;
        if (www.isDone)
        {
            string trimText = www.text.Trim();
            if (trimText == "Success")
            {
                mainPlayCanvas.SetActive(false);
                IntroCanvas.SetActive(true);
            }
            else
            {
                
            }
        }
    }

    public IEnumerator ForceLogOut()
    {
        WWW www = new WWW(url + logoutUrl + "?id=" + System.Uri.EscapeUriString(userId)
            + "&loginKey=" + System.Uri.EscapeUriString(loginKey));
        yield return www;
        if (www.isDone)
        {
            string trimText = www.text.Trim();
            if (trimText == "Success")
            {
                forceLogoutDone = true;
                Application.Quit();
            }
            else
            {

            }
        }
    }

    public void LogoutButton()
    {
        StartCoroutine(LogOut());
    }

    void OnApplicationQuit()
    {
        StartCoroutine(ForceLogOut());

        if (!forceLogoutDone)
            Application.CancelQuit();
    }
}
