﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuHandler : MonoBehaviour {

    public GameObject IntroCanvas;
    public GameObject LoginForm;
    public GameObject SignupForm;
	// Use this for initialization

    public void BackButtonPress()
    {
        LoginForm.SetActive(false);
        SignupForm.SetActive(false);
        IntroCanvas.SetActive(true);
    }

    public void LoginButtonPress()
    {
        LoginForm.SetActive(true);
        SignupForm.SetActive(false);
        IntroCanvas.SetActive(false);

    }

    public void SignupButtonPress()
    {
        SignupForm.SetActive(true);
        LoginForm.SetActive(false);
        IntroCanvas.SetActive(false);
    }
}
