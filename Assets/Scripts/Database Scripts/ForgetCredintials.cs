﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ForgetCredintials : MonoBehaviour {

    protected string forgetURL = "https://multigame.4mobily.net/public/api/v1/user/forgetUsername/";
    protected string resetPasswordURL = "https://multigame.4mobily.net/public/api/v1/user/forgetPassword/";
    // Use this for initialization

    public InputField emailInput;
    public Toggle usernameToggle;
    public Toggle passwordToggle;
    public Button resetButton;

	void Start () {
        //ForgetUsernameOnClick();
       // ResetPasswordOnClick();
	}
	
	// Update is called once per frame
	void Update () {
       
	}

    public void ForgetButtonClick()
    {
        if (usernameToggle.isOn && !passwordToggle.isOn)
        {
            ForgetUsernameOnClick();
        }
        else if (passwordToggle.isOn && !usernameToggle.isOn)
        {
            ResetPasswordOnClick();
        }
        else if (usernameToggle.isOn && passwordToggle.isOn)
        {
            Debug.Log("Both Coroutine Running");
            StartCoroutine(bothChecked());
        }
    }

    public IEnumerator bothChecked()
    {

        ForgetUsernameOnClick();
        yield return new WaitForSeconds(5);
        ResetPasswordOnClick();
    }

    public void ForgetUsernameOnClick()
    {
        StartCoroutine(ForgetUsername(emailInput.text));
    }

    public void ResetPasswordOnClick()
    {
        StartCoroutine(ResetPassword(emailInput.text));
    }

    public IEnumerator ForgetUsername(string email)
    {
        WWWForm toSendForm = new WWWForm();
        Debug.Log(email);
        toSendForm.AddField("", email);
        byte[] rawData = toSendForm.data;

        WWW www = new WWW(forgetURL + email, rawData);

        yield return www;

        if (www.isDone)
        {
            Response myResponse = JsonUtility.FromJson<Response>(www.text);
            Debug.Log(myResponse.status);

            if (myResponse.status == 200)
            {
                //reset successful}
                Debug.Log("user Reset Successful!");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("user Reset Successful!"));
            }
            else if (myResponse.status == 404)
            {
                //user not found
                Debug.Log("User not found!");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("User not found!"));
            }
            else if (myResponse.status == 211)
            {
                //you already requested a username change wait for 1 hour since the last reset
                Debug.Log("Reset Request Done in the last 1 hour");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("you already requested a username change wait for 1 hour since the last reset"));
            }
        }
    }

    public IEnumerator ResetPassword(string email)
    {
        WWWForm toSendForm = new WWWForm();
        Debug.Log(email);
        toSendForm.AddField("", email);
        byte[] rawData = toSendForm.data;

        WWW www = new WWW(resetPasswordURL + email,rawData);

        yield return www;

        if (www.isDone)
        {
            Response myResponse = JsonUtility.FromJson<Response>(www.text);
            Debug.Log(myResponse.status);
            if (myResponse.status == 200)
            {
                //reset successful}
                Debug.Log("Password Reset Successful!");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Password Reset Successful!"));

            }
            else if (myResponse.status == 404)
            {
                //user not found
                Debug.Log("User not found!");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("User not found!"));

            }
            else if (myResponse.status == 212)
            {
                //you already requested a username change wait for 10 hour since the last reset
                Debug.Log("you requested password reset in the last 10 hours");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("you requested password reset in the last 10 hours"));
            }
        }
    }
}

[System.Serializable]
public class Response
{
    public int status;
}