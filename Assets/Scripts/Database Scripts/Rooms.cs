﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Rooms : MonoBehaviour {
    public static Rooms instance { set; get; }
    LoginResult roomLogIn;
    roomResult successfulRoom;
    protected string createRoomURL = "https://multigame.4mobily.net/public/api/v1/room/create";
    protected string getRoomInfoURL = "https://multigame.4mobily.net/public/api/v1/room/";
    protected string getRoomListURL = "https://multigame.4mobily.net/public/api/v1/room/list";
    protected string JoinRoomURL = "https://multigame.4mobily.net/public/api/v1/room/";
    protected string LeaveRoomURL = "https://multigame.4mobily.net/public/api/v1/room/";
    protected string StartRoomURL = "https://multigame.4mobily.net/public/api/v1/room/";
    protected string SetMeAsReadyURL = "https://multigame.4mobily.net/public/api/v1/room/";
    protected string SetMeAsNotReadyURL = "https://multigame.4mobily.net/public/api/v1/room/";
    protected string EndGameRoomURL = "https://multigame.4mobily.net/public/api/v1/room/";
    protected string ForceQuitGameURL = "https://multigame.4mobily.net/public/api/v1/room/";
//    public InputField roomName, password;
//    public Toggle isPassword;
    public string gameType = "";
    public UiMenuValues UiMenuValuesScript;
	public GameObject roomUiElement;
	public GameObject roomsContentView;
    bool forceQuitGame, forceLeaveRoom;
    // Use this for initialization
	void Start () {
        instance = this;
        UiMenuValuesScript = GameObject.Find("UiMenuValuesHandler").GetComponent<UiMenuValues>();
        forceLeaveRoom = forceQuitGame = false;
		DontDestroyOnLoad(this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    //done
    public void createRoom(ulong matchid)
    {
        Debug.Log("Create Room Routine");
		UiAnimationManager.instance.resetWaitingRoomUI();
        StartCoroutine(createRoomRoutine(matchid));
    }
	public void successLogin(){
		roomLogIn = GameObject.Find("loginData").GetComponent<Login>().successfulLoginUser;
	}
	public void successLogOut()
	{roomLogIn = null;}


    //done
    public void getRoomInfo(int room_id)
    {
        StartCoroutine(getRoomInfoRoutine(room_id));
    }
    //done
    public void getRoomSearchResult()
    {
        string roomName = "";
        string page = "";
        StartCoroutine(getRoomSearchResultRoutine(roomName, page));
    }
	public void getRoomSearchResult(string roomName)
	{
		string page = "";
		StartCoroutine(getRoomSearchResultRoutine(roomName, page));
	}

    public void joinRoom(int room_id, string password, long match_id)
    {
        Debug.Log("2" + UiMenuValues.instance.roomToEnterPassword);
		UiAnimationManager.instance.resetWaitingRoomUI();
        StartCoroutine(JoinRoomRoutine(room_id, password, match_id));
    }
    public void leaveRoom(int room_id)
    {
        StartCoroutine(LeaveRoomRoutine(room_id));
    }
    public void leaveRoom()
    {
        StartCoroutine(LeaveRoomRoutine(UiMenuValues.instance.activeRoomId));
        StartCoroutine(SetMeAsNotReadyRoutine(UiMenuValues.instance.activeRoomId));

    }
    public void startRoom(int room_id)
    {
        StartCoroutine(StartRoomRoutine(room_id));
    }

    public void SetReadyForRoom(int room_id)
    {
        StartCoroutine(SetMeAsReadyRoutine(room_id));
    }
    public void SetNotReadyForRoom(int room_id)
    {
        StartCoroutine(SetMeAsNotReadyRoutine(room_id));
    }
    public void EndGameRoom(int room_id, int winner_id)
    {
        StartCoroutine(EndRoomGameRoutine(room_id, winner_id));
    }
    public void ForceQuitGameRoom()
    {
        int room_id = 0;
        StartCoroutine(ForceQuitGame(room_id));
    }
    //done
    private IEnumerator createRoomRoutine(ulong matchid)
    {
        WWWForm toSendForm = new WWWForm();
        toSendForm.AddField("room_name", UiMenuValuesScript.roomNameToBeCreated);
        if (UiMenuValuesScript.creatRoomWithPassword)
        {
            toSendForm.AddField("use_password", "yes");
            toSendForm.AddField("password", UiMenuValuesScript.creatRoomPassword);
        }
        if(!UiMenuValuesScript.creatRoomWithPassword)
        {
            toSendForm.AddField("use_password", "no");
        }
		if(UiMenuValues.instance.gameType == "snake"){
			toSendForm.AddField("round_time",10);
		}else if(UiMenuValues.instance.gameType == "chess"){
			toSendForm.AddField("round_time",UiMenuValues.instance.roundTime);
		} else if(UiMenuValues.instance.gameType == "domino"){
			toSendForm.AddField("round_time",10);
			toSendForm.AddField("game_score",UiMenuValues.instance.roundTime);
		}

		toSendForm.AddField("game_type", UiMenuValues.instance.gameType);
        toSendForm.AddField("match_id", matchid.ToString());
        Dictionary<string, string> headers = toSendForm.headers;
        byte[] rawData = toSendForm.data;
        Debug.Log(roomLogIn.data.token);
        headers["Authorization"] = "Bearer " + roomLogIn.data.token;
        WWW www = new WWW(createRoomURL, rawData, headers);

        yield return www;

        if (www.isDone)
        {
			UiAnimationManager.instance.hideLoadingPanel();

            roomResult result = JsonUtility.FromJson<roomResult>(www.text);
            Debug.Log(result.status);
            if (result.status == 200)
            {
                Debug.Log("Room was created successfuly!");
                UiMenuValues.instance.activeRoomId = result.data.room_id;
                getRoomInfo(result.data.room_id);
				UiAnimationManager.instance.showLoadingPanel();

                
            }
            else if (result.status == 210)
            {
                Debug.Log("invalid roomname!");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Invalid room name",1.5f));
				if(gameType == "chess"){

					GameObject go = GameObject.Find("UNETChessObject");
					m_UNETChess.instance.ShutDownConnection();
					Destroy(go,1f);}
				

            }
            else if (result.status == 216)
            {
                Debug.Log("User already have room");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("User already have room",1.5f));if(gameType == "chess"){

					GameObject go = GameObject.Find("UNETChessObject");
					m_UNETChess.instance.ShutDownConnection();
					Destroy(go,1f);}

            }
            else if (result.status == 217)
            {
                Debug.Log("Error in create new room");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Error in create new room",1.5f));if(gameType == "chess"){

					GameObject go = GameObject.Find("UNETChessObject");
					m_UNETChess.instance.ShutDownConnection();
					Destroy(go,1f);}
            }
            else if (result.status == 215)
            {
                Debug.Log("Round time Invalid");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Round time Invalid",1.5f));if(gameType == "chess"){

					GameObject go = GameObject.Find("UNETChessObject");
					m_UNETChess.instance.ShutDownConnection();
					Destroy(go,1f);}
            }
            else if (result.status == 214)
            {
                Debug.Log("Game Type Invalid");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Game Type Invalid",1.5f));if(gameType == "chess"){

					GameObject go = GameObject.Find("UNETChessObject");
					m_UNETChess.instance.ShutDownConnection();
					Destroy(go,1f);}
            }
            else if (result.status == 213)
            {
                Debug.Log("Room name Exists");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Room name Exists",1.5f));if(gameType == "chess"){

					GameObject go = GameObject.Find("UNETChessObject");
					m_UNETChess.instance.ShutDownConnection();
					Destroy(go,1f);}
            }
            else if (result.status == 207)
            {
                Debug.Log("Invalid Password");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Invalid Password",1.5f));if(gameType == "chess"){

					GameObject go = GameObject.Find("UNETChessObject");
					m_UNETChess.instance.ShutDownConnection();
					Destroy(go,1f);}
            }
        }
    }
    //done
    private IEnumerator getRoomInfoRoutine(int room_id)
    {
        //getRoomInfoURL + "{" + room_id + "}" + "/info";
        WWWForm toSendForm = new WWWForm();
        toSendForm.AddField("", room_id);
        Dictionary<string, string> headers = toSendForm.headers;
        byte[] rawData = toSendForm.data;
        Debug.Log(roomLogIn.data.token);
        headers["Authorization"] = "Bearer " + roomLogIn.data.token;
        WWW www = new WWW(getRoomInfoURL + room_id +  "/info",rawData,headers);
        Debug.Log(getRoomInfoURL  + room_id  + "/info");
        yield return www;

        if (www.isDone)
        {
			UiAnimationManager.instance.hideLoadingPanel();

            Debug.Log("Send Done");
            foundRoomInfo result = JsonUtility.FromJson<foundRoomInfo>(www.text);
            Debug.Log(www.text);
            if (result.status == 218)
            {
                Debug.Log("Room not found");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Room not found"));
            }
            else if (result.status == 200)
            {
                Debug.Log("Room Found");
                //if (result.data == null)
                //{
                //    Debug.Log("Room Data Null");
                //}
                //Here we are going to assign the data from the database to the waitingroom
                //Transfer to Waiting room UI layout 
				UiAnimationManager.instance.showWaitingRoomData(result.data.host.username,"",result.data.players.Length.ToString(),result.data.game_type.ToString(),result.data.game_type.ToString() == "domino" ? result.data.game_score.ToString() : result.data.round_time.ToString() ,"Not Ready");

            }
        }
    }
    //done
    private IEnumerator getRoomSearchResultRoutine(string room_name, string page)
    {
		UiAnimationManager.instance.refreshButton.interactable = false;
		UiAnimationManager.instance.searchButton.interactable = true;


        WWWForm toSendForm = new WWWForm();
        toSendForm.AddField("search_word", room_name);
        toSendForm.AddField("page", page);

        Dictionary<string, string> headers = toSendForm.headers;
        byte[] rawData = toSendForm.data;
        Debug.Log(roomLogIn.data.token);
        headers["Authorization"] = "Bearer " + roomLogIn.data.token;
        WWW www = new WWW(getRoomListURL, rawData, headers);

        yield return www;


        if (www.isDone)
        {
            searchRoomResult result = JsonUtility.FromJson<searchRoomResult>(www.text);
			Debug.Log(www.text);
			UiAnimationManager.instance.refreshButton.interactable = true;
			UiAnimationManager.instance.searchButton.interactable = true;
            if(result.status == 200)
            {
                Debug.Log("Successful Found Rooms");
                for (int i = 0; i < result.data.Length; i++)
                {
					if((result.data[i].game_type != UiMenuValues.instance.gameType))
						continue;


					GameObject temp =  Instantiate(roomUiElement);
                    Debug.Log("Match ID from Database Result"+result.data[i].match_id);
                    long match_id_passing = long.Parse(result.data[i].match_id);
					temp.GetComponent<RoomUiElement>().assignRoomData(result.data[i].room_name, result.data[i].host.username, result.data[i].game_type == "domino" ? result.data[i].game_score : result.data[i].round_time, result.data[i].use_password, result.data[i].id,match_id_passing,(i%2 == 0) ? true:false);
					temp.transform.SetParent(roomsContentView.transform);
					temp.transform.localScale = new Vector3(1,1,1);
					RoomsUiMananger.instance.updateContentHeight();
                }
            }
        }
    }

	public void cleanRoomsList()
	{
			for(int i = 0 ;  i < roomsContentView.transform.childCount ; i++)
			{
				Destroy( roomsContentView.transform.GetChild(i).gameObject);
			}
	}
   
    private IEnumerator JoinRoomRoutine(int room_id, string room_password, long match_id)
    {
        Debug.Log(room_password);
        WWWForm toSendForm = new WWWForm();
        toSendForm.AddField("password", room_password);
        Dictionary<string, string> headers = toSendForm.headers;
        byte[] rawData = toSendForm.data;
        headers["Authorization"] = "Bearer " + roomLogIn.data.token;
        Debug.Log("room_id" + room_id + "room_password" + room_password + "ip" + "MatchID " + match_id);
        WWW www = new WWW(JoinRoomURL + room_id + "/join", rawData, headers);

        yield return www;

        if (www.isDone)
        {
            JoinRoomResult result = JsonUtility.FromJson<JoinRoomResult>(www.text);
			UiAnimationManager.instance.hideJoinRoomPasswordPanel();

            Debug.Log(www.text);
            if (result.status == 200)
            {
                forceQuitGame = forceLeaveRoom = false;
                Debug.Log("Okay");
                switch (gameType)
                {
                    case "chess":
                        m_UNETGameManager.instance.ConnectToChess(match_id,room_password);
                        Debug.Log("Successfully joined from rooms with server");
                        break;

                    case "domino":
                        m_UNETGameManager.instance.ConnectToDomino(match_id, room_password);
					    Debug.Log("Successfully joined from rooms with server 'Domino'");
                        break;

                    case "snake":
                        m_UNETGameManager.instance.ConnectToSnake(match_id, room_password);
                        Debug.Log("Successfully joined from rooms with server 'Snake'");
                        break;
                }
            }
            else if (result.status == 218)
            {
                Debug.Log("Room Not Found");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Room Not Found",1.5f));

            }
            else if (result.status == 219)
            {
                Debug.Log("Room is Full");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Room is Full",1.5f));

            }
            else if (result.status == 221)
            {
                Debug.Log("Wrong Password");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Wrong Password",3f));
            }
            else if (result.status == 220)
            {
                Debug.Log("Cannot Join Room");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Cannot Join Room",1.5f));

            }
            else if (result.status == 227)
            {
                Debug.Log("User Already in Another Room");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("User Already in Another Room",1.5f));

            }
        }
        
    }

    private IEnumerator LeaveRoomRoutine(int room_id)
    {
        WWWForm toSendForm = new WWWForm();
        toSendForm.AddField("", room_id);
        Dictionary<string, string> headers = toSendForm.headers;
        byte[] rawData = toSendForm.data;
        headers["Authorization"] = "Bearer " + roomLogIn.data.token;
        WWW www = new WWW(LeaveRoomURL + room_id + "/leave", rawData, headers);

        yield return www;

        if (www.isDone)
        {
            LeaveRoomResult result = JsonUtility.FromJson<LeaveRoomResult>(www.text);
            Debug.Log(www.text);
            if (result.status == 200)
            {

                Debug.Log("Left Room Successfuly!");


                if (gameType == "chess")
                {
                    m_UNETChess c = FindObjectOfType<m_UNETChess>();
                    if (c.isHost)
                    {
                        c.UNETSend("CLFT|" + 1);
                        c.ClientChess("CLFT|" + 1);
                        //forceQuitGame = forceLeaveRoom = true;
                    }
                    else{

                        c.UNETSend("CLFT|" + 2); 
                        c.ClientChess("CLFT|" + 2);
                        //forceQuitGame = forceLeaveRoom = true;
                    }
                }
                else if (gameType == "domino")
                {
                    m_UNETDomino c = FindObjectOfType<m_UNETDomino>();
                    if (c.isHost)
                    {
                        c.UNETSend("CLFT|" + 1); 
                        c.ClientDomino("CLFT|" + 1);
                        //forceQuitGame = forceLeaveRoom = true;
                    }
                    else
                    {

                        c.UNETSend("CLFT|" + 2);
                        c.ClientDomino("CLFT|" + 2);
                        //forceQuitGame = forceLeaveRoom = true;
                    }
                }
                else if (gameType == "snake")
                {
                    m_UNETSnake c = FindObjectOfType<m_UNETSnake>();
                    if (c.isHost)
                    {
                        c.UNETSend("CLFT|" + 1); 
                        c.ClientSnake("CLFT|" + 1);
                        //forceQuitGame = forceLeaveRoom = true;
                    }
                    else
                    {

                        c.UNETSend("CLFT|" + 2); 
                        c.ClientSnake("CLFT|" + 2);
                        //forceQuitGame = forceLeaveRoom = true;
                    }
                }
            }
            else if (result.status == 218)
            {
                Debug.Log("Room wasn't found");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Room wasn't found",1.5f));
            }
            else if (result.status == 200)
            {
                Debug.Log("Cannot leave room");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Cannot leave room",1.5f));

            }
        }
    }


    private IEnumerator ForceLeaveRoomRoutine(int room_id)
    {
        WWWForm toSendForm = new WWWForm();
        toSendForm.AddField("", room_id);
        Dictionary<string, string> headers = toSendForm.headers;
        byte[] rawData = toSendForm.data;
        headers["Authorization"] = "Bearer " + roomLogIn.data.token;
        WWW www = new WWW(LeaveRoomURL + room_id + "/leave", rawData, headers);

        yield return www;

        if (www.isDone)
        {
            LeaveRoomResult result = JsonUtility.FromJson<LeaveRoomResult>(www.text);
            Debug.Log(www.text);
            if (result.status == 200)
            {

                Debug.Log("Left Room Successfuly!");


                if (gameType == "chess")
                {
                    m_UNETChess c = FindObjectOfType<m_UNETChess>();
                    if (c.isHost)
                    {
                        c.UNETSend("CLFT|" + 1);
                        c.ClientChess("CLFT|" + 1);
                    }
                    else
                    {

                        c.UNETSend("CLFT|" + 2);
                        c.ClientChess("CLFT|" + 2);
                    }
                    //ServerChess s = FindObjectOfType<ServerChess>();
                    //if (s != null)
                    //  Destroy(s.gameObject);
                    //if (c != null)
                    //  Destroy(c.gameObject);

                    forceQuitGame = forceLeaveRoom = true;
                    //c.CloseSocket();
                    Application.Quit();
                }
                else if (gameType == "domino")
                {
                    m_UNETDomino c = FindObjectOfType<m_UNETDomino>();
                    if (c.isHost)
                    {
                        c.UNETSend("CLFT|" + 1);
                        c.ClientDomino("CLFT|" + 1);
                    }
                    else
                    {

                        c.UNETSend("CLFT|" + 2);
                        c.ClientDomino("CLFT|" + 2);
                    }

                    forceQuitGame = forceLeaveRoom = true;
                    //c.CloseSocket();
                    Application.Quit();
                }
                else if (gameType == "snake")
                {
                    m_UNETSnake c = FindObjectOfType<m_UNETSnake>();
                    if (c.isHost)
                    {
                        c.UNETSend("CLFT|" + 1);
                        c.ClientSnake("CLFT|" + 1);

                    }
                    else
                    {

                        c.UNETSend("CLFT|" + 2);
                        c.ClientSnake("CLFT|" + 2);

                    }

                    forceQuitGame = forceLeaveRoom = true;
                    // c.CloseSocket();
                    Application.Quit();
                }

                else if (result.status == 218)
                {
                    Debug.Log("Room wasn't found");
					StartCoroutine(UiAnimationManager.instance.showFailureMassage("Room wasn't found",1.5f));
                }
                else if (result.status == 200)
                {
                    Debug.Log("Cannot leave room");
					StartCoroutine(UiAnimationManager.instance.showFailureMassage("Cannot leave room",1.5f));
                }
            }

        }
    }
    private IEnumerator StartRoomRoutine(int room_id)
    {
        WWWForm toSendForm = new WWWForm();
        toSendForm.AddField("", room_id);
        Dictionary<string, string> headers = toSendForm.headers;
        byte[] rawData = toSendForm.data;
        headers["Authorization"] = "Bearer " + roomLogIn.data.token;
        WWW www = new WWW(StartRoomURL + room_id + "/start", rawData, headers);

        yield return www;

        if (www.isDone)
        {
			
            ReadyState result = JsonUtility.FromJson<ReadyState>(www.text);
            Debug.Log(www.text);
			UiAnimationManager.instance.hideLoadingPanel();

            if (result.status == 200)
            {
                Debug.Log("Joined Room!");
                if (gameType == "chess")
                {
                    m_UNETChess c = FindObjectOfType<m_UNETChess>();
                    c.UNETSend("CSTRT|");
                    c.ClientChess("CSTRT|");

                }
                else if (gameType == "domino")
                {
                    m_UNETDomino c = FindObjectOfType<m_UNETDomino>();
                    c.UNETSend("CSTRT|");
                    c.ClientDomino("CSTRT|");

                }
                else if (gameType == "snake")
                {
                    m_UNETSnake c = FindObjectOfType<m_UNETSnake>();
                    c.UNETSend("CSTRT|"); 
                    c.ClientSnake("CSTRT|");

                }
				Login.instance.activeRoomId = UiMenuValues.instance.activeRoomId;

            }
            else if (result.status == 225)
            {
                Debug.Log("Not Owner Of Room");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Not Owner Of Room",1.5f));
            }
            else if (result.status == 223)
            {
                Debug.Log("Player Count less than 2");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Player Count less than 2",1.5f));
            }
            else if (result.status == 224)
            {
                Debug.Log("room not ready");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("room not ready",1.5f));
            }
            else if (result.status == 222)
            {
                Debug.Log("cannot update room");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("cannot update room",1.5f));
            }
            else if (result.status == 218)
            {
                Debug.Log("room not found");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("room not found",1.5f));
            }
        }
    }
    private IEnumerator SetMeAsReadyRoutine(int room_id)
    {
        WWWForm toSendForm = new WWWForm();
        toSendForm.AddField("", room_id);
        Dictionary<string, string> headers = toSendForm.headers;
        byte[] rawData = toSendForm.data;
        headers["Authorization"] = "Bearer " + roomLogIn.data.token;
        WWW www = new WWW(SetMeAsReadyURL + room_id + "/ready", rawData, headers);

        yield return www;

        if (www.isDone)
        {
            ReadyState result = JsonUtility.FromJson<ReadyState>(www.text);

            print(www.text);
            if (result.status == 200)
            {
                //okay successful
                if (gameType == "chess")
                {
                    m_UNETChess c = FindObjectOfType<m_UNETChess>();
                    if (c.isHost)
                    {
                        c.UNETSend("CRDY|" + "1");
                        c.ClientChess("CRDY|" + "1");

                    }

                    else
                    {
                        c.UNETSend("CRDY|" + "2"); 
                        c.ClientChess("CRDY|" + "2");
                    }
                }
                else if (gameType == "snake")
                {
                    m_UNETSnake c = FindObjectOfType<m_UNETSnake>();
                    if (c.isHost)
                    {
                        c.UNETSend("CRDY|" + "1");
                        c.ClientSnake("CRDY|" + "1");

                    }
                    else
                    {
                        c.UNETSend("CRDY|" + "2");
                        c.ClientSnake("CRDY|" + "2");
                    }
                }
                else if (gameType == "domino")
                {
                    m_UNETDomino c = FindObjectOfType<m_UNETDomino>();
                    if (c.isHost)
                    {
                        c.UNETSend("CRDY|" + "1");
                        c.ClientDomino("CRDY|" + "1");
                    }
                    else
                    {
                        c.UNETSend("CRDY|" + "2"); 
                        c.ClientDomino("CRDY|" + "2");
                    }
                }
            }
            else if (result.status == 222)
            {
				UiAnimationManager.instance.hideLoadingPanel();
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("cannot update room",1.5f));
            }
            else if (result.status == 218)
            {
				UiAnimationManager.instance.hideLoadingPanel();
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("room not found",1.5f));
            }
        }
    }

    private IEnumerator SetMeAsNotReadyRoutine(int room_id)
    {
        WWWForm toSendForm = new WWWForm();
        toSendForm.AddField("", room_id);
        Dictionary<string, string> headers = toSendForm.headers;
        byte[] rawData = toSendForm.data;
        headers["Authorization"] = "Bearer " + roomLogIn.data.token;
        WWW www = new WWW(SetMeAsNotReadyURL + room_id + "/notReady", rawData, headers);

        yield return www;

        if (www.isDone)
        {
            ReadyState result = JsonUtility.FromJson<ReadyState>(www.text);
			Debug.Log(www.text);
            if (result.status == 200)
            {
                //okay successful
                if (gameType == "chess")
                {
                    m_UNETChess c = FindObjectOfType<m_UNETChess>();
                    if (c.isHost)
                    {
                        c.UNETSend("CNRDY|" + "1");
                        c.ClientChess("CNRDY|" + "1");
                    }

                    else
                    {
                        c.UNETSend("CNRDY|" + "2");
                        c.ClientChess("CNRDY|" + "2");
                    }
                }
                else if (gameType == "snake")
                {
                    m_UNETSnake c = FindObjectOfType<m_UNETSnake>();
                    if (c.isHost)
                    {
                        c.UNETSend("CNRDY|" + "1");
                        c.ClientSnake("CNRDY|" + "1");
                    }
                    else
                    {
                        c.UNETSend("CNRDY|" + "2");
                        c.ClientSnake("CNRDY|" + "2");
                    }
                }
                else if (gameType == "domino")
                {
                    m_UNETDomino c = FindObjectOfType<m_UNETDomino>();
                    if (c.isHost)
                    {
                        c.UNETSend("CNRDY|" + "1"); c.ClientDomino("CNRDY|" + "1");
                    }
                    else
                    {
                        c.UNETSend("CNRDY|" + "2");
                        c.ClientDomino("CNRDY|" + "2");
                    }
                }
            }
            else if (result.status == 222)
            {
                //cannot update room
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("cannot update room",1.5f));
            }
            else if (result.status == 218)
            {
                //room not found 
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("room not found",1.5f));
            }
        }
    }
    private IEnumerator EndRoomGameRoutine(int room_id, int winner_id)
    {
        WWWForm toSendForm = new WWWForm();
        toSendForm.AddField("winner_id", winner_id);
        Dictionary<string, string> headers = toSendForm.headers;
        byte[] rawData = toSendForm.data;
        headers["Authorization"] = "Bearer " + roomLogIn.data.token;
        WWW www = new WWW(EndGameRoomURL + room_id + "/endRoom", rawData, headers);

        yield return www;

        if (www.isDone)
        {
			
            ReadyState result = JsonUtility.FromJson<ReadyState>(www.text);
			Debug.Log(result.ToString());
            if (result.status == 200)
            {
                //okay successful
            }
            else if (result.status == 222)
            {
                //cannot update room
            }
            else if (result.status == 218)
            {
                //room not found 
            }
            else if (result.status == 201)
            {
                //there is data required}
            }
            else if (result.status == 225)
            {
                //not the owner of the room
            }

        }
    }
    private IEnumerator ForceQuitGame(int room_id)
    {
        WWWForm toSendForm = new WWWForm();
        toSendForm.AddField("", room_id);
        Dictionary<string, string> headers = toSendForm.headers;
        byte[] rawData = toSendForm.data;
        headers["Authorization"] = "Bearer " + roomLogIn.data.token;
        WWW www = new WWW(ForceQuitGameURL + room_id + "/forceQuit", rawData, headers);

        yield return www;

        if (www.isDone)
        {
            if (int.Parse(www.text) == 200)
            {
                //okay successful
            }
        }
    }

    
	void OnApplicationQuit()
	{

		if (!forceLeaveRoom && !forceQuitGame && ( m_UNETChess.instance != null /*|| m_UNETChess.instance != null || m_UNETSnake.instance != null)*/)) //
        {
            Application.CancelQuit();
			StartCoroutine(ForceLeaveRoomRoutine(UiMenuValues.instance.activeRoomId));
			m_UNETChess.instance = null ;
			m_UNETDomino.instance = null ;
		    m_UNETSnake.instance = null ;
        }



       // leaveRoom();
	}






}

[System.Serializable]
public class toSendRoom
{
    public string room_name;
    public string use_password;
    public int round_time;
    public string game_type;
    public string password;
    public string Authorization;
}

[System.Serializable]
public class roomResult
{
    public int status;
    public roomData data;
}
[System.Serializable]
public class roomData
{
    public int room_id;
}


//All the classes for the Get Room Info Coroutine
[System.Serializable]
public class foundRoomInfo
{
    public int status;
    public foundRoomData data;
}
[System.Serializable]
public class foundRoomData
{
    public string room_name;
    public int round_time;
    public int use_password;
    public int user_id;
	public int id;
    public string state;
	public string game_type;
	public string game_score;
    public ulong match_id;
	public foundRoomHost host;
	public foundRoomPlayers[] players;
}
[System.Serializable]
public class foundRoomHost
{
    public int id;
    public string username;
    public string first_name;
    public string last_name;
}
[System.Serializable]
public class foundRoomPlayers
{
    public int id;
    public string username;
    public string first_name;
    public string last_name;
    public foundRoomPlayersPivot pivot;
}
[System.Serializable]
public class foundRoomPlayersPivot
{
    public int room_id;
    public int user_id;
}

[System.Serializable]
public class searchRoomResult
{
    public int status;
    public searchRoomResultData[] data;
}
[System.Serializable]
public class searchRoomResultData
{
    public string room_name;
    public int round_time;
    public int use_password;
    public int user_id;
    public int id;
//	public string 
    public string match_id;
	public string game_type;
	public int game_score;
    public searchRoomResultDataUser host;
}
[System.Serializable]
public class searchRoomResultDataUser
{
    public int id;
    public string username;
    public string first_name;
    public string last_name;
}

[System.Serializable]
public class JoinRoomResult
{
    public int status;
}

[System.Serializable]
public class LeaveRoomResult{
    public int status;
    public LeaveRoomResultData data;
}

[System.Serializable]
public class LeaveRoomResultData
{
    public string room_name;
    public string ip_address;
}

public class ReadyState
{
    public int status;
}