﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Register : MonoBehaviour {

    public InputField usernameInput, passwordInput, mailInput, phoneInput;
    public string countryGrabber;
    protected string registerURL = "https://multigame.4mobily.net/public/api/v1/user/register";
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

        public IEnumerator RegisterUser(User toRegisterUser)
    {
        WWWForm toSendForm = new WWWForm();
        toSendForm.AddField("mobile_country_code", toRegisterUser.mobile_country_code);
        toSendForm.AddField("mobile_number", toRegisterUser.mobile_number);
        toSendForm.AddField("country_code", toRegisterUser.country_code);
        toSendForm.AddField("email", toRegisterUser.email);
        toSendForm.AddField("username", toRegisterUser.username);

        Dictionary<string, string> headers = toSendForm.headers;
        byte[] rawData = toSendForm.data;

        headers["password"] = toRegisterUser.password.ToString() ;
        WWW www = new WWW(registerURL, rawData, headers);
		UiAnimationManager.instance.showLoadingPanel();
        yield return www;
		UiAnimationManager.instance.hideLoadingPanel();
        if(www.isDone){
        //    //string[] result = www.text.Split(':');
        //    Debug.Log(result[0]);
        //    Debug.Log(result[1]);
            RegisterResult result = JsonUtility.FromJson<RegisterResult>(www.text);
            Debug.Log("Status ->" + result.status);
            if(result.status == 200)
            {
                Debug.Log("Registration Successful!");
				UiMenuValues.instance.putRegistrationDataInLoginMenu();
				UiAnimationManager.instance.showRegisterationSuccessfulPanel();

            }
            else if (result.status == 202)
            {
                Debug.Log("Entered Mobile is Invalid!");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Entered Mobile is Invalid!"));
            }
            else if (result.status == 204)
            {
                Debug.Log("Entered mail is Invalid!");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Entered mail is Invalid!"));

            }
            else if (result.status == 205)
            {
                Debug.Log("Entered Mobile is already exists!");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Entered Mobile is already exists!"));

            }
            else if (result.status == 206)
            {
                Debug.Log("Registered Mail Already Exists!");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Registered Mail Already Exists!"));

            }
            else if (result.status == 207)
            {
                Debug.Log("Entered Password is Invalid!");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Entered Password is Invalid!"));
            }
            else if (result.status == 208)
            {
                Debug.Log("Entered Username is Invalid!");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Entered Username is Invalid!"));

            }
            else if (result.status == 209)
            {
                Debug.Log("Username Already Exists!");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Username Already Exists!"));

            }
			else if (result.status == 201)
			{
				Debug.Log("Data Required Missing");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Data Required Missing, Be Sure To Fill all Fields"));

			}
        }


    }

        public void RegisterOnClick()
        {
            User newUser = new User();
            newUser.password = passwordInput.text;
            countryGrabber = GameObject.Find("flag panel button").GetComponent<CountryDetection>().countryCode;
            Debug.Log(countryGrabber);
            newUser.mobile_country_code = countryGrabber;
            newUser.mobile_number = phoneInput.text;
            newUser.country_code = countryGrabber;
            newUser.email = mailInput.text;
            newUser.username = usernameInput.text;
            string serializedUser = JsonUtility.ToJson(newUser);
            Debug.Log(serializedUser);
            StartCoroutine(RegisterUser(newUser));
        }
}


[System.Serializable]
public class User
{
    public string first_name;
    public string last_name;
    public string username;
    public string password;
    public string email;
    public string country_code;
    public string mobile_number;
    public string mobile_country_code;
}

[System.Serializable]
public class RegisterResult
{
    public int status;
    public RegisterData data;
}

[System.Serializable]
public class RegisterData
{
    public int user_id;
    public string token;
}