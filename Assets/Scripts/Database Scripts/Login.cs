﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Login : MonoBehaviour {

    public static Login instance { set; get; }
    public LoginResult successfulLoginUser;
    public UserProfileRespone userScores;
	public bool hasLogined;
	public int activeRoomId;
	public string userName;


    protected string loginURL = "https://multigame.4mobily.net/public/api/v1/user/auth";
    protected string GetProfileURL = "https://multigame.4mobily.net/public/api/v1/user/";
    protected string GetAccDataURL = "https://multigame.4mobily.net/public/api/v1/user/getAccount";
    protected string updateAccountDetailURL = "https://multigame.4mobily.net/public/api/v1/user/updateAccount";
    protected string changePassowrdURL = "https://multigame.4mobily.net/public/api/v1/user/updatePassword";
    protected string changeMailURL = "https://multigame.4mobily.net/public/api/v1/user/updateAccount";
    protected string changeMobileURL = "https://multigame.4mobily.net/public/api/v1/user/updateMobile";
    protected string pingURL = "https://multigame.4mobily.net/public/api/v1/user/ping";

    private m_UNETGameManager serverManager;
    // Use this for initialization
	void Start () {
        //serverManager = GameObject.Find("ServerManager").GetComponent<m_UNETGameManager>();
		if (instance == null)
		{
			instance = this;
		} 
		else if (instance != this)
		{
			Destroy(this.gameObject);
		}
        serverManager = GameObject.Find("UNETManager").GetComponent<m_UNETGameManager>();
		DontDestroyOnLoad(this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    void FixedUpdate() {
        
    }

    public void invokePing()
    {
        StartCoroutine(RepeatPingRoutine());

    }

    private IEnumerator PingFunction()
    {
        WWWForm toSendForm = new WWWForm();
        toSendForm.AddField("", "");
        Dictionary<string, string> headers = toSendForm.headers;
        byte[] rawData = toSendForm.data;
        Debug.Log("Invoke Pinging");
        headers["Authorization"] = "Bearer " + Login.instance.successfulLoginUser.data.token;
        WWW www = new WWW(pingURL, rawData, headers);
        yield return www;

        if (www.isDone)
        {
            PingResponse result = JsonUtility.FromJson<PingResponse>(www.text);
            Debug.Log(result.status);
            if (result.status == 200)
            {
                Debug.Log("Pinged Successfully to the server");
            }
        }

    }

    private IEnumerator RepeatPingRoutine()
    {
        if (hasLogined)
        {
            StartCoroutine(PingFunction());
            Debug.Log("RepeatPingRoutine");
            yield return new WaitForSeconds(30.0f);
            StartCoroutine(RepeatPingRoutine());
        }
    }

    //done
    public void LoginOnClick()
    {
		if(!Login.instance.hasLogined)
		{
			StartCoroutine(LoginUser(UiMenuValues.instance.userName, UiMenuValues.instance.password));
		}

    }
    //done
    public void getAccountDataClick()
    {
        StartCoroutine(GetAccountDataRoutine());
    }

    

    public void getProfileDataClick()
    {
        StartCoroutine(GetUserProfileRoutine(successfulLoginUser.data.user.id));
    }
    public void ChangePassword(string oldPassword, string newPassword)
    {
        string email = successfulLoginUser.data.user.email;
        Debug.Log("Change Password Before Routine");
        //these to be obtained from ui
        StartCoroutine(ChangePasswordRoutine(email, oldPassword, newPassword));
    }
    public void ChangeMail(string newMail, string currentPassword)
    {
        StartCoroutine(ChangeMailRoutine(newMail, currentPassword));
    }
    public void ChangePhoneNumber(int newMobilePhone, string password)
    {
        string countryCode = successfulLoginUser.data.user.country_code;
        StartCoroutine(ChangeMobileRoutine(countryCode, newMobilePhone, password));
    }
    public IEnumerator LoginUser(string username, string password)
    {
        WWWForm toSendForm = new WWWForm();
        toSendForm.AddField("username", username);

        Dictionary<string, string> headers = toSendForm.headers;
        byte[] rawData = toSendForm.data;

        headers["password"] = password;

        WWW www = new WWW(loginURL, rawData, headers);
		UiAnimationManager.instance.showLoadingPanel();
        yield return www;
		UiAnimationManager.instance.hideLoadingPanel();
        if (www.isDone)
        {
            LoginResult result = JsonUtility.FromJson<LoginResult>(www.text);
            Debug.Log("Status ->"+ www.text);
            if (result.status == 200)
            {
                Debug.Log("Login successful!");
				UiAnimationManager.instance.showLoginSuccessfulPanel();
				UiAnimationManager.instance.fillDataInAccountSettingsMenu(UiMenuValues.instance.loginUserName.text,result.data.user.country_code,result.data.user.email,result.data.user.mobile_number.ToString());
                successfulLoginUser = result;

				Login.instance.userName = UiMenuValues.instance.userName;
				serverManager.currentPlayerName = Login.instance.userName;
                //serverManager.currentUserIP = successfulLoginUser.data.user.ip_address;
				UiMenuValues.instance.saveUserName();
				Rooms room = GameObject.Find("RoomManager").GetComponent<Rooms>();
				room.successLogin();
                //Debug.Log(successfulLoginUser.data.user.ip_address);
				getProfileDataClick();

                invokePing();
            }
            else if (result.status == -4)
            {
                Debug.Log("Invalid Credentials!");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Wrong UserName Or Password!"));
            }
            else if (result.status == 201)
            {
                Debug.Log("Required Data Missing!");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Required Data Missing!"));
            }
            else if (result.status == 226)
            {
                //user not active status
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Check you E-mail for account activation!"));
            }
        }
    }

    private IEnumerator GetUserProfileRoutine(int user_id)
    {
        WWWForm toSendForm = new WWWForm();
        toSendForm.AddField("", "");
        Dictionary<string, string> headers = toSendForm.headers;
        byte[] rawData = toSendForm.data;
        headers["Authorization"] = "Bearer " + successfulLoginUser.data.token;
        WWW www = new WWW(GetProfileURL  + user_id + "/profile",rawData,headers);
        Debug.Log(GetProfileURL + user_id + "/profile");

        yield return www;

        if (www.isDone)
        {
            Debug.Log(www.text);
            UserProfileRespone userFound = JsonUtility.FromJson<UserProfileRespone>(www.text);
            userScores = userFound;
            if (userFound.status == 200)
            {
                Debug.Log("User Profile Retrieved Successfuly");

				UiAnimationManager.instance.showProfileData(userFound.data.profile.username,userFound.data.profile.results.chess_wins.ToString(),
					userFound.data.profile.results.chess_loses.ToString(),userFound.data.profile.results.snake_wins.ToString(),
					userFound.data.profile.results.snake_loses.ToString(),userFound.data.profile.results.domino_wins.ToString(),userFound.data.profile.results.domino_loses.ToString(),
					userFound.data.profile.level.ToString(),userFound.data.profile.xp.ToString());

            }
            else if (userFound.status == 226)
            {
                Debug.Log("User is InActive");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Check you E-mail for account activation!"));
            }
        }
    }
    private IEnumerator GetAccountDataRoutine()
    {
        WWWForm toSendForm = new WWWForm();
        toSendForm.AddField("", "");
        Dictionary<string, string> headers = toSendForm.headers;
        byte[] rawData = toSendForm.data;
        headers["Authorization"] = "Bearer " + successfulLoginUser.data.token;
        Debug.Log(successfulLoginUser.data.token);

        WWW www = new WWW(GetAccDataURL, rawData, headers);
        yield return www;

        if (www.isDone)
        {
            Debug.Log(www.text);
            GetAccountDataResult result = JsonUtility.FromJson<GetAccountDataResult>(www.text);

            if (result.status == 200)
            {
                Debug.Log("Successfuly Retreived Data");
                getProfileDataClick();

				 
            }
            else if (result.status == 226 || result.status == 401)
            {
                Debug.Log("User is Inactive");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Check you E-mail for account activation!"));
            }
        }
    }

    private IEnumerator UpdateUserAccountRoutine(string userName,string oldPassword, string newPassword)
    {
        WWWForm toSendForm = new WWWForm();
        toSendForm.AddField("username", userName);
        toSendForm.AddField("email", successfulLoginUser.data.user.email);
        toSendForm.AddField("mobile_number", successfulLoginUser.data.user.mobile_number);
        string temp = successfulLoginUser.data.user.mobile_number.ToString();
        Debug.Log(temp[0]);
        toSendForm.AddField("mobile_country_code", successfulLoginUser.data.user.country_code);
        toSendForm.AddField("country_code", successfulLoginUser.data.user.country_code);
        toSendForm.AddField("password", oldPassword);
        toSendForm.AddField("new_password", newPassword);
        //toSendForm.AddField("first_name", successfulLoginUser.data.user.first_name);
        //toSendForm.AddField("last_name", successfulLoginUser.data.user.last_name);
        
        Dictionary<string, string> headers = toSendForm.headers;

        byte[] rawData = toSendForm.data;
        headers["Authorization"] = "Bearer " + successfulLoginUser.data.token;

        WWW www = new WWW(updateAccountDetailURL, rawData, headers);
        yield return www;

        if (www.isDone)
        {
            UpdateAccountDetailResponse result = JsonUtility.FromJson<UpdateAccountDetailResponse>(www.text);
            if (result.status == 200)
            {
                Debug.Log("Changed Successfully");
                successfulLoginUser.data.token = result.data.token;
            }
            else if (result.status == 226)
            {
                Debug.Log("User is inactive");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("User is inactive!"));
            }
            else if (result.status == 202)
            {
                Debug.Log("Mobile is Invalid");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Mobile is Invalid!"));
            }
            else if (result.status == 204)
            {
                Debug.Log("email invalid");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("email invalid!"));
            }
            else if (result.status == 201)
            {
                Debug.Log("Data required missing");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Data required missing!"));
            }
            else if (result.status == 205)
            {
                Debug.Log("Mobile exists");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Mobile exists!"));
            }
            else if (result.status == 207)
            {
                Debug.Log("Invalid Password");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Invalid Password!"));
            }
            else if (result.status == 208)
            {
                Debug.Log("Invalid Username");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Invalid Username!"));
            }
            else if (result.status == 209)
            {
                Debug.Log("username exists");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("username exists!"));
            }
            else if (result.status == 505)
            {
                Debug.Log("Create User Error");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Create User Error!"));
            }
            else if (result.status == 606)
            {
                Debug.Log("PDO EXC");
            }
            else if (result.status == 221)
            {
                Debug.Log("Passowrd Wrong!");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Passowrd Wrong!"));
            }
            else if (result.status == 210)
            {
                Debug.Log("Invalid Name");
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Invalid Name!"));
            }
            else if (result.status == -5)
            {
                Debug.Log("Couldn't Create Token");

            }
        }
    }
    private IEnumerator ChangePasswordRoutine(string email, string oldPassword, string newPassowrd)
    {
        WWWForm toSendForm = new WWWForm();
        toSendForm.AddField("email", email);
        toSendForm.AddField("new_password", newPassowrd);
        Dictionary<string, string> headers = toSendForm.headers;
        byte[] rawData = toSendForm.data;
        headers["Authorization"] = "Bearer " + successfulLoginUser.data.token;
        headers["password"] = oldPassword;
        WWW www = new WWW(changePassowrdURL, rawData, headers);
		UiAnimationManager.instance.showLoadingPanel();

        yield return www;

        if (www.isDone)
        {
            Debug.Log("Sending Done");
            updatePasswordResult result = JsonUtility.FromJson<updatePasswordResult>(www.text);

            if (result.status == 200)
            {
                Debug.Log("Password Changed Successfuly");
				UiAnimationManager.instance.hideLoadingPanel();
                UiAnimationManager.instance.PasswordChangedSuccessfully();
				Invoke("goBackToAccountSettingView",2f);
            }
            else if (result.status == 220)
            {
                Debug.Log("user is inactive");
				UiAnimationManager.instance.hideLoadingPanel();
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("user is inactive!"));

            }
            else if (result.status == 207)
            {
                Debug.Log("Invalid Passowrd");
				UiAnimationManager.instance.hideLoadingPanel();
                StartCoroutine(UiAnimationManager.instance.showFailureMassage("Invalid Password!"));
            }
        }
    }

    private IEnumerator ChangeMailRoutine(string email, string password)
    {
        WWWForm toSendForm = new WWWForm();
        toSendForm.AddField("email", email);
        Dictionary<string, string> headers = toSendForm.headers;
        byte[] rawData = toSendForm.data;
        headers["Authorization"] = "Bearer " + successfulLoginUser.data.token;
        headers["password"] = password;
        WWW www = new WWW(changeMailURL, rawData, headers);
		UiAnimationManager.instance.showLoadingPanel();
        yield return www;

        if (www.isDone)
        {
            updateMailResult result = JsonUtility.FromJson<updateMailResult>(www.text);

            if (result.status == 200)
            {
				UiAnimationManager.instance.hideLoadingPanel();
                Debug.Log("email Changed Successfuly");
                UiAnimationManager.instance.EmailChangedSuccessfully();
				UiAnimationManager.instance.fillDataInAccountSettingsMenu(UiMenuValues.instance.loginUserName.text,Login.instance.successfulLoginUser.data.user.country_code.ToString(),UiMenuValues.instance.newEmail.ToString(),Login.instance.successfulLoginUser.data.user.mobile_number.ToString());
            }
            else if (result.status == 220)
            {
                Debug.Log("user is inactive");
				UiAnimationManager.instance.hideLoadingPanel();
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("user is inactive!"));
            }
            else if (result.status == 204)
            {
                Debug.Log("Invalid Mail");
				UiAnimationManager.instance.hideLoadingPanel();
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Invalid Mail!"));
            }
            else if (result.status == 206)
            {
                Debug.Log("Email Already Exists");
				UiAnimationManager.instance.hideLoadingPanel();
                StartCoroutine(UiAnimationManager.instance.showFailureMassage("Email Already Exists!",2f));
            }
        }
    }

    private IEnumerator ChangeMobileRoutine(string newMobileCountryCode, int newMobileNumber, string password)
    {
		print("inside ChangeMobileRoutine");
        WWWForm toSendForm = new WWWForm();
        toSendForm.AddField("email", successfulLoginUser.data.user.email);
        toSendForm.AddField("mobile_country_code", successfulLoginUser.data.user.country_code);
        toSendForm.AddField("mobile_number", newMobileNumber);
        Dictionary<string, string> headers = toSendForm.headers;
        byte[] rawData = toSendForm.data;
        headers["Authorization"] = "Bearer " + successfulLoginUser.data.token;
        headers["password"] = password;
        WWW www = new WWW(changeMobileURL, rawData, headers);
		UiAnimationManager.instance.showLoadingPanel();

        yield return www;

        if (www.isDone)
        {
            updatePhoneResult result = JsonUtility.FromJson<updatePhoneResult>(www.text);

            if (result.status == 200)
            {
                Debug.Log("Mobile Phone Changed Successfuly");
				UiAnimationManager.instance.hideLoadingPanel();
                StartCoroutine(UiAnimationManager.instance.showFailureMassage("Mobile Changed Successfully!",2f));
				Invoke("goBackToAccountSettingView",2f);
				UiAnimationManager.instance.fillDataInAccountSettingsMenu(UiMenuValues.instance.loginUserName.text,Login.instance.successfulLoginUser.data.user.country_code,Login.instance.successfulLoginUser.data.user.email,UiMenuValues.instance.newPhoneNumber);

            }
            else if (result.status == 220)
            {
                Debug.Log("user is inactive");
				UiAnimationManager.instance.hideLoadingPanel();
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("user is inactive!"));
            }
            else if (result.status == 202)
            {
                Debug.Log("Invalid Phone Number");
				UiAnimationManager.instance.hideLoadingPanel();
				StartCoroutine(UiAnimationManager.instance.showFailureMassage("Invalid Phone Number!"));
            }
            else if (result.status == 206)
            {
                Debug.Log("Mobile Already Exists");
				UiAnimationManager.instance.hideLoadingPanel();
                StartCoroutine(UiAnimationManager.instance.showFailureMassage("Mobile Already Exists!",2f));
            }
        }
    }


	void goBackToAccountSettingView()
	{
		UiAnimationManager.instance.showAccountSettingMenu();
	}

	void OnDisable()
	{
		SceneManager.sceneLoaded -= loginBackToMainMenuAfterWinning;
	}

	void OnEnable()
	{
		SceneManager.sceneLoaded += loginBackToMainMenuAfterWinning;
	}

	void loginBackToMainMenuAfterWinning(Scene scene, LoadSceneMode mode)
	{
		if (scene.name == "main_menu_scene" && hasLogined)
		{
			serverManager = GameObject.Find("UNETManager").GetComponent<m_UNETGameManager>();
			Rooms room = GameObject.Find("RoomManager").GetComponent<Rooms>();
			room.successLogin();
			UiMenuValues.instance.userName = Login.instance.userName;
		}
	}



	public void signOut()
	{
		
//		GameObject temp = new GameObject().AddComponent<Login>();
//		temp.name = this.gameObject.name;

//		GameObject temp =  Instantiate(this.gameObject);
//		temp.GetComponent<Login>().instance = temp.GetComponent<Login>();
//		Destroy(this.gameObject);
		successfulLoginUser = null ;
		hasLogined = false ;
//		UiMenuValues.instance.userName = "";
//		UiMenuValues.instance.password = "";


		Debug.Log("log out successful!");
//		UiAnimationManager.instance.fillDataInAccountSettingsMenu("","","","");

		Login.instance.userName = "";
		serverManager.currentPlayerName = Login.instance.userName;
		GameObject.Find("RoomManager").GetComponent<Rooms>().successLogOut();

	}

   

}
[System.Serializable]
public class LoginResult
{
    public int status;
    public LoginData data;
}

[System.Serializable]
public class LoginData
{
    public string token;
    public LoginUser user;
}
[System.Serializable]
public class LoginUser
{
    public int id;
    public string first_name;
    public string last_name;
    public string email;
    public int mobile_number;
    public string country_code;
    public string ip_address;
    public string status;
	public int level;
	public int xp;
}
[System.Serializable]
public class UserProfileRespone
{
    public int status;
    public UserProfileResponseData data;
}
[System.Serializable]
public class UserProfileResponseData
{
    public UserProfileResponseDataProfile profile;
}
[System.Serializable]
public class UserProfileResponseDataProfile{
    public string first_name;
    public string last_name;
    public string username;
	public int id;
	public int level;
	public int xp;
    public UserProfileResponseDAtaProfileResults results;
}
[System.Serializable]
public class UserProfileResponseDAtaProfileResults
{
    public int id;
    public int user_id;
    public int snake_wins;
    public int chess_wins;
    public int domino_wins;
    public int snake_loses;
    public int chess_loses;
    public int domino_loses;
}

[System.Serializable]
public class GetAccountDataResult
{
    public int status;
    public GetAccountDataResultData data;
}
[System.Serializable]
public class GetAccountDataResultData
{
    public GetAccountDataResultDataAccount account;
}
[System.Serializable]
public class GetAccountDataResultDataAccount{
    public string first_name;
    public string last_name;
    public string username;
    public int id;
    public string email;
    public string mobile_number;
    public string country_code;
}
[System.Serializable]
public class UpdateAccountDetailResponse
{
    public int status;
    public UpdateAccountDetailResponseData data;
}
[System.Serializable]
public class UpdateAccountDetailResponseData
{
    public string user_id;
    public string token;
}

[System.Serializable]
public class updateMailResult
{
    public int status;
    public updateMailResultData data;
}
[System.Serializable]
public class updateMailResultData
{
    public int user_id;
}
[System.Serializable]
public class updatePasswordResult
{
    public int status;
    public updatePassowrdResultData data;
}
[System.Serializable]
public class updatePassowrdResultData
{
    public int user_id;
    public string token;
}
[System.Serializable]
public class updatePhoneResult
{
    public int status;
    public updatePhoneResultData data;
}
[System.Serializable]
public class updatePhoneResultData
{
    public int user_id;
}



//public UserProfileResponseDAtaProfileResults results;
//
//
//public int id;
//public int level;
//public int xp;
