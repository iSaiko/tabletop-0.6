﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SnakeAndLadderUIValues : MonoBehaviour {
    public Text Player1Name, Player1Score,Player2Name, Player2Score;
    public Image Player1Flag, Player2Flag;
    public m_UNETSnake client;

	public GameObject flagsLoaderObject;
	// Use this for initialization
	void Start () {
        client = GameObject.Find("UNETSnakeObject").GetComponent<m_UNETSnake>();
//        if (client.isHost) {
//            Player1Name.text = client.currentPlayerName;
//            Player2Name.text = client.secondPlayerName;
//
//        }
//        else
//        {
//            Player1Name.text = client.secondPlayerName;
//            Player2Name.text = client.currentPlayerName;
//
//        }

		Player1Name.text = client.currentPlayerName;
		Player2Name.text = client.secondPlayerName;

		getPlayerFlags();
	}
	
	// Update is called once per frame
	void Update () {
        if (client.isHost)
        {
            Player1Score.text = (SnakeBoard.instance.player1.index + 1).ToString();
            Player2Score.text = (SnakeBoard.instance.player2.index + 1).ToString();
        }
        else
        {

            Player1Score.text = (SnakeBoard.instance.player2.index + 1).ToString();
            Player2Score.text = (SnakeBoard.instance.player1.index + 1).ToString();
        }
            
	}



	public void getPlayerFlags()
	{
		
		CountryDetection countryGetter = flagsLoaderObject.GetComponent<CountryDetection>();

		Player1Flag.sprite =  countryGetter.LoadFlag(m_UNETSnake.instance.currentPlayerCountry);
		Player2Flag.sprite =  countryGetter.LoadFlag(m_UNETSnake.instance.secondPlayerCountry);
		Destroy(flagsLoaderObject,0.2f);
	}

}
