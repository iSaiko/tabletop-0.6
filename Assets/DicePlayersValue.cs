﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DicePlayersValue : MonoBehaviour {
    public Sprite[] DiceFacesArray = new Sprite[6];

    public Image[] DiceIndexContainer = new Image[2];
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void ModifyDiceThrowImage1(int index)
    {
        DiceIndexContainer[0].sprite = DiceFacesArray[index - 1];
    }
    public void ModifyDiceThrowImage2(int index)
    {
        DiceIndexContainer[1].sprite = DiceFacesArray[index - 1];
    }
}
