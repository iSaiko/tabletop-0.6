﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsButtonSL : MonoBehaviour {

    public bool settingsOpen;
    Quaternion closeRot =  Quaternion.Euler(new Vector3(0f, 0f, 146f));
    Quaternion openRot =  Quaternion.Euler(new Vector3(0f, 0f, -30f));
    public GameObject gear;
    public bool endedrot;
	// Use this for initialization
	void Start () {
        settingsOpen = false;
        endedrot = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void settingPressed()
    {
        if (!settingsOpen && endedrot)
        {
            StartCoroutine(RotationRoutine(closeRot, openRot));
            settingsOpen = !settingsOpen;
        }
        else if( settingsOpen && endedrot)
        {
            StartCoroutine(RotationRoutine(openRot, closeRot));
            settingsOpen = !settingsOpen;
        }
    }

    IEnumerator RotationRoutine(Quaternion startingRotation, Quaternion targetRotation)
    {
        float elapsedTime = 0.0f;
        float time = 0.5f;
        while (elapsedTime < time)
        {
            endedrot = false;
            elapsedTime += Time.deltaTime; // <- move elapsedTime increment here
            // Rotations
            transform.rotation = Quaternion.Slerp(startingRotation, targetRotation, (elapsedTime / time));
            gear.transform.rotation = Quaternion.Slerp(startingRotation, targetRotation, (elapsedTime / time));
            yield return new WaitForEndOfFrame();
        }
        endedrot = true;
    }
}
