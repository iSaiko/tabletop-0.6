using UnityEngine;
using System.Collections;
using ArabicSupport;
using UnityEngine.UI;

public class FixGUITextCS : MonoBehaviour {

    public string text;
	public bool tashkeel = true;
	public bool hinduNumbers = true;
    public Text uiText;

    // Use this for initialization
    void Start () {
        uiText.text = text;
        GetComponent<Text>().text = ArabicFixer.Fix(text, tashkeel, hinduNumbers);
    }
}
