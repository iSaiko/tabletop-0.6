﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class ChessPromoter : MonoBehaviour {

    public static ChessPromoter instance { set; get; }


    //Player 1 Prefabs
    public GameObject Player1Queen;
    public GameObject Player1Rook;
    public GameObject Player1Knight;
    public GameObject Player1Bishop;


    //Player 2 Prefabs
    public GameObject Player2Queen;
    public GameObject Player2Rook;
    public GameObject Player2Knight;
    public GameObject Player2Bishop;


    public GameObject Team1InspectorObject, Team2InspectorObject;

    public GameObject player1Promoter;
    public GameObject player2Promoter;
	// Use this for initialization
	void Start () {
        instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //Promoting Player 1 Pieces Methods
    public void PromotePlayer1ToQueen()
    {
        GameObject heldpiece = PlayerControl.instance.heldPiece;

        //TODO Instantiate Piece Prefab as GameObject
        GameObject queenObject = Instantiate(Player1Queen, new Vector3(heldpiece.transform.position.x, 18.52f, heldpiece.transform.position.z), heldpiece.transform.rotation) as GameObject;

        //TODO Update Piece Row, Column and Position to match the held Pawn
        //queenObject.GetComponent<Piece>()
        queenObject.GetComponent<Piece>().onPiece = queenObject;
        queenObject.GetComponent<Piece>().player1PromotionCanvas = player1Promoter;
        queenObject.GetComponent<Piece>().player2PromotionCanvas = player2Promoter;
        queenObject.GetComponent<Queen>().queenRow = heldpiece.GetComponent<Piece>().row;
        queenObject.GetComponent<Queen>().queenColum = heldpiece.GetComponent<Piece>().column;
        queenObject.GetComponent<Piece>().row = heldpiece.GetComponent<Piece>().row;
        queenObject.GetComponent<Piece>().column = heldpiece.GetComponent<Piece>().column;

        //TODO UpdateAfterAttack 

        queenObject.GetComponent<Piece>().updateAfterAttack();

        //TODO Queen Index == heldpieceindex
        int index = heldpiece.GetComponent<Piece>().pieceIndex;
        queenObject.GetComponent<Piece>().pieceIndex = index;
        queenObject.transform.SetParent(Team1InspectorObject.transform);
        queenObject.transform.localPosition = new Vector3(queenObject.transform.localPosition.x, 18.52f, queenObject.transform.localPosition.z);
        ////TODO loop through Team pieces and Chess pieces
        //int pieceIndexChessPieces;
        //int pieceIndexTeamPieces;
        //int pieceIndexChessBoard;
        //pieceIndexChessPieces = PlayerControl.instance.chessPieces.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);
        //pieceIndexTeamPieces = PlayerControl.instance.team1.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);
        //pieceIndexChessBoard = ChessBoard.instance.team1.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);

        //TODO remove the piece where heldpiece index == its index
        PlayerControl.instance.team1.Remove(heldpiece);
        PlayerControl.instance.chessPieces.Remove(heldpiece);
        ChessBoard.instance.team1.Remove(heldpiece);

        //TODO Add Queen to Team1 list
        PlayerControl.instance.chessPieces.Add(queenObject);

        //TODO Add Queen to Chess Pieces
        PlayerControl.instance.team1.Add(queenObject);

        //TODO Add Queen to team list in chess board
        ChessBoard.instance.team1.Add(queenObject);

        //TODO heldPiece set active false and == to null 
        Destroy(PlayerControl.instance.heldPiece);
        PlayerControl.instance.heldPiece = null;

        //TODO Send Promotion to Network where ("CPROM|" + 1 + pawnIndex + 1(if Queen) )
        Debug.Log("Promoting from Function player 1");
        PlayerControl.instance.SendPromotionToServer("CPROM|", 1, index, 1);
        player1Promoter.SetActive(false);
        player2Promoter.SetActive(false);

    }

    public void PromotePlayer1ToRook()
    {
        GameObject heldpiece = PlayerControl.instance.heldPiece;

        //TODO Instantiate Piece Prefab as GameObject
        GameObject rookObject = Instantiate(Player1Rook, new Vector3(heldpiece.transform.position.x, 11f, heldpiece.transform.position.z), heldpiece.transform.rotation) as GameObject;

        //TODO Update Piece Row, Column and Position to match the held 
        rookObject.GetComponent<Piece>().onPiece = rookObject;
        rookObject.GetComponent<Piece>().player1PromotionCanvas = player1Promoter;
        rookObject.GetComponent<Piece>().player2PromotionCanvas = player2Promoter;
        rookObject.GetComponent<Rook>().rookRow = heldpiece.GetComponent<Piece>().row;
        rookObject.GetComponent<Rook>().rookColum = heldpiece.GetComponent<Piece>().column;
        rookObject.GetComponent<Piece>().row = heldpiece.GetComponent<Piece>().row;
        rookObject.GetComponent<Piece>().column = heldpiece.GetComponent<Piece>().column;

        //TODO UpdateAfterAttack 
        rookObject.GetComponent<Piece>().updateAfterAttack();

        //TODO Queen Index == heldpieceindex
        int index = heldpiece.GetComponent<Piece>().pieceIndex;
        rookObject.GetComponent<Piece>().pieceIndex = index;
        rookObject.transform.SetParent(Team1InspectorObject.transform);
        rookObject.transform.localPosition = new Vector3(rookObject.transform.localPosition.x, 11f, rookObject.transform.localPosition.z);

        ////rookObject loop through Team pieces and Chess pieces
        //int pieceIndexChessPieces;
        //int pieceIndexTeamPieces;
        //int pieceIndexChessBoard;
        //pieceIndexChessPieces = PlayerControl.instance.chessPieces.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);
        //pieceIndexTeamPieces = PlayerControl.instance.team1.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);
        //pieceIndexChessBoard = ChessBoard.instance.team1.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);

        //TODO remove the piece where heldpiece index == its index
        PlayerControl.instance.team1.Remove(heldpiece);
        PlayerControl.instance.chessPieces.Remove(heldpiece);
        ChessBoard.instance.team1.Remove(heldpiece);

        //TODO Add Queen to Team1 list
        PlayerControl.instance.chessPieces.Add(rookObject);

        //TODO Add Queen to Chess Pieces
        PlayerControl.instance.team1.Add(rookObject);

        //TODO Add Queen to team list in chess board
        ChessBoard.instance.team1.Add(rookObject);

        //TODO heldPiece set active false and == to null 
        Destroy(PlayerControl.instance.heldPiece);
        PlayerControl.instance.heldPiece = null;

        //TODO Send Promotion to Network where ("CPROM|" + 1 + pawnIndex + 1(if Queen) )
        Debug.Log("Promoting from Function player 1");
        PlayerControl.instance.SendPromotionToServer("CPROM|", 1, index, 4);
        player1Promoter.SetActive(false);
        player2Promoter.SetActive(false);

    }

    public void PromotePlayer1ToKnight()
    {
        GameObject heldpiece = PlayerControl.instance.heldPiece;

        //TODO Instantiate Piece Prefab as GameObject
        GameObject knightObject = Instantiate(Player1Knight, new Vector3(heldpiece.transform.position.x, 11f, heldpiece.transform.position.z), Quaternion.Euler(heldpiece.transform.rotation.x, -90f, heldpiece.transform.rotation.z)) as GameObject;
        Debug.Log(knightObject.transform.localPosition.y + " not local " + knightObject.transform.position.y);
        //TODO Update Piece Row, Column and Position to match the held Pawn
        knightObject.GetComponent<Piece>().onPiece = knightObject;
        knightObject.GetComponent<Piece>().player1PromotionCanvas = player1Promoter;
        knightObject.GetComponent<Piece>().player2PromotionCanvas = player2Promoter;
        knightObject.GetComponent<Knight>().knightRow = heldpiece.GetComponent<Piece>().row;
        knightObject.GetComponent<Knight>().knightColum = heldpiece.GetComponent<Piece>().column;
        knightObject.GetComponent<Piece>().row = heldpiece.GetComponent<Piece>().row;
        knightObject.GetComponent<Piece>().column = heldpiece.GetComponent<Piece>().column;

        //TODO UpdateAfterAttack 
        knightObject.GetComponent<Piece>().updateAfterAttack();

        //TODO Queen Index == heldpieceindex
        int index = heldpiece.GetComponent<Piece>().pieceIndex;
        knightObject.GetComponent<Piece>().pieceIndex = index;
        knightObject.transform.SetParent(Team1InspectorObject.transform);
        knightObject.transform.localPosition = new Vector3(knightObject.transform.localPosition.x, 11, knightObject.transform.localPosition.z);
        ////rookObject loop through Team pieces and Chess pieces
        //int pieceIndexChessPieces;
        //int pieceIndexTeamPieces;
        //int pieceIndexChessBoard;
        //pieceIndexChessPieces = PlayerControl.instance.chessPieces.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);
        //pieceIndexTeamPieces = PlayerControl.instance.team1.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);
        //pieceIndexChessBoard = ChessBoard.instance.team1.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);

        //TODO remove the piece where heldpiece index == its index
        PlayerControl.instance.team1.Remove(heldpiece);
        PlayerControl.instance.chessPieces.Remove(heldpiece);
        ChessBoard.instance.team1.Remove(heldpiece);

        //TODO Add Queen to Team1 list
        PlayerControl.instance.chessPieces.Add(knightObject);

        //TODO Add Queen to Chess Pieces
        PlayerControl.instance.team1.Add(knightObject);

        //TODO Add Queen to team list in chess board
        ChessBoard.instance.team1.Add(knightObject);

        //TODO heldPiece set active false and == to null 
        Destroy(PlayerControl.instance.heldPiece);
        PlayerControl.instance.heldPiece = null;

        //TODO Send Promotion to Network where ("CPROM|" + 1 + pawnIndex + 1(if Queen) )
        Debug.Log("Promoting from Function player 1");
        PlayerControl.instance.SendPromotionToServer("CPROM|", 1, index, 2);
        player1Promoter.SetActive(false);
        player2Promoter.SetActive(false);

    }

    public void PromotePlayer1ToBishop() {

        GameObject heldpiece = PlayerControl.instance.heldPiece;

        //TODO Instantiate Piece Prefab as GameObject
        GameObject bishopObject = Instantiate(Player1Bishop, new Vector3(heldpiece.transform.position.x, 17.8f, heldpiece.transform.position.z), heldpiece.transform.rotation) as GameObject;

        //TODO Update Piece Row, Column and Position to match the held Pawn
        bishopObject.GetComponent<Piece>().onPiece = bishopObject;
        bishopObject.GetComponent<Piece>().player1PromotionCanvas = player1Promoter;
        bishopObject.GetComponent<Piece>().player2PromotionCanvas = player2Promoter;
        
        bishopObject.GetComponent<Bishop>().bishopRow = heldpiece.GetComponent<Piece>().row;
        bishopObject.GetComponent<Bishop>().bishopColum = heldpiece.GetComponent<Piece>().column;
        bishopObject.GetComponent<Piece>().row = heldpiece.GetComponent<Piece>().row;
        bishopObject.GetComponent<Piece>().column = heldpiece.GetComponent<Piece>().column;

        //TODO UpdateAfterAttack 
        bishopObject.GetComponent<Piece>().updateAfterAttack();

        //TODO Queen Index == heldpieceindex
        int index = heldpiece.GetComponent<Piece>().pieceIndex;
        bishopObject.GetComponent<Piece>().pieceIndex = index;
        bishopObject.transform.SetParent(Team1InspectorObject.transform);
        bishopObject.transform.localPosition = new Vector3(bishopObject.transform.localPosition.x, 17.8f, bishopObject.transform.localPosition.z);

        ////rookObject loop through Team pieces and Chess pieces
        //int pieceIndexChessPieces;
        //int pieceIndexTeamPieces;
        //int pieceIndexChessBoard;
        //pieceIndexChessPieces = PlayerControl.instance.chessPieces.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);
        //pieceIndexTeamPieces = PlayerControl.instance.team1.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);
        //pieceIndexChessBoard = ChessBoard.instance.team1.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);

        //TODO remove the piece where heldpiece index == its index
        PlayerControl.instance.team1.Remove(heldpiece);
        PlayerControl.instance.chessPieces.Remove(heldpiece);
        ChessBoard.instance.team1.Remove(heldpiece);

        //TODO Add Queen to Team1 list
        PlayerControl.instance.chessPieces.Add(bishopObject);

        //TODO Add Queen to Chess Pieces
        PlayerControl.instance.team1.Add(bishopObject);

        //TODO Add Queen to team list in chess board
        ChessBoard.instance.team1.Add(bishopObject);

        //TODO heldPiece set active false and == to null 
        Destroy(PlayerControl.instance.heldPiece);
        PlayerControl.instance.heldPiece = null;

        //TODO Send Promotion to Network where ("CPROM|" + 1 + pawnIndex + 1(if Queen) )
        Debug.Log("Promoting from Function player 1");
        PlayerControl.instance.SendPromotionToServer("CPROM|", 1, index, 3);
        player1Promoter.SetActive(false);
        player2Promoter.SetActive(false);

    }


    //Promoting Player 2 Pieces Methods

    public void PromotePlayer2ToQueen()
    {

        GameObject heldpiece = PlayerControl.instance.heldPiece;

        //TODO Instantiate Piece Prefab as GameObject
        GameObject queenObject = Instantiate(Player2Queen, new Vector3(heldpiece.transform.position.x, 18.52f, heldpiece.transform.position.z), heldpiece.transform.rotation) as GameObject;

        //TODO Update Piece Row, Column and Position to match the held Pawn
        //queenObject.GetComponent<Piece>()
        queenObject.GetComponent<Piece>().onPiece = queenObject;
        queenObject.GetComponent<Piece>().player1PromotionCanvas = player1Promoter;
        queenObject.GetComponent<Piece>().player2PromotionCanvas = player2Promoter;
        queenObject.GetComponent<Queen>().queenRow = heldpiece.GetComponent<Piece>().row;
        queenObject.GetComponent<Queen>().queenColum = heldpiece.GetComponent<Piece>().column;
        queenObject.GetComponent<Piece>().row = heldpiece.GetComponent<Piece>().row;
        queenObject.GetComponent<Piece>().column = heldpiece.GetComponent<Piece>().column;

        //TODO UpdateAfterAttack 

        queenObject.GetComponent<Piece>().updateAfterAttack();

        //TODO Queen Index == heldpieceindex
        int index = heldpiece.GetComponent<Piece>().pieceIndex;
        queenObject.GetComponent<Piece>().pieceIndex = index;
        queenObject.transform.SetParent(Team2InspectorObject.transform);
        queenObject.transform.localPosition = new Vector3(queenObject.transform.localPosition.x, 18.52f, queenObject.transform.localPosition.z);

        ////TODO loop through Team pieces and Chess pieces
        //int pieceIndexChessPieces;
        //int pieceIndexTeamPieces;
        //int pieceIndexChessBoard;
        //pieceIndexChessPieces = PlayerControl.instance.chessPieces.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);
        //pieceIndexTeamPieces = PlayerControl.instance.team1.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);
        //pieceIndexChessBoard = ChessBoard.instance.team1.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);

        //TODO remove the piece where heldpiece index == its index
        PlayerControl.instance.team2.Remove(heldpiece);
        PlayerControl.instance.chessPieces.Remove(heldpiece);
        ChessBoard.instance.team2.Remove(heldpiece);

        //TODO Add Queen to Team1 list
        PlayerControl.instance.chessPieces.Add(queenObject);

        //TODO Add Queen to Chess Pieces
        PlayerControl.instance.team2.Add(queenObject);

        //TODO Add Queen to team list in chess board
        ChessBoard.instance.team2.Add(queenObject);

        //TODO heldPiece set active false and == to null 
        Destroy(PlayerControl.instance.heldPiece);
        PlayerControl.instance.heldPiece = null;

        //TODO Send Promotion to Network where ("CPROM|" + 1 + pawnIndex + 1(if Queen) )
        Debug.Log("Promoting from Function player 1");
        PlayerControl.instance.SendPromotionToServer("CPROM|", 2, index, 1);
        player1Promoter.SetActive(false);
        player2Promoter.SetActive(false);

    }

    public void PromotePlayer2ToRook()
    {
        GameObject heldpiece = PlayerControl.instance.heldPiece;

        //TODO Instantiate Piece Prefab as GameObject
        GameObject rookObject = Instantiate(Player2Rook, new Vector3(heldpiece.transform.position.x, 11f, heldpiece.transform.position.z), heldpiece.transform.rotation) as GameObject;

        //TODO Update Piece Row, Column and Position to match the held Pawn
        rookObject.GetComponent<Piece>().onPiece = rookObject;
        rookObject.GetComponent<Piece>().player1PromotionCanvas = player1Promoter;
        rookObject.GetComponent<Piece>().player2PromotionCanvas = player2Promoter;
        rookObject.GetComponent<Rook>().rookRow = heldpiece.GetComponent<Piece>().row;
        rookObject.GetComponent<Rook>().rookColum = heldpiece.GetComponent<Piece>().column;
        rookObject.GetComponent<Piece>().row = heldpiece.GetComponent<Piece>().row;
        rookObject.GetComponent<Piece>().column = heldpiece.GetComponent<Piece>().column;

        //TODO UpdateAfterAttack 
        rookObject.GetComponent<Piece>().updateAfterAttack();

        //TODO Queen Index == heldpieceindex
        int index = heldpiece.GetComponent<Piece>().pieceIndex;
        rookObject.GetComponent<Piece>().pieceIndex = index;
        rookObject.transform.SetParent(Team2InspectorObject.transform);
        rookObject.transform.localPosition = new Vector3(rookObject.transform.localPosition.x, 11f, rookObject.transform.localPosition.z);

        ////rookObject loop through Team pieces and Chess pieces
        //int pieceIndexChessPieces;
        //int pieceIndexTeamPieces;
        //int pieceIndexChessBoard;
        //pieceIndexChessPieces = PlayerControl.instance.chessPieces.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);
        //pieceIndexTeamPieces = PlayerControl.instance.team1.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);
        //pieceIndexChessBoard = ChessBoard.instance.team1.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);

        //TODO remove the piece where heldpiece index == its index
        PlayerControl.instance.team2.Remove(heldpiece);
        PlayerControl.instance.chessPieces.Remove(heldpiece);
        ChessBoard.instance.team2.Remove(heldpiece);

        //TODO Add Queen to Team1 list
        PlayerControl.instance.chessPieces.Add(rookObject);

        //TODO Add Queen to Chess Pieces
        PlayerControl.instance.team2.Add(rookObject);

        //TODO Add Queen to team list in chess board
        ChessBoard.instance.team2.Add(rookObject);

        //TODO heldPiece set active false and == to null 
        Destroy(PlayerControl.instance.heldPiece);
        PlayerControl.instance.heldPiece = null;

        //TODO Send Promotion to Network where ("CPROM|" + 1 + pawnIndex + 1(if Queen) )
        Debug.Log("Promoting from Function player 2");
        PlayerControl.instance.SendPromotionToServer("CPROM|", 2, index, 4);
        player1Promoter.SetActive(false);
        player2Promoter.SetActive(false);

    }

    public void PromotePlayer2ToKnight()
    {
        GameObject heldpiece = PlayerControl.instance.heldPiece;

        //TODO Instantiate Piece Prefab as GameObject
        GameObject knightObject = Instantiate(Player2Knight, new Vector3(heldpiece.transform.position.x, 11f, heldpiece.transform.position.z), Quaternion.Euler(heldpiece.transform.rotation.x, 90f, heldpiece.transform.rotation.z)) as GameObject;

        //TODO Update Piece Row, Column and Position to match the held Pawn
        knightObject.GetComponent<Piece>().onPiece = knightObject;
        knightObject.GetComponent<Piece>().player1PromotionCanvas = player1Promoter;
        knightObject.GetComponent<Piece>().player2PromotionCanvas = player2Promoter;
        knightObject.GetComponent<Knight>().knightRow = heldpiece.GetComponent<Piece>().row;
        knightObject.GetComponent<Knight>().knightColum = heldpiece.GetComponent<Piece>().column;
        knightObject.GetComponent<Piece>().row = heldpiece.GetComponent<Piece>().row;
        knightObject.GetComponent<Piece>().column = heldpiece.GetComponent<Piece>().column;

        //TODO UpdateAfterAttack 
        knightObject.GetComponent<Piece>().updateAfterAttack();

        //TODO Queen Index == heldpieceindex
        int index = heldpiece.GetComponent<Piece>().pieceIndex;
        knightObject.GetComponent<Piece>().pieceIndex = index;
        knightObject.transform.SetParent(Team2InspectorObject.transform);
        knightObject.transform.localPosition = new Vector3(knightObject.transform.localPosition.x, 11, knightObject.transform.localPosition.z);

        ////rookObject loop through Team pieces and Chess pieces
        //int pieceIndexChessPieces;
        //int pieceIndexTeamPieces;
        //int pieceIndexChessBoard;
        //pieceIndexChessPieces = PlayerControl.instance.chessPieces.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);
        //pieceIndexTeamPieces = PlayerControl.instance.team1.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);
        //pieceIndexChessBoard = ChessBoard.instance.team1.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);

        //TODO remove the piece where heldpiece index == its index
        PlayerControl.instance.team2.Remove(heldpiece);
        PlayerControl.instance.chessPieces.Remove(heldpiece);
        ChessBoard.instance.team2.Remove(heldpiece);

        //TODO Add Queen to Team1 list
        PlayerControl.instance.chessPieces.Add(knightObject);

        //TODO Add Queen to Chess Pieces
        PlayerControl.instance.team2.Add(knightObject);

        //TODO Add Queen to team list in chess board
        ChessBoard.instance.team2.Add(knightObject);

        //TODO heldPiece set active false and == to null 
        Destroy(PlayerControl.instance.heldPiece);
        PlayerControl.instance.heldPiece = null;

        //TODO Send Promotion to Network where ("CPROM|" + 1 + pawnIndex + 1(if Queen) )
        Debug.Log("Promoting from Function player 2");
        PlayerControl.instance.SendPromotionToServer("CPROM|", 2, index, 2);
        player1Promoter.SetActive(false);
        player2Promoter.SetActive(false);

    }

    public void PromotePlayer2ToBishop() {
        GameObject heldpiece = PlayerControl.instance.heldPiece;

        //TODO Instantiate Piece Prefab as GameObject
        GameObject bishopObject = Instantiate(Player2Bishop, new Vector3(heldpiece.transform.position.x, 17.8f, heldpiece.transform.position.z), heldpiece.transform.rotation) as GameObject;

        //TODO Update Piece Row, Column and Position to match the held Pawn
        bishopObject.GetComponent<Piece>().onPiece = bishopObject;
        bishopObject.GetComponent<Piece>().player1PromotionCanvas = player1Promoter;
        bishopObject.GetComponent<Piece>().player2PromotionCanvas = player2Promoter;
        
        bishopObject.GetComponent<Bishop>().bishopRow = heldpiece.GetComponent<Piece>().row;
        bishopObject.GetComponent<Bishop>().bishopColum = heldpiece.GetComponent<Piece>().column;
        bishopObject.GetComponent<Piece>().row = heldpiece.GetComponent<Piece>().row;
        bishopObject.GetComponent<Piece>().column = heldpiece.GetComponent<Piece>().column;

        //TODO UpdateAfterAttack 
        bishopObject.GetComponent<Piece>().updateAfterAttack();

        //TODO Queen Index == heldpieceindex
        int index = heldpiece.GetComponent<Piece>().pieceIndex;
        bishopObject.GetComponent<Piece>().pieceIndex = index;
        bishopObject.transform.SetParent(Team2InspectorObject.transform);
        bishopObject.transform.localPosition = new Vector3(bishopObject.transform.localPosition.x, 17.8f, bishopObject.transform.localPosition.z);

        ////rookObject loop through Team pieces and Chess pieces
        //int pieceIndexChessPieces;
        //int pieceIndexTeamPieces;
        //int pieceIndexChessBoard;
        //pieceIndexChessPieces = PlayerControl.instance.chessPieces.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);
        //pieceIndexTeamPieces = PlayerControl.instance.team1.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);
        //pieceIndexChessBoard = ChessBoard.instance.team1.FindIndex(GameObject => GameObject.GetComponent<Piece>().pieceIndex == index);

        //TODO remove the piece where heldpiece index == its index
        PlayerControl.instance.team2.Remove(heldpiece);
        PlayerControl.instance.chessPieces.Remove(heldpiece);
        ChessBoard.instance.team2.Remove(heldpiece);

        //TODO Add Queen to Team1 list
        PlayerControl.instance.chessPieces.Add(bishopObject);

        //TODO Add Queen to Chess Pieces
        PlayerControl.instance.team2.Add(bishopObject);

        //TODO Add Queen to team list in chess board
        ChessBoard.instance.team2.Add(bishopObject);

        //TODO heldPiece set active false and == to null 
        Destroy(PlayerControl.instance.heldPiece);
        PlayerControl.instance.heldPiece = null;

        //TODO Send Promotion to Network where ("CPROM|" + 1 + pawnIndex + 1(if Queen) )
        Debug.Log("Promoting from Function player 2");
        PlayerControl.instance.SendPromotionToServer("CPROM|", 2, index, 3);
        player1Promoter.SetActive(false);
        player2Promoter.SetActive(false);

    }

    //TODO Promote from Network

    public void PromoteFromNetwork(string[] aData)
    {
        if (int.Parse(aData[1]) == 1)
        {
            Debug.Log("Received Promotion from Network" + aData);
            // TODO Promotion for player 1
            int pawnIndex = int.Parse(aData[2]);
            GameObject pawnFromchessPieces = PlayerControl.instance.chessPieces.Where(x => x.GetComponent<Piece>().pieceIndex == pawnIndex).SingleOrDefault();

            switch (int.Parse(aData[3]))
            {
                case 1:
                    //TODO Promote to queen
                    GameObject queenObject = Instantiate(Player1Queen, new Vector3(pawnFromchessPieces.transform.position.x, 18.52f, pawnFromchessPieces.transform.position.z), pawnFromchessPieces.transform.rotation) as GameObject;

                    //TODO Update Piece Row, Column and Position to match the held Pawn
                     queenObject.GetComponent<Piece>().onPiece = queenObject;
                     queenObject.GetComponent<Piece>().player1PromotionCanvas = player1Promoter;
                     queenObject.GetComponent<Piece>().player2PromotionCanvas = player2Promoter;
                    queenObject.GetComponent<Queen>().queenRow = pawnFromchessPieces.GetComponent<Piece>().row;
                    queenObject.GetComponent<Queen>().queenColum = pawnFromchessPieces.GetComponent<Piece>().column;
                    queenObject.GetComponent<Piece>().row = pawnFromchessPieces.GetComponent<Piece>().row;
                    queenObject.GetComponent<Piece>().column = pawnFromchessPieces.GetComponent<Piece>().column;

                    //TODO UpdateAfterAttack 
                    queenObject.GetComponent<Piece>().updateAfterAttack();

                    queenObject.GetComponent<Piece>().pieceIndex = pawnIndex;
                    queenObject.transform.SetParent(Team1InspectorObject.transform);
                    queenObject.transform.localPosition = new Vector3(queenObject.transform.localPosition.x, 18.52f, queenObject.transform.localPosition.z);
                    PlayerControl.instance.chessPieces.Remove(pawnFromchessPieces);
                    PlayerControl.instance.team1.Remove(pawnFromchessPieces);
                    ChessBoard.instance.team1.Remove(pawnFromchessPieces);

                    //TODO Add Queen to Team1 list
                    PlayerControl.instance.chessPieces.Add(queenObject);

                    //TODO Add Queen to Chess Pieces
                    PlayerControl.instance.team1.Add(queenObject);

                    //TODO Add Queen to team list in chess board
                    ChessBoard.instance.team1.Add(queenObject);

                    Destroy(pawnFromchessPieces);
                    pawnFromchessPieces = null;
                    PlayerControl.instance.playerTurn = 2;
                    break;


                case 2:
                    //TODO Promote to Knight
                    GameObject KnightObject = Instantiate(Player1Knight, new Vector3(pawnFromchessPieces.transform.position.x, 11f, pawnFromchessPieces.transform.position.z), Quaternion.Euler(pawnFromchessPieces.transform.rotation.x, -90f, pawnFromchessPieces.transform.rotation.z)) as GameObject;

                    //TODO Update Piece Row, Column and Position to match the held Pawn
                    KnightObject.GetComponent<Piece>().onPiece = KnightObject;
                    KnightObject.GetComponent<Piece>().player1PromotionCanvas = player1Promoter;
                    KnightObject.GetComponent<Piece>().player2PromotionCanvas = player2Promoter;
                    KnightObject.GetComponent<Knight>().knightRow = pawnFromchessPieces.GetComponent<Piece>().row;
                    KnightObject.GetComponent<Knight>().knightColum = pawnFromchessPieces.GetComponent<Piece>().column;
                    KnightObject.GetComponent<Piece>().row = pawnFromchessPieces.GetComponent<Piece>().row;
                    KnightObject.GetComponent<Piece>().column = pawnFromchessPieces.GetComponent<Piece>().column;

                    //TODO UpdateAfterAttack 
                    KnightObject.GetComponent<Piece>().updateAfterAttack();

                    KnightObject.GetComponent<Piece>().pieceIndex = pawnIndex;
                    KnightObject.transform.SetParent(Team1InspectorObject.transform);
                    KnightObject.transform.localPosition = new Vector3(KnightObject.transform.localPosition.x, 11f, KnightObject.transform.localPosition.z);

                    PlayerControl.instance.chessPieces.Remove(pawnFromchessPieces);
                    PlayerControl.instance.team1.Remove(pawnFromchessPieces);
                    ChessBoard.instance.team1.Remove(pawnFromchessPieces);

                    //TODO Add Queen to Team1 list
                    PlayerControl.instance.chessPieces.Add(KnightObject);

                    //TODO Add Queen to Chess Pieces
                    PlayerControl.instance.team1.Add(KnightObject);

                    //TODO Add Queen to team list in chess board
                    ChessBoard.instance.team1.Add(KnightObject);

                    Destroy(pawnFromchessPieces);
                    pawnFromchessPieces = null;
                    PlayerControl.instance.playerTurn = 2;
                    break;


                case 3:
                    //TODO Promote to Bishop
                    GameObject BishopObject = Instantiate(Player1Bishop, new Vector3(pawnFromchessPieces.transform.position.x, 17.8f, pawnFromchessPieces.transform.position.z), pawnFromchessPieces.transform.rotation) as GameObject;

                    //TODO Update Piece Row, Column and Position to match the held Pawn
                    BishopObject.GetComponent<Piece>().onPiece = BishopObject;
                    BishopObject.GetComponent<Piece>().player1PromotionCanvas = player1Promoter;
                    BishopObject.GetComponent<Piece>().player2PromotionCanvas = player2Promoter;
                    BishopObject.GetComponent<Bishop>().bishopRow = pawnFromchessPieces.GetComponent<Piece>().row;
                    BishopObject.GetComponent<Bishop>().bishopColum = pawnFromchessPieces.GetComponent<Piece>().column;
                    BishopObject.GetComponent<Piece>().row = pawnFromchessPieces.GetComponent<Piece>().row;
                    BishopObject.GetComponent<Piece>().column = pawnFromchessPieces.GetComponent<Piece>().column;

                    //TODO UpdateAfterAttack 
                    BishopObject.GetComponent<Piece>().updateAfterAttack();
                    BishopObject.GetComponent<Piece>().pieceIndex = pawnIndex;
                    BishopObject.transform.SetParent(Team1InspectorObject.transform);
                    BishopObject.transform.localPosition = new Vector3(BishopObject.transform.localPosition.x, 17.8f, BishopObject.transform.localPosition.z);

                    PlayerControl.instance.chessPieces.Remove(pawnFromchessPieces);
                    PlayerControl.instance.team1.Remove(pawnFromchessPieces);
                    ChessBoard.instance.team1.Remove(pawnFromchessPieces);

                    //TODO Add Queen to Team1 list
                    PlayerControl.instance.chessPieces.Add(BishopObject);

                    //TODO Add Queen to Chess Pieces
                    PlayerControl.instance.team1.Add(BishopObject);

                    //TODO Add Queen to team list in chess board
                    ChessBoard.instance.team1.Add(BishopObject);

                    Destroy(pawnFromchessPieces);
                    pawnFromchessPieces = null;
                    PlayerControl.instance.playerTurn = 2;
                    break;


                case 4:
                    //TODO Promote to Rook
                    GameObject RookObject = Instantiate(Player1Rook, new Vector3(pawnFromchessPieces.transform.position.x, 11f, pawnFromchessPieces.transform.position.z), pawnFromchessPieces.transform.rotation) as GameObject;

                    //TODO Update Piece Row, Column and Position to match the held Pawn
                    RookObject.GetComponent<Piece>().onPiece = RookObject;
                    RookObject.GetComponent<Piece>().player1PromotionCanvas = player1Promoter;
                    RookObject.GetComponent<Piece>().player2PromotionCanvas = player2Promoter;
                    RookObject.GetComponent<Rook>().rookRow = pawnFromchessPieces.GetComponent<Piece>().row;
                    RookObject.GetComponent<Rook>().rookColum = pawnFromchessPieces.GetComponent<Piece>().column;
                    RookObject.GetComponent<Piece>().row = pawnFromchessPieces.GetComponent<Piece>().row;
                    RookObject.GetComponent<Piece>().column = pawnFromchessPieces.GetComponent<Piece>().column;

                    //TODO UpdateAfterAttack 
                    RookObject.GetComponent<Piece>().updateAfterAttack();
                    RookObject.GetComponent<Piece>().pieceIndex = pawnIndex;
                    RookObject.transform.SetParent(Team1InspectorObject.transform);
                    RookObject.transform.localPosition = new Vector3(RookObject.transform.localPosition.x, 11f, RookObject.transform.localPosition.z);

                    PlayerControl.instance.chessPieces.Remove(pawnFromchessPieces);
                    PlayerControl.instance.team1.Remove(pawnFromchessPieces);
                    ChessBoard.instance.team1.Remove(pawnFromchessPieces);

                    //TODO Add Queen to Team1 list
                    PlayerControl.instance.chessPieces.Add(RookObject);

                    //TODO Add Queen to Chess Pieces
                    PlayerControl.instance.team1.Add(RookObject);

                    //TODO Add Queen to team list in chess board
                    ChessBoard.instance.team1.Add(RookObject);

                    Destroy(pawnFromchessPieces);
                    pawnFromchessPieces = null;
                    PlayerControl.instance.playerTurn = 2;
                    break;
            }

            player1Promoter.SetActive(false);
            player2Promoter.SetActive(false);


        }
        else if (int.Parse(aData[1]) == 2)
        {
            Debug.Log("Received Promotion from Network" + aData);

            //TODO Promotion for player 2 
            int pawnIndex = int.Parse(aData[2]);
            GameObject pawnFromchessPieces = PlayerControl.instance.chessPieces.Where(x => x.GetComponent<Piece>().pieceIndex == pawnIndex).SingleOrDefault();

            switch (int.Parse(aData[3]))
            {
                case 1:
                    //TODO Promote to queen
                    GameObject queenObject = Instantiate(Player2Queen, new Vector3(pawnFromchessPieces.transform.position.x, 18.52f, pawnFromchessPieces.transform.position.z), pawnFromchessPieces.transform.rotation) as GameObject;

                    //TODO Update Piece Row, Column and Position to match the held Pawn
                    queenObject.GetComponent<Piece>().onPiece = queenObject;
                     queenObject.GetComponent<Piece>().player1PromotionCanvas = player1Promoter;
                     queenObject.GetComponent<Piece>().player2PromotionCanvas = player2Promoter;
                    queenObject.GetComponent<Queen>().queenRow = pawnFromchessPieces.GetComponent<Piece>().row;
                    queenObject.GetComponent<Queen>().queenColum = pawnFromchessPieces.GetComponent<Piece>().column;
                    queenObject.GetComponent<Piece>().row = pawnFromchessPieces.GetComponent<Piece>().row;
                    queenObject.GetComponent<Piece>().column = pawnFromchessPieces.GetComponent<Piece>().column;

                    //TODO UpdateAfterAttack 
                    queenObject.GetComponent<Piece>().updateAfterAttack();

                    queenObject.GetComponent<Piece>().pieceIndex = pawnIndex;
                    queenObject.transform.SetParent(Team2InspectorObject.transform);
                    queenObject.transform.localPosition = new Vector3(queenObject.transform.localPosition.x, 18.52f, queenObject.transform.localPosition.z);

                    PlayerControl.instance.chessPieces.Remove(pawnFromchessPieces);
                    PlayerControl.instance.team2.Remove(pawnFromchessPieces);
                    ChessBoard.instance.team2.Remove(pawnFromchessPieces);

                    //TODO Add Queen to Team1 list
                    PlayerControl.instance.chessPieces.Add(queenObject);

                    //TODO Add Queen to Chess Pieces
                    PlayerControl.instance.team2.Add(queenObject);

                    //TODO Add Queen to team list in chess board
                    ChessBoard.instance.team2.Add(queenObject);

                    Destroy(pawnFromchessPieces);
                    pawnFromchessPieces = null;
                    PlayerControl.instance.playerTurn = 1;
                    break;


                case 2:
                    //TODO Promote to Knight
                    GameObject KnightObject = Instantiate(Player2Knight, new Vector3(pawnFromchessPieces.transform.position.x, 11f, pawnFromchessPieces.transform.position.z), Quaternion.Euler(pawnFromchessPieces.transform.rotation.x, 90f, pawnFromchessPieces.transform.rotation.z)) as GameObject;

                    //TODO Update Piece Row, Column and Position to match the held Pawn
                    KnightObject.GetComponent<Piece>().onPiece = KnightObject;
                    KnightObject.GetComponent<Piece>().player1PromotionCanvas = player1Promoter;
                    KnightObject.GetComponent<Piece>().player2PromotionCanvas = player2Promoter;
                    KnightObject.GetComponent<Knight>().knightRow = pawnFromchessPieces.GetComponent<Piece>().row;
                    KnightObject.GetComponent<Knight>().knightColum = pawnFromchessPieces.GetComponent<Piece>().column;
                    KnightObject.GetComponent<Piece>().row = pawnFromchessPieces.GetComponent<Piece>().row;
                    KnightObject.GetComponent<Piece>().column = pawnFromchessPieces.GetComponent<Piece>().column;

                    //TODO UpdateAfterAttack 
                    KnightObject.GetComponent<Piece>().updateAfterAttack();

                    KnightObject.GetComponent<Piece>().pieceIndex = pawnIndex;
                    KnightObject.transform.SetParent(Team2InspectorObject.transform);
                    KnightObject.transform.localPosition = new Vector3(KnightObject.transform.localPosition.x, 11f, KnightObject.transform.localPosition.z);

                    PlayerControl.instance.chessPieces.Remove(pawnFromchessPieces);
                    PlayerControl.instance.team2.Remove(pawnFromchessPieces);
                    ChessBoard.instance.team2.Remove(pawnFromchessPieces);

                    //TODO Add Queen to Team1 list
                    PlayerControl.instance.chessPieces.Add(KnightObject);

                    //TODO Add Queen to Chess Pieces
                    PlayerControl.instance.team2.Add(KnightObject);

                    //TODO Add Queen to team list in chess board
                    ChessBoard.instance.team2.Add(KnightObject);

                    Destroy(pawnFromchessPieces);
                    pawnFromchessPieces = null;
                    PlayerControl.instance.playerTurn = 1;
                    break;


                case 3:
                    //TODO Promote to Bishop
                    GameObject BishopObject = Instantiate(Player2Bishop, new Vector3(pawnFromchessPieces.transform.position.x, 17.8f, pawnFromchessPieces.transform.position.z), pawnFromchessPieces.transform.rotation) as GameObject;

                    //TODO Update Piece Row, Column and Position to match the held Pawn
                    BishopObject.GetComponent<Piece>().onPiece = BishopObject;
                    BishopObject.GetComponent<Piece>().player1PromotionCanvas = player1Promoter;
                    BishopObject.GetComponent<Piece>().player2PromotionCanvas = player2Promoter;
                    BishopObject.GetComponent<Bishop>().bishopRow = pawnFromchessPieces.GetComponent<Piece>().row;
                    BishopObject.GetComponent<Bishop>().bishopColum = pawnFromchessPieces.GetComponent<Piece>().column;
                    BishopObject.GetComponent<Piece>().row = pawnFromchessPieces.GetComponent<Piece>().row;
                    BishopObject.GetComponent<Piece>().column = pawnFromchessPieces.GetComponent<Piece>().column;

                    //TODO UpdateAfterAttack 
                    BishopObject.GetComponent<Piece>().updateAfterAttack();
                    BishopObject.GetComponent<Piece>().pieceIndex = pawnIndex;
                    BishopObject.transform.SetParent(Team2InspectorObject.transform);
                    BishopObject.transform.localPosition = new Vector3(BishopObject.transform.localPosition.x, 17.8f, BishopObject.transform.localPosition.z);

                    PlayerControl.instance.chessPieces.Remove(pawnFromchessPieces);
                    PlayerControl.instance.team2.Remove(pawnFromchessPieces);
                    ChessBoard.instance.team2.Remove(pawnFromchessPieces);

                    //TODO Add Queen to Team1 list
                    PlayerControl.instance.chessPieces.Add(BishopObject);

                    //TODO Add Queen to Chess Pieces
                    PlayerControl.instance.team2.Add(BishopObject);

                    //TODO Add Queen to team list in chess board
                    ChessBoard.instance.team2.Add(BishopObject);

                    Destroy(pawnFromchessPieces);
                    pawnFromchessPieces = null;
                    PlayerControl.instance.playerTurn = 1;
                    break;


                case 4:
                    //TODO Promote to Rook
                    GameObject RookObject = Instantiate(Player2Rook, new Vector3(pawnFromchessPieces.transform.position.x, 11f, pawnFromchessPieces.transform.position.z), pawnFromchessPieces.transform.rotation) as GameObject;

                    //TODO Update Piece Row, Column and Position to match the held Pawn
                    RookObject.GetComponent<Piece>().onPiece = RookObject;
                    RookObject.GetComponent<Piece>().player1PromotionCanvas = player1Promoter;
                    RookObject.GetComponent<Piece>().player2PromotionCanvas = player2Promoter;
                    RookObject.GetComponent<Rook>().rookRow = pawnFromchessPieces.GetComponent<Piece>().row;
                    RookObject.GetComponent<Rook>().rookColum = pawnFromchessPieces.GetComponent<Piece>().column;
                    RookObject.GetComponent<Piece>().row = pawnFromchessPieces.GetComponent<Piece>().row;
                    RookObject.GetComponent<Piece>().column = pawnFromchessPieces.GetComponent<Piece>().column;

                    //TODO UpdateAfterAttack 
                    RookObject.GetComponent<Piece>().updateAfterAttack();
                    RookObject.GetComponent<Piece>().pieceIndex = pawnIndex;
                    RookObject.transform.SetParent(Team2InspectorObject.transform);
                    RookObject.transform.localPosition = new Vector3(RookObject.transform.localPosition.x, 11f, RookObject.transform.localPosition.z);

                    PlayerControl.instance.chessPieces.Remove(pawnFromchessPieces);
                    PlayerControl.instance.team2.Remove(pawnFromchessPieces);
                    ChessBoard.instance.team2.Remove(pawnFromchessPieces);

                    //TODO Add Queen to Team1 list
                    PlayerControl.instance.chessPieces.Add(RookObject);

                    //TODO Add Queen to Chess Pieces
                    PlayerControl.instance.team2.Add(RookObject);

                    //TODO Add Queen to team list in chess board
                    ChessBoard.instance.team2.Add(RookObject);

                    Destroy(pawnFromchessPieces);
                    pawnFromchessPieces = null;
                    PlayerControl.instance.playerTurn = 1;
                    break;
            }
        player1Promoter.SetActive(false);
        player2Promoter.SetActive(false);

        }
    }
}
