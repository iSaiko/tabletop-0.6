﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {


	private Renderer playerRenderer;

	public Color playerColor;
	public float animationDuration;
	public int index;
    public bool shouldMove;


	public Animation animation;
//	public AnimationClip rotateUp;
//	public AnimationClip rotateDown;
	AnimationClip clip ;



	void Start () 
	
	{
		clip = new AnimationClip();
		clip.legacy = true;

        shouldMove = false; 
		playerRenderer = gameObject.GetComponent<Renderer>();
		playerRenderer.material.color = playerColor;

	}


		

	public void moveWithAnimation(int animationIndex)
	{
        
		animation = GetComponent<Animation>();
        if (animationIndex < 1)
        {
            this.transform.position = new Vector3(SnakeBoard.instance.tiles[0].transform.position.x, SnakeBoard.instance.tiles[0].transform.parent.transform.position.y, SnakeBoard.instance.tiles[0].transform.position.z);

        }


        if (animationIndex > index)
        {
            if(SnakeBoard.instance.tiles[index].snakeHead)
            {
                int tailToGo = SnakeBoard.instance.tiles[index].tailIndex;
                print("tail to go : " + tailToGo);
                index = tailToGo - 1;
                jump(index);
                shouldMove = false;
                print("finishedMoving  :  " + shouldMove);
                return;
            }else if(SnakeBoard.instance.tiles[index].ladderTail){
                int headToGo = SnakeBoard.instance.tiles[index].topIndex;
                print("tail to go : " + headToGo);
                index = headToGo - 1;
                jump(index);
                shouldMove = false;
                print("finishedMoving  :  " + shouldMove);
                return;
            }
            else
            {
                shouldMove = false;
                print("finishedMoving  :  " + shouldMove);
                return;
            }
            
        }
		if(animationIndex >=1)
		{


		// animating postion from current postion to the next

			AnimationCurve curve = AnimationCurve.Linear(0,transform.position.x,animationDuration,SnakeBoard.instance.tiles[animationIndex].transform.position.x);
			clip.SetCurve("", typeof(Transform), "localPosition.x", curve);


			curve = AnimationCurve.Linear(0,transform.position.y,animationDuration,SnakeBoard.instance.tiles[animationIndex].transform.parent.transform.position.y);
			clip.SetCurve("", typeof(Transform), "localPosition.y", curve);


			curve = AnimationCurve.Linear(0,transform.position.z,animationDuration,SnakeBoard.instance.tiles[animationIndex].transform.position.z - 2);
			curve.AddKey(animationDuration/2,SnakeBoard.instance.tiles[animationIndex].transform.position.z - 10);
			clip.SetCurve("", typeof(Transform), "localPosition.z", curve);


		// animating scale 


		curve = AnimationCurve.Linear(0,transform.lossyScale.x,animationDuration,transform.lossyScale.x);
		curve.AddKey(animationDuration/2,(float)(transform.lossyScale.x+2));
		clip.SetCurve("", typeof(Transform), "lossyScale.z", curve);

		curve = AnimationCurve.Linear(0,transform.lossyScale.y,animationDuration,transform.lossyScale.y);
		curve.AddKey(animationDuration/2,(float)(transform.lossyScale.y+2));
		clip.SetCurve("", typeof(Transform), "lossyScale.y", curve);

		curve = AnimationCurve.Linear(0,transform.lossyScale.z,animationDuration,transform.lossyScale.z);
		curve.AddKey(animationDuration/2,(float)(transform.lossyScale.z+2));
		clip.SetCurve("", typeof(Transform), "lossyScale.z", curve);


		animation.AddClip(clip, "movePosition");
		animation.Play("movePosition");
		

			StartCoroutine(nextMove(animationDuration,animationIndex+1));


		}


	}



	IEnumerator nextMove(float animationDuration,int nextMove) {
		yield return new WaitForSeconds(animationDuration);
		SoundsManager.instance.playSnakePieceMovementSound();
		moveWithAnimation(nextMove);
	}



    public void jump(int destination)
    {


        // animating postion from current postion to the next

        AnimationCurve curve = AnimationCurve.Linear(0, transform.position.x, animationDuration, SnakeBoard.instance.tiles[destination].transform.position.x);
        clip.SetCurve("", typeof(Transform), "localPosition.x", curve);


        curve = AnimationCurve.Linear(0, transform.position.y, animationDuration, SnakeBoard.instance.tiles[destination].transform.parent.transform.position.y);
        clip.SetCurve("", typeof(Transform), "localPosition.y", curve);


        curve = AnimationCurve.Linear(0, transform.position.z, animationDuration, SnakeBoard.instance.tiles[destination].transform.position.z - 2);
        curve.AddKey(animationDuration / 2, SnakeBoard.instance.tiles[destination].transform.position.z - 10);
        clip.SetCurve("", typeof(Transform), "localPosition.z", curve);


        // animating scale 


        curve = AnimationCurve.Linear(0, transform.lossyScale.x, animationDuration, transform.lossyScale.x);
        curve.AddKey(animationDuration / 2, (float)(transform.lossyScale.x + 2));
        clip.SetCurve("", typeof(Transform), "lossyScale.z", curve);

        curve = AnimationCurve.Linear(0, transform.lossyScale.y, animationDuration, transform.lossyScale.y);
        curve.AddKey(animationDuration / 2, (float)(transform.lossyScale.y + 2));
        clip.SetCurve("", typeof(Transform), "lossyScale.y", curve);


        curve = AnimationCurve.Linear(0, transform.lossyScale.z, animationDuration, transform.lossyScale.z);
        curve.AddKey(animationDuration / 2, (float)(transform.lossyScale.z + 2));
        clip.SetCurve("", typeof(Transform), "lossyScale.z", curve);


        animation.AddClip(clip, "movePosition");
        animation.Play("movePosition");


    }



	
	
	
	
	
	
}
