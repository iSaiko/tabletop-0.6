﻿using UnityEngine;
using System.Collections;

public class Stile : MonoBehaviour {
	
	public Material availableMaterial ;
	private Material normalMaterial ;
	public bool available;
    public GameObject Indicator;
    public GameObject IndicatorCreated;
//	public static SnakeBoard snakeBoard; 
    public Material unAvailableMaterial;
	public  Renderer myRenderer ;
	private Color intialColor;

    public bool snakeHead;
    public bool ladderTail;
    public bool snakeTail;
    public bool ladderHead;
    public int tailIndex;
    public int topIndex;
	void Start () {
		myRenderer = GetComponent<Renderer> ();
		normalMaterial = myRenderer.material;
		intialColor = myRenderer.material.color;
//		snakeBoard = GameObject.Find("snakeBoard").GetComponent<SnakeBoard>();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void makeAvailable()
	{
		myRenderer.material = unAvailableMaterial;
//        myRenderer.material.color = Color.red;
        IndicatorCreated = Instantiate(Indicator, new Vector3(transform.position.x, transform.position.y + 5, transform.position.z -15), new Quaternion(-90f,0f,0f,-45f)) as GameObject;
		available = true;

	}

	public void makeUnAvailable()
	{
        //myRenderer.material = normalMaterial;
        //myRenderer.material.color = intialColor;
        myRenderer.material = normalMaterial;
		available = false;
        Destroy(IndicatorCreated);

	}


}
