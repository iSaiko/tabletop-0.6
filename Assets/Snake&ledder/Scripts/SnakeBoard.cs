﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using ArabicSupport;


public  class SnakeBoard : MonoBehaviour {


    public static SnakeBoard instance { set; get; }

	public Stile[] tiles;
    [SerializeField]
    public int BoardNumber;
//	public int player1Index = -1;
//	public int player2Index = -1;
	public bool player1Entered = false;
	public bool player2Entered = false;
	public bool player1HasBonusMove = false;
	public bool player2HasBonusMove = false;

    public GameObject Dice;

    public Button ButtonThrow;
    private int[] Board1SnakeIndex = { 24 , 34, 43, 83, 73, 95,
                                         4, 8, 22, 36, 49,75 };
    private int[] Board1LadderIndex = { 20, 14, 31, 58, 77, 80, 92, 99,
                                        2, 5, 9, 41, 33, 62, 90, 81};

    private int[] Board2SnakeIndex = { 24, 34, 46, 43, 89, 84,
                                        4, 8, 15, 22, 52, 56};
    private int[] Board2LadderIndex = { 18, 14, 31, 77, 58, 80, 93, 99,
                                        2, 5, 9, 33, 41, 44, 74, 81};

    private int[] Board3SnakeIndex = { 19, 24, 37, 34, 43, 45, 61, 69, 89, 93, 84, 99,
                                         1, 4, 5, 8, 22, 25, 41, 49, 52, 54, 56, 62 };
    private int[] Board3LadderIndex = {17,31,71,80,
                                          3,9,35,44 };
    public GameObject Board1;
    public GameObject Board2;
    public GameObject Board3;

	public int diceRoll ;
	public Player player1;
	public Player player2;
	public bool player1Turn;
	private float timer ; 
	public float timerValue ;
	public bool diceRolled  = false ;

    private m_UNETSnake Client; 
    public Transform chatMessageContainer;
    public GameObject messagePrefab;
	//public static SnakeBoard instance;

    public bool isHost;
//	public Stile selectedTile;
    public bool someoneWon;

	public GameObject EndGameCanvas;

	void Start () 
	{
        someoneWon = false;
        Client = GameObject.Find("UNETSnakeObject").GetComponent<m_UNETSnake>();
        BoardNumber = Client.BoardNumber;
        isHost = Client.isHost;
        //isHost = true;
        player1Turn = true;
		instance = this;
		player1.index = 0 ;
		player2.index = 0 ;
		timer = timerValue;
        if (BoardNumber == 1)
        {
           // Debug.Log("Board 1");

            Board1.SetActive(true);
            //snake head and tails
            for (int i = 0; i < Board1SnakeIndex.Length / 2; i++)
            {
                tiles[Board1SnakeIndex[i] - 1].snakeHead = true;
                tiles[Board1SnakeIndex[i] - 1].tailIndex = Board1SnakeIndex[i + 6];
                tiles[Board1SnakeIndex[i] - 1].myRenderer = tiles[Board1SnakeIndex[i] - 1].GetComponent<Renderer>();
                //tiles[Board1SnakeIndex[i] - 1].myRenderer.material.color = Color.black;
            }
            for (int i = Board1SnakeIndex.Length / 2; i < Board1SnakeIndex.Length; i++)
            {
                tiles[Board1SnakeIndex[i] - 1].snakeTail = true;
                tiles[Board1SnakeIndex[i] - 1].myRenderer = tiles[Board1SnakeIndex[i] - 1].GetComponent<Renderer>();
                //tiles[Board1SnakeIndex[i] - 1].myRenderer.material.color = Color.red;
            }
            //ladder head and tails
            for (int i = 0; i < Board1LadderIndex.Length / 2; i++)
            {
                tiles[Board1LadderIndex[i] - 1].ladderHead = true;
                tiles[Board1LadderIndex[i] - 1].myRenderer = tiles[Board1LadderIndex[i] - 1].GetComponent<Renderer>();
                //tiles[Board1LadderIndex[i] - 1].myRenderer.material.color = Color.magenta;
            }
            for (int i = Board1LadderIndex.Length / 2; i < Board1LadderIndex.Length; i++)
            {
                tiles[Board1LadderIndex[i] - 1].ladderTail = true;
                tiles[Board1LadderIndex[i] - 1].topIndex = Board1LadderIndex[i-8];
                tiles[Board1LadderIndex[i] - 1].myRenderer = tiles[Board1LadderIndex[i] - 1].GetComponent<Renderer>();
                //tiles[Board1LadderIndex[i] - 1].myRenderer.material.color = Color.green;
            }
        }

        if (BoardNumber == 2)
        {
            Board2.SetActive(true);
            //Debug.Log("Board 2");
            for (int i = 0; i < Board2SnakeIndex.Length / 2; i++)
            {
                tiles[Board2SnakeIndex[i] - 1].snakeHead = true;
                tiles[Board2SnakeIndex[i] - 1].tailIndex = Board2SnakeIndex[i + 6];
                tiles[Board2SnakeIndex[i] - 1].myRenderer = tiles[Board2SnakeIndex[i] - 1].GetComponent<Renderer>();
                //tiles[Board2SnakeIndex[i] - 1].myRenderer.material.color = Color.black;
            }
            for (int i = Board2SnakeIndex.Length / 2; i < Board2SnakeIndex.Length; i++)
            {
                tiles[Board2SnakeIndex[i] - 1].snakeTail = true;
                tiles[Board2SnakeIndex[i] - 1].myRenderer = tiles[Board2SnakeIndex[i] - 1].GetComponent<Renderer>();
                //tiles[Board2SnakeIndex[i] - 1].myRenderer.material.color = Color.red;
            }

            //ladder head and tails
            for (int i = 0; i < Board1LadderIndex.Length / 2; i++)
            {
                tiles[Board2LadderIndex[i] - 1].ladderHead = true;
                tiles[Board2LadderIndex[i] - 1].myRenderer = tiles[Board2LadderIndex[i] - 1].GetComponent<Renderer>();
                //tiles[Board2LadderIndex[i] - 1].myRenderer.material.color = Color.magenta;
            }
            for (int i = Board2LadderIndex.Length / 2; i < Board2LadderIndex.Length; i++)
            {
                tiles[Board2LadderIndex[i] - 1].ladderTail = true;
                tiles[Board2LadderIndex[i] - 1].topIndex = Board2LadderIndex[i - 8];
                tiles[Board2LadderIndex[i] - 1].myRenderer = tiles[Board2LadderIndex[i] - 1].GetComponent<Renderer>();
                //tiles[Board2LadderIndex[i] - 1].myRenderer.material.color = Color.green;
            }
        }

        if (BoardNumber == 3)
        {
            //Debug.Log("Board 3");

            Board3.SetActive(true);
            for (int i = 0; i < Board3SnakeIndex.Length / 2; i++)
            {
                tiles[Board3SnakeIndex[i] - 1].snakeHead = true;
                tiles[Board3SnakeIndex[i] - 1].tailIndex = Board3SnakeIndex[i + 12];
                tiles[Board3SnakeIndex[i] - 1].myRenderer = tiles[Board3SnakeIndex[i] - 1].GetComponent<Renderer>();
                //tiles[Board3SnakeIndex[i] - 1].myRenderer.material.color = Color.black;
            }
            for (int i = Board3SnakeIndex.Length / 2; i < Board3SnakeIndex.Length; i++)
            {
                tiles[Board3SnakeIndex[i] - 1].snakeTail = true;
                tiles[Board3SnakeIndex[i] - 1].myRenderer = tiles[Board3SnakeIndex[i] - 1].GetComponent<Renderer>();
               // tiles[Board3SnakeIndex[i] - 1].myRenderer.material.color = Color.red;
            }

            //ladder head and tails
            for (int i = 0; i < Board1LadderIndex.Length / 2; i++)
            {
                tiles[Board3LadderIndex[i] - 1].ladderHead = true;
                tiles[Board3LadderIndex[i] - 1].myRenderer = tiles[Board3LadderIndex[i] - 1].GetComponent<Renderer>();
               // tiles[Board3LadderIndex[i] - 1].myRenderer.material.color = Color.magenta;
            }
            for (int i = Board3LadderIndex.Length / 2; i < Board3LadderIndex.Length; i++)
            {
                tiles[Board3LadderIndex[i] - 1].ladderTail = true;
                tiles[Board3LadderIndex[i] - 1].topIndex = Board3LadderIndex[i - 4];
                tiles[Board3LadderIndex[i] - 1].myRenderer = tiles[Board3LadderIndex[i] - 1].GetComponent<Renderer>();
               // tiles[Board3LadderIndex[i] - 1].myRenderer.material.color = Color.green;
            }
        }
        //Debug.Log(isHost);
	}
	
	void Update () 
	{
        if (someoneWon)
            return;

        if (player1.transform.position == player2.transform.position)
        {
            player1.transform.position += new Vector3(0, 2.5f, 0);
            player2.transform.position -= new Vector3(0, 2.5f, 0);

        }
        //timer -= Time.deltaTime;
        //if(timer <= 0)
        //{
        //    SendSwitchTurnCommandToServer("CSWTCH|");

        //}
		

		if(Input.GetMouseButtonDown(0))
		{

				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit rayHit;
				Debug.DrawLine(ray.origin,ray.direction,Color.green,1f);
                //Debug.Log("Pressed Left Button");
				if(Physics.Raycast (ray, out rayHit,1000 ,LayerMask.GetMask("board")))
				{
                    //Debug.Log("Pressed on SnakeBoard");
                    Stile selectedTile = findTile(rayHit);
					move(selectedTile , player1Turn);
				}

		}
	}
	


	void OnGUI() {
            //GUI.Button(new Rect(Screen.width/2 +350, Screen.height/2 +200, 75, 25), "throw dice");
        if((isHost && player1Turn) || (!isHost && !player1Turn)){
            //ButtonThrow.interactable = true;
            Dice.SetActive(true);
        }
        else
        {
            //ButtonThrow.interactable = false;
            Dice.SetActive(false);

        }
        //if (GUI.Button(new Rect(Screen.width/2 +350, Screen.height/2 +200, 75, 25), "throw dice"))
        //{
        //    //throwDice();
        //}


		//GUI.Label(new Rect(Screen.width/8,Screen.height/8 , 128.0f, 32.0f), timeToDisplay());


		
	}

    
	public void throwDice()
	{

		if(!diceRolled)
		{
			 diceRoll = Random.Range(1,7);
			//print ("dice number is : " + diceRoll );

			if(player1Turn)
            {
                SendPlayerDiceRoll("CROLL|", diceRoll, 1);
                //if(!player1Entered && diceRoll!= 6)
                //{

                //    //SendSwitchTurnCommandToServer("CSWTCH|");
                //    return;
                //}
                //else 

                //{
                //    player1.index  = (!player1Entered) ? 0 : player1.index + diceRoll ; 
                //    player1Entered = true;
                //   // SendPlayerEnteredCase("CPEC|", 1);
                //    player1HasBonusMove = (diceRoll == 6) ? true : false ;
                   if(player1.index + diceRoll  < 100)
                   {
                        tiles[player1.index].makeAvailable();
                   }
                //    else 
                //    {
                //        player1.index -= diceRoll; 
                //        //SendSwitchTurnCommandToServer("CSWTCH|");
                //        diceRolled = true;

                //        return;

                //    }
                //}

			}
			if(!player1Turn)
			{
                SendPlayerDiceRoll("CROLL|", diceRoll, 2);

                //if(!player2Entered && diceRoll!= 6)
                //{

                //   // SendSwitchTurnCommandToServer("CSWTCH|");
                //    return;
                //}
                //else
                //{
                //    player2.index = (!player2Entered) ? 0 : player2.index+ diceRoll ; 
                //    player2Entered = true;
                //  //  SendPlayerEnteredCase("CPEC|", 2);

                //    player2HasBonusMove = (diceRoll == 6) ? true : false ;
                    if(player2.index + diceRoll < 100)
                    {
                        tiles[player2.index ].makeAvailable();
                    }
                //    else 
                //    {
                //        player2.index -= diceRoll;
                //        //SendSwitchTurnCommandToServer("CSWTCH|");
                //        diceRolled = true;

                //        return;
						
                //    }
                //}
				
			}

		}
        //print("throw dice method is called");

	}

  void victory(bool player1Turn)
	{
		if(player1Turn)
		{print("player 1 has won the game");}

		else if(!player1Turn)
		{print("player 2 has won the game");}

	}

	Stile findTile(RaycastHit rayHit)
	{
		for(int i = 0 ; i<tiles.Length ; i++)
		{
			if(tiles[i].transform.gameObject == rayHit.collider.gameObject)
			{return tiles[i];}

		}

		Stile voidStile = new Stile();
		return voidStile;
	}


	private string timeToDisplay ()
	{
		int timer = Mathf.FloorToInt(this.timer);
		return timer.ToString();
	}

	public void switchTurns()
	{
		diceRolled = false ;
		player1Turn = !player1Turn;
		timer = timerValue ;
	}

	private void move(Stile selectedTile , bool player1Turn)
	{

		if(player1Turn && isHost)
		{
				if(selectedTile.available)
			{
                //Debug.Log("Move Player 1");
//				player1.transform.position = new Vector3(selectedTile.transform.position.x,selectedTile.transform.parent.transform.position.y,selectedTile.transform.position.z);
                SendMoveCommandToServer("CMOV|", 1, diceRoll, (player1HasBonusMove) ? 1 : 0,player1.index);
                   // player1.moveWithAnimation(player1.index - diceRoll +1);
				selectedTile.makeUnAvailable();

                //checkVictory(player1Turn);
                //if (player1HasBonusMove)
                //{
                //    diceRolled = false;
                //    player1HasBonusMove = false;
                //    return;
                //}
                //else
                //{
                //    //SendSwitchTurnCommandToServer("CSWTCH|");
                //}
			
            }

		}
		else if(!player1Turn && !isHost)
		{
			if(selectedTile.available)
			{

                //Debug.Log("Move Player 2");
//				player2.transform.position = new Vector3(selectedTile.transform.position.x,selectedTile.transform.parent.transform.position.y,selectedTile.transform.position.z);
                SendMoveCommandToServer("CMOV|", 2, diceRoll,(player2HasBonusMove)?1:0, player2.index);

                if (selectedTile.snakeHead)
                {
                    //go back to snake tail
                }
				selectedTile.makeUnAvailable();
                //checkVictory(!player1Turn);
                //if(player2HasBonusMove)
                //{
                //    diceRolled = false ;
                //    player2HasBonusMove = false;
                //    return;
                //}
               // SendSwitchTurnCommandToServer("CSWTCH|");

			}

		}
	}

	private void checkVictory(bool player1Turn)
	{
		if(player1Turn && player1.index  > 99 )
		{

			victory(player1Turn);

            m_UNETGameManager go = GameObject.FindObjectOfType<m_UNETGameManager>();
            m_UNETSnake go2 = GameObject.FindObjectOfType<m_UNETSnake>();
            

            Destroy(go.gameObject);
            Destroy(go2.gameObject);
		}
		if(!player1Turn && player2.index > 99 )
		{
			victory(!player1Turn);

            m_UNETGameManager go = GameObject.FindObjectOfType<m_UNETGameManager>();
            m_UNETSnake go2 = GameObject.FindObjectOfType<m_UNETSnake>();

            Destroy(go.gameObject);
            Destroy(go2.gameObject);
		}
	}




    public void SendMoveCommandToServer(string cmnd, int playerNum, int diceRoll, int hasBonusMove, int newIndex)
    {
        string msg = cmnd;
        msg += playerNum + "|";
        msg += diceRoll + "|";
        msg += hasBonusMove + "|";
        msg += newIndex;

        Client.UNETSend(msg);
        Client.ClientSnake(msg);
    }

    public void SendSwitchTurnCommandToServer(string cmnd) {
        string msg = cmnd;
       // Debug.Log("Switch from Board!");
        Client.UNETSend(msg); 
        Client.ClientSnake(msg);

    }

    public void SendPlayerEnteredCase(string cmnd, int playerNum)
    {
        string msg = cmnd;
        msg += playerNum;

        Client.UNETSend(msg); Client.ClientSnake(msg);


    }

    public void SendPlayerDiceRoll(string cmnd, int diceRoll, int player)
    {
        string msg = cmnd;
        msg += diceRoll + "|";
        msg += player;

        Client.UNETSend(msg); 
        Client.ClientSnake(msg);

    }


    public void ChatMessage(string msg)
    {
        string messageToShow = "";
        GameObject go = Instantiate(messagePrefab) as GameObject;
        string[] chatmsg = msg.Split(':');
        //Debug.Log(chatmsg[0]+chatmsg[1]);
        //messageToShow = "<color=red>" + chatmsg[0] + "</color>" + chatmsg[1];
        //Debug.Log(UiMenuValues.instance.player1NameLabelWR.text);
        if (chatmsg[0].Equals(m_UNETSnake.instance.currentPlayerName))
            messageToShow = "<color=blue><b>" + chatmsg[0] + "</b></color>" + " : " + ArabicFixer.Fix(chatmsg[1], true, true);
        else if (chatmsg[0].Equals(m_UNETSnake.instance.secondPlayerName))
            messageToShow = "<color=red><b>" + chatmsg[0] + "</b></color>" + " : " + ArabicFixer.Fix(chatmsg[1], true, true);
        go.transform.SetParent(chatMessageContainer);
        go.transform.localScale = new Vector3(1f, 1.2f, 1f);
        go.GetComponentInChildren<Text>().text = messageToShow;


    }

    public void SendChatMessage()
    {
        InputField i = GameObject.Find("MessageInput").GetComponent<InputField>();
        if (i.text == "")
            return;

        Client.UNETSend("CMSG|" + m_UNETGameManager.instance.currentPlayerName + ":" + i.text);
        Client.ClientSnake("CMSG|" + m_UNETGameManager.instance.currentPlayerName + ":" + i.text);

        i.text = "";
    }
    public void SendSurrender()
    {
        int playerNum = 0;
        if (Client.isHost) playerNum = 1;
        else if (!Client.isHost) playerNum = 2;

        string msg = "SRNDR|" + playerNum;
        Client.UNETSend(msg);
    }

	public void iSurrender(){
		EndGameCanvas.SetActive(true);
		EndGameCanvas.GetComponentInChildren<Text>().text = "You have lost the game" ;

		StartCoroutine(afterAnnouncingWinner());
	}

	public void ExitGameButton(){
		StartCoroutine(afterAnnouncingWinner());
	}

	IEnumerator afterAnnouncingWinner()
	{
		m_UNETGameManager go = GameObject.FindObjectOfType<m_UNETGameManager>();
		m_UNETSnake go2 = GameObject.FindObjectOfType<m_UNETSnake>();
		//		go2.ShutDownRoom();
		go2.ShutDownConnection();

		yield return new WaitForSeconds(2f);
		Destroy(Rooms.instance.gameObject);
		Destroy(go.gameObject);
		Destroy(go2.gameObject);
		Resources.UnloadUnusedAssets();
		SceneManager.LoadScene("main_menu_scene");

		//we should be going to main menu Or to the available rooms menu 
	}
}
