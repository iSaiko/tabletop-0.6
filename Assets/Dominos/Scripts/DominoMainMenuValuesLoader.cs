﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DominoMainMenuValuesLoader : MonoBehaviour {

	public Text player1Label;
	public Text player2Label;

	public Text totalGameScoreLabel1;
	public Text totalGameScoreLabel2;

	void Start () {
		Invoke("loadMainMenuValues",.05f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
		
	private void loadMainMenuValues()
	{
		player1Label.text = m_UNETDomino.instance.currentPlayerName;
		player2Label.text = m_UNETDomino.instance.secondPlayerName;
		totalGameScoreLabel1.text = totalGameScoreLabel2.text = m_UNETDomino.instance.GameScore.ToString();
		Destroy(this.gameObject);
	}
}
