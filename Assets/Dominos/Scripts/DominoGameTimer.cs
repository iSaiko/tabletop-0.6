﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DominoGameTimer : MonoBehaviour {



	public float gameTimeCounter;

	public  int gameSeconds;
	public int gameMinutes;
	public int gameHours;
	private float timer;

	public static DominoGameTimer instance;
	public Text dominoGameTimeLabel;





	void Start () {

		instance = this;

        float[] times = m_UNETDomino.instance.loadGameTime();
		gameTimeCounter = times[0];
		gameSeconds = (int)times[1];
		gameMinutes = (int)times[2];
		gameHours = (int)times[3];
	}


	void Update () 
	{

		// displaying total game time
		gameTimeCounter += Time.deltaTime;
		gameSeconds = Mathf.CeilToInt(gameTimeCounter);


		if(gameSeconds > 59 )
		{
			gameMinutes += 1;
			gameSeconds = 0 ;
			gameTimeCounter = 0 ;
		}

		if(gameMinutes > 59)
		{
			gameHours +=1;
			gameMinutes = 0;
		}

		dominoGameTimeLabel.text = gameHours.ToString()+" : "+gameMinutes.ToString()+" : "+gameSeconds.ToString();
	}


	void OnDestroy ()
	{
        m_UNETDomino.instance.saveGameTime(gameTimeCounter, gameSeconds, gameMinutes, gameHours);
	}


	void OnDisable ()
	{
        m_UNETDomino.instance.saveGameTime(gameTimeCounter, gameSeconds, gameMinutes, gameHours);
	}




}
