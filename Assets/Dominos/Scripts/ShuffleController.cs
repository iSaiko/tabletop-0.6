﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShuffleController : MonoBehaviour {


	public static ShuffleController instance ;

	public GameObject stockPieceUiGrid;
	public GameObject stockPieceUiText;

	public GameObject handPiecesPanel;
	public GameObject dummyPiecesPanel;

	public Animator shuffleAnimator;

	public Animator player1PiecesAnimator;
	public Animator player2PiecesAnimator;
	public AudioClip drawSound;


	private Vector3 initialPosition;


	void Awake()
	{
		instance = this;

	}

	void Start () 
	{
//		transform.position = new Vector3 (1000f,1000f,1000f) ;
	}

	public void hideStockPiecesUiTiles()
	{
		handPiecesPanel.SetActive(false);
		dummyPiecesPanel.SetActive(false);
		stockPieceUiGrid.SetActive(false);
		stockPieceUiText.SetActive(false);
	}

	void showStockPiecesUiTiles()
	{
		stockPieceUiGrid.SetActive(true);
		stockPieceUiText.SetActive(true);
		handPiecesPanel.SetActive(true);
		dummyPiecesPanel.SetActive(true);
		player2PiecesAnimator.SetTrigger("slidePlayer2HandAtStart");
		player1PiecesAnimator.SetTrigger("slidePlayer1HandAtStart");


		Destroy(this.gameObject,1f);
	}

	public void startShuffling()
	{
//		print("we got to the startSuffling function");
		transform.localPosition = new Vector3 (-0.73f,1.055945f,1.280079f);
		transform.localRotation = Quaternion.Euler(0f,36.562f,0f);
//		print("the position of the shuffle pieces should  be modified");
		shuffleAnimator.SetTrigger("shuffle");
	}


	public void playDrawSound()
	{
		SoundsManager.instance.playDominoDrawSound();
	}




}
