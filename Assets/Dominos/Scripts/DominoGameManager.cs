﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GooglePlayGames;

public class DominoGameManager : MonoBehaviour {

	public static DominoGameManager instance;

	public DominoPlayer player1;
	public DominoPlayer player2;


	public GameObject stockPiecePanel;
	public GameObject UiStockPiece;


	public int roundScore;


	public GameObject roundEndPanel;
	public GameObject dominoGameEndCanvas;

	public GameObject player1Panel;
	public GameObject player2Panel;

	public Animator continuePlayPanelAnimator;


	void Awake()
	{
		instance = this;
	}

	void Start ()
	{
        //assign game score here from database
	}
		
	public void decideStartTurn()
	{
		//print("player1.isHost  ?  :" + player1.isHost.ToString());
		//print("isPiecesDistriputed.isHost  ?  :" +  PiecesManager.instance.isPiecesDistriputed.ToString());
		if (PiecesManager.instance.isPiecesDistriputed) 
		{
			int biggestValue1 = 0; 
			int biggestValue2 = 0; 
			
			for (int i = 0; i < player1.playerPieces.Count; i++) 
			{
				if (player1.playerPieces [i].upperValue == player1.playerPieces [i].lowerValue) {
					biggestValue1 = (player1.playerPieces [i].upperValue > biggestValue1) ? player1.playerPieces [i].upperValue : biggestValue1;
				}
			}
			
			for (int i = 0; i < player2.playerPieces.Count; i++) 
			{
				if (player2.playerPieces [i].upperValue == player2.playerPieces [i].lowerValue) {
					biggestValue2 = (player2.playerPieces [i].upperValue > biggestValue2) ? player2.playerPieces [i].upperValue : biggestValue2;
				}
			}
		
			if (biggestValue1 == biggestValue2) 
			{
				Debug.Log("switching server");
				PiecesManager.instance.SendTurn("CTURN|",1);
			}
			if( biggestValue1 > biggestValue2 ) 
			{
				PiecesManager.instance.SendTurn("CTURN|",1);
			}
			else if(biggestValue2 > biggestValue1)
			{
				PiecesManager.instance.SendTurn("CTURN|",2);
			}


		}

	}



	public void playBiggestPiece()
	{
		int biggestValue1 = 0 ; 

		for(int i = 0 ; i <player1.playerPieces.Count; i++)
		{
			if(player1.playerPieces[i].upperValue == player1.playerPieces[i].lowerValue)
			{
				biggestValue1 = (player1.playerPieces[i].upperValue > biggestValue1 ) ? player1.playerPieces[i].upperValue : biggestValue1 ;
			}
		}

		if(biggestValue1 == 0 )
		{
			for(int i = 0 ; i <player1.playerPieces.Count; i++)
			{
					player1.playerPieces[i].isFeasibleToPlay = true;
			}
		}
		else
		{
			for(int i = 0 ; i <player1.playerPieces.Count; i++)
			{
				if( biggestValue1 == player1.playerPieces[i].upperValue   &&  player1.playerPieces[i].upperValue == player1.playerPieces[i].lowerValue)
				{
					player1.playerPieces[i].setFeasibleToPlay();
					player1.playerPieces[0].isFeasibleToPlay = true;

				}
			}
		}
			
	}


	public void checkFeasibleMoves()
	{
		int feasiblePiecesCount = 0 ;
		int end1Number = DominoGround.instance.end1Number ;
		int end2Number = DominoGround.instance.end2Number ;

		for(int i = 0 ; i <player1.playerPieces.Count; i++)
		{
			if(player1.playerPieces[i].upperValue == end1Number || player1.playerPieces[i].lowerValue == end1Number  || player1.playerPieces[i].upperValue == end2Number || player1.playerPieces[i].lowerValue == end2Number)
			{
				player1.playerPieces[i].isFeasibleToPlay = true ;
				feasiblePiecesCount++;
			}
			else
			{
				player1.playerPieces[i].isFeasibleToPlay = false ;
			}
		}
			
		if(feasiblePiecesCount == 0)
		{
			if(IsGameLocked())
			{
				// send that the round ended and tell every one to calcuate score;
				PiecesManager.instance.SendRoundScore("CSCORE|",0,-1);			
			}
			else
			{
				if(PiecesManager.instance.stockPieces.Count != 0)
				{
					showStockPiecesAPanel();
				}
				else 
				{
					PiecesManager.instance.SendSwitchTurnAfterNoStockPieces();
//					SNP();
				}
			}
		}
		else
		{
			hideStockPiecePanel();
		}
	}


	public DominoPiece getPieceByID(int pieceID)
	{

		for(int i = 0 ; i < player1.playerPieces.Count ; i++)
		{
			if(pieceID == player1.playerPieces[i].PieceID)
			{
				return player1.playerPieces[i] ;
			}
		}

		for(int i = 0 ; i < player2.playerPieces.Count ; i++)
		{
			if(pieceID == player2.playerPieces[i].PieceID)
			{
				return player2.playerPieces[i] ;
			}
		}

		for(int i = 0 ; i < PiecesManager.instance.stockPieces.Count ; i++)
		{
			if(pieceID == PiecesManager.instance.stockPieces[i].PieceID)
			{
				return PiecesManager.instance.stockPieces[i];
			}
		}

		return new DominoPiece();

	}

	public void destroyPieceByID(int pieceID)
	{

		for(int i = 0 ; i < player1.playerPieces.Count ; i++)
		{
			if(pieceID == player1.playerPieces[i].PieceID)
			{
				player1.playerPieces.RemoveAt(i) ;
			}
		}

		for(int i = 0 ; i < player2.playerPieces.Count ; i++)
		{
			if(pieceID == player2.playerPieces[i].PieceID)
			{
				player2.playerPieces.RemoveAt(i) ;
			}
		}

		for(int i = 0 ; i < PiecesManager.instance.stockPieces.Count ; i++)
		{
			if(pieceID == PiecesManager.instance.stockPieces[i].PieceID)
			{
				PiecesManager.instance.stockPieces.RemoveAt(i) ;
			}
		}

	}



	public void switchTurns(bool isHostTurn)
	{
		print("first switchTruns function activated");
		if(player1.isHost && isHostTurn)
		{
			print("player1.isHost ?  : "  + player1.isHost.ToString());
			print("isHostTurn ?  : "  + isHostTurn.ToString());
			player1.isMyTurn = true;
			player2.isMyTurn = false;
		}
		else if(player1.isHost && !isHostTurn)
		{
			print("player1.isHost ?  : "  + player1.isHost.ToString());
			print("isHostTurn ?  : "  + isHostTurn.ToString());
			player1.isMyTurn = false;
			player2.isMyTurn = true;
		}
		else if (!player1.isHost && isHostTurn)
		{
			print("player1.isHost ?  : "  + player1.isHost.ToString());
			print("isHostTurn ?  : "  + isHostTurn.ToString());
			player1.isMyTurn = false;
			player2.isMyTurn = true;
		}
		else if (!player1.isHost && !isHostTurn)
		{
			print("player1.isHost ?  : "  + player1.isHost.ToString());
			print("isHostTurn ?  : "  + isHostTurn.ToString());
			player1.isMyTurn = true ;
			player2.isMyTurn = false;
		}
	}


	public void switchTurns()
	{
		player1.isMyTurn = !player1.isMyTurn;
		player2.isMyTurn = !player2.isMyTurn;
		if(player1.isMyTurn)
		{
			checkFeasibleMoves();
		}
		highlightPlayerTurn(player1.isMyTurn);
	}



	public void highlightPlayerTurn(bool isMyTurn)
	{
		if(isMyTurn)
		{
			Destroy(player2Panel.transform.GetChild(0).GetComponent<Outline>());
//			Destroy(player2Panel.transform.GetChild(0).GetComponent<Shadow>());
			player1Panel.transform.GetChild(1).gameObject.SetActive(true);
			player2Panel.transform.GetChild(1).gameObject.SetActive(false);
			Outline tempOutline =  player1Panel.transform.GetChild(0).gameObject.AddComponent<Outline>() ;
			tempOutline.effectDistance = new Vector2 (3,3);
			tempOutline.effectColor = Color.black;
			tempOutline.useGraphicAlpha = false;
//			Shadow tempShadow =  player1Panel.transform.GetChild(0).gameObject.AddComponent<Shadow>() ;
//			tempShadow.effectDistance = new Vector2 (3,3);
//			tempShadow.effectColor = Color.gray;
		}
		else
		{
			Destroy(player1Panel.transform.GetChild(0).GetComponent<Outline>());
//			Destroy(player1Panel.transform.GetChild(0).GetComponent<Shadow>());

			player1Panel.transform.GetChild(1).gameObject.SetActive(false);
			player2Panel.transform.GetChild(1).gameObject.SetActive(true);
			Outline tempOutline =  player2Panel.transform.GetChild(0).gameObject.AddComponent<Outline>() ;
			tempOutline.effectDistance = new Vector2 (3,3);
			tempOutline.effectColor = Color.black;
			tempOutline.useGraphicAlpha = false;

//			Shadow tempShadow =  player1Panel.transform.GetChild(0).gameObject.AddComponent<Shadow>() ;
//			tempShadow.effectDistance = new Vector2 (3,3);
//			tempShadow.effectColor = Color.gray;


		}

	}

	public void showStockPiecesAPanel()
	{
		stockPiecePanel.SetActive(true);

		for(int i  = 0 ; i < stockPiecePanel.transform.childCount ; i++)
		{
			Destroy(stockPiecePanel.transform.GetChild(i).gameObject);
		}

		for(int i = 0 ; i < PiecesManager.instance.stockPieces.Count ; i++)
		{
			GameObject temp	 = Instantiate(UiStockPiece,stockPiecePanel.transform);
			temp.transform.localScale = new Vector3 (1,1,1);
			temp.transform.localRotation = Quaternion.Euler(0,0,0);
			temp.transform.localPosition  = new Vector3 (0,0,0);
		}
	}
	public void hideStockPiecePanel()
	{
		stockPiecePanel.SetActive(false);
		for(int i  = 0 ; i < stockPiecePanel.transform.childCount ; i++)
		{
			Destroy(stockPiecePanel.transform.GetChild(i).gameObject);
		}
	}

	public void destroyLastPieceInStock()
	{
		Destroy(stockPiecePanel.transform.GetChild(stockPiecePanel.transform.childCount -1).gameObject);
	}



	public void checkRoundWinning()
	{
		if(player1.playerPieces.Count <= 0)
		{
			if(player1.isHost)
			{
				PiecesManager.instance.SendRoundScore("CSCORE|",1,-1);
			}
			else
			{
				PiecesManager.instance.SendRoundScore("CSCORE|",2,-1);
			}
		}
		else if(player2.playerPieces.Count <= 0){
			
		}
		else
		{
			switchTurns();
		}

	}


	public void assignScore()
	{

		int enemyScore = 0 ;

		for(int i = 0 ; i < player2.playerPieces.Count ; i++)
		{
			roundScore += player2.playerPieces[i].upperValue;
			roundScore += player2.playerPieces[i].lowerValue;
		}

		for(int j = 0 ; j < player1.playerPieces.Count ; j++)
		{
			enemyScore += player1.playerPieces[j].upperValue;
			enemyScore += player1.playerPieces[j].lowerValue;
		}

		if(player1.isHost)
		{
			m_UNETDomino.instance.hostScore += roundScore;
			m_UNETDomino.instance.clientScore += enemyScore;

		}
		else
		{
			m_UNETDomino.instance.clientScore += roundScore;
			m_UNETDomino.instance.hostScore += enemyScore;

		}

		m_UNETDomino.instance.player1roundScores.Add(roundScore);
		m_UNETDomino.instance.player2roundScores.Add(enemyScore);


		roundScore = 0 ;
		checkWinner();
		Player2Hand.instance.replaceAllDummies();

	}


    public void checkWinner()
    {
		print("Total game score is : " + m_UNETDomino.instance.GameScore) ;
		
		if(m_UNETDomino.instance.hostScore >= m_UNETDomino.instance.GameScore)
        {
			//host is a winner 
//			PiecesManager.instance.SendDominoEndGameStates("CEND",
//			announceDominoGameWinner(m_UNETDomino.instance.clientName);

			if(player1.isHost)
			{
				announceDominoGameWinner(true); //m_UNETDomino.instance.clientName
			}
			else
			{
				announceDominoGameWinner(true);
			}
        }
		else if(m_UNETDomino.instance.clientScore >= m_UNETDomino.instance.GameScore)
		{
			if(player1.isHost)
			{
				announceDominoGameWinner(false);
			}
			else
			{
				announceDominoGameWinner(false); // m_UNETDomino.instance.clientName
			}
		}
        else
        {
//            AnotherRound();
			Player2Hand.instance.replaceAllDummies();
        }
    }

    public void AnotherRound()
    {
		print ("we go to another round method");
		m_UNETDomino.instance.roundNumber ++;
		m_UNETDomino.instance.reloadDominoScene();

//		for(int i = 0 ; i < player1.playerPieces.Count ; i++)
//		{
//			Destroy(player1.playerPieces[i].gameObject);
//		}
//
//		for(int i = 0 ; i < player2.playerPieces.Count ; i++)
//		{
//			Destroy(player2.playerPieces[i].gameObject);
//		}
//
//		for(int i = 0 ; i < PiecesManager.instance.stockPieces.Count ; i++)
//		{
//			Destroy(PiecesManager.instance.stockPieces[i].gameObject);
//		}
//
//		player1.playerPieces.Clear();
//		player2.playerPieces.Clear();
//		PiecesManager.instance.stockPieces.Clear();
//		PiecesManager.instance.allPiecesServer.Clear();
//		roundEndPanel.SetActive(false);
//		PiecesManager.instance.DestributPieces();

    }



	public bool IsGameLocked()
	{
		int end1 = DominoGround.instance.end1Number;
		int end2 = DominoGround.instance.end2Number;

		for(int i = 0 ; i < player1.playerPieces.Count ; i++)
		{
			if(end1 == player1.playerPieces[i].upperValue || end1 == player1.playerPieces[i].lowerValue  )
			{
				return false;
			}
			if (end2 == player1.playerPieces[i].upperValue || end2 == player1.playerPieces[i].lowerValue )
			{
				return false;
			}

		}
		for(int i = 0 ; i < player2.playerPieces.Count ; i++)
		{
			if(end1 == player2.playerPieces[i].upperValue || end1 == player2.playerPieces[i].lowerValue  )
			{
				return false;
			}
			if (end2 == player2.playerPieces[i].upperValue || end2 == player2.playerPieces[i].lowerValue )
			{
				return false;
			}

		}
		for(int i = 0 ; i < PiecesManager.instance.stockPieces.Count ; i++)
		{
			if(end1 == PiecesManager.instance.stockPieces[i].upperValue || end1 ==  PiecesManager.instance.stockPieces[i].lowerValue  )
			{
				return false;
			}
			if (end2 == PiecesManager.instance.stockPieces[i].upperValue || end2 == PiecesManager.instance.stockPieces[i].lowerValue )
			{
				return false;
			}

		}
		return true;
	}

//
//	public void SNP()
//	{
//		print("inside  SSNP Case");
//		switchTurns();
//		hideStockPiecePanel();
//		if(DominoGameManager.instance.player1.isMyTurn)
//		{
//			print("show continue play panel should be excuted");
//			DominoGameManager.instance.showContinuePlayPanel();
//		}
//	}

	public void showContinuePlayPanel()
	{
		print("inside showContinuePlayPanel");
		continuePlayPanelAnimator.SetTrigger("play");
	}

	public void autoZoom()
	{
		Camera.main.GetComponent<PinchZoom>().autoZoom();
	}


	public void muteSound()
	{
		
	}
	public void muteMusic()
	{
		
	}



	public void announceDominoGameWinner(bool isHostWhoWon)
	{
		dominoGameEndCanvas.SetActive(true);
		if(isHostWhoWon && player1.isHost)
		{
			dominoGameEndCanvas.GetComponentInChildren<Text>().text = "You Have Won The Game with Score of : " + m_UNETDomino.instance.hostScore.ToString() ;
			Rooms.instance.EndGameRoom(Login.instance.activeRoomId,Login.instance.successfulLoginUser.data.user.id);
            Social.ReportProgress(TableTopPlayServiceResources.achievement_dominood, 100.0f, (bool success) =>
            {
                Debug.Log("Achievment Successfully Unlocked");
            });

            PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_domironze, 2, (bool success) =>
            {
                Debug.Log("Achievment Successfully Unlocked");
            });
            PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_domilver, 2, (bool success) =>
            {
                Debug.Log("Achievment Successfully Unlocked");
            });
            PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_domino, 2, (bool success) =>
            {
                Debug.Log("Achievment Successfully Unlocked");
            });
		}
		else if(!isHostWhoWon && !player1.isHost)
		{
			dominoGameEndCanvas.GetComponentInChildren<Text>().text = "You have won the game with score of : " + m_UNETDomino.instance.clientScore.ToString() ;
			Rooms.instance.EndGameRoom(Login.instance.activeRoomId,Login.instance.successfulLoginUser.data.user.id);
            Social.ReportProgress(TableTopPlayServiceResources.achievement_dominood, 100.0f, (bool success) =>
            {
                Debug.Log("Achievment Successfully Unlocked");
            });

            PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_domironze, 2, (bool success) =>
            {
                Debug.Log("Achievment Successfully Unlocked");
            });
            PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_domilver, 2, (bool success) =>
            {
                Debug.Log("Achievment Successfully Unlocked");
            });
            PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_domino, 2, (bool success) =>
            {
                Debug.Log("Achievment Successfully Unlocked");
            });
		}
		else 
		{
			dominoGameEndCanvas.GetComponentInChildren<Text>().text = "You have lost The game with score of : " + ((player1.isHost) ? m_UNETDomino.instance.hostScore.ToString() : m_UNETDomino.instance.clientScore.ToString()) ;
		}
		StartCoroutine(afterAnnouncingDominoGameWinner());
	}


	public IEnumerator otherDominoPlayerSurrendered()
	{

		dominoGameEndCanvas.SetActive(true);
		dominoGameEndCanvas.GetComponentInChildren<Text>().text = " You have won the game \n the other player has surrendered " ;
		Rooms.instance.EndGameRoom(Login.instance.activeRoomId,Login.instance.successfulLoginUser.data.user.id);
        Social.ReportProgress(TableTopPlayServiceResources.achievement_dominood, 100.0f, (bool success) =>
        {
            Debug.Log("Achievment Successfully Unlocked");
        });

        PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_domironze, 2, (bool success) =>
        {
            Debug.Log("Achievment Successfully Unlocked");
        });
        PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_domilver, 2, (bool success) =>
        {
            Debug.Log("Achievment Successfully Unlocked");
        });
        PlayGamesPlatform.Instance.IncrementAchievement(TableTopPlayServiceResources.achievement_domino, 2, (bool success) =>
        {
            Debug.Log("Achievment Successfully Unlocked");
        });
		yield return new WaitForSeconds(1f);
		StartCoroutine(afterAnnouncingDominoGameWinner());
	}

	public void iSurrender()
	{
		dominoGameEndCanvas.SetActive(true);
		dominoGameEndCanvas.GetComponentInChildren<Text>().text = "You have lost the game" ;
		StartCoroutine(afterAnnouncingDominoGameWinner());
	}



	IEnumerator afterAnnouncingDominoGameWinner()
	{
		m_UNETGameManager go = GameObject.FindObjectOfType<m_UNETGameManager>();
		m_UNETDomino go2 = GameObject.FindObjectOfType<m_UNETDomino>();
		go2.ShutDownConnection();
		yield return new WaitForSeconds(3f);
		//dominoGameEndCanvas.SetActive(false);
		Destroy(Rooms.instance.gameObject);
		Destroy(go.gameObject);
		Destroy(go2.gameObject);
		Resources.UnloadUnusedAssets();
		SceneManager.LoadScene("main_menu_scene");
	}

   


}

	







