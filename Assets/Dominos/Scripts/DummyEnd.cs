﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyEnd : MonoBehaviour {

	private int direction;
	private bool isEnd1;

	public Transform upperPointAnchor;
	public Transform lowerPointAnchor;


	public void moveSelectedPiece(DominoPiece selectedPiece)
	{
		print(direction);
		switch (direction) {

		case Constants.moveLeft:
			PiecesManager.instance.SendDominoMovementDataToServer("CMOV|",selectedPiece.PieceID.ToString(),Constants.moveLeft,(isEnd1) ?  1 : 2  , (DominoGameManager.instance.player1.isHost) ? 1 : 2);

			break;

		case Constants.moveRight:
			PiecesManager.instance.SendDominoMovementDataToServer("CMOV|",selectedPiece.PieceID.ToString(),Constants.moveRight,(isEnd1) ?  1 : 2 , (DominoGameManager.instance.player1.isHost) ? 1 : 2 );

			break;

		case Constants.moveUp:
			PiecesManager.instance.SendDominoMovementDataToServer("CMOV|",selectedPiece.PieceID.ToString(),Constants.moveUp,(isEnd1) ?  1 : 2 , (DominoGameManager.instance.player1.isHost) ? 1 : 2 );

			break;

		case Constants.moveDown:
			PiecesManager.instance.SendDominoMovementDataToServer("CMOV|",selectedPiece.PieceID.ToString(),Constants.moveDown,(isEnd1) ?  1 : 2 , (DominoGameManager.instance.player1.isHost) ? 1 : 2 );

			break;
		}


		// check moving in both ends again
		///  here
	}

	public void setDummyValues(int direction , bool isEnd1)
	{
		this.direction = direction;
		this.isEnd1 = isEnd1;
	}

	public void putDummyLeft(Vector3 anchorPostion)
	{
		transform.position = new Vector3 (0,0,0);
		transform.rotation = Quaternion.Euler(180f,0,0);
		transform.position =  anchorPostion + upperPointAnchor.position ;
	}
	public void putDummyUp(Vector3 anchorPostion)
	{
		transform.position = new Vector3 (0,0,0);
		transform.rotation = Quaternion.Euler(180f,90f,0);
		transform.position =  anchorPostion + upperPointAnchor.position ;
	}
	public void putDummyRight(Vector3 anchorPostion)
	{
		transform.position = new Vector3 (0,0,0);
		transform.rotation = Quaternion.Euler(180f,0,0);
		transform.position =  anchorPostion - upperPointAnchor.position ;
	}
	public void putDummyDown(Vector3 anchorPostion)
	{
		transform.position = new Vector3 (0,0,0);
		transform.rotation = Quaternion.Euler(180f,90f,0);
		transform.position =  anchorPostion - upperPointAnchor.position ;
	}







}
