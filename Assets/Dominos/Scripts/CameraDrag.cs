﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDrag : MonoBehaviour {


	public bool isDragging = true;
	public bool startDrag ;

	public Vector3 oldPos;
	public Vector3 dragOrigin;
	public float dragSpeed ;


	private float touchTime;
	private float timeToStartDrag = 0.1f; 
	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () 
	{
//		#if UNITY_STANDALONE || UNITY_EDITOR || UNITY_STANDALONE_WIN

		if(startDrag  && !DominoGround.instance.isDraggingPiece)  
		{
			if(Input.GetMouseButton(0))
			{
				if(isDragging )
				{
					oldPos = transform.position;
					dragOrigin = Camera.main.ScreenToViewportPoint(Input.mousePosition);   //Get the ScreenVector the mouse clicked
				}

				if(!isDragging)
				{
					Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition) - dragOrigin;    //Get the difference between where the mouse clicked and where it moved
					float xPos = Mathf.Clamp(oldPos.x + (-pos.x * dragSpeed),-8.5f,8.5f);
					float zPos = Mathf.Clamp(oldPos.z + (-pos.y * dragSpeed),-13.5f,17.5f);
					transform.position = new Vector3 (xPos,transform.position.y,zPos);   //Move the position of the camera to simulate a drag, speed * 10 for screen to worldspace conversion
				}

				isDragging = !isDragging;
			}
		}

		if (!startDrag && Input.GetMouseButtonDown(0))
		{
			startDrag = true;
			isDragging = true;
		}

		if(Input.GetMouseButtonUp(0))
		{
			startDrag = false;
		}
//		#else
//		startDrag = true ;
//		if(Input.touchCount == 1)
//		{
//			
//			switch (Input.GetTouch(0).phase) 
//			{
//			case TouchPhase.Began:
//				
//				if(startDrag)
//				{
//					
//						if(isDragging )
//						{
//							oldPos = transform.position;
//							dragOrigin = Camera.main.ScreenToViewportPoint(Input.GetTouch(0).position);   //Get the ScreenVector the mouse clicked
//						}
//
//						if(!isDragging)
//						{
//							Vector3 pos = Camera.main.ScreenToViewportPoint(Input.GetTouch(0).position) - dragOrigin;    //Get the difference between where the mouse clicked and where it moved
//							float xPos = Mathf.Clamp(oldPos.x + (-pos.x * dragSpeed),-8.5f,8.5f);
//							float zPos = Mathf.Clamp(oldPos.z + (-pos.y * dragSpeed),-13.5f,17.5f);
//							transform.position = new Vector3 (xPos,transform.position.y,zPos);   //Move the position of the camera to simulate a drag, speed * 10 for screen to worldspace conversion
//						}
//
//						isDragging = !isDragging;
//
//				}
//
//				else if(!startDrag)
//				{
//					touchTime += Time.deltaTime ;
//					if(touchTime > timeToStartDrag)
//					{
//						startDrag = true;
//						isDragging = true;
//						touchTime = 0 ;
//					}
//				}
//
//
//
//				break;
//
//			case TouchPhase.Ended :
////				startDrag = false;
//				isDragging = true;
//				touchTime = 0 ;
//				break;
//			}



		

////				if (!startDrag && Input.GetMouseButtonDown(0))
////				{
////					startDrag = true;
////					isDragging = true;
////				}
//
//
//		}







//		#endif


	

	

	}





}
