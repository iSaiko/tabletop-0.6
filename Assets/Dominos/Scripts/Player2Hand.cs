﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player2Hand : MonoBehaviour {

	public static Player2Hand instance;


	public GridLayoutGroup handPiecesLayout ;
	public int topPadding;
	public int horizontalOffsit; 

	public GameObject[] halfsPrefabs;

	private  bool isAnimationPlaying;

	void Awake() 
	{
		instance =  this;
	}
	void Update ()
	{
		if(!isAnimationPlaying)
		{
			RectOffset rect = new RectOffset(horizontalOffsit - 115*transform.childCount ,0,topPadding,0);
			handPiecesLayout.padding = rect ;
		}
	}

	public void replaceAllDummies()
	{
		Player2Piece[] dummies = GetComponentsInChildren<Player2Piece>();
		for(int i = 0 ; i < DominoGameManager.instance.player2.playerPieces.Count ; i++)
		{
			dummies[i].replaceDummyWithTrue(halfsPrefabs[DominoGameManager.instance.player2.playerPieces[i].upperValue],halfsPrefabs[DominoGameManager.instance.player2.playerPieces[i].lowerValue]);
		}

		GetComponent<Animator>().SetTrigger("slidePlayer2Hand");
	}

	public void flipAllPlayer2Pieces()
	{
		if(DominoGameManager.instance.player2.playerPieces.Count > 0)
		{
			Player2Piece[] dummies = GetComponentsInChildren<Player2Piece>();
			for(int i = 0 ; i < DominoGameManager.instance.player2.playerPieces.Count ; i++)
			{
				dummies[i].flip();
			}
		}
		else
		{
			DominoRoundEndController.instance.gameObject.SetActive(true);
			DominoRoundEndController.instance.GetComponent<Image>().fillAmount = 1;
			DominoRoundEndController.instance.tellPlayerHeLost();
		}

		StartCoroutine(waitAndStartNewRound());

	}

	IEnumerator waitAndStartNewRound()
	{
		yield return new WaitForSeconds(5f);
		DominoGameManager.instance.AnotherRound();

	}


}
