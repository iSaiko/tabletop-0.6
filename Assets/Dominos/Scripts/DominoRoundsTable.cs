﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DominoRoundsTable : MonoBehaviour {


	public GameObject roundStats;
	public RectTransform rectTransform;
	public Text player1TotalLabel;
	public Text player2TotalLabel;


	void Awake()
	{
		m_UNETDomino.instance.dominoRoundTable = this;
	}

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void CleanRoundStatsTable()
	{
		for(int i = 0 ; i < transform.childCount ; i++)
		{
			Destroy(transform.GetChild(i).gameObject);
		}
	}

	public void addRoundStats(int RoundNumber, int player1RoundScore, int player2RoundScore)
	{
		
		GameObject temp =  Instantiate(roundStats,this.transform);
		temp.transform.localScale = new Vector3 (1,1,1);
		temp.transform.localPosition = new Vector3 (0,0,0);		
		temp.transform.localRotation = Quaternion.Euler(0,0,0);


		rectTransform.sizeDelta = new Vector2 (0 ,transform.childCount * transform.GetChild(0).GetComponent<RectTransform>().rect.height); //rectTransform.rect.width
//			GetComponent<RectTransform>().rect = transform.childCount * transform.GetChild(0).GetComponent<RectTransform>().rect.height;


		Text[] labels = temp.GetComponentsInChildren<Text>();
		labels[0].text = RoundNumber.ToString();
		labels[1].text = (player1RoundScore  == -1 ) ? "-" : player1RoundScore.ToString();
		labels[2].text = (player2RoundScore  == -1 ) ? "-" : player2RoundScore.ToString();

		player1TotalLabel.text = (m_UNETDomino.instance.isHost) ? m_UNETDomino.instance.hostScore.ToString() : m_UNETDomino.instance.clientScore.ToString() ;
		player2TotalLabel.text = (m_UNETDomino.instance.isHost) ? m_UNETDomino.instance.clientScore.ToString()  : m_UNETDomino.instance.hostScore.ToString() ;
	}


}
