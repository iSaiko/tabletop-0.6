﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DominoRoundEndController : MonoBehaviour {

	public Image backGround;
	private bool isFaddingToNewRound;
	private float faddingTime;
	private float timer;

	public static DominoRoundEndController instance; 
	void Awake()
	{
        m_UNETDomino.instance.dominoRoundEndController = this;
		gameObject.SetActive(false);
		instance = this;


	}


	void FixedUpdate () 
	{
		if(isFaddingToNewRound)
		{
			timer += Time.fixedDeltaTime;
			backGround.fillAmount = Mathf.Lerp(1,0,(timer/faddingTime));

			if(backGround.fillAmount == 0 )
			{
				isFaddingToNewRound = false;
				gameObject.SetActive(false);
//				ShuffleController.instance.startShuffling();
				ShuffleController.instance.hideStockPiecesUiTiles();
				ShuffleController.instance.startShuffling();
			}

		}
			
	}

	IEnumerator fadeToNewRound(float faddingTime)
	{
		yield return new WaitForSeconds(2f);
		hideRoundData();
		this.faddingTime = faddingTime ;
		this.timer = 0 ;
		backGround.fillAmount = 1f;
		isFaddingToNewRound = true ;


	}



	public void showRoundData(int roundScore)
	{
		backGround.fillAmount = 1 ;
		gameObject.SetActive(true);

		if(roundScore != 0)
		{
			gameObject.transform.GetChild(0).gameObject.SetActive(true);
			gameObject.transform.GetChild(1).gameObject.SetActive(false);
			gameObject.transform.GetChild(2).gameObject.SetActive(true);
			gameObject.transform.GetChild(3).gameObject.SetActive(true);
			gameObject.transform.GetChild(3).GetComponent<Text>().text = "";
			gameObject.transform.GetChild(3).GetComponent<Text>().text = roundScore.ToString();
		}
		else
		{
//			gameObject.transform.GetChild(0).gameObject.SetActive(false);
//			gameObject.transform.GetChild(1).gameObject.SetActive(true);
//			gameObject.transform.GetChild(2).gameObject.SetActive(false);
//			gameObject.transform.GetChild(3).gameObject.SetActive(false);
			gameObject.transform.GetChild(0).gameObject.SetActive(true);
			gameObject.transform.GetChild(1).gameObject.SetActive(false);
			gameObject.transform.GetChild(2).gameObject.SetActive(true);
			gameObject.transform.GetChild(3).gameObject.SetActive(true);
			gameObject.transform.GetChild(3).GetComponent<Text>().text = "";
			gameObject.transform.GetChild(3).GetComponent<Text>().text = roundScore.ToString();
		}

		StartCoroutine(fadeToNewRound(2f));
	}



	public void hideRoundData()
	{
		gameObject.transform.GetChild(0).gameObject.SetActive(false);
		gameObject.transform.GetChild(1).gameObject.SetActive(false);
		gameObject.transform.GetChild(2).gameObject.SetActive(false);
		gameObject.transform.GetChild(3).gameObject.SetActive(false);
		gameObject.transform.GetChild(4).gameObject.SetActive(false);

	}

	public void tellPlayerHeLost()
	{
		{
			gameObject.transform.GetChild(0).gameObject.SetActive(false);
			gameObject.transform.GetChild(1).gameObject.SetActive(true);
			gameObject.transform.GetChild(2).gameObject.SetActive(false);
			gameObject.transform.GetChild(3).gameObject.SetActive(false);
		}
	}



}
