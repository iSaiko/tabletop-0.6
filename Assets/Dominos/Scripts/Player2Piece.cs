﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2Piece : MonoBehaviour {

	bool flipIsDone = false;
	bool flipIsStarted = false;
//	float tempTime = 0 ;
//	float flipDuration ;
//	Quaternion nextRotation;
//	Quaternion oldRotation;
//

	public void replaceDummyWithTrue(GameObject upperHalfprefab,GameObject lowerHalfprefab)
	{
		GameObject upper =  transform.GetChild(1).gameObject;
		GameObject lower =  transform.GetChild(2).gameObject;

		GameObject upperHalf = Instantiate(upperHalfprefab,this.transform);
		upperHalf.transform.localPosition = new Vector3 (0f,0f,0f);
		upperHalf.transform.localRotation = Quaternion.Euler(0f,0f,0f);
		upperHalf.transform.localScale = new Vector3 (1f,1f,1f);
		Destroy(upper);

		GameObject lowerHalf = Instantiate(lowerHalfprefab,this.transform);
		lowerHalf.transform.localPosition = new Vector3 (4.62f,0f,0f);
		lowerHalf.transform.localRotation = Quaternion.Euler (0f,-180f,0f);
		lowerHalf.transform.localScale = new Vector3 (1f,1f,1f);
		Destroy(lower);

 	}


//	public void flip()
//	{
//
//		print("we entered the flip function");
//		
//		Animation animation  = transform.gameObject.AddComponent<Animation> ();
//		AnimationClip clip = new AnimationClip();
//		clip.legacy = true;
//
////		Quaternion nextRotation = Quaternion.Euler(0f,90f,0f) ;
//		Quaternion nextRotation = transform.localRotation  * Quaternion.Euler(0f,180f,0f) ;
//		print(transform.localRotation.y);
//		print(transform.localRotation.eulerAngles.y);
//		print(nextRotation.y);
//		print(nextRotation.eulerAngles.y);
//		print(transform.rotation.y);
//		print(transform.rotation.eulerAngles.y);
//		AnimationCurve curve = AnimationCurve.Linear(0,transform.localRotation.y,3f,nextRotation.y);
//		clip.SetCurve("", typeof(Transform), "localRotation.y", curve);
//
//		animation.AddClip(clip, "movePosition");
//		animation.Play("movePosition");
//
////		GetComponent<Animator>().SetTrigger("flipPiece");
//	}

	public void flip()
	{

//		this.flipDuration  = flipDuration ;
//		oldRotation  = transform.localRotation;
//		oldRotation =  Quaternion.Euler(0f,-90f,-90f) ;
//		nextRotation = transform.localRotation  * Quaternion.Euler(0f,180f,0f) ;
//		nextRotation =  Quaternion.Euler(0f,90f,-90f) ;
		flipIsStarted  = true ;

//		while(!flipIsDone)
//		{
//			transform.Rotate(0f,1f,0f);
////			transform.localRotation = Quaternion.Lerp(transform.localRotation,nextRotation,(tempTime/flipDuration)) ;
//	//		tempTime +=Time.deltaTime ;
//			if(transform.localRotation.Equals(nextRotation))
//			{
//				print("any");
//				flipIsDone = true;
//			}
//		}

	}

	void FixedUpdate()
	{
		if(flipIsStarted && !flipIsDone)
		{
//			print("we are rotating in Update function");
//			print(tempTime);
//			print((tempTime/flipDuration));
//			print(oldRotation.eulerAngles.x + " "+oldRotation.eulerAngles.y + " "+oldRotation.eulerAngles.y);
//			print(nextRotation.eulerAngles.x + " "+nextRotation.eulerAngles.y + " "+nextRotation.eulerAngles.y);
//			print(Quaternion.Lerp(oldRotation,nextRotation,(tempTime/flipDuration)));
//			transform.localRotation = Quaternion.Lerp(oldRotation,nextRotation,(tempTime/flipDuration)) ;
//			tempTime +=Time.fixedDeltaTime;
//			print(tempTime);

			transform.Rotate(4.5f,0f,0f);
			if(transform.localRotation.eulerAngles.y <= 90)
			{
				flipIsDone = true;
			}
		}



	}



//
//	void OnMouseDown()
//	{
//		transform.Rotate(10f,0f,0f);
//		print(transform.localRotation.eulerAngles.x + " "+transform.localRotation.eulerAngles.y + " "+transform.localRotation.eulerAngles.y);
//		print("the piece should rotate");
//	}


}
