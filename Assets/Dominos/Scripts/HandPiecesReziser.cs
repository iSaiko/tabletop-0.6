﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandPiecesReziser : MonoBehaviour {

	public GridLayoutGroup handPiecesLayout ;
	public int topPadding;
	public int horizontalOffsit; 
	public int scrollOffsit; 
	public int piecesNumberThreshold;
	public GameObject HorizontalScrollButtons;
	public Button mybutton;


	private bool isScrolling;
	private bool isScrollingRight;
	private bool isScrollingLeft;
	void Start () 
	{
//		mybutton.OnPointerDown();
	}
	void Update ()
	{
		if(transform.childCount >= piecesNumberThreshold)
		{
			if(isScrolling)
				return;
			
			HorizontalScrollButtons.SetActive(true);
			scrollOffsit = horizontalOffsit;
			isScrolling = true ;
			return;
		
		}
		else
		{
			HorizontalScrollButtons.SetActive(false);
//			horizontalOffsit = scrollOffsit;
			isScrolling = false ;
		}


		RectOffset rect = new RectOffset(horizontalOffsit - 115*transform.childCount ,0,topPadding,0);
		handPiecesLayout.padding = rect ;
	}


	void FixedUpdate()
	{
		if(isScrollingRight)
		{
			scrollOffsit += 20 ;
			scrollOffsit = Mathf.Clamp(scrollOffsit,-1000,5000);
			RectOffset rect = new RectOffset(scrollOffsit - 115*transform.childCount ,0,topPadding,0);
			handPiecesLayout.padding = rect ;
		}
		else if(isScrollingLeft)
		{
			scrollOffsit -= 20 ;
			scrollOffsit = Mathf.Clamp(scrollOffsit,-1000,5000);
			RectOffset rect = new RectOffset(scrollOffsit - 115*transform.childCount ,0,topPadding,0);
			handPiecesLayout.padding = rect ;
		}


	}


	public void scrollRight()
	{
		isScrollingRight = true;
	}
	public void stopScrollingRight()
	{
		isScrollingRight = false;
	}
	public void scrollLeft()
	{
		isScrollingLeft = true;
	}
	public void stopScrollingLeft()
	{
		isScrollingLeft = false;
	}



}
