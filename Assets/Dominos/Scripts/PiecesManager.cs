﻿using ArabicSupport;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PiecesManager : MonoBehaviour {
				

	public static PiecesManager instance;

	public List<DominoPiece> allPieces ;
    public List<DominoPiece> allPiecesServer;
	public List<DominoPiece> stockPieces ;
    public bool isHost;
	public GameObject dominoPrefab;
	public GameObject[] dominoNumbersPrefab = new GameObject[7]; 

	public GameObject dummyPiecePrefab;
	public GameObject piecesPanel;
	public GameObject dummypiecesPanel;
	public bool isPiecesDistriputed = false;
	public Text stockPiecesCounter ;
	public GameObject stockPieceIndicationPanel;
	public GameObject stockPieceIndicationItem;


	private DominoPiece temp ;
	private GameObject tempObj;
	private GameObject upperHalf;
	private GameObject lowerHalf;

    public m_UNETDomino Client;

    public Transform chatMessageContainer;
    public GameObject messagePrefab;

	void Awake()
	{
		instance = this;
		allPieces = new List<DominoPiece>();
		stockPieces	 = new List<DominoPiece>();
		Client = GameObject.Find("UNETDominoObject").GetComponent<m_UNETDomino>();
		isHost = Client.isHost;
		DominoGameManager.instance.player1.isHost = isHost;
		DominoGameManager.instance.player2.isHost = !isHost;

//		print("Client.isHost  :   " + Client.isHost.ToString());
//		print("player1.isHost   :   " + DominoGameManager.instance.player1.isHost.ToString());
//		print("player2.isHost   :   " + DominoGameManager.instance.player2.isHost.ToString());
//
//		Invoke("generatePieces",0.001f);

	}


	void Start ()
	{
		generatePieces();
	}

//	public void assignHostOrClient()
//	{
//		isHost = Client.isHost;
//		DominoGameManager.instance.player1.isHost = isHost;
//		DominoGameManager.instance.player2.isHost = !isHost;
//	}


	public void generatePieces()
	{
		int pieceIndex = 1;
		int index = 0;
		for (int i = 0; i <= 6; i++)
		{
			for (int j = index; j <= 6; j++)
			{
				tempObj =  Instantiate(dominoPrefab) as GameObject;
				temp  = tempObj.GetComponent<DominoPiece>();

				upperHalf =  Instantiate(dominoNumbersPrefab[i]) as GameObject ;  //  dominoNumbersPrefab[i];
				upperHalf.transform.position = temp.upperTransform.position;
				upperHalf.transform.rotation = temp.upperTransform.rotation;
				upperHalf.transform.localScale = temp.upperTransform.localScale;
				temp.upperRenderer = upperHalf.GetComponent<Renderer>();
				Destroy(temp.upperTransform.gameObject);
				temp.upperTransform = null;
				upperHalf.transform.SetParent(tempObj.transform);


				lowerHalf = Instantiate(dominoNumbersPrefab[j]) as GameObject;  // dominoNumbersPrefab[j]; 
				lowerHalf.transform.position = temp.lowerTransform.position ; 
				lowerHalf.transform.rotation = temp.lowerTransform.rotation ; 
				lowerHalf.transform.localScale = temp.lowerTransform.localScale ; 
				temp.lowerRenderer = lowerHalf.GetComponent<Renderer>();
				Destroy(temp.lowerTransform.gameObject);
				temp.lowerTransform = null;
				lowerHalf.transform.SetParent(tempObj.transform);

				temp.transform.name = i.ToString() + j.ToString();
				temp.upperValue = i; 
				temp.lowerValue  = j;
				temp.PieceID = pieceIndex ;
				pieceIndex++;
				temp.transform.position = new Vector3 (-20f,100f,100f);
				allPieces.Add(temp);
                allPiecesServer.Add(temp);

			}
			index++;
		}
		if(DominoGameManager.instance.player1.isHost)
		{
//			DestributPieces();
			Invoke("DestributPieces",3.5f);
		}
        //if not host 
	}





	public void DestributPieces()
	{
        
		for(int i = 0 ; i < 7 ; i++)
		{
			int randomIndex = Random.Range(0,allPieces.Count);
			DominoGameManager.instance.player1.playerPieces.Add(allPieces[randomIndex]);
			allPieces.RemoveAt(randomIndex);
		}

		for(int i = 0 ; i < 7 ; i++)
		{
			int randomIndex = Random.Range(0,allPieces.Count);
			DominoGameManager.instance.player2.playerPieces.Add(allPieces[randomIndex]);
			allPieces.RemoveAt(randomIndex);
		}

		stockPieces = allPieces ;
//		allPieces = null ;
        string allPiecesToSend = "";
        for (int i = 0; i < DominoGameManager.instance.player1.GetComponent<DominoPlayer>().playerPieces.Count; i++)
        {
            allPiecesToSend += DominoGameManager.instance.player1.GetComponent<DominoPlayer>().playerPieces[i].PieceID + "|"; 
        }
       // Debug.Log(allPiecesToSend);
        for (int i = 0; i < DominoGameManager.instance.player2.GetComponent<DominoPlayer>().playerPieces.Count; i++)
        {
            allPiecesToSend += DominoGameManager.instance.player2.GetComponent<DominoPlayer>().playerPieces[i].PieceID + "|";
//			DominoGameManager.instance.player2.playerPieces[i].transform.position = new Vector3(100,100,100);
        }
        //Debug.Log(allPiecesToSend);
        for (int i = 0; i < stockPieces.Count; i++)
        {
//			stockPieces[i].transform.position = new Vector3(100,100,100);
            allPiecesToSend += stockPieces[i].PieceID + "|";
        }
        //Debug.Log(allPiecesToSend);

        SendDistributionDataToServer("CASN|", allPiecesToSend);
		//ShowPieces();

		isPiecesDistriputed = true;
		Invoke("invokestartturn",1.5f);
	}
	public void invokestartturn(){
		DominoGameManager.instance.decideStartTurn();
	}
    public void ShowPieces()
    {
//        if (isHost)
            showPiecesToPlayer1();
//        else
//            showPiecesToPlayer2();

    }
	public void showPiecesToPlayer1()
	{
		for(int i = 0 ; i < DominoGameManager.instance.player1.playerPieces.Count ; i++)
		{
			DominoGameManager.instance.player1.playerPieces[i].gameObject.layer = 5 ;
			DominoGameManager.instance.player1.playerPieces[i].transform.rotation = Quaternion.Euler(0,90,0);
			DominoGameManager.instance.player1.playerPieces[i].gameObject.AddComponent<LayoutElement>() ;
			DominoGameManager.instance.player1.playerPieces[i].gameObject.transform.SetParent(piecesPanel.transform);
			DominoGameManager.instance.player1.playerPieces[i].transform.localScale =  new Vector3 (37.41232f,37.41232f,37.41232f);
			DominoGameManager.instance.player1.playerPieces[i].transform.localPosition =  new Vector3 (DominoGameManager.instance.player1.playerPieces[i].transform.localPosition.x 
				,DominoGameManager.instance.player1.playerPieces[i].transform.localPosition.y ,897.895f);


		}

		for(int i = 0 ; i < DominoGameManager.instance.player2.playerPieces.Count ; i++)
		{
			GameObject dummy = Instantiate(dummyPiecePrefab);
			dummy.AddComponent<LayoutElement>() ;
			dummy.transform.SetParent(dummypiecesPanel.transform);
			dummy.transform.localPosition = new Vector3 (dummy.transform.localPosition.x,dummy.transform.localPosition.y,935.2785f);
			dummy.transform.localScale = new Vector3 (37.41115f,37.41115f,37.41115f);
		}
//        Debug.Log("distplayer 1");

	}

    public void showPiecesToPlayer2()
    {
        for (int i = 0; i < DominoGameManager.instance.player2.playerPieces.Count; i++)
        {
            DominoGameManager.instance.player2.playerPieces[i].gameObject.layer = 5;
            DominoGameManager.instance.player2.playerPieces[i].gameObject.AddComponent<LayoutElement>();
            DominoGameManager.instance.player2.playerPieces[i].transform.Rotate(0f, 90f, 0f);
            DominoGameManager.instance.player2.playerPieces[i].gameObject.transform.SetParent(piecesPanel.transform);
        }

        for (int i = 0; i < DominoGameManager.instance.player1.playerPieces.Count; i++)
        {
            GameObject dummy = Instantiate(dummyPiecePrefab);
            dummy.AddComponent<LayoutElement>();
            dummy.transform.SetParent(dummypiecesPanel.transform);
        }
        Debug.Log("distplayer 2");
    }

    private void SendDistributionDataToServer(string cmnd, string pieces)
    {
        string msg = cmnd;
        msg += pieces;

        Client.UNETSend(msg);
        Client.ClientDomino(msg);
    }

//	public void SendDominoMovementDataToServer(string cmnd, string pieceToMoveIndex, float x, float y, float z, float rotX, float rotY, float rotZ, int turn)
//    {
//        string msg = cmnd;
//        msg += pieceToMoveIndex + "|"; 
//        msg += x + "|";
//        msg += y + "|";
//        msg += z + "|";
//        msg += rotX + "|";
//        msg += rotY + "|";
//        msg += rotZ + "|";
//        msg += turn + "|";
//        Client.Send(msg);
//    
//    }
//
	public void SendDominoMovementDataToServer(string cmnd, string pieceToMoveIndex,int moveDirection , int isEnd1,int IsHostWhoPlayed)
	{
		string msg = cmnd;
		msg += pieceToMoveIndex + "|"; 
		msg += moveDirection + "|";
		msg += isEnd1 + "|";
		msg += IsHostWhoPlayed;
		Debug.Log("Movement send to server" + msg);
        Client.UNETSend(msg);
        Client.ClientDomino(msg);

	}
	private void SendDominoStockDrawToServer(string cmnd, string piecesIndex,int isHostWhoDrew)
    {
        string msg = cmnd;
		msg += piecesIndex + "|";
		msg += isHostWhoDrew;

        Client.UNETSend(msg);
        Client.ClientDomino(msg);
    }

	public void SendSwitchTurnAfterNoStockPieces()
	{
        Client.UNETSend("CSNP|");
        Client.ClientDomino("CSNP|");
	}

	public void SendTurn(string cmnd, int playerToStart){
		string msg = cmnd;
		msg += playerToStart;
		Debug.Log(msg);

        Client.UNETSend(msg);
        Client.ClientDomino(msg);
	}


    public void SendRoundScore(string cmnd, int isHostWin, int score)
    {
        string msg = cmnd;
		msg += isHostWin + "|";
        msg += score;

        Client.UNETSend(msg);
        Client.ClientDomino(msg);
    }


	public void SendDominoEndGameStates(string cmnd, int isHostWin, int score)
	{
		string msg = cmnd;
		msg += isHostWin + "|";
		msg += score;

        Client.UNETSend(msg);
        Client.ClientDomino(msg);
	}
    public void SendSurrender()
    {
        int playerNum = 0;
        if(Client.isHost) playerNum = 1;
        else if(!Client.isHost) playerNum = 2;

        string msg = "SRNDR|" + playerNum;
        Client.UNETSend(msg);
    }
	public void removeDummyPiece(bool IsHostWhoPlayed)
	{
		if(DominoGameManager.instance.player1.isHost && !IsHostWhoPlayed)
		{
			Destroy(dummypiecesPanel.transform.GetChild(dummypiecesPanel.transform.childCount -1).gameObject);
		}
		else if(!DominoGameManager.instance.player1.isHost && IsHostWhoPlayed)
		{
			Destroy(dummypiecesPanel.transform.GetChild(dummypiecesPanel.transform.childCount -1).gameObject);
		}
	}


	public void drawFromStockPieces()
	{
		int index = Random.Range(0,stockPieces.Count);
		DominoPiece temp = stockPieces[index];
		DominoGameManager.instance.destroyLastPieceInStock();
		SendDominoStockDrawToServer("CSTOCK|",temp.PieceID.ToString(),(DominoGameManager.instance.player1.isHost) ?  1 : 2);
	}

	public void applyStockDrawPieceFromNetwork(int pieceID,bool isHostWhoDrew)
	{
		
		DominoPiece temp = DominoGameManager.instance.getPieceByID(pieceID);
		stockPieces.Remove(temp);
		updateStockPieceIndicationPanel(stockPieces.Count);

		if((DominoGameManager.instance.player1.isHost && isHostWhoDrew)  || (!DominoGameManager.instance.player1.isHost && !isHostWhoDrew))
		{
			DominoGameManager.instance.player1.playerPieces.Add(temp);
			temp.gameObject.layer = 5 ;
			temp.gameObject.AddComponent<LayoutElement>() ;
			temp.transform.Rotate(0f,90f,0f);
			temp.gameObject.transform.SetParent(piecesPanel.transform);
			temp.transform.localScale =  new Vector3 (37.41232f,37.41232f,37.41232f);
			temp.transform.localPosition =  new Vector3 (temp.transform.localPosition.x ,temp.transform.localPosition.y ,897.895f);

			DominoGameManager.instance.checkFeasibleMoves();

		}
		else
		{
			DominoGameManager.instance.player2.playerPieces.Add(temp);
			GameObject dummy = Instantiate(dummyPiecePrefab);
			dummy.AddComponent<LayoutElement>();
			dummy.transform.SetParent(dummypiecesPanel.transform);
			dummy.transform.localPosition = new Vector3 (dummy.transform.localPosition.x,dummy.transform.localPosition.y,935.2785f);
			dummy.transform.localScale = new Vector3 (37.41115f,37.41115f,37.41115f);
		}

	}

	public void updateStockPieceIndicationPanel(int stockCount)
	{
		stockPiecesCounter.text = stockCount.ToString();

		for(int i  = 0 ; i < stockPieceIndicationPanel.transform.childCount ; i++)
		{
			Destroy(stockPieceIndicationPanel.transform.GetChild(i).gameObject);
		}

		for(int i = 0 ; i < stockCount ; i++)
		{
			GameObject temp	 = Instantiate(stockPieceIndicationItem,stockPieceIndicationPanel.transform);
			temp.transform.localScale = new Vector3 (1,1,1);
			temp.transform.localRotation = Quaternion.Euler(0,0,90f);
			temp.transform.localPosition  = new Vector3 (0,0,0);
		}

	}

    public void ChatMessage(string msg)
    {
        string messageToShow = "";
        GameObject go = Instantiate(messagePrefab) as GameObject;
        string[] chatmsg = msg.Split(':');
        //Debug.Log(chatmsg[0]+chatmsg[1]);
        //messageToShow = "<color=red>" + chatmsg[0] + "</color>" + chatmsg[1];
        //Debug.Log(UiMenuValues.instance.player1NameLabelWR.text);
        if (chatmsg[0].Equals(m_UNETDomino.instance.currentPlayerName))
            messageToShow = "<color=blue><b>" + chatmsg[0] + "</b></color>" + " : " + ArabicFixer.Fix(chatmsg[1], true, true);
        else if (chatmsg[0].Equals(m_UNETDomino.instance.secondPlayerName))
            messageToShow = "<color=red><b>" + chatmsg[0] + "</b></color>" + " : " + ArabicFixer.Fix(chatmsg[1], true, true);
        go.transform.SetParent(chatMessageContainer);
        go.transform.localScale = new Vector3(1f, 1.2f, 1f);
        go.GetComponentInChildren<Text>().text = messageToShow;
         

    }

    public void SendChatMessage()
    {
        InputField i = GameObject.Find("MessageInput").GetComponent<InputField>();
        if (i.text == "")
            return;

        Client.UNETSend("CMSG|" + m_UNETGameManager.instance.currentPlayerName + ":" + i.text);
        Client.ClientDomino("CMSG|" + m_UNETGameManager.instance.currentPlayerName + ":" + i.text);

        i.text = "";
    }

}
