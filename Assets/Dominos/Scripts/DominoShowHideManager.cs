﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DominoShowHideManager : MonoBehaviour {

	internal  bool isScorePanelHidden; 
	internal bool isSidePieceIndicationPanelHidden; 
	internal bool isDominoSettingsStripvisible; 


	public Animator dominoCanvasAnimator ;



	public void toggleDominoScorePanelState()
	{
		print("toggle DominoScore Panel");
		 
		if(isScorePanelHidden)
		{
			dominoCanvasAnimator.SetTrigger("showDominoScore");
			isScorePanelHidden =  !isScorePanelHidden;
		}
		else
		{
			dominoCanvasAnimator.SetTrigger("hideDominoScore");
			isScorePanelHidden =  !isScorePanelHidden;
		}
	}
	public void toggleDominoSidePiecesIndicationPanel()
	{
		if(isSidePieceIndicationPanelHidden)
		{
			dominoCanvasAnimator.SetTrigger("showSidePieceIndicationPanel");
			isSidePieceIndicationPanelHidden = !isSidePieceIndicationPanelHidden ;
		}
		else
		{
			dominoCanvasAnimator.SetTrigger("hideSidePieceIndicationPanel");
			isSidePieceIndicationPanelHidden = !isSidePieceIndicationPanelHidden ;
		}

	}

	public void toggleDominoSettingsStrip()
	{

		if(isDominoSettingsStripvisible)
		{
			dominoCanvasAnimator.SetTrigger("hideDominoSettingStrip");
			isDominoSettingsStripvisible = ! isDominoSettingsStripvisible ;
		}
		else
		{
			dominoCanvasAnimator.SetTrigger("showDominoSettingStrip");
			isDominoSettingsStripvisible = ! isDominoSettingsStripvisible ;
		}
	}







}
