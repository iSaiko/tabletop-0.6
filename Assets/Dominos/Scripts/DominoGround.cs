﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DominoGround : MonoBehaviour {

	public static DominoGround instance ;


	public GameObject startingPoint;

	public int end1Number;
	public int end2Number;

	public Transform end1ActivePoint;
	public Transform end2ActivePoint ;
	public Transform throwingDestination;


	public DominoPiece end1LastPiece;
	public DominoPiece end2LastPiece;

	public DummyEnd end1Dummy;
	public DummyEnd end2Dummy;

	public bool isDraggingPiece;

	public float throwDominoAnimationDuration ;


	private bool isFirstMove = true ;

	private int end1PatternStage = 1;
	private int end2PatternStage = 1;

	private int end1PatternCounter;
	private int end2PatternCounter;


	private int dominoThrowingState;
	private const int throwingStarted = 0;
	private const int throwingInProgress = 1;
	private const int throwingEnded = 2;
	private Vector3 throwingStartPosition ; 
	private Vector3 throwingEndPosition ; 
	private float throwingCounter;


	private DominoPiece selectedPiece;




	void Awake()
	{
		instance = this;
	}

	void Update () 
	{
//		print(isDraggingPiece);
		if(DominoGameManager.instance.player1.isMyTurn && !DominoGameManager.instance.player2.isMyTurn && Input.GetMouseButtonDown(0))
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit rayHit;

			if(Physics.Raycast (ray, out rayHit,1000 ,LayerMask.GetMask("UI")))
			{

				if(rayHit.transform.GetComponent<DominoPiece>()) 
				{
					 this.selectedPiece = rayHit.transform.GetComponent<DominoPiece>();
					if(!selectedPiece.isHighlighted)
					{
						highlightSelectedPiece(selectedPiece);
						if(!isFirstMove)
						{
							hideEndDummies();
							checkFeasibeEnds(selectedPiece);
						}
						//check and show feasible end 

					}
					else if(selectedPiece.isHighlighted && selectedPiece.isFeasibleToPlay)
					{
						if(isFirstMove)
						{
							unHightlightAllPiece();
							PiecesManager.instance.SendDominoMovementDataToServer("CMOV|",selectedPiece.PieceID.ToString(),Constants.firstMove,1,(DominoGameManager.instance.player1.isHost) ? 1 : 2);
							end1LastPiece = this.selectedPiece;
							end2LastPiece = this.selectedPiece;
						}

					}
				}

			}
			else if(Physics.Raycast (ray, out rayHit,1000 ,LayerMask.GetMask("Default")))
			{
				
				if(rayHit.transform.tag == "end1")
				{
					this.selectedPiece.UnhighLight();
					end1Dummy.moveSelectedPiece(selectedPiece);
//					end1LastPiece = this.selectedPiece;
//					this.selectedPiece = null;
				}
				else if(rayHit.transform.tag == "end2")
				{
					selectedPiece.UnhighLight();
					end2Dummy.moveSelectedPiece(selectedPiece);
//					end2LastPiece = this.selectedPiece;
//					this.selectedPiece = null;
				}
				unHightlightAllPiece();
				hideEndDummies();
			}
		
		

		}
//		else if(DominoGameManager.instance.player1.isMyTurn && !DominoGameManager.instance.player2.isMyTurn && Input.GetMouseButtonUp(0))
//		{
//
//			isDraggingPiece = false;
//
//
//		}
	}




	public void moveFirstPiece(DominoPiece selectedPiece)
	{
		selectedPiece.gameObject.layer =  0;
		Destroy(selectedPiece.GetComponent<LayoutElement>());
		selectedPiece.transform.SetParent(this.transform);
		selectedPiece.transform.localScale = new Vector3 (0.09090909f,5.074584f,0.09090909f);


		if(isFirstMove)
		{
	
			if(selectedPiece.upperValue == selectedPiece.lowerValue)
			{

				selectedPiece.transform.rotation = Quaternion.Euler(0f,90,0f);

				selectedPiece.transform.position = (startingPoint.transform.position + (selectedPiece.transform.position - selectedPiece.middlePointAnchor.position));
				end1Number = selectedPiece.upperValue ;
				end2Number = selectedPiece.upperValue;
				end1ActivePoint.position = selectedPiece.leftMiddleAnchor.position;
				end2ActivePoint.position = selectedPiece.rightMiddleAnchor.position;
			}
			else
			{

				selectedPiece.transform.rotation = Quaternion.Euler(0f,180f,0f);

				selectedPiece.transform.position = (startingPoint.transform.position + (selectedPiece.transform.position - selectedPiece.middlePointAnchor.position));
				end1Number = selectedPiece.lowerValue;
				end2Number = selectedPiece.upperValue;
				end1ActivePoint.position = selectedPiece.lowerPointAnchor.position;
				end2ActivePoint.position = selectedPiece.upperPointAnchor.position;
			}
			updateEnd1Pattern();
			updateEnd2Pattern();
			isFirstMove = false;
			SoundsManager.instance.playDominoThrowSound();

//			PiecesManager.instance.SendDominoMovementDataToServer("CMOV|",selectedPiece.PieceID.ToString(),Constants.firstMove,1);

		}



	}


	public void moveRight(DominoPiece selectedPiece,bool isEnd1)
	{
		selectedPiece.gameObject.layer =  0;
		Destroy(selectedPiece.GetComponent<LayoutElement>());
		selectedPiece.transform.SetParent(this.transform);
		selectedPiece.transform.localScale = new Vector3 (0.09090909f,5.074584f,0.09090909f);


		int endNumber =  (isEnd1) ? end1Number : end2Number; 
		if(endNumber  == selectedPiece.lowerValue )
		{
			selectedPiece.transform.localRotation = Quaternion.Euler(0f,180f,0f);

			if(isEnd1)
			{
//				throwingDestination.position = end1ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.lowerPointAnchor.position);
//				throwDominoPiece(selectedPiece,throwingDestination,true,false);

//				selectedPiece.transform.position = end1ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.lowerPointAnchor.position);
//				end1ActivePoint.position = selectedPiece.upperPointAnchor.position;
//				end1Number = selectedPiece.upperValue;
//				//				if(end1LastPiece!=null){end1LastPiece.flush();}
//				end1LastPiece = selectedPiece;
				StartCoroutine(updatePieceEnd1Lower(selectedPiece));
			}
			else
			{
//				throwingDestination.position = end1ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.lowerPointAnchor.position);
//				throwDominoPiece(selectedPiece,throwingDestination,false,false);


//				selectedPiece.transform.position = end2ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.lowerPointAnchor.position);
//				end2ActivePoint.position = selectedPiece.upperPointAnchor.position;
//				end2Number = selectedPiece.upperValue;
//				//				if(end2LastPiece!=null){end2LastPiece.flush();}
//				end2LastPiece = selectedPiece;
//				updatePieceEnd2Lower(selectedPiece);
				StartCoroutine(updatePieceEnd2Lower(selectedPiece));

			}

		}
		else if(endNumber  == selectedPiece.upperValue)
		{
			selectedPiece.transform.localRotation = Quaternion.Euler(0f,0,0f);
			if(isEnd1)
			{
//				throwingDestination.position = end1ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.upperPointAnchor.position);
//				throwDominoPiece(selectedPiece,throwingDestination,true,true);

//				selectedPiece.transform.position = end1ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.upperPointAnchor.position);
//				end1ActivePoint.position = selectedPiece.lowerPointAnchor.position;
//				end1Number = selectedPiece.lowerValue;
//				//				if(end1LastPiece!=null){end1LastPiece.flush();}
//				end1LastPiece = selectedPiece;
				StartCoroutine(updatePieceEnd1Upper(selectedPiece));

			}
			else
			{
//				throwingDestination.position = end1ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.upperPointAnchor.position);
//				throwDominoPiece(selectedPiece,throwingDestination,false,true);

//				selectedPiece.transform.position = end2ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.upperPointAnchor.position);
//				end2ActivePoint.position = selectedPiece.lowerPointAnchor.position;
//				end2Number = selectedPiece.lowerValue;
//				//				if(end2LastPiece!=null){end2LastPiece.flush();}
//				end2LastPiece = selectedPiece;
				StartCoroutine(updatePieceEnd2Upper(selectedPiece));

			}

		}
	}


	public void moveLeft(DominoPiece selectedPiece,bool isEnd1)
	{

		selectedPiece.gameObject.layer =  0;
		Destroy(selectedPiece.GetComponent<LayoutElement>());
		selectedPiece.transform.SetParent(this.transform);
		selectedPiece.transform.localScale = new Vector3 (0.09090909f,5.074584f,0.09090909f);

		int endNumber =  (isEnd1) ? end1Number : end2Number; 
		if(endNumber  == selectedPiece.lowerValue )
		{
			selectedPiece.transform.rotation = Quaternion.Euler(0f,0f,0f);
			if(isEnd1)
			{
//				selectedPiece.transform.position = end1ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.lowerPointAnchor.position);
//				end1ActivePoint.position = selectedPiece.upperPointAnchor.position;
//				end1Number = selectedPiece.upperValue;
////				if(end1LastPiece!=null){end1LastPiece.flush();}
//				end1LastPiece = selectedPiece;
				StartCoroutine(updatePieceEnd1Lower(selectedPiece));
			}
			else
			{
//				selectedPiece.transform.position = end2ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.lowerPointAnchor.position);
//				end2ActivePoint.position = selectedPiece.upperPointAnchor.position;
//				end2Number = selectedPiece.upperValue;
////				if(end2LastPiece!=null){end2LastPi
				/// ece.flush();}
//				end2LastPiece = selectedPiece;
				StartCoroutine(updatePieceEnd2Lower(selectedPiece));
			}


		}
		else if(endNumber  == selectedPiece.upperValue)
		{
			selectedPiece.transform.rotation = Quaternion.Euler(0f,180f,0f);
			if(isEnd1)
			{
//				selectedPiece.transform.position = end1ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.upperPointAnchor.position);
//				end1ActivePoint.position = selectedPiece.lowerPointAnchor.position;
//				end1Number = selectedPiece.lowerValue;
////				if(end1LastPiece!=null){end1LastPiece.flush();}
//				end1LastPiece = selectedPiece;
				StartCoroutine(updatePieceEnd1Upper(selectedPiece));
			}
			else
			{
//				selectedPiece.transform.position = end2ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.upperPointAnchor.position);
//				end2ActivePoint.position = selectedPiece.lowerPointAnchor.position;
//				end2Number = selectedPiece.lowerValue;
////				if(end2LastPiece!=null){end2LastPiece.flush();}
//				end2LastPiece = selectedPiece;
				StartCoroutine(updatePieceEnd2Upper(selectedPiece));
			}

		}
	}

	public void moveUp(DominoPiece selectedPiece,bool isEnd1)
	{

		selectedPiece.gameObject.layer =  0;
		Destroy(selectedPiece.GetComponent<LayoutElement>());
		selectedPiece.transform.SetParent(this.transform);
		selectedPiece.transform.localScale = new Vector3 (0.09090909f,5.074584f,0.09090909f);


		int endNumber =  (isEnd1) ? end1Number : end2Number; 
		if(endNumber  == selectedPiece.lowerValue )
		{
			selectedPiece.transform.rotation = Quaternion.Euler(0f,90,0f);

			if(isEnd1)
			{
//				selectedPiece.transform.position = end1ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.lowerPointAnchor.position);
//				end1ActivePoint.position = selectedPiece.upperPointAnchor.position;
//				end1Number = selectedPiece.upperValue;
////				if(end1LastPiece!=null){end1LastPiece.flush();}
//				end1LastPiece = selectedPiece;
				StartCoroutine(updatePieceEnd1Lower(selectedPiece));
			}
			else
			{
//				selectedPiece.transform.position = end2ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.lowerPointAnchor.position);
//				end2ActivePoint.position = selectedPiece.upperPointAnchor.position;
//				end2Number = selectedPiece.upperValue;
////				if(end2LastPiece!=null){end2LastPiece.flush();}
//				end2LastPiece = selectedPiece;
				StartCoroutine(updatePieceEnd2Lower(selectedPiece));
			}

		}
		else if(endNumber  == selectedPiece.upperValue)

		{
			selectedPiece.transform.rotation = Quaternion.Euler(0f,270,0f);

			if(isEnd1)
			{
//				selectedPiece.transform.position = end1ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.upperPointAnchor.position);
//				end1ActivePoint.position = selectedPiece.lowerPointAnchor.position;
//				end1Number = selectedPiece.lowerValue;
////				if(end1LastPiece!=null){end1LastPiece.flush();}
//				end1LastPiece = selectedPiece;
				StartCoroutine(updatePieceEnd1Upper(selectedPiece));
			}
			else
			{
//				selectedPiece.transform.position = end2ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.upperPointAnchor.position);
//				end2ActivePoint.position = selectedPiece.lowerPointAnchor.position;
//				end2Number = selectedPiece.lowerValue;
////				if(end2LastPiece!=null){end2LastPiece.flush();}
//				end2LastPiece = selectedPiece;
				StartCoroutine(updatePieceEnd2Upper(selectedPiece));
			}

		}
	}




	public void moveDown(DominoPiece selectedPiece,bool isEnd1)
	{
		selectedPiece.gameObject.layer =  0;
		Destroy(selectedPiece.GetComponent<LayoutElement>());
		selectedPiece.transform.SetParent(this.transform);
		selectedPiece.transform.localScale = new Vector3 (0.09090909f,5.074584f,0.09090909f);


		int endNumber =  (isEnd1) ? end1Number : end2Number; 
		if(endNumber  == selectedPiece.lowerValue )
		{
			selectedPiece.transform.rotation = Quaternion.Euler(0f,270,0f);

			if(isEnd1)
			{
//				selectedPiece.transform.position = end1ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.lowerPointAnchor.position);
//				end1ActivePoint.position = selectedPiece.upperPointAnchor.position;
//				end1Number = selectedPiece.upperValue;
////				if(end1LastPiece!=null){end1LastPiece.flush();}
//				end1LastPiece = selectedPiece;
				StartCoroutine(updatePieceEnd1Lower(selectedPiece));


			}
			else
			{
//				selectedPiece.transform.position = end2ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.lowerPointAnchor.position);
//				end2ActivePoint.position = selectedPiece.upperPointAnchor.position;
//				end2Number = selectedPiece.upperValue;
////				if(end2LastPiece!=null){end2LastPiece.flush();}
//				end2LastPiece = selectedPiece;
				StartCoroutine(updatePieceEnd2Lower(selectedPiece));

			}

		}
		else if(endNumber  == selectedPiece.upperValue)

		{
			selectedPiece.transform.rotation = Quaternion.Euler(0f,90,0f);

			if(isEnd1)
			{
//				selectedPiece.transform.position = end1ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.upperPointAnchor.position);
//				end1ActivePoint.position = selectedPiece.lowerPointAnchor.position;
//				end1Number = selectedPiece.lowerValue;
////				if(end1LastPiece!=null){end1LastPiece.flush();}
//				end1LastPiece = selectedPiece;
				StartCoroutine(updatePieceEnd1Upper(selectedPiece));
			}
			else
			{
//				selectedPiece.transform.position = end2ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.upperPointAnchor.position);
//				end2ActivePoint.position = selectedPiece.lowerPointAnchor.position;
//				end2Number = selectedPiece.lowerValue;
////				if(end2LastPiece!=null){end2LastPiece.flush();}
//				end2LastPiece = selectedPiece;
				StartCoroutine(updatePieceEnd2Upper(selectedPiece));
			}

		}
	}




	public void updateEnd1Pattern() // DominoPiece selectedPiece
	{
		switch (end1PatternStage) 
		{
		case 1 :

//			moveLeft(selectedPiece,true);
			end1Dummy.setDummyValues(Constants.moveLeft,true);
			end1Dummy.putDummyLeft(end1ActivePoint.position);
			end1PatternCounter++;
			print(end1PatternCounter);
			end1PatternStage = (end1PatternCounter >= 4) ? end1PatternStage+1 : end1PatternStage;
			if(end1PatternStage == 2){
				end1ActivePoint.position = changeDirection(end1LastPiece,end1Number);
				end1Dummy.setDummyValues(Constants.moveUp,true);
				end1Dummy.putDummyUp(end1ActivePoint.position);
				end1PatternCounter = 0 ;
			}
			break;


		case 2 :

//			moveUp(selectedPiece,true);
			end1Dummy.setDummyValues(Constants.moveUp,true);
			end1Dummy.putDummyUp(end1ActivePoint.position);
			end1PatternCounter++;
			end1PatternStage = (end1PatternCounter >= 4) ? end1PatternStage+1 : end1PatternStage;
			if(end1PatternStage == 3){
				end1ActivePoint.position = changeDirection(end1LastPiece,end1Number);
				end1Dummy.setDummyValues(Constants.moveRight,true);
				end1Dummy.putDummyRight(end1ActivePoint.position);
				end1PatternCounter = 0 ;
			}
			break;



		case 3 :
//			moveRight(selectedPiece,true);
			end1Dummy.setDummyValues(Constants.moveRight,true);
			end1Dummy.putDummyRight(end1ActivePoint.position);
			end1PatternCounter++;
			end1PatternStage = (end1PatternCounter >= 5) ? end1PatternStage+1 : end1PatternStage;
			if(end1PatternStage == 4){
				end1ActivePoint.position = changeDirection(end1LastPiece,end1Number);
				end1Dummy.setDummyValues(Constants.moveDown,true);
				end1Dummy.putDummyDown(end1ActivePoint.position);
				end1PatternCounter = 0 ;
			}
			break;


		case 4 :
			end1Dummy.setDummyValues(Constants.moveDown,true);
			end1Dummy.putDummyDown(end1ActivePoint.position);
			end1PatternCounter++;
			end1PatternStage = (end1PatternCounter >= 3) ? end1PatternStage+1 : end1PatternStage;
			if(end1PatternStage == 5){
				end1ActivePoint.position = changeDirection(end1LastPiece,end1Number);
				end1Dummy.setDummyValues(Constants.moveLeft,true);
				end1Dummy.putDummyLeft(end1ActivePoint.position);
				end1PatternCounter = 0 ;
			}

			break;

		case 5 :
			end1Dummy.setDummyValues(Constants.moveLeft,true);
			end1Dummy.putDummyLeft(end1ActivePoint.position);
			break;
		}



	}	

	public void updateEnd2Pattern() // DominoPiece selectedPiece
	{
		switch (end2PatternStage) 
		{
		case 1 :

//			moveRight(selectedPiece,false);
			end2Dummy.setDummyValues(Constants.moveRight,false);
			end2Dummy.putDummyRight(end2ActivePoint.position);
			end2PatternCounter++;
			end2PatternStage = (end2PatternCounter >= 4) ? end2PatternStage+1 : end2PatternStage;
			if(end2PatternStage == 2){
				end2ActivePoint.position = changeDirection(end2LastPiece,end2Number);
				end2Dummy.setDummyValues(Constants.moveDown,false);
				end2Dummy.putDummyDown(end2ActivePoint.position);
				end2PatternCounter = 0 ;
			}
			break;


		case 2 :

//			moveDown(selectedPiece,false);
			end2Dummy.setDummyValues(Constants.moveDown,false);
			end2Dummy.putDummyDown(end2ActivePoint.position);
			end2PatternCounter++;
			end2PatternStage = (end2PatternCounter >= 4) ? end2PatternStage+1 : end2PatternStage;
			if(end2PatternStage == 3){
				end2ActivePoint.position = changeDirection(end2LastPiece,end2Number);
				end2Dummy.setDummyValues(Constants.moveLeft,false);
				end2Dummy.putDummyLeft(end2ActivePoint.position);
				end2PatternCounter = 0 ;
			}
			break;

		case 3 :
//			moveLeft(selectedPiece,false);
			end2Dummy.setDummyValues(Constants.moveLeft,false);
			end2Dummy.putDummyLeft(end2ActivePoint.position);
			end2PatternCounter++;
			end2PatternStage = (end2PatternCounter >= 5) ? end2PatternStage+1 : end2PatternStage;
			if(end2PatternStage == 4){
				end2ActivePoint.position = changeDirection(end2LastPiece,end2Number);
				end2Dummy.setDummyValues(Constants.moveUp,false);
				end2Dummy.putDummyUp(end2ActivePoint.position);
				end2PatternCounter = 0 ;
			}
			break;


		case 4 :
//			moveUp(selectedPiece,false);
			end2Dummy.setDummyValues(Constants.moveUp,false);
			end2Dummy.putDummyUp(end2ActivePoint.position);
			end2PatternCounter++;
			end2PatternStage = (end2PatternCounter >= 3) ? end2PatternStage+1 : end2PatternStage;
			if(end2PatternStage == 5){
				end2ActivePoint.position = changeDirection(end2LastPiece,end2Number);
				end2Dummy.setDummyValues(Constants.moveRight,false);
				end2Dummy.putDummyRight(end2ActivePoint.position);
				end2PatternCounter = 0 ;
			}

			break;

		case 5 :
			end2Dummy.setDummyValues(Constants.moveRight,false);
			end2Dummy.putDummyRight(end2ActivePoint.position);
			break;

		}


	}
	public void unHightlightAllPiece()
	{
		foreach (DominoPiece piece in DominoGameManager.instance.player1.playerPieces) 
		{
			piece.UnhighLight();
		}
	}
	public void highlightSelectedPiece(DominoPiece piece)
	{
		unHightlightAllPiece();
		piece.highLight();
	}


	public Vector3 changeDirection(DominoPiece selectedPiece,int endValue)
	{
		return (endValue == selectedPiece.upperValue) ? selectedPiece.rightUpAnchor.position : selectedPiece.leftDownAnchor.position ; 
	}




	public void checkFeasibeEnds(DominoPiece selectedPiece)
	{
		if( selectedPiece.isFeasibleToPlay && (selectedPiece.upperValue == end1Number || selectedPiece.lowerValue == end1Number)  )
		{
			end1Dummy.gameObject.SetActive(true);
		}
		if( selectedPiece.isFeasibleToPlay && (selectedPiece.upperValue == end2Number || selectedPiece.lowerValue == end2Number ) )
		{
			end2Dummy.gameObject.SetActive(true);
		}

	}

	public void hideEndDummies()
	{
		end1Dummy.gameObject.SetActive(false);
		end2Dummy.gameObject.SetActive(false);
	}


	public void updateCorrectEnd(bool isEnd1)
	{

		if(isEnd1)
		{
			updateEnd1Pattern();
		}
		else
		{
			updateEnd2Pattern();
		}
	}

//
//	public void throwDominoPiece(DominoPiece dominoPiece ,Transform jumpDestination,bool isUpperSide,bool isEnd1)
//	{
//		 
//
//		Animation animation = dominoPiece.transform.gameObject.AddComponent<Animation>();
//		AnimationClip clip ;
//		clip = new AnimationClip();
//		clip.legacy = true;
//
////		print(jumpDestination.x + " " + jumpDestination.y + " " +jumpDestination.z + " ");
////		print(transform.position.x + " " + transform.position.y + " " +transform.position.z + " ");
//
//
//
//		// animating postion from current postion to destination
//
//		AnimationCurve curve = AnimationCurve.Linear(0, dominoPiece.transform.localPosition.x, throwDominoAnimationDuration,jumpDestination.localPosition.x);
////		AnimationCurve curve = AnimationCurve.Linear(0, dominoPiece.transform.InverseTransformPoint(dominoPiece.transform.position).x, throwDominoAnimationDuration,jumpDestination.InverseTransformPoint(jumpDestination.position).x);
////		curve.AddKey(throwDominoAnimationDuration / 2 , jumpDestination.InverseTransformPoint(jumpDestination.position).x +5f);
//		clip.SetCurve("", typeof(Transform), "localPosition.x", curve);
//
//
//		curve = AnimationCurve.Linear(0, dominoPiece.transform.localPosition.y, throwDominoAnimationDuration,jumpDestination.localPosition.y);
////		curve = AnimationCurve.Linear(0, dominoPiece.transform.InverseTransformPoint(dominoPiece.transform.position).y, throwDominoAnimationDuration,jumpDestination.InverseTransformPoint(jumpDestination.position).y);
////		curve.AddKey(throwDominoAnimationDuration / 2 , jumpDestination.InverseTransformPoint(jumpDestination.position).y +5f);
//		clip.SetCurve("", typeof(Transform), "localPosition.y", curve);
//
//
//		curve = AnimationCurve.Linear(0, dominoPiece.transform.localPosition.z, throwDominoAnimationDuration,jumpDestination.localPosition.z);
////		curve = AnimationCurve.Linear(0, dominoPiece.transform.InverseTransformPoint(dominoPiece.transform.position).z, throwDominoAnimationDuration,jumpDestination.InverseTransformPoint(jumpDestination.position).z);
////		curve.AddKey(throwDominoAnimationDuration / 2 , jumpDestination.InverseTransformPoint(jumpDestination.position).z +5f);
//		clip.SetCurve("", typeof(Transform), "localPosition.z", curve);
//
//
//
//		// animating scale 
//
//
//		//		curve = AnimationCurve.Linear(0, transform.lossyScale.x, animationDuration, transform.lossyScale.x);
//		//		curve.AddKey(animationDuration / 2, (float)(transform.lossyScale.x + 2));
//		//		clip.SetCurve("", typeof(Transform), "lossyScale.z", curve);
//		//
//		//		curve = AnimationCurve.Linear(0, transform.lossyScale.y, animationDuration, transform.lossyScale.y);
//		//		curve.AddKey(animationDuration / 2, (float)(transform.lossyScale.y + 2));
//		//		clip.SetCurve("", typeof(Transform), "lossyScale.y", curve);
//		//
//		//
//		//		curve = AnimationCurve.Linear(0, transform.lossyScale.z, animationDuration, transform.lossyScale.z);
//		//		curve.AddKey(animationDuration / 2, (float)(transform.lossyScale.z + 2));
//		//		clip.SetCurve("", typeof(Transform), "lossyScale.z", curve);
//		//
//
//		animation.AddClip(clip, "throwDominoPiece");
//		animation.Play("throwDominoPiece");
//
////		StartCoroutine(afterThrowingDominoPiece(throwDominoAnimationDuration+.1f,isEnd1,isUpperSide));
////		Destroy(animation,throwDominoAnimationDuration+.1f);
//
//	}
//
//	IEnumerator afterThrowingDominoPiece(float throwDominoAnimationDuration,bool isUpperSide,bool isEnd1)
//	{
//		yield return new WaitForSeconds(throwDominoAnimationDuration);
//
//		print("afterThrowingDominoPiece");
//		if(!isUpperSide)
//		{
//			if(isEnd1)
//			{
//
//
//			}
//			else
//			{
//
//			}
//		}
//		else if(isUpperSide)
//		{
//			if(isEnd1)
//			{
//
//			}
//			else
//			{
//
//			}
//		}
//
//	}



	public IEnumerator updatePieceEnd1Lower(DominoPiece selectedPiece)
	{

		print("we at the start of coroutine");
		print(dominoThrowingState);
		if(dominoThrowingState == throwingStarted)
		{
			print(dominoThrowingState);

			throwingStartPosition = selectedPiece.transform.position;
			throwingEndPosition = end1ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.lowerPointAnchor.position);
			dominoThrowingState = throwingInProgress ;
			throwingCounter = 0;
//			Debug.DrawLine(throwingStartPosition,throwingEndPosition,Color.red,20f);
		}
		if (dominoThrowingState == throwingInProgress )
		{
			print(dominoThrowingState);
			while(dominoThrowingState == throwingInProgress)
			{


				selectedPiece.transform.position = Vector3.Lerp(throwingStartPosition,throwingEndPosition,throwingCounter*3);
				throwingCounter  += Time.fixedDeltaTime ;
				if(throwingCounter*2.5 >= 1)
				{
					print("domino piece has been threw to the destinate position");
					dominoThrowingState = throwingEnded;
					print(dominoThrowingState);

					StopCoroutine(updatePieceEnd1Lower(selectedPiece));
					SoundsManager.instance.playDominoThrowSound();

					end1ActivePoint.position = selectedPiece.upperPointAnchor.position;
					end1Number = selectedPiece.upperValue;
//					end1LastPiece.flush();
					end1LastPiece = selectedPiece;
					dominoThrowingState = 0;
					DominoGameManager.instance.checkRoundWinning();
//					DominoGameManager.instance.switchTurns();
					DominoGameManager.instance.autoZoom();
					print(dominoThrowingState);
					updateEnd1Pattern();
				}
				yield return null;
			}

		}

	}



	public IEnumerator updatePieceEnd1Upper(DominoPiece selectedPiece)
	{
		if(dominoThrowingState == throwingStarted)
		{
			throwingStartPosition = selectedPiece.transform.position;
			throwingEndPosition = end1ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.upperPointAnchor.position);
			dominoThrowingState = throwingInProgress ;
			throwingCounter = 0;
//			Debug.DrawLine(throwingStartPosition,throwingEndPosition,Color.red,20f);
		}
		if (dominoThrowingState == throwingInProgress )
		{
			while(dominoThrowingState == throwingInProgress)
			{
				selectedPiece.transform.position = Vector3.Lerp(throwingStartPosition,throwingEndPosition,throwingCounter*3);
				throwingCounter  += Time.fixedDeltaTime ;
				if(throwingCounter*2.5 >= 1)
				{
					dominoThrowingState = throwingEnded;
					StopCoroutine(updatePieceEnd1Upper(selectedPiece));
					SoundsManager.instance.playDominoThrowSound();

					end1ActivePoint.position = selectedPiece.lowerPointAnchor.position;
					end1Number = selectedPiece.lowerValue;
//					end1LastPiece.flush();
					end1LastPiece = selectedPiece;
					dominoThrowingState = 0;
					DominoGameManager.instance.checkRoundWinning();
//					DominoGameManager.instance.switchTurns();
					DominoGameManager.instance.autoZoom();
					updateEnd1Pattern();
				}
				yield return null;
			}

		}

	}

	public IEnumerator updatePieceEnd2Lower(DominoPiece selectedPiece)
	{
		print("we at the start of coroutine");
		print(dominoThrowingState);
		if(dominoThrowingState == throwingStarted)
		{
			print(dominoThrowingState);

			throwingStartPosition = selectedPiece.transform.position;
			throwingEndPosition = end2ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.lowerPointAnchor.position);
			dominoThrowingState = throwingInProgress ;
			throwingCounter = 0;
//			Debug.DrawLine(throwingStartPosition,throwingEndPosition,Color.red,20f);
		}
		if (dominoThrowingState == throwingInProgress )
		{
			print(dominoThrowingState);
			while(dominoThrowingState == throwingInProgress)
			{


				selectedPiece.transform.position = Vector3.Lerp(throwingStartPosition,throwingEndPosition,throwingCounter*3);
				throwingCounter  += Time.fixedDeltaTime ;
				if(throwingCounter*2.5 >= 1)
				{
					print("domino piece has been threw to the destinate position");
					dominoThrowingState = throwingEnded;
					print(dominoThrowingState);

					StopCoroutine(updatePieceEnd2Lower(selectedPiece));
					SoundsManager.instance.playDominoThrowSound();

					end2ActivePoint.position = selectedPiece.upperPointAnchor.position;
					end2Number = selectedPiece.upperValue;
//					end2LastPiece.flush();
					end2LastPiece = selectedPiece;
					dominoThrowingState = 0;
					DominoGameManager.instance.checkRoundWinning();
//					DominoGameManager.instance.switchTurns();
					DominoGameManager.instance.autoZoom();
					print(dominoThrowingState);
					updateEnd2Pattern();
				}
				yield return null;
			}

		}

	}

	public IEnumerator updatePieceEnd2Upper(DominoPiece selectedPiece)
	{
//		print("we at the start of coroutine");
//		print(dominoThrowingState);
		if(dominoThrowingState == throwingStarted)
		{
			print(dominoThrowingState);

			throwingStartPosition = selectedPiece.transform.position;
			throwingEndPosition = end2ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.upperPointAnchor.position);
			dominoThrowingState = throwingInProgress ;
			throwingCounter = 0;
//			Debug.DrawLine(throwingStartPosition,throwingEndPosition,Color.red,20f);
		}
		if (dominoThrowingState == throwingInProgress )
		{
			print(dominoThrowingState);
			while(dominoThrowingState == throwingInProgress)
			{


				selectedPiece.transform.position = Vector3.Lerp(throwingStartPosition,throwingEndPosition,throwingCounter*3);
				throwingCounter  += Time.fixedDeltaTime ;
				if(throwingCounter*2.5 >= 1)
				{
//					print("domino piece has been threw to the destinate position");
					dominoThrowingState = throwingEnded;
//					print(dominoThrowingState);
					StopCoroutine(updatePieceEnd2Upper(selectedPiece));
					SoundsManager.instance.playDominoThrowSound();

					end2ActivePoint.position = selectedPiece.lowerPointAnchor.position;
					end2Number = selectedPiece.lowerValue;
//					end2LastPiece.flush();
					end2LastPiece = selectedPiece;
					dominoThrowingState = 0;
					DominoGameManager.instance.checkRoundWinning();
//					DominoGameManager.instance.switchTurns();
					DominoGameManager.instance.autoZoom();
//					print(dominoThrowingState);
					updateEnd2Pattern();
				}
				yield return null;
			}

		}

	}




//	void updatePieceEnd1Lower(DominoPiece selectedPiece)
//	{
//		selectedPiece.transform.position = end1ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.lowerPointAnchor.position);
//		end1ActivePoint.position = selectedPiece.upperPointAnchor.position;
//		end1Number = selectedPiece.upperValue;
//		end1LastPiece = selectedPiece;
//	}
//
//	void updatePieceEnd2Lower(DominoPiece selectedPiece)
//	{
//		selectedPiece.transform.position = end2ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.lowerPointAnchor.position);
//		end2ActivePoint.position = selectedPiece.upperPointAnchor.position;
//		end2Number = selectedPiece.upperValue;
//		end2LastPiece = selectedPiece;
//	}
//
//	void updatePieceEnd1Upper(DominoPiece selectedPiece)
//	{
//		selectedPiece.transform.position = end1ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.upperPointAnchor.position);
//		end1ActivePoint.position = selectedPiece.lowerPointAnchor.position;
//		end1Number = selectedPiece.lowerValue;
//		end1LastPiece = selectedPiece;
//	}
//
//	void updatePieceEnd2Upper(DominoPiece selectedPiece)
//	{
//		selectedPiece.transform.position = end2ActivePoint.position  +  (selectedPiece.transform.position - selectedPiece.upperPointAnchor.position);
//		end2ActivePoint.position = selectedPiece.lowerPointAnchor.position;
//		end2Number = selectedPiece.lowerValue;
//		end2LastPiece = selectedPiece;
//	}




}
