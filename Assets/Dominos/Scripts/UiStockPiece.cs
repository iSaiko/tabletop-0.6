﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiStockPiece : MonoBehaviour {
	public Button pieceButton;
	public void drawPiece()
	{
		PiecesManager.instance.drawFromStockPieces();
		SoundsManager.instance.playDominoDrawSound();
	}

}
