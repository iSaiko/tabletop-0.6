﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DominoFirstMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	

	}


	public void playBiggestPiece()
	{

		DominoPlayer  player1 = DominoGameManager.instance.player1;

		if(!player1.isMyTurn)
			return;


		int biggestValue1 = 0 ; 


		for(int i = 0 ; i <player1.playerPieces.Count; i++)
		{
			if(player1.playerPieces[i].upperValue == player1.playerPieces[i].lowerValue)
			{
				biggestValue1 = (player1.playerPieces[i].upperValue > biggestValue1 ) ? player1.playerPieces[i].upperValue : biggestValue1 ;
			}
		}

		if(biggestValue1 == 0 )
		{
			for(int i = 0 ; i <player1.playerPieces.Count; i++)
			{
				player1.playerPieces[i].isFeasibleToPlay = true;
			}
		}
		else
		{
			for(int i = 0 ; i <player1.playerPieces.Count; i++)
			{
				if( biggestValue1 == player1.playerPieces[i].upperValue   &&  player1.playerPieces[i].upperValue == player1.playerPieces[i].lowerValue)
				{
					player1.playerPieces[i].setFeasibleToPlay();
					player1.playerPieces[0].isFeasibleToPlay = true;
				}
			}
		}
		DominoGameManager.instance.highlightPlayerTurn(player1.isMyTurn);
		Destroy(this,2f);
	}




}
