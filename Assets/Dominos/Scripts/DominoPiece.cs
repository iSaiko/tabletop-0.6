﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DominoPiece : MonoBehaviour {



	public int upperValue;
	public int lowerValue;
	public int PieceID;
	public bool isFeasibleToPlay; 
	private bool hasbeenFlushed ;

	public Transform upperTransform;
	public Transform lowerTransform;
	 
	public Transform upperPointAnchor;
	public Transform lowerPointAnchor;
	public Transform middlePointAnchor;
	public Transform rightUpAnchor;
	public Transform rightMiddleAnchor;
	public Transform rightDownAnchor;
	public Transform leftUpAnchor;
	public Transform leftMiddleAnchor;
	public Transform leftDownAnchor;


	public Material highlighMaterial;
	public Material normalMaterial;

//	public Renderer[] myRenderer;
	public Renderer upperRenderer;
	public Renderer lowerRenderer;
	public bool isHighlighted ;

	public Vector3 intitialPosition;


//
//	private int dominoThrowingState;
//	private const int throwingStarted = 0;
//	private const int throwingInProgress = 1;
//	private const int throwingEnded = 2;
//	private Vector3 throwingStartPosition ; 
//	private Vector3 throwingEndPosition ; 
//	private float throwingCounter;
//	private bool isEnd1 ;
//	private bool isUpper;
//


	void Start () 
	{
		isFeasibleToPlay = false;
		hasbeenFlushed = false;
//		myRenderer = this.transform.GetComponentsInChildren<Renderer>();
	}

//	void Update()
//	{
//		if(isHighlighted)
//		{
//
//			Debug.DrawLine(transform.position,new Vector3 (transform.position.x , transform.position.y -1000 , transform.position.z));
//			Debug.DrawLine(transform.position,DominoGround.instance.end1ActivePoint.position);
//			print(Vector3.Angle(transform.position - new Vector3 (transform.position.x , transform.position.y -1000 , transform.position.z),(transform.position - DominoGround.instance.end1ActivePoint.position)));
//			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
//			RaycastHit rayHit;
//			if(Physics.Raycast(ray ,out rayHit,1000f ,LayerMask.NameToLayer("Default")))
//			{
//				Debug.DrawLine(Camera.main.ScreenToViewportPoint(Input.mousePosition),rayHit.point,Color.green);
//			}
//		}

//
//	}


//	void FixedUpdate()
//	{
//		if(dominoThrowingState != throwingInProgress)
//			return ;
//
//
//		this.transform.position = Vector3.Lerp(throwingStartPosition,throwingStartPosition,throwingCounter * 0.01f);
//		throwingCounter  += Time.fixedDeltaTime ;
//		if(this.transform.position == throwingEndPosition)
//		{
//
//			print("domino piece hit the ground");
//
//			dominoThrowingState = throwingEnded;
//
//		}
//
//
//
//	}

	public void highLight()
	{
		//DominoGround.instance.isDraggingPiece = true ;
//		myRenderer[1].material = highlighMaterial;
//		myRenderer[2].material = highlighMaterial;
		upperRenderer.material = highlighMaterial;
		lowerRenderer.material = highlighMaterial;
		isHighlighted = true;
		intitialPosition  = new Vector3(transform.localPosition.x ,transform.localPosition.y,transform.localPosition.z) ;
		transform.position  =  new Vector3(transform.position.x ,transform.position.y + 1f,transform.position.z) ;
	}
	public void UnhighLight()
	{
		if(isHighlighted)
		{
//			myRenderer[1].material = normalMaterial;
//			myRenderer[2].material = normalMaterial;
			upperRenderer.material = normalMaterial;
			lowerRenderer.material = normalMaterial;
			isHighlighted = false;
			transform.localPosition  = intitialPosition;  
			DominoGround.instance.isDraggingPiece = false ;

		}
	}

	public void flush()
	{
		print(hasbeenFlushed);

		if(!hasbeenFlushed)
		{

		Destroy(this.transform.GetComponent<BoxCollider>());
		Destroy(upperPointAnchor.gameObject);
		Destroy(lowerPointAnchor.gameObject);
		Destroy(rightUpAnchor.gameObject);
		Destroy(rightMiddleAnchor.gameObject);
		Destroy(rightDownAnchor.gameObject);
		Destroy(leftUpAnchor.gameObject);
		Destroy(leftMiddleAnchor.gameObject);
		Destroy(leftDownAnchor.gameObject);
		}
		hasbeenFlushed = true;
	}


//	public void throwPieceOnGround(Vector3 endPosition ,bool isEnd1 ,bool isUpper)
//	{
//		throwingCounter = 0 ;
//		throwingStartPosition = this.transform.position ;
//		throwingEndPosition  = endPosition ;
//
//		this.isEnd1  = isEnd1 ;
//		this.isUpper = isUpper;
//
//		dominoThrowingState = throwingInProgress ;
//
//	}
//

	public void setFeasibleToPlay()
	{
		Invoke("waitToFeasible",1f);
	}

	public void waitToFeasible()
	{
		isFeasibleToPlay = true ;
		highLight();
	}

}
