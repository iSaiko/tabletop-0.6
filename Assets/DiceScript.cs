﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DiceScript : MonoBehaviour {

    private bool firstTouch;
    private bool isDiceRotating;
    float rotSpeed;
    Quaternion rot1 =  Quaternion.Euler(new Vector3(90f, -140f, 0f));
    Quaternion rot2 = Quaternion.Euler(new Vector3(0f, -140f, 0f));
    Quaternion rot3 = Quaternion.Euler(new Vector3(-90f, -140f, 0f));
    Quaternion rot4 = Quaternion.Euler(new Vector3(-180f, -140f, 0f));
    Quaternion rot5 = Quaternion.Euler(new Vector3(-180f, -230f, 0f));
    Quaternion rot6 = Quaternion.Euler(new Vector3(-180f, -50f, 0f));

    m_UNETSnake thisPlayerClient;
    Player thisPlayer;
	// Use this for initialization
	void Start () {
        thisPlayerClient = GameObject.Find("UNETSnakeObject").GetComponent<m_UNETSnake>();
        firstTouch = false;
        isDiceRotating = false;
        rotSpeed = 0.5f;
        if (thisPlayerClient.isHost)
        {
            thisPlayer = GameObject.Find("player1").GetComponent<Player>();
        }
        else
        {
            thisPlayer = GameObject.Find("player2").GetComponent<Player>();

        }
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        //transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(new Vector3(Random.Range(0, 180), Random.Range(0, 180), Random.Range(0, 180))), Time.deltaTime * rotSpeed);
        if (isDiceRotating)
            transform.Rotate(1 * rotSpeed * 50, 1 * rotSpeed * 5, 1 * rotSpeed * 25);
    }

    void OnMouseDown()
    {
        if (!thisPlayer.shouldMove)
        {
            if (!firstTouch)
            {
                isDiceRotating = true;
                firstTouch = !firstTouch;
            }
            else
            {
                SnakeBoard.instance.throwDice();
                Quaternion currentRot = Quaternion.Euler(new Vector3(transform.localRotation.x, transform.localRotation.y, transform.localRotation.z));

                switch (SnakeBoard.instance.diceRoll)
                {
                    case 1:
                        // Debug.Log(SnakeBoard.instance.diceRoll);
                        StartCoroutine(DiceRotationStop(currentRot, rot1));
                        break;
                    case 2:
                        //Debug.Log(SnakeBoard.instance.diceRoll);

                        StartCoroutine(DiceRotationStop(currentRot, rot2));

                        break;
                    case 3:
                        //  Debug.Log(SnakeBoard.instance.diceRoll);

                        StartCoroutine(DiceRotationStop(currentRot, rot3));

                        break;
                    case 4:
                        //  Debug.Log(SnakeBoard.instance.diceRoll);

                        StartCoroutine(DiceRotationStop(currentRot, rot4));

                        break;
                    case 5:
                        //  Debug.Log(SnakeBoard.instance.diceRoll);

                        StartCoroutine(DiceRotationStop(currentRot, rot5));

                        break;
                    case 6:
                        //   Debug.Log(SnakeBoard.instance.diceRoll);

                        StartCoroutine(DiceRotationStop(currentRot, rot6));

                        break;
                }
                isDiceRotating = false;
                firstTouch = !firstTouch;
            }
        }
    }
   


    void DiceRotationMethod()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(new Vector3(Random.Range(0, 180), Random.Range(0, 180), Random.Range(0, 180))), Time.deltaTime * rotSpeed);
    }

    IEnumerator DiceRotationStop(Quaternion startPosition, Quaternion endPosition)
    {
		float t = 0;
		while (t < 3f) {
			t += Time.deltaTime * 100 * 1;
           // Debug.Log("DiceRotationCoroutine");
			transform.localRotation = Quaternion.Slerp(startPosition, endPosition, t);
            if (transform.localRotation == endPosition)
                isDiceRotating = false;
			//Debug.Log (t);
			yield return null;
		}
		yield return 0;

        thisPlayer.shouldMove = true;
	
    }
}
