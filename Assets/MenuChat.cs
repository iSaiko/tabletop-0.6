﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;
public class MenuChat : MonoBehaviour {


    public Transform chatMessageContainer;
    public GameObject messagePrefab;
    m_UNETChess clientChess;
    public string currentPlayerName;
   // m_UNETSnake clientSL;
    //m_UNETDomino m_UNETDomino;
	// Use this for initialization
	void Start () {
        currentPlayerName = UiMenuValues.instance.userName;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ChatMessage(string msg)
    {
        string messageToShow = "";
        GameObject go = Instantiate(messagePrefab) as GameObject;
        //msg = msg.Replace(" ", "");
        string[] chatmsg = msg.Split(':');
        Debug.Log(chatmsg[0]+chatmsg[1]);
        //messageToShow = "<color=red>" + chatmsg[0] + "</color>" + chatmsg[1];
        //Debug.Log(UiMenuValues.instance.player1NameLabelWR.text);
		if(Rooms.instance.gameType == "chess"){
			if(chatmsg[0].Equals(m_UNETChess.instance.currentPlayerName))
                messageToShow = "<color=red><b>" + chatmsg[0] + "</b></color>" + " : " + ArabicFixer.Fix(chatmsg[1], true, true);
			else if (chatmsg[0].Equals(m_UNETChess.instance.secondPlayerName))
				messageToShow = "<color=blue><b>" + chatmsg[0] + "</b></color>" + " : " + ArabicFixer.Fix(chatmsg[1],true,true);
		}
		else if (Rooms.instance.gameType == "domino"){
		if(chatmsg[0].Equals(m_UNETDomino.instance.currentPlayerName))
            messageToShow = "<color=red><b>" + chatmsg[0] + "</b></color>" + " : " + ArabicFixer.Fix(chatmsg[1], true, true);
		else if (chatmsg[0].Equals(m_UNETDomino.instance.secondPlayerName))
            messageToShow = "<color=blue><b>" + chatmsg[0] + "</b></color>" + " : " + ArabicFixer.Fix(chatmsg[1], true, true);
		}
		else if(Rooms.instance.gameType == "snake"){
			if(chatmsg[0].Equals(m_UNETSnake.instance.currentPlayerName))
                messageToShow = "<color=red><b>" + chatmsg[0] + "</b></color>" + " : " + ArabicFixer.Fix(chatmsg[1], true, true);
			else if (chatmsg[0].Equals(m_UNETSnake.instance.secondPlayerName))
                messageToShow = "<color=blue><b>" + chatmsg[0] + "</b></color>" + " : " + ArabicFixer.Fix(chatmsg[1], true, true);
		}
	

        go.transform.SetParent(chatMessageContainer);
        go.transform.localScale = new Vector3(1f, 1f, 1f);
        Debug.Log(messageToShow);
        go.GetComponentInChildren<Text>().text = messageToShow;

    }

    public void SendChatMessage()
    {
        InputField i = GameObject.Find("MessageInput").GetComponent<InputField>();
        if (i.text == "")
            return;
        if (Rooms.instance.gameType == "chess")
        {
            m_UNETChess clientChess;
            clientChess = FindObjectOfType<m_UNETChess>();

			clientChess.UNETSend("CMMSG|"+ clientChess.currentPlayerName + ":" + i.text);
			clientChess.ClientChess("CMMSG|" + clientChess.currentPlayerName + ":" + i.text);
        }
        else if (Rooms.instance.gameType == "snake")
        {
            m_UNETSnake clientSL;
            clientSL = FindObjectOfType<m_UNETSnake>();
			clientSL.UNETSend("CMMSG|" + clientSL.currentPlayerName + ":" + i.text);
			clientSL.ClientSnake("CMMSG|" + clientSL.currentPlayerName + ":" + i.text);
        }
        else if (Rooms.instance.gameType == "domino")
        {
            m_UNETDomino clientTest;
            clientTest = FindObjectOfType<m_UNETDomino>();
			clientTest.UNETSend("CMMSG|" + clientTest.currentPlayerName + ":" + i.text);
			clientTest.ClientDomino("CMMSG|" + clientTest.currentPlayerName + ":" + i.text);
        }
         
        i.text = "";
    }
}
