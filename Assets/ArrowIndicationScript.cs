﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowIndicationScript : MonoBehaviour {

    private float floatSpeed = .75f;
    private float movementDistance = 5f;
    private float startingZ;
    private bool isMovingUp = true;

    void Start () {
        startingZ = transform.position.z;
	}
	
	// Update is called once per frame
	void Update () {
        Float();
	}

    private void Float()
    {


        float newZ = transform.position.z + (isMovingUp ? 1 : -1) * 2 * movementDistance * floatSpeed * Time.deltaTime;

        if (newZ > startingZ + movementDistance)
        {
            newZ = startingZ + movementDistance;
            isMovingUp = false;
        }
        else if (newZ < startingZ)
        {
            newZ = startingZ;
            isMovingUp = true;
        }

        transform.position = new Vector3(transform.position.x, transform.position.y, newZ);
    }
}
