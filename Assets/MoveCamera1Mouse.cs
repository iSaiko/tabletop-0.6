﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera1Mouse : MonoBehaviour {

	//
	// VARIABLES
	//
    Vector3 centerLookAt;
    GameObject gen;
    Camera Player1Camera;
    public GameObject BoardCenter;
	public float turnSpeed = 4.0f;		// Speed of camera turning when mouse moves in along an axis
	public float panSpeed = 4.0f;		// Speed of the camera when being panned
	public float zoomSpeed = 4.0f;		// Speed of the camera going back and forth
	
	private Vector3 mouseOrigin;	// Position of cursor when mouse dragging starts
	private bool isPanning;		// Is the camera being panned?
	private bool isRotating;	// Is the camera being rotated?
	private bool isZooming;		// Is the camera zooming?

    private Vector3 player1NormalCoord;
    private Quaternion player1NormalRot;

    private float posx,posy,posz;
    void Start()
    {
        //gen = GameObject.Find("Generator");
        //Invoke("applyInvoke", 0.01f);
        centerLookAt = BoardCenter.transform.position;
        Player1Camera = GameObject.Find("Player1Camera").GetComponent<Camera>();
        player1NormalCoord = new Vector3(-26f, 40f, -70.8f);
        player1NormalRot = Quaternion.Euler(47f, 0f, 0f);
        transform.localPosition = player1NormalCoord;
        transform.localRotation = player1NormalRot;
        //transform.LookAt(centerLookAt);
    }


	//
	// UPDATE
	//
	
	void Update () 
	{

		if(!ChessCameraMovementEnabling.isFreeMovement)
			return;


        float d = Input.GetAxis("Mouse ScrollWheel");
        
        posx = transform.position.x;
        posy = transform.position.y;
        posz = transform.position.z;
       // transform.LookAt(centerLookAt);
        //transform.localRotation = new Quaternion(Mathf.Clamp(transform.localRotation.x, 0f, 45), Mathf.Clamp(transform.localRotation.y, 0f, 45), Mathf.Clamp(transform.localRotation.z, 0f, 45), Mathf.Clamp(transform.localRotation.w, 0f, 45));
        //transform.LookAt(centerLookAt);
		// Get the right mouse button
        //if(Input.GetMouseButtonDown(1))
        //{
        //    // Get mouse origin
        //    mouseOrigin = Input.mousePosition;
        //    isRotating = true;
        //}
		
		// Get the left mouse button
		if(Input.GetMouseButtonDown(0))
		{
			// Get mouse origin
			mouseOrigin = Input.mousePosition;
			isPanning = true;
		}
		
		// Get the middle mouse button
		if(d != 0)
		{
			// Get mouse origin
			mouseOrigin = Input.mousePosition;
			isZooming = true;
//            Debug.Log(isZooming);
		}
		
		// Disable movements on button release
        //if (!Input.GetMouseButton(2)) isRotating=false;
		if (!Input.GetMouseButton(0)) isPanning=false;
		if (d == 0) isZooming=false;
		
		// Rotate camera along X and Y axis
        //if (isRotating)
        //{
        //        Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - mouseOrigin);

        //    transform.RotateAround(transform.position, transform.right, -pos.y * turnSpeed);
        //    transform.RotateAround(transform.position, Vector3.up, pos.x * turnSpeed);
        //}
		
		// Move the camera on it's XY plane
		if (isPanning)
		{
           
            Vector3 pos = Player1Camera.ScreenToViewportPoint(Input.mousePosition - mouseOrigin);

            
                //Vector3 move = new Vector3(pos.x * panSpeed, pos.y * panSpeed, 0);
            
                //transform.Translate(move, Space.Self);
                //transform.LookAt(centerLookAt);
//                Debug.Log("is Panning");
                transform.RotateAround(centerLookAt, transform.right, -pos.y * turnSpeed);
                transform.RotateAround(centerLookAt, Vector3.up, pos.x * turnSpeed);
                transform.LookAt(centerLookAt);

		}
		
		// Move the camera linearly along Z axis
		if (isZooming)
		{


            Vector3 pos = Player1Camera.ScreenToViewportPoint(Input.mousePosition - mouseOrigin);
            if (pos.y == 0)
                pos.y = 0.1f;
            if (pos.x > 40)
                pos.x = 40;
            if (d > 0)
            {
                Vector3 move = pos.y * 10 * zoomSpeed * transform.forward;
//                Debug.Log(pos.y + " " + d + " " + transform.forward);
                transform.Translate(move, Space.World);
            }
            if (d < 0)
            {
                Vector3 move = -pos.y * 10 * zoomSpeed * transform.forward;
//                Debug.Log(pos.y + " " + d + " " + transform.forward);
                transform.Translate(move, Space.World);
            }
//            Debug.Log(d);
        }
	}

    void LateUpdate()
    {
        if (isZooming)
        transform.LookAt(centerLookAt);

                transform.localPosition = new Vector3(Mathf.Clamp(transform.localPosition.x,-90,40), Mathf.Clamp(transform.localPosition.y, 0, 55), Mathf.Clamp(transform.localPosition.z, -90, -20));

    }
    //void applyInvoke()
    //{
    //    centerLookAt = new Vector3(gen.GetComponent<Generator>().maxDepth / 2, gen.GetComponent<Generator>().maxDepth / 2, gen.GetComponent<Generator>().maxDepth / 2);
    //    Debug.Log(centerLookAt);
    //    transform.LookAt(centerLookAt);
    //}

   
}
