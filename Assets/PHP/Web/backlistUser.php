<?php 
	include 'functions/functions.php';

	$user = new MOUser($logUser, $logPass, $logHost, $logDbName);
	if(!empty($_REQUEST['addPlayer'])){
		if($user->blacklistUser(urldecode($_REQUEST['id']), urldecode($_REQUEST['loginKey']), urldecode($_REQUEST['blacklistId']))){
			echo 'Success';
		} else {
			echo $user->errorMessage;
		}
	} else if(!empty($_REQUEST['addPlayerName'])) {		
		if($user->blacklistUser(urldecode($_REQUEST['id']), urldecode($_REQUEST['loginKey']), null, urldecode($_REQUEST['blacklistName']))){
			echo 'Success';
		} else {
			echo $user->errorMessage;
		}
	}  else if(!empty($_REQUEST['remove'])) {		
		if($user->removeFromBlacklist(urldecode($_REQUEST['id']), urldecode($_REQUEST['loginKey']), urldecode($_REQUEST['blacklistId']))){
			echo 'Success';
		} else {
			echo $user->errorMessage;
		}
	} else if(!empty($_REQUEST['search'])) {		
		if($user->searchBlacklist(urldecode($_REQUEST['id']), urldecode($_REQUEST['loginKey']))){
			echo $user->XMLoutput;
		} else {
			echo 'ERROR'.$user->errorMessage;
		}
	}
?>