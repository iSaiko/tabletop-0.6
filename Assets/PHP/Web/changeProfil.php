<?php 
	include 'functions/functions.php';	
	if(!empty($_REQUEST['changeData'])){
		$user = new MOUser($logUser, $logPass, $logHost, $logDbName);
		if($user->changeProfil(urldecode($_REQUEST['id']), 
				urldecode($_REQUEST['loginKey']),
				urldecode($_REQUEST['username']),
				urldecode($_REQUEST['mail']))){
			echo 'Success|'.$user->userName.'|'.$user->userMail;
		} else {
			echo $user->errorMessage;
		}
	} elseif(!empty($_REQUEST['changePass'])){
		$user = new MOUser($logUser, $logPass, $logHost, $logDbName);
		if($user->changePassword(urldecode($_REQUEST['id']),
				urldecode($_REQUEST['loginKey']),
				urldecode($_REQUEST['currentPassword']),
				urldecode($_REQUEST['newPassword']))){
			echo 'Success';
		} else {
			echo $user->errorMessage;
		}
	} 
?>