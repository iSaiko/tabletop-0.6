<?php 
	include 'functions/functions.php';
	
	$game = new MOGame($logUser, $logPass, $logHost, $logDbName);
	if(!empty($_REQUEST['changeStatus'])){
		if($game->updateGameStatus(urldecode($_REQUEST['id']), urldecode($_REQUEST['loginKey']), 
				urldecode($_REQUEST['gameId']), urldecode($_REQUEST['gameStatus']))){
			echo 'Success';
		} else {
			echo $game->errorMessage;
		}
	}
	
	if(!empty($_REQUEST['changeMap'])){
		if($game->updateGameMap(urldecode($_REQUEST['id']), urldecode($_REQUEST['loginKey']),
				urldecode($_REQUEST['gameId']), urldecode($_REQUEST['gameMap']))){
			echo 'Success';
		} else {
			echo $game->errorMessage;
		}
	}
	
	if(!empty($_REQUEST['addPlayer'])){
		if($game->addPlayer(urldecode($_REQUEST['id']), urldecode($_REQUEST['loginKey']),
				urldecode($_REQUEST['gameId']))){
			echo 'Success';
		} else {
			echo $game->errorMessage;
		}
	}
	
	if(!empty($_REQUEST['removePlayer']) || !empty($_REQUEST['removeServerPlayer'])){
		$isDedicated=false;
		if(!empty($_REQUEST['removeServerPlayer'])){
			$isDedicated = true;
		}
		if($game->removePlayer(urldecode($_REQUEST['id']), urldecode($_REQUEST['loginKey']),
				urldecode($_REQUEST['playerId']), urldecode($_REQUEST['gameId']), $isDedicated)){
			echo 'Success';
		} else {
			echo $game->errorMessage;
		}
	}
	
	if(!empty($_REQUEST['hostCall']) || !empty($_REQUEST['serverCall'])){
		$isDedicated=false;
		if(!empty($_REQUEST['serverCall'])){
			$isDedicated = true;
		}
		if($game->hostCall(urldecode($_REQUEST['id']), urldecode($_REQUEST['loginKey']),
				urldecode($_REQUEST['gameId']), $isDedicated)){
			echo 'Success';
		} else {
			echo $game->errorMessage;
		}
	}
