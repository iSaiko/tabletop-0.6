<?php 
	include 'functions/functions.php';
	$game = new MOGame($logUser, $logPass, $logHost, $logDbName);
	if(empty($_REQUEST['deleteServer']) && empty($_REQUEST['deleteServerGame'])){
		if($game->exitGame(urldecode($_REQUEST['id']), urldecode($_REQUEST['loginKey']))){
			if(!empty($_REQUEST['logout'])){
				if($game->logoutUser(urldecode($_REQUEST['id']), urldecode($_REQUEST['loginKey']))){
					echo 'Success';
				}else {
					echo $game->errorMessage.'|';
				}
			} else {
				echo 'Success';
			}		
		} else {
			echo $game->errorMessage;
		}
	} else if(!empty($_REQUEST['deleteServer'])){
		if($game->exitServer(urldecode($_REQUEST['id']), urldecode($_REQUEST['loginKey']))){
			echo 'Success';
		} else {
			echo $game->errorMessage;
		}
	} else if(!empty($_REQUEST['deleteServerGame'])){
		if($game->exitGameServer(urldecode($_REQUEST['id']), urldecode($_REQUEST['loginKey']),urldecode($_REQUEST['gameId']))){
			echo 'Success';
		} else {
			echo $game->errorMessage;
		}
	}
?>