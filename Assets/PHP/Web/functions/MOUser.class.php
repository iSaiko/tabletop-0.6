<?php
// MOUser : manage all interactions with the users MySQL table

class MOUser extends MODb{
	
	/**************** YOU CAN CHANGE THESE PARAMETERS ********************/
	// Sender e-mail :
	private $mailSender = 'islam.m@zoho.com';	
	
	// Email for account create :
	// Subject :
	private $mailRegisterSubject = 'Welcome to Tech World Games';
	// Message (in HTML)
	private $mailRegisterContent = '<span style="font-family:arial; font-size:14px">Welcome to Tech World Game demo, <br /><br />You are now registred with the following userName : <span style="font-weight:bold; color:#009BC1;">$userName</span><br/><br />
  We hope you\'ll enjoy Tech World Games which is available on Google Play Store <a href="https://www.google.com">here</a><br/><br />
  <b>Note</b> : for support or questions, don\'t send e-mails on this address <br>
  <br>
Thanks. <br /><br /><br/>Best regards,<br />
Islam Mohamed</span>';
	
	// Email for "forgotten username" :
	// Subject :
	private $mailUserNameSubject = 'Your username on Tech World Games ';
	// Message (in HTML)
	private $mailUserNameContent = '<span style="font-family:arial; font-size:14px">Hello,<br/><br />You are registred on Tech World Games with the following username : <span style="font-weight:bold; color:#009BC1;">$userName</span><br/><br /><br/>We hope you\'ll enjoy MultiOnline which is available on Unity Asset Store <a href="https://www.google.com">here</a><br/><br /><b>Note</b> : for support or questions, don\'t send e-mails on this address,<br />
<br />
 Thanks. <br /><br /><br/>Best regards, <br />Islam Mohamed</span>';
	
	// Email for "forgotten password" :
	// Subject :
	private $mailNewPassSubject= 'Your password on Tech World Games';
	// Message (in HTML) :
	private $mailNewPassContent = '<span style="font-family:arial; font-size:14px">Hello,<br/><br />This is your new password for login on Tech World Games : <span style="font-weight:bold; color:#009BC1;">$newPass</span><br/>Once you\'ll be login, you could change this password from the profil settings.<br/><br/><br/> We hope you\'ll enjoy our Games which is available on Google Play Store <a href="https://www.google.com">here</a><br/><br /><b>Note</b> : for support or questions, don\'t send e-mails on this address, <br />
<br />
Thanks. <br /><br /><br/>Best regards, <br />Islam Mohamed</span>';
	
	/************************************************************/
	private $errorMessage=null;
	private $userName;
	private $userMail;
	private $userGameStatus;
	protected $userId;
	protected $loginKey;
	protected $privateIP;
	protected $publicIP;
	
	// Constructor
	function __construct($user, $pass, $host, $dbName){
		parent::__construct($user, $pass, $host, $dbName);
	}
	
	// Destructor
	function __destruct(){
		parent::__destruct();
	}
	
	// Getter
	public function __get($nom){
		if(isset($this->$nom)){
			return $this->$nom;
		}
	}
	
	// Save a new user
	public function saveUser($userName, $mail, $pass, $privateIP, $sendMail) {		
		if(!empty($userName) && !empty($mail) && !empty($pass)) {
			if($this->nameFilter($userName) && $this->mailFilter($mail) && $this->passFilter($pass)){
				
				$checkUserName = $this->query_fetchObject('SELECT id FROM
					users WHERE userName=\''.secure_db($userName).'\'');
				
				$checkMail = $this->query_fetchObject('SELECT id FROM
					users WHERE mail=\''.secure_db($mail).'\'');
				
				if(empty($checkUserName->id) && empty($checkMail->id)) {
					$savePass = crypt(secure_prepare_db($pass));	
					$array = array(
						':id' => '',
						':userName' => secure_prepare_db($userName),
						':mail' => secure_prepare_db($mail),
						':privateIP' => secure_prepare_db($privateIP),
						':publicIP' => secure_prepare_db($_SERVER['REMOTE_ADDR']),
						':pass' => $savePass,						
						':login' => false,
						':loginKey' => null,
						':lastUpdate' => null
					);					
					$this->prepare_exec('INSERT INTO users VALUES
						(:id,:userName,:mail,:privateIP,:publicIP,:pass,NOW(),:login,:loginKey,:lastUpdate)', $array);
					
					if($sendMail == 1){
						// Send e-mail
						$message = str_replace('$userName', $userName, $this->mailRegisterContent);
						mail($mail, $this->mailRegisterSubject, $message, "From: $this->mailSender\nContent-Type: text/html;charset=utf-8");
					}
					
					return true;
				} else {
					if(!empty($checkUserName->id)) {					
						$this->errorMessage.= '|nameExist';
					}
					if(!empty($checkMail->id)){
						$this->errorMessage.= '|mailExist';
					}
				}
				
			} else {
				if(!$this->nameFilter($userName)){
					$this->errorMessage.= '|nameFilter';
				}
				if (!$this->mailFilter($mail)) {
					$this->errorMessage.= '|mailFilter';
				}
				if (!$this->passFilter($pass)) {
					$this->errorMessage.= '|passFilter';
				}
			}
		} else {
			if(empty($userName)){
				$this->errorMessage.= '|emptyName';
			}
			if(empty($mail)){
				$this->errorMessage.= '|emptyMail';
			}
			if(empty($pass)){
				$this->errorMessage.= '|emptyPass';
			}
		}
		return false;
	}
	
	// Login a user
	public function logUser($userName, $pass, $privateIP, $callInterval){		
		if(!empty($userName) && !empty($pass)){
			if($this->nameFilter($userName) && $this->passFilter($pass)){
				$check = $this->query_fetchObject('SELECT id, mail, pass, login, 				
						SECOND(lastUpdate) AS second,
						MINUTE(lastUpdate) AS minute,
						HOUR(lastUpdate) AS hour,
						MONTH(lastUpdate) AS month,	
						DAY(lastUpdate) AS day,		
						YEAR(lastUpdate) AS year
						FROM users WHERE userName=\''.secure_db($userName).'\'');
				if(!empty($check->id) && !empty($check->pass)){					
					$pass = crypt(secure_prepare_db($pass), display_db($check->pass));				
					if($pass == $check->pass && 
					(!$check->login ||
					mktime($check->hour, $check->minute, $check->second, $check->month, $check->day, $check->year) < time()-$callInterval)){
						/* Create the login key with the username and the current timestamp.
						 * The obtained string is after hashed with cryp() function.
						 *  In this way, we are quite sure that the login key is unique and irreproducible */
						$this->loginKey =  $userName.time();
						$this->loginKey = crypt($this->loginKey);
						
						$array = array(
							':id' => $check->id,
							':privateIP' => secure_prepare_db($privateIP),
							':publicIP' => secure_prepare_db($_SERVER['REMOTE_ADDR']),
							':login' => true,
							':loginKey' => secure_prepare_db($this->loginKey)
						);			
						$this->prepare_exec('UPDATE users SET privateIP=:privateIP, publicIP=:publicIP, 
								login=:login, loginKey=:loginKey, lastUpdate=NOW() WHERE id=:id', $array);
						$this->userId = $check->id;
						$this->userName = $userName;
						$this->privateIP = $privateIP;
						$this->publicIP = $_SERVER['REMOTE_ADDR'];
						$this->userMail = $check->mail;
						return true;
					} else {
						if($pass != $check->pass){
							$this->errorMessage.= '|errorPass';
						}
						if($check->login && 
						 mktime($check->hour, $check->minute, $check->second, $check->month, $check->day, $check->year) >= time()-$callInterval){
							$this->errorMessage.= '|errorAlreadyLog';
						}
					}
				} else {
					$this->errorMessage.= '|errorName';
				}
			} else {
				if(!$this->nameFilter($userName)){
					$this->errorMessage.= '|nameFilter';
				}				
				if (!$this->passFilter($pass)) {
					$this->errorMessage.= '|passFilter';
				}
			}			
		} else {
			if(empty($userName)){
				$this->errorMessage.= '|emptyName';
			}			
			if(empty($pass)){
				$this->errorMessage.= '|emptyPass';
			}
		}
		return false;
	}
	
	// Update the field LastUpdate
	public function playerCall($userId, $loginKey){
		if($this->checkUser($userId, $loginKey)){		
			$array = array(':id' => $this->userId);
			$this->prepare_exec('UPDATE users SET lastUpdate=NOW() WHERE id=:id', $array);
			return true;
		} else {
			$this->errorMessage.= '|badGameId';
		}		
		return false;
	}
	
	// If a user have forgot his username or password
	public function frogotLogin($mail, $sendUserName, $forgotPass){
		if(!empty($mail) && $this->mailFilter($mail)){
			$check = $this->query_fetchObject('SELECT id, userName FROM users WHERE mail =\''.secure_db($mail).'\'');
			if(!empty($check->id)){
				if($sendUserName == 1){
					// Send e-mail
					$message = str_replace('$userName', display_db($check->userName), $this->mailUserNameContent);
					mail($mail, $this->mailUserNameSubject, $message, "From: $this->mailSender\nContent-Type: text/html;charset=utf-8");
				}
				if($forgotPass == 1){	
					// Define new password
					$newPass = rand(10000, 99999);
					$newPassCrypt = crypt($newPass);
					$array = array(':id' => $check->id, ':pass'=> secure_prepare_db($newPassCrypt));
					$this->prepare_exec('UPDATE users SET pass=:pass WHERE id=:id', $array);
					
					// Send e-mail
					$message = str_replace('$newPass',$newPass, $this->mailNewPassContent);
					mail($mail, $this->mailNewPassSubject, $message, "From: $this->mailSender\nContent-Type: text/html;charset=utf-8");
				}
				return true;
			} else {
				$this->errorMessage.= '|errorUserMail';
			}
		} else {
			if(empty($mail)){
				$this->errorMessage.= '|emptyMail';
			}
			if(!$this->mailFilter($mail)){
				$this->errorMessage.= '|mailFilter';
			}
		}
		return false;
	}

	// Logout user
	public function logoutUser($id, $loginKey){
		if($this->checkUser($id, $loginKey)){
			$array = array(':id'=> $this->userId, ':login'=>false, ':loginKey'=>null);
			$this->prepare_exec('UPDATE users SET login=:login,loginKey=:loginKey WHERE id=:id', $array);
			return true;
		} else {					
			return false;
		}
	}
	
	// Change the username or email of a user
	public function changeProfil($id, $loginKey, $userName=null, $mail=null){
		if($this->checkUser($id, $loginKey)){			
			if(!empty($userName) && $this->nameFilter($userName)){				
				$checkUserName = $this->query_fetchObject('SELECT id FROM
					users WHERE id <> \''.$this->userId.'\' AND userName=\''.secure_db($userName).'\'');
					
				if(empty($checkUserName->id)){
					$this->userName =  secure_prepare_db($userName);				
					$array = array(
							':id' => $this->userId,
							':userName' => $this->userName);						
					$this->prepare_exec('UPDATE users SET userName=:userName WHERE id=:id', $array);
				} else {
					$this->errorMessage.= '|nameExist';
					return false;
				}
			} else {
				if(!empty($userName) && !$this->nameFilter($userName)){
					$this->errorMessage.= '|errorName';
					return false;
				}
			}
			if(!empty($mail) && $this->mailFilter($mail)){
				$checkMail = $this->query_fetchObject('SELECT id FROM
					users WHERE id <> \''.$this->userId.'\' AND mail=\''.secure_db($mail).'\'');
					
				if(empty($checkMail->id)){	
					$this->userMail =  secure_prepare_db($mail);
					$array = array(
							':id' => $this->userId,
							':mail' => $this->userMail);
						
					$this->prepare_exec('UPDATE users SET mail=:mail WHERE id=:id', $array);
				} else {
					$this->errorMessage.= '|mailExist';
					return false;
				}
			} else {
				if(!empty($mail) && !$this->mailFilter($mail)){
					$this->errorMessage.= '|errorMail';
					return false;
				}
			}
		} else {
			return false;
		}		
		return true;
	}
	
	// Change the password of a user
	public function changePassword($id, $loginKey, $password, $newPassword){
		if($this->checkUser($id, $loginKey)){
			$check = $this->query_fetchObject('SELECT pass FROM users WHERE id = \''.$this->userId.'\'');
			$pass = crypt(secure_prepare_db($password), display_db($check->pass));
			if($pass == $check->pass && $this->passFilter($newPassword)){
				$array = array(':id' => $this->userId, ':pass' => secure_prepare_db(crypt($newPassword)));
				$this->prepare_exec('UPDATE users SET pass=:pass WHERE id=:id', $array);
				return true;
			} else {
				if($pass != $check->pass){
					$this->errorMessage.= '|errorPass';
				}
				if(!$this->passFilter($newPassword)){
					$this->errorMessage.= '|passFilter';
				}
			}
		}
		return false;	
	}
			
	public function blacklistUser($id, $loginKey, $blacklistId=null, $blacklistName=null){
		if($this->checkUser($id, $loginKey)){
			if($blacklistId != null) {
				$check = $this->query_fetchObject('SELECT id FROM users WHERE id = \''.secure_db($blacklistId).'\'');
				if(!empty($check->id)){
					$array = array(':users_id' => $this->userId, ':excludeUser_id' => $check->id);
					$this->prepare_exec('INSERT INTO users_exclusions VALUES(:users_id, :excludeUser_id, NOW())', $array);
					return true;
				} else {
					$this->errorMessage.= '|babBlacklistId';
				}
			} elseif($blacklistName != null) {
				$check = $this->query_fetchObject('SELECT id FROM users WHERE userName  = \''.secure_db($blacklistName).'\'');
				if(!empty($check->id)){
					$array = array(':users_id' => $this->userId, ':excludeUser_id' => $check->id);
					$this->prepare_exec('INSERT INTO users_exclusions VALUES(:users_id, :excludeUser_id, NOW())', $array);
					return true;
				} else {
					$this->errorMessage.= '|badBlacklistName';
				}
			}
		}
		return false;	
	}	
	
	public function removeFromBlacklist($id, $loginKey, $blacklistId){
		if($this->checkUser($id, $loginKey)){
			$check = $this->query_fetchObject('SELECT excludeUser_id 
					FROM users_exclusions WHERE users_id =\''.$this->userId.'\' AND excludeUser_id = \''.secure_db($blacklistId).'\'');
			if(!empty($check->excludeUser_id)){
				$array = array(':excludeUser_id' => $check->excludeUser_id);
				$this->prepare_exec('DELETE FROM users_exclusions WHERE excludeUser_id=:excludeUser_id', $array);
				return true;
			} else {
				$this->errorMessage.= '|badBlacklistId';
			}			
		}
		return false;
	}
	
	public function searchBlacklist($id, $loginKey){
		if($this->checkUser($id, $loginKey)){
			$count = $this->query_fetchObject('SELECT COUNT(*) AS total FROM users_exclusions 
					WHERE users_id = \''.$this->userId.'\'');
			if($count->total > 0){
				$blacklist = $this->query_fetchAll('SELECT
				ue.excludeUser_id AS id,				
				DATE_FORMAT(ue.dateReg, \'%m/%d/%Y\') AS listRegister,
				u.userName AS userName						
				FROM users_exclusions ue INNER JOIN users u
				ON (ue.excludeUser_id = u.id)
				WHERE ue.users_id = \''.$this->userId.'\'
				ORDER BY ue.dateReg DESC');
				
				$this->XMLoutput = null;
				foreach($blacklist as $value){
					$this->XMLoutput.= '<entry id="'.utf8_encode($value['id']).'">';
					$this->XMLoutput.= '<userName>'.utf8_encode(display_db($value['userName'])).'</userName>';
					$this->XMLoutput.= '<date>'.utf8_encode(display_db($value['listRegister'])).'</date>';
					$this->XMLoutput.= '</entry>';
				}
				$this->XMLoutput = '<blackList>'.$this->XMLoutput.'</blackList>';
				return true;
				
			} 
			$this->errorMessage = '|noBlacklist';
		}
		return false;
	}
	
	public function addInFriendlist($id, $loginKey, $friendId){
		if($this->checkUser($id, $loginKey)){
			if($friendId != null) {
				$check = $this->query_fetchObject('SELECT id FROM users WHERE id = \''.secure_db($friendId).'\'');
				if(!empty($check->id)){
					$array = array(':users_id1' => $this->userId, ':users_id2' => $check->id);
					$this->prepare_exec('INSERT INTO users_friends VALUES(:users_id1, :users_id2, NOW())', $array);
					return true;
				} else {
					$this->errorMessage.= '|badFriendId';
				}
			}
		}
		return false;
	}
	
	public function removeFromFriendlist($id, $loginKey, $friendId){
		if($this->checkUser($id, $loginKey)){
			$check = $this->query_fetchObject('SELECT users_id1, users_id2 FROM users_friends 
					WHERE (users_id1 =\''.$this->userId.'\' AND users_id2 = \''.secure_db($friendId).'\') 
					OR (users_id2 =\''.$this->userId.'\' AND users_id1 = \''.secure_db($friendId).'\')');
			
			if(!empty($check->users_id1) AND !empty($check->users_id2)){
				if($check->users_id1 == $this->userId){
					$array = array(':users_id1' => $this->userId, ':users_id2' => $check->users_id2);
				} else {
					$array = array(':users_id1' => $check->users_id1, ':users_id2' => $this->userId);
				}				
				
				$this->prepare_exec('DELETE FROM users_friends WHERE users_id1=:users_id1 AND users_id2=:users_id2', $array);
				return true;
			} else {
				$this->errorMessage.= '|babFriendId';
			}
		}
		return false;
	}
	
	public function searchFriendlist($id, $loginKey){
		if($this->checkUser($id, $loginKey)){
			$count = $this->query_fetchObject('SELECT COUNT(*) AS total FROM users_friends
					 WHERE users_id1 = \''.$this->userId.'\' OR users_id2 = \''.$this->userId.'\'');
			if($count->total > 0){
				$friendlist = $this->query_fetchAll('SELECT
				f.users_id1 AS id1,
				f.users_id2 AS id2,
				DATE_FORMAT(f.dateReg, \'%m/%d/%Y\') AS listRegister,
				u.userName AS userName,
				u.login AS login,
				u2.userName AS userName2,
				u2.login AS login2,						
				ug.game_id AS gameId,
				ug2.game_id AS gameId2,	
						
				g.userHost_id AS gameHostId,
				g.server_id AS serverId,
				g.name AS gameName,
				g.port AS gamePort,	
				g.map AS gameMap,	
				g.maxPlayer AS gameMaxPlayer,	
				g.usePass AS gameUsePass,	
				g.status AS gameStatus,
				DATE_FORMAT(g.dateReg, \'%H:%i\') AS gameRegister,			
				g.dateReg AS gameRegisterDate,	
				g.maxPlayer AS maxPlayer,
				COUNT(uhg.user_id) AS totalPlayer,
					
				g2.userHost_id AS gameHostId2,
				g2.server_id AS serverId2,
				g2.name AS gameName2,
				g2.port AS gamePort2,	
				g2.map AS gameMap2,	
				g2.maxPlayer AS gameMaxPlayer2,	
				g2.usePass AS gameUsePass2,	
				g2.status AS gameStatus2,
				DATE_FORMAT(g2.dateReg, \'%H:%i\') AS gameRegister2,			
				g2.dateReg AS gameRegisterDate2,	
				COUNT(uhg2.user_id) AS totalPlayer2														
									
				FROM users_friends f
				INNER JOIN users u ON (f.users_id1 = u.id)
				INNER JOIN users u2 ON (f.users_id2 = u2.id)
				LEFT JOIN users_has_games ug ON (f.users_id1 = ug.user_id)
				LEFT JOIN users_has_games ug2 ON (f.users_id2 = ug2.user_id)	
				LEFT JOIN games g ON (ug.game_id = g.id)		
				LEFT JOIN users_has_games uhg ON (g.id = uhg.game_id)		
				LEFT JOIN games g2 ON (ug2.game_id = g2.id)			
				LEFT JOIN users_has_games uhg2 ON (g2.id = uhg2.game_id)
				WHERE f.users_id1 = \''.$this->userId.'\' OR f.users_id2 = \''.$this->userId.'\'
				GROUP BY f.users_id1, f.users_id2');
	
				
				$this->XMLoutput = null;
				foreach($friendlist as $value){
					if($value['id1'] == $this->userId){
						$id = $value['id2'];
						$username = $value['userName2'];
						$login = $value['login2'];
						$gameId = $value['gameId2'];
						$gameHostId=$value['gameHostId2'];
						$gameName = $value['gameName2'];
						$gamePort = $value['gamePort2'];
						$gameMap = $value['gameMap2'];
						$gameMaxPlayer = $value['gameMaxPlayer2'];
						$gameUsePass = $value['gameUsePass2'];
						$gameStatus = $value['gameStatus2'];
						$gameRegister = $value['gameRegister2'];
						$gameRegisterDate = $value['gameRegisterDate2'];						
						$totalPlayer = $value['totalPlayer2'];			
						$hostId = $value['gameHostId2'];
						$serverId = $value['serverId2'];
						if(!empty($value['maxPlayer2']) && $value['maxPlayer2'] != null) {
							$gameMaxPlayers = $value['maxPlayer2'];
						} else {
							$gameMaxPlayers=0;
						}
					} else {
						$id = $value['id1'];
						$username = $value['userName'];
						$login = $value['login'];
						$gameId = $value['gameId'];
						$gameHostId=$value['gameHostId'];
						$gameName = $value['gameName'];
						$gamePort = $value['gamePort'];
						$gameMap = $value['gameMap'];
						$gameMaxPlayer = $value['gameMaxPlayer'];
						$gameUsePass = $value['gameUsePass'];
						$gameStatus = $value['gameStatus'];
						$gameRegister = $value['gameRegister'];
						$gameRegisterDate = $value['gameRegisterDate'];
						$totalPlayer = $value['totalPlayer'];
						$hostId = $value['gameHostId'];
						$serverId = $value['serverId'];
						if(!empty($value['maxPlayer']) && $value['maxPlayer'] != null) {
							$gameMaxPlayers = $value['maxPlayer'];
						} else {
							$gameMaxPlayers=0;
						}				
					}	
					
					if($hostId == null || $hostId == '') {
						$hostData = $this->query_fetchObject('SELECT name, privateIp, publicIp FROM servers WHERE id = \''.$serverId.'\'');
					} else {
						$hostData = $this->query_fetchObject('SELECT userName AS name, privateIP as privateIp, publicIP AS publicIp FROM users WHERE id = \''.$hostId.'\'');
					}
					$hostName = $hostData->name;
					$hostPrivateIp = $hostData->privateIp;
					$hostPublicIp = $hostData->publicIp;						
					
					$exclude=0;
					$checkExclusion = $this->query_fetchObject('SELECT users_id FROM users_exclusions
					 WHERE users_id = \''.$gameHostId.'\' AND excludeUser_id = \''.$this->userId.'\'');
					if(!empty($checkExclusion->users_id)){
						$exclude=1;
					}	
					
					if(empty($gameId)){
						$gameId=0;
					}
					
					$this->XMLoutput.= '<entry id="'.utf8_encode($id).'">';
					$this->XMLoutput.= '<userName>'.utf8_encode(display_db($username)).'</userName>';
					$this->XMLoutput.= '<isLogin>'.utf8_encode(display_db($login)).'</isLogin>';				
					$this->XMLoutput.= '<dateFriend>'.utf8_encode(display_db($value['listRegister'])).'</dateFriend>';
					$this->XMLoutput.= '<isExcludeByHost>'.utf8_encode(display_db($exclude)).'</isExcludeByHost>';
					$this->XMLoutput.= '<game id="'.utf8_encode(display_db($gameId)).'">';
						$this->XMLoutput.= '<type>1</type>';
						$this->XMLoutput.= '<name>'.utf8_encode(display_db($gameName)).'</name>';
						$this->XMLoutput.= '<port>'.utf8_encode(display_db($gamePort)).'</port>';
						$this->XMLoutput.= '<map>'.utf8_encode(display_db($gameMap)).'</map>';
						$this->XMLoutput.= '<currentPlayers>'.utf8_encode($totalPlayer).'</currentPlayers>';
						$this->XMLoutput.= '<maxPlayer>'.utf8_encode(display_db($gameMaxPlayer)).'</maxPlayer>';
						$this->XMLoutput.= '<usePass>'.utf8_encode(display_db($gameUsePass)).'</usePass>';
						$this->XMLoutput.= '<status>'.utf8_encode(display_db($gameStatus)).'</status>';
						$this->XMLoutput.= '<register>'.utf8_encode(display_db($gameRegister)).'</register>';
						$this->XMLoutput.= '<registerDate>'.utf8_encode(display_db($gameRegisterDate)).'</registerDate>';
						$this->XMLoutput.= '<hostId>'.utf8_encode(display_db($gameHostId)).'</hostId>';
						$this->XMLoutput.= '<hostName>'.utf8_encode(display_db($hostName)).'</hostName>';
						$this->XMLoutput.= '<hostLocation></hostLocation>';
						$this->XMLoutput.= '<hostPrivateIp>'.utf8_encode(display_db($hostPrivateIp)).'</hostPrivateIp>';
						$this->XMLoutput.= '<hostPublicIp>'.utf8_encode(display_db($hostPublicIp)).'</hostPublicIp>';
					$this->XMLoutput.='</game>';					
					$this->XMLoutput.= '</entry>';
				}
				$this->XMLoutput = '<friendList>'.$this->XMLoutput.'</friendList>';
				return true;
	
			}
			$this->errorMessage = '|noFriendlist';
		}
		return false;
	}
		
	// Check if a user is login (call before do any action when we received a query for a user)
	protected function checkUser($id, $loginKey, $isServer=false){
		if(!empty($id) && !empty($loginKey)){			
			if(!$isServer){
				$check = $this->query_fetchObject('SELECT id, publicIP, loginKey from users WHERE id=\''.secure_db($id).'\'');
			} else {
				$check = $this->query_fetchObject('SELECT id, publicIP, loginKey from servers WHERE id=\''.secure_db($id).'\'');
			}
			if(!empty($check->id) && $check->loginKey == secure_db($loginKey)){
				$this->userId = $check->id;
				$this->publicIP = $check->publicIP;
				$this->loginKey = $check->loginKey;								
				return true;
			} else {
				if(empty($check->id)){
					$this->errorMessage.= '|badId';
				}
				if($check->loginKey != secure_db($loginKey)){
					$this->errorMessage.= '|badLoginKey';
				}
			}
		}else {
			if(empty($id)){
				$this->errorMessage.= '|emptyId';
			}
			if(empty($loginKey)){
				$this->errorMessage.= '|emptyLoginKey';
			}
		}
		return false;
	}
	
	/********************************** FILTERS *******************************/	
	protected function mailFilter($var){
		if(is_string($var) AND strlen($var) < 100){
			if(preg_match('#^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-zA-Z]{2,4}$#', $var)){
				return TRUE;
			}else	{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}
	
	protected function passFilter($var){
		if(is_string($var) AND strlen($var) < 100 AND strlen($var) > 3){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	protected function nameFilter($var){
		if(is_string($var) AND strlen($var) < 100){
			if(preg_match('#^[a-zA-Z0-9_ ]+$#', $var)){
				return TRUE;
			}else	{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}
}
?>