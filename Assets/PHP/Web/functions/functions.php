<?php	

error_reporting(0);
	include 'logData.php';
	include 'MODb.class.php';	
	include 'MOUser.class.php';
	include 'MOGame.class.php';
	include 'MOServer.class.php';
	
	// Secure data before save it on MySQL
	function secure_db($var){	 
		 $var = trim($var);
		 $var = preg_replace('#</?|>#', '', $var);
		 $var = addcslashes($var, '%_()');	
		 $var = htmlentities($var, ENT_QUOTES, 'UTF-8');	
		 $var = preg_replace('#</?script>#', '', $var);
		 $var = preg_replace('#java|javascript|ajax#i', '', $var);		
		 return $var;
	}
	
	/// Secure data before save it on MySQL (for prepared query)
	function secure_prepare_db($var){	 
		 $var = trim($var);
		 $var = preg_replace('#</?|>#', '', $var);
		 $var = addcslashes($var, '%_()');	
		 $var = htmlentities($var, ENT_QUOTES, 'UTF-8');	
		 $var = preg_replace('#</?script>#', '', $var);
		 $var = preg_replace('#java|javascript|ajax#i', '', $var);		
		 return $var;
	}
	
	//Prepare MySQL data to be displayed
	function display_db($var){
		 $var = stripslashes($var);
		 return $var;
	}
	
?>