<?php
// MOServer : manage all interactions with the server MySQL table

class MOServer extends MOUser {
	
	/************************************************************/
	private $errorMessage;
	private $name;
	private $location;
	private $adminMail;

	// Constructor
	function __construct($user, $pass, $host, $dbName){
		parent::__construct($user, $pass, $host, $dbName);
	}
	
	// Destructor
	function __destruct(){
		parent::__destruct();
	}
	
	// Getter
	public function __get($nom){
		if(isset($this->$nom)){
			return $this->$nom;
		}
	}
	
	// Save a new user
	public function saveServer($name, $mail, $pass, $privateIP) {	
		$this->errorMessage=null;
		if(!empty($name) && !empty($mail)  && !empty($pass)) {
			
			if($this->nameFilter($name)  && $this->mailFilter($mail) && $this->passFilter($pass)){
				
				$checkName = $this->query_fetchObject('SELECT id FROM
					servers WHERE name=\''.secure_db($name).'\'');
				
				if(empty($checkName->id)) {
					$savePass = crypt(secure_prepare_db($pass));	
					$array = array(
						':id' => '',
						':name' => secure_prepare_db($name),
						':pass' => $savePass,
						':adminMail' => secure_prepare_db($mail),
						':privateIP' => secure_prepare_db($privateIP),
						':publicIP' => secure_prepare_db($_SERVER['REMOTE_ADDR']),
						':login' => false,
						':loginKey' => NULL
					);					
					$this->prepare_exec('INSERT INTO servers VALUES
						(:id,:name,:pass,:adminMail,:privateIP,:publicIP,:login,:loginKey,NOW())', $array);
					return true;
				} else {
					$this->errorMessage.= '|nameExist';
				}
				
			} else {
				if(!$this->nameFilter($name)){
					$this->errorMessage.= '|nameFilter';
				}
				if (!$this->mailFilter($mail)) {
					$this->errorMessage.= '|mailFilter';
				}
				if (!$this->passFilter($pass)) {
					$this->errorMessage.= '|passFilter';
				}
			}
		} else {
			if(empty($name)){
				$this->errorMessage.= '|emptyName';
			}
			if(empty($mail)){
				$this->errorMessage.= '|emptyMail';
			}
			if(empty($pass)){
				$this->errorMessage.= '|emptyPass';
			}
		}
		return false;
	}
	
	public function logServer($name, $pass){
		$this->errorMessage=null;
		if(!empty($name) && !empty($pass)){
			if($this->nameFilter($name) && $this->passFilter($pass)){
				$check = $this->query_fetchObject('SELECT * FROM servers WHERE name=\''.secure_db($name).'\'');
				if(!empty($check->id)){
					$pass = crypt(secure_prepare_db($pass), display_db($check->pass));
					if($pass == $check->pass){
						$this->userId = $check->id;
						$this->name= display_db($check->name);
						$this->adminMail = display_db($check->adminMail);
						$this->publicIp = display_db($check->publicIp);
						$this->loginKey =  $userName.time();
						$this->loginKey = crypt($this->loginKey);						
						$array = array(
								':id' => $check->id,
								':publicIP' => secure_prepare_db($_SERVER['REMOTE_ADDR']),
								':login' => true,
								':loginKey' => secure_prepare_db($this->loginKey)
						);
						$this->prepare_exec('UPDATE servers SET publicIP=:publicIP,
								login=:login, loginKey=:loginKey WHERE id=:id', $array);
						return true;						
					} else {
						$this->errorMessage='|badPass';
					}
				} else {
					$this->errorMessage='|badName';
				}		
			} else {
				if(!$this->nameFilter($name)){
					$this->errorMessage.= '|nameFilter';
				}				
				if (!$this->passFilter($pass)) {
					$this->errorMessage.= '|passFilter';
				}
				}	
		} else {
			if(empty($name)){
				$this->errorMessage.= '|emptyName';
			}					
			if(empty($pass)){
				$this->errorMessage.= '|emptyPass';
			}
		}
		return false;
	}
	
	public function changeServerProfil($id, $loginKey, $name, $mail) {
		$this->errorMessage=null;
		if($this->checkServer($id, $loginKey)){
			if(!empty($name) && !empty($location) && !empty($mail)) {
				if($this->nameFilter($name) && $this->mailFilter($mail)){
					$checkName = $this->query_fetchObject('SELECT id FROM
							servers WHERE name=\''.secure_db($name).'\' AND id<>\''.$this->userId.'\'');
			
						if(empty($checkName->id)) {
							$array = array(
									':id' => $this->userId,
									':name' => secure_prepare_db($name),
									':adminMail' => secure_prepare_db($mail)
							);
							$this->prepare_exec('UPDATE servers SET
								name=:name, location=:location, adminMail=:adminMail
								WHERE id=:id', $array);
							return true;
						} else {					
							$this->errorMessage.= '|nameExist';					
						}
					} else {
						if(!$this->nameFilter($name)){
							$this->errorMessage.= '|nameFilter';
						}						
						if (!$this->mailFilter($mail)) {
							$this->errorMessage.= '|mailFilter';
						}				
				}			
			} else {
				if(empty($name)){
					$this->errorMessage.= '|emptyName';
				}
				if(empty($location)){
					$this->errorMessage.= '|emptyLocation';
				}
				if(empty($mail)){
					$this->errorMessage.= '|emptyMail';
				}		
			} 	
		} else {}	
		return false;
	}
	
	// Change the password of a user
	public function changeServerPass($id, $loginKey, $password, $newPassword){
		$this->errorMessage=null;
		if($this->checkServer($id, $loginKey)){
			if(!empty($password) && !empty($newPassword)) {		
				if($this->passFilter($newPassword)){
					$checkPass = $this->query_fetchObject('SELECT pass FROM servers WHERE id = \''.$this->userId.'\'');
					$pass = crypt(secure_prepare_db($password), display_db($checkPass->pass));
					if($pass == $checkPass->pass){
						$array = array(':id' => $this->userId, ':pass' => secure_prepare_db(crypt($newPassword)));
						$this->prepare_exec('UPDATE servers SET pass=:pass WHERE id=:id', $array);
						return true;
					} else {
						$this->errorMessage.= '|errorPass';					
					}
				} else {	
					if(empty($checkExist->id)){			
						$this->errorMessage.= '|errorId';
					}
					if(!$this->passFilter($newPassword)){
						$this->errorMessage.= '|passFilter';
					}
				}
			} else {
				$this->errorMessage.= '|emptyPass';
			}
		} else {}
		return false;
	}
	

	// Check if a user is login (call before do any action when we received a query for a user)
	private function checkServer($id, $loginKey){
		if(!empty($id) && !empty($loginKey)){			
			$check = $this->query_fetchObject('SELECT id, name, adminMail, publicIP, loginKey from servers WHERE id=\''.secure_db($id).'\'');			
			if(!empty($check->id) && $check->loginKey == secure_db($loginKey)){
				$this->userId = $check->id;
				$this->name = display_db($check->name);
				$this->adminMail = display_db($check->adminMail);
				$this->publicIP = $check->publicIP;
				$this->loginKey = $check->loginKey;
				return true;
			} else {
				if(empty($check->id)){
					$this->errorMessage.= '|badId';
				}
				if($check->loginKey != secure_db($loginKey)){
					$this->errorMessage.= '|badLoginKey';
				}
			}
		}else {
			if(empty($id)){
				$this->errorMessage.= '|emptyId';
			}
			if(empty($loginKey)){
				$this->errorMessage.= '|emptyLoginKey';
			}
		}
		return false;
	}
}
?>