<?php
/* MOMysql : this PHP class is used for the connections and queries to MySQL
 * This class is the mother of all others
 */
 
class MOMysql {
	
	/**************** YOU MUST COMPLETE THESE PARAMETERS ********************/
	// Enter here your database username :
	private $user = '75468';
	// Enter here your database passwords :
	private $pass = 'adzx48jk7';
	// Enter here your database server (it's often "localhost") :
	private $host = 'mysql2.alwaysdata.com';
	// Enter here the name of your database :
	private $dbName = 'inteldesign_02';	
	/*********************************************************************/
	
	private $connect;
	protected $dbResult;
		 
	// Connect to MySQL	
	function __construct(){
		mysql_connect($host, $user, $pass, $dbName);					
	}
			
	function __destruct(){
		$this->close();
	}
		
	// Close connect
	public function close(){
	 	mysql_close();
	}
		
		
	// Simple query to MySQL
	public function query($query){
		try{
			$data = mysql_query ($query);
			return $data;
		}catch(PDOException $e){
			$this->getError($e);
		}	
	}		
		
	// Get the last saved ID
	public function lastId(){
		return mysql_insert_id ();
	} 
		
	public function fetch($query){
		try{
			$data =mysql_fetch_row($query) ;
			return $data;			
		}catch(PDOException $e){
			$this->getError($e);
		}	
	}
			
	// Return the result of a query with fetchObject 
	public function fetchObjet($query){
		try{
			$data = mysql_fetch_object($query);					
			return $data;			
		}catch(PDOException $e){
			$this->getError($e);
		}	
	}
		
	// Return the result of a query with fetchAll
	public function fetchAll($query){
		try{
			$data = mysql_fetch_array($query, MYSQL_ASSOC);
			return $data;
		}catch(PDOException $e){
			$this->getError($e);
		}	
	}
		
	// Simpe exec 
	public function exec($var){
		try{
			$query = mysql_query($var);
		}catch(PDOException $e){
			$this->getError($e);
		}	
	}
	
	// Query MySQL return with fetchAll
	public function query_fetchAll($var){		 
		try{
			$query =  $this->query($var);
			$data = $this->fetchAll($query);
			 return $data;
		}catch(PDOException $e){
			$this->getError($e);
		}	
	}
	
	// Query MySQL return with fetchObject
	public function query_fetchObject($var){		 
		try{
			$query =  $this->query($var);
			$data = $this->fetchObjet($query);
			return $data;
		}catch(PDOException $e){
			$this->getError($e);
		}	
	}
			
	// Pepare and execute a query
	public function prepare_exec($var, $var_array){
		try{
			$query = mysql_query($var);
		}catch(PDOException $e){
			$this->getError($e);
		}
	}	
}	
 ?>