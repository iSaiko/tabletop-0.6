<?php 
// MOGame : manage all interactions with the games MySQL table
class MOGame extends MOUser{

	private $gameId;
	private $gameName;
	private $gamePort;
	private $gameMap;
	private $gameMaxPlayer;
	private $gameTotalPlayer;
	private $gameStatus;
	private $gameUsePass;
	private $gameRegister;	
	private $gameRegisterDate;
	private $XMLoutput;
	
	// Constructor
	function __construct($user, $pass, $host, $dbName){
		parent::__construct($user, $pass, $host, $dbName);
	}
	
	// Destructor
	function __destruct(){
		parent::__destruct();
	}
	
	// Getter
	public function __get($nom){
		if(isset($this->$nom)){
			return $this->$nom;
		}
	}
	
	// Save a new game
	public function saveGame($userId, $loginKey, $gameName, $port, $maxPlayer, $map, $usePass, $isDedicated=false){
		if($this->checkUser($userId, $loginKey, $isDedicated)){
			if(!empty($gameName) && !empty($port) && !empty($maxPlayer)){
				if($this->nameFilter($gameName) && is_numeric($maxPlayer) && is_numeric($port)){
					if($usePass == 0){
						$usePass = false;
					} else {
						$usePass = true;
					}
					
					if($isDedicated){
						$userHostId = null;
						$serverId = $this->userId;
						$isStarted = '1';
					} else {
						$userHostId = $this->userId;
						$serverId = null;
						$isStarted = '0';
					}					

					$this->gameName = $gameName;
					$this->gamePort = $port;
					$this->gameMaxPlayer = $maxPlayer;
					$this->gameMap = $map;
					$this->gameStatus = $isStarted;
					$this->gameUsePass = $usePass;
					$this->gameTotalPlayer = 1;
					
					$array = array(':id' => '',
						':userHost_id' => $userHostId,
						':server_id' => $serverId,
						':name' => secure_prepare_db($this->gameName),
						':port' => secure_prepare_db($this->gamePort),
						':map' => secure_prepare_db($this->gameMap),
						':maxPlayer' => secure_prepare_db($this->gameMaxPlayer),		
						':usePass' => $this->gameUsePass,
						':status' => secure_prepare_db($this->gameStatus));
					
					$this->prepare_exec('INSERT INTO games VALUES(:id, :userHost_id, :server_id,
						:name, :port, :map, :maxPlayer, :usePass, :status, NOW(), NOW())', $array);
					$this->gameId = $this->lastId();
					
					$getDate = $this->query_fetchObject('SELECT DATE_FORMAT(dateReg, \'%H:%i\') AS gameRegister,	
					dateReg AS gameRegisterDate FROM games WHERE id=\''.$this->gameId.'\'');
					
					$this->gameRegister = $getDate->gameRegister;		
					$this->gameRegisterDate =$getDate->gameRegisterDate;
					if(!$isDedicated){
						$array2 = array(':user_id' => $this->userId, 
							':game_id' => $this->gameId);
						$this->prepare_exec('INSERT INTO users_has_games VALUES(:user_id, :game_id)', $array2);
					}
					return true;
				} else {
					if(!$this->nameFilter($gameName)){
						$this->errorMessage.= '|errorGameName';
					}					
					if(!is_numeric($maxPlayer)){
						$this->errorMessage.= '|errorMaxPlayer';
					}
					if(!is_numeric($port)){
						$this->errorMessage.= '|errorPort';
					}
				}
			} else {
				if(empty($gameName)){
					$this->errorMessage.= '|emptyGameName';
				}
				if(empty($port)){
					$this->errorMessage.= '|emptyPort';
				}				
				if(empty($maxPlayer)){
					$this->errorMessage.= '|emptyMaxPlayer';
				}
			} 
		}
		return false;
	}
	

	// Save a game after host migration
	public function saveRehostedGame($userId, $loginKey, $gameId, $gameName, $port, $map, $maxPlayer, $usePass, $started, $register){
	
		if($this->checkUser($userId, $loginKey)){
			if(!empty($gameId) &&!empty($gameName) && !empty($port) && !empty($map) && !empty($maxPlayer)){
				if($this->nameFilter($gameName)&& $this->nameFilter($map) && is_numeric($gameId) && is_numeric($maxPlayer) && is_numeric($port)){
					if($usePass == 0){
						$usePass = false;
					} else {
						$usePass = true;
					}
					
					if($started == 0){
						$started = false;
					} else {
						$started = true;
					}

					$check = $this->query_fetchObject('SELECT id FROM games WHERE id=\''.secure_db($gameId).'\'');
					if(!empty($check->id)){
						$this->exec('DELETE FROM games WHERE id=\''.$check->id.'\'');
					}
					
					$this->gameId = $gameId;
					$this->gameName = $gameName;
					$this->gamePort = $port;
					$this->gameMap = $map;
					$this->gameMaxPlayer = $maxPlayer;
					$this->gameStatus = $started;
					$this->gameUsePass = $usePass;
					$this->gameRegister = $register;
							
					$array = array(':id' => $this->gameId,
							':userHost_id' => $this->userId,
							':server_id' => NULL,
							':name' => secure_prepare_db($this->gameName),
							':port' => secure_prepare_db($this->gamePort),
							':map' => secure_prepare_db($this->gameMap),
							':maxPlayer' => secure_prepare_db($this->gameMaxPlayer),
							':usePass' => $this->gameUsePass,
							':status' => secure_prepare_db($this->gameStatus),
							':dateReg' => secure_prepare_db($this->gameRegister));
						
					$this->prepare_exec('INSERT INTO games VALUES(:id, :userHost_id, :server_id,
						:name, :port, :map, :maxPlayer, :usePass, :status, :dateReg, NOW())', $array);				
						
					$array2 = array(':user_id' => $this->userId,
							':game_id' => $this->gameId);
					$this->prepare_exec('INSERT INTO users_has_games VALUES(:user_id, :game_id)', $array2);
					return true;
				} 
			} 
		}
		return false;
	}
	
	// Exit game : delete the player on the users_has_games table
	// if the player is the last on of the game, it delete the game too
	public function exitGame($userId, $loginKey){
		if($this->checkUser($userId, $loginKey)){
			$games = $this->query_fetchAll('SELECT game_id FROM users_has_games  					
				WHERE user_id=\''.$this->userId.'\'');
			
			foreach($games AS $value){
				if(!empty($value['game_id'])){
					$game = $this->query_fetchObject('SELECT 
						COUNT(uhg.game_id) AS totalPlayer,
						g.userHost_id AS hostId 
						FROM games g INNER JOIN users_has_games uhg
						ON (g.id = uhg.game_id)
						 WHERE g.id=\''.$value['game_id'].'\'');
					
					if($game->hostId != NULL && $game->hostId != "" && ($game->totalPlayer <= 1 || $game->hostId == $this->userId)){
						$this->exec('DELETE FROM games WHERE id=\''.$value['game_id'].'\'');
					} 					
				}
			}
			$this->exec('DELETE FROM users_has_games WHERE user_id=\''.$this->userId.'\'');
			return true;
		} else {
			return false;			
		}
	}
	
	// Delete all the game hosted by a server when he logout
	public function exitServer($userId, $loginKey){
		if($this->checkUser($userId, $loginKey, true)){
			$checkExist = $this->query_fetchObject('SELECT id FROM
						servers WHERE id=\''.secure_db($userId).'\'');
			if(!empty($checkExist->id)){
				$this->exec('DELETE FROM games WHERE server_id=\''.$checkExist->id.'\'');	
				return true;
			} else {
				$this->errorMessage = '|noGame';
			}
		}
		return false;
	}
	
	// Delete all the game hosted by a server when he logout
	public function exitGameServer($userId, $loginKey, $gameId){
		if($this->checkUser($userId, $loginKey, true)){
			$checkHost = $this->query_fetchObject('SELECT id FROM
					servers WHERE id=\''.secure_db($userId).'\'');
			$checkGame = $this->query_fetchObject('SELECT id FROM
					games WHERE id=\''.secure_db($gameId).'\'');
			if(!empty($checkHost->id) && !empty($checkGame->id)){
				$this->exec('DELETE FROM games WHERE id=\''.$checkGame->id.'\' AND server_id=\''.$checkHost->id.'\'');
			} else {
				if(empty($checkGame->id)){
					$this->errorMessage = '|noGame';
				} else {
					$this->errorMessage = '|noHost';
				}
			}
		}
		return false;
	}
	
	// Search the games
	public function searchGames($userId, $loginKey, $callInterval){
		$this->XMLoutput = null;
		if($this->checkUser($userId, $loginKey)){
			$countCli = $this->query_fetchObject('SELECT COUNT(*) AS totalResult FROM games WHERE server_id IS NULL');
			$countServ = $this->query_fetchObject('SELECT COUNT(*) AS totalResult FROM games WHERE userHost_id IS NULL');
			if($countCli->totalResult  > 0){
			$games = $this->query_fetchAll('SELECT 
				g.id AS gameId,
				g.userHost_id AS hostId,
				g.name AS gameName,
				g.port AS gamePort,	
				g.map AS gameMap,	
				g.maxPlayer AS gameMaxPlayer,	
				g.usePass AS gameUsePass,	
				g.status AS gameStatus,
				DATE_FORMAT(g.dateReg, \'%H:%i\') AS gameRegister,
				YEAR(g.lastUpdate) AS lastUpdate_y,	
				MONTH(g.lastUpdate) AS lastUpdate_mo,
				DAY(g.lastUpdate) AS lastUpdate_d,	
				HOUR(g.lastUpdate) AS lastUpdate_h,
				MINUTE(g.lastUpdate) AS lastUpdate_m,
				SECOND(g.lastUpdate) AS lastUpdate_s,			
				g.dateReg AS gameRegisterDate,	
				u.userName AS hostName,
				u.privateIP AS hostPrivateIp,
				u.publicIP AS hostPublicIp			
				FROM games g INNER JOIN users u
				ON (g.userHost_id = u.id) 				
				ORDER BY g.dateReg DESC');
				
			$blacklisted = $this->query_fetchAll('SELECT users_id FROM users_exclusions WHERE excludeUser_id=\''.$this->userId.'\'');
			$blacklistedId = array();
			$i=0;
			foreach($blacklisted AS $blacklistId){
				$blacklistedId[i] = $blacklistId['users_id'];
				$i++;
			}
			unset($blacklisted);						
				foreach($games as $value){
					if(!in_array($value['hostId'], $blacklistedId)){
						$players = $this->query_fetchObject('SELECT COUNT(*) AS totalPlayers FROM
							users_has_games WHERE game_id=\''.$value['gameId'].'\'');
						// If the total player of the game is not 0
						if($players->totalPlayers > 0) {							
							//If the last call of the game is older than the callInterval
							if(mktime($value['lastUpdate_h'], $value['lastUpdate_m'], $value['lastUpdate_s'], $value['lastUpdate_mo'], $value['lastUpdate_d'], $value['lastUpdate_y']) >= time()-$callInterval){						
								$this->XMLoutput.= '<game id="'.utf8_encode($value['gameId']).'">';
								$this->XMLoutput.= '<type>1</type>';
								$this->XMLoutput.= '<name>'.utf8_encode(display_db($value['gameName'])).'</name>';
								$this->XMLoutput.= '<port>'.utf8_encode(display_db($value['gamePort'])).'</port>';
								$this->XMLoutput.= '<map>'.utf8_encode(display_db($value['gameMap'])).'</map>';
								$this->XMLoutput.= '<currentPlayers>'.utf8_encode($players->totalPlayers).'</currentPlayers>';
								$this->XMLoutput.= '<maxPlayer>'.utf8_encode(display_db($value['gameMaxPlayer'])).'</maxPlayer>';
								$this->XMLoutput.= '<usePass>'.utf8_encode(display_db($value['gameUsePass'])).'</usePass>';
								$this->XMLoutput.= '<status>'.utf8_encode(display_db($value['gameStatus'])).'</status>';
								$this->XMLoutput.= '<register>'.utf8_encode(display_db($value['gameRegister'])).'</register>';
								$this->XMLoutput.= '<registerDate>'.utf8_encode(display_db($value['gameRegisterDate'])).'</registerDate>';
								$this->XMLoutput.= '<hostId>'.utf8_encode(display_db($value['hostId'])).'</hostId>';
								$this->XMLoutput.= '<hostName>'.utf8_encode(display_db($value['hostName'])).'</hostName>';
								
								$this->XMLoutput.= '<hostPrivateIp>'.utf8_encode(display_db($value['hostPrivateIp'])).'</hostPrivateIp>';
								$this->XMLoutput.= '<hostPublicIp>'.utf8_encode(display_db($value['hostPublicIp'])).'</hostPublicIp>';
								$this->XMLoutput.= '</game>';
							} else {
								// Else : delete the game
								$array = array(':id' => $value['gameId']);
								$this->prepare_exec('DELETE FROM games WHERE id=:id', $array);
							}
						} else if($players->totalPlayers <= 0){
							$array = array(':id' => $value['gameId']);
							$this->prepare_exec('DELETE FROM games WHERE id=:id', $array);
						}	
					}			
				}						
			}
			if($countServ->totalResult  > 0){
				$games = $this->query_fetchAll('SELECT
				g.id AS gameId,
				g.server_id AS hostId,
				g.name AS gameName,
				g.port AS gamePort,
				g.map AS gameMap,
				g.maxPlayer AS gameMaxPlayer,
				g.usePass AS gameUsePass,
				g.status AS gameStatus,
				DATE_FORMAT(g.dateReg, \'%H:%i\') AS gameRegister,
				YEAR(g.lastUpdate) AS lastUpdate_y,
				MONTH(g.lastUpdate) AS lastUpdate_mo,
				DAY(g.lastUpdate) AS lastUpdate_d,
				HOUR(g.lastUpdate) AS lastUpdate_h,
				MINUTE(g.lastUpdate) AS lastUpdate_m,
				SECOND(g.lastUpdate) AS lastUpdate_s,
				g.dateReg AS gameRegisterDate,
				s.name AS hostName,
				s.privateIp AS hostPrivateIp,
				s.publicIp AS hostPublicIp
				FROM games g INNER JOIN servers s
				ON (g.server_id = s.id)
				ORDER BY g.dateReg DESC');
				
				foreach($games as $value){
					$players = $this->query_fetchObject('SELECT COUNT(*) AS totalPlayers FROM
						users_has_games WHERE game_id=\''.$value['gameId'].'\'');						// If the last call of the game is older than the callInterval
						
					if(mktime($value['lastUpdate_h'], $value['lastUpdate_m'], $value['lastUpdate_s'], $value['lastUpdate_mo'], $value['lastUpdate_d'], $value['lastUpdate_y']) >= time()-$callInterval){
						$this->XMLoutput.= '<game id="'.utf8_encode($value['gameId']).'">';
						$this->XMLoutput.= '<type>0</type>';
						$this->XMLoutput.= '<name>'.utf8_encode(display_db($value['gameName'])).'</name>';
						$this->XMLoutput.= '<port>'.utf8_encode(display_db($value['gamePort'])).'</port>';
						$this->XMLoutput.= '<map>'.utf8_encode(display_db($value['gameMap'])).'</map>';
						$this->XMLoutput.= '<currentPlayers>'.utf8_encode($players->totalPlayers).'</currentPlayers>';
						$this->XMLoutput.= '<maxPlayer>'.utf8_encode(display_db($value['gameMaxPlayer'])).'</maxPlayer>';
						$this->XMLoutput.= '<usePass>'.utf8_encode(display_db($value['gameUsePass'])).'</usePass>';
						$this->XMLoutput.= '<status>'.utf8_encode(display_db($value['gameStatus'])).'</status>';
						$this->XMLoutput.= '<register>'.utf8_encode(display_db($value['gameRegister'])).'</register>';
						$this->XMLoutput.= '<registerDate>'.utf8_encode(display_db($value['gameRegisterDate'])).'</registerDate>';
						$this->XMLoutput.= '<hostId>'.utf8_encode(display_db($value['hostId'])).'</hostId>';
						$this->XMLoutput.= '<hostName>'.utf8_encode(display_db($value['hostName'])).'</hostName>';
						
						$this->XMLoutput.= '<hostPrivateIp>'.utf8_encode(display_db($value['hostPrivateIp'])).'</hostPrivateIp>';
						$this->XMLoutput.= '<hostPublicIp>'.utf8_encode(display_db($value['hostPublicIp'])).'</hostPublicIp>';
						$this->XMLoutput.= '</game>';
					} else {
						// Else : delete the game
						$array = array(':id' => $value['gameId']);
						$this->prepare_exec('DELETE FROM games WHERE id=:id', $array);
					}								
				}
			}		
			
			if(($countCli->totalResult == 0 &&$countServ->totalResult == 0) || ($this->XMLoutput != null && $this->XMLoutput != '')){
				$this->XMLoutput = '<gameList>'.$this->XMLoutput.'</gameList>';
				return true;
			} else {
				$this->errorMessage = '|noGame';
			}
		}
		return false;
	}
	
	// Update a game status (when the game begin)
	public function updateGameStatus($userId, $loginKey, $gameId, $gameStatus){
		if($this->checkUser($userId, $loginKey)){
			$check = $this->query_fetchObject('SELECT id FROM games WHERE id=\''.secure_db($gameId).'\'');
			if(!empty($check->id) && ($gameStatus == '0' || $gameStatus == '1')){
				$array = array(':id' => $check->id, ':status' => secure_prepare_db($gameStatus));
				$this->prepare_exec('UPDATE games SET status=\''.secure_db($gameStatus).'\' WHERE id=\''.$check->id.'\'', $array);
				return true;
			} else {
				if(empty($check->id)){
					$this->errorMessage.= '|badGameId';
				}
				if($gameStatus != '0' && $gameStatus != '1'){
					$this->errorMessage = '|badGameStatus';
				}
			}			
		}
		return false;
	}
	

	// Update a game map (when the host choose/change the map)
	public function updateGameMap($userId, $loginKey, $gameId, $gameMap){
		if($this->checkUser($userId, $loginKey)){
			$check = $this->query_fetchObject('SELECT id FROM games WHERE id=\''.secure_db($gameId).'\'');
			if(!empty($check->id) && $this->nameFilter($gameMap)){
				$array = array(':id' => $check->id, ':map' => secure_prepare_db($gameMap));
				$this->prepare_exec('UPDATE games SET map=\''.secure_db($gameMap).'\' WHERE id=\''.$check->id.'\'', $array);
				return true;
			} else {
				if(empty($check->id)){
					$this->errorMessage.= '|badGameId';
				}
				if(!$this->nameFilter($gameMap)){
					$this->errorMessage = '|badGameMap';
				}
			}
		}
		return false;
	}
	
	// Add a player in users_has_games
	public function addPlayer($userId, $loginKey, $gameId){
		if($this->checkUser($userId, $loginKey)){
			$check = $this->query_fetchObject('SELECT id FROM games WHERE id=\''.secure_db($gameId).'\'');
			if(!empty($check->id)){
				$array = array(':user_id' => $this->userId, ':game_id' => $check->id);
				$this->prepare_exec('INSERT INTO users_has_games VALUES(:user_id, :game_id)', $array);
				return true;
			} else {				
				$this->errorMessage.= '|badGameId';
			}
		}
		return false;
	}
	
	// Remove a player of users_has_games (call by the host)
	public function removePlayer($hostId, $loginKey, $playerId, $gameId, $isDedicated=false){
		if($this->checkUser($hostId, $loginKey, $isDedicated)){
			$check = $this->query_fetchObject('SELECT user_id FROM users_has_games WHERE user_id=\''.secure_db($playerId).'\' AND  game_id =\''.secure_db($gameId).'\'');
			if(!empty($check->user_id)){
				$this->exec('DELETE FROM users_has_games WHERE user_id=\''.$check->user_id.'\'');
				return true;
			} else {
				$this->errorMessage.= '|badGameId';
			}
		}
		return false;
	}
	
	// Update the game's host
	public function hostCall($userId, $loginKey, $gameId, $isDedicated=false){
		if($this->checkUser($userId, $loginKey, $isDedicated)){
			$check = $this->query_fetchObject('SELECT id FROM games WHERE id=\''.secure_db($gameId).'\'');
			if(!empty($check->id)){
				$array = array(':id' => $check->id);
				$this->prepare_exec('UPDATE games SET lastUpdate=NOW() WHERE id=:id', $array);
				return true;
			} else {
				$this->errorMessage.= '|badGameId';
			}
		}
		return false;
	}

}
?>