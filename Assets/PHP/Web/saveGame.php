<?php 
	include 'functions/functions.php';
	
	$game = new MOGame($logUser, $logPass, $logHost, $logDbName);
	if(!empty($_REQUEST['saveRehosted'])){	
		if($game->saveRehostedGame(urldecode($_REQUEST['id']), urldecode($_REQUEST['loginKey']),
				urldecode($_REQUEST['gameId']), urldecode($_REQUEST['gameName']), urldecode($_REQUEST['gamePort']),
				urldecode($_REQUEST['gameMap']), urldecode($_REQUEST['maxPlayer']), urldecode($_REQUEST['usePass']),
				urldecode($_REQUEST['started']), urldecode($_REQUEST['register']))) {
			echo 'Success';					
		} else {
			echo $game->errorMessage;
		}
	} else{
		$isDedicated=false;
		if(!empty($_REQUEST['dedicated'])){
			$isDedicated=true;
		}
		if($game->saveGame(urldecode($_REQUEST['id']), urldecode($_REQUEST['loginKey']),			
				 urldecode($_REQUEST['gameName']), urldecode($_REQUEST['gamePort']),
				 urldecode($_REQUEST['maxPlayer']), urldecode($_REQUEST['map']), urldecode($_REQUEST['usePass']), $isDedicated)) {
			echo 'Success|'
					.$game->gameId.'|'
					.$game->gameName.'|'
					.$game->gamePort.'|'
					.$game->gameMaxPlayer.'|'
					.$game->gameStatus.'|'
					.$game->gameRegister.'|'
					.$game->gameRegisterDate;
		} else {
			echo $game->errorMessage;
		}
	}
?>