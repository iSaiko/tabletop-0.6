<?php 
	include 'functions/functions.php';
	$user = new MOUser($logUser, $logPass, $logHost, $logDbName);
	if(!empty($_REQUEST['call'])) {
		if($user->playerCall(urldecode($_REQUEST['id']), urldecode($_REQUEST['loginKey']))){
			echo 'Success';
		} else {
			echo $user->errorMessage;
		}
	} elseif(!empty($_REQUEST['login'])) {
		if($user->logUser(urldecode($_REQUEST['userName']), urldecode($_REQUEST['pass']), urldecode($_REQUEST['privateIP']), urldecode($_REQUEST['callInterval']))){
			echo 'Success|'.$user->userId.'|'.$user->userName.'|'.$user->userMail.'|'.$user->publicIP.'|'.$user->loginKey;
		} else {
			echo $user->errorMessage;
		}
	} 
?>