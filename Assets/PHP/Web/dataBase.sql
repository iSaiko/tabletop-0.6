--
-- Table structure for table `games`
--

CREATE TABLE IF NOT EXISTS `games` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userHost_id` int(10) unsigned DEFAULT NULL,
  `server_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(100) COLLATE latin1_german1_ci NOT NULL,
  `port` varchar(10) COLLATE latin1_german1_ci NOT NULL,
  `map` varchar(150) COLLATE latin1_german1_ci DEFAULT NULL,
  `maxPlayer` tinyint(3) unsigned NOT NULL,
  `usePass` tinyint(1) DEFAULT NULL,
  `status` enum('0','1') COLLATE latin1_german1_ci NOT NULL,
  `dateReg` datetime NOT NULL,
  `lastUpdate` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`userHost_id`),
  KEY `server_id` (`server_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `servers`
--

CREATE TABLE IF NOT EXISTS `servers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `adminMail` varchar(150) NOT NULL,
  `privateIp` varchar(100) NOT NULL,
  `publicIp` varchar(100) NOT NULL,
  `login` tinyint(1) NOT NULL DEFAULT '0',
  `loginKey` varchar(150) DEFAULT NULL,
  `dateReg` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userName` varchar(100) COLLATE latin1_german1_ci NOT NULL,
  `mail` varchar(100) COLLATE latin1_german1_ci NOT NULL,
  `privateIP` varchar(100) COLLATE latin1_german1_ci DEFAULT NULL,
  `publicIP` varchar(100) COLLATE latin1_german1_ci DEFAULT NULL,
  `pass` varchar(100) COLLATE latin1_german1_ci NOT NULL,
  `dateReg` timestamp NULL DEFAULT NULL,
  `login` tinyint(1) DEFAULT NULL,
  `loginKey` varchar(100) COLLATE latin1_german1_ci DEFAULT NULL,
  `lastUpdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users_exclusions`
--

CREATE TABLE IF NOT EXISTS `users_exclusions` (
  `users_id` int(10) unsigned NOT NULL,
  `excludeUser_id` int(10) unsigned NOT NULL,
  `dateReg` datetime NOT NULL,
  PRIMARY KEY (`users_id`,`excludeUser_id`),
  KEY `users_id` (`users_id`),
  KEY `excludeUser_id` (`excludeUser_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `users_friends`
--

CREATE TABLE IF NOT EXISTS `users_friends` (
  `users_id1` int(11) unsigned NOT NULL,
  `users_id2` int(11) unsigned NOT NULL,
  `dateReg` datetime NOT NULL,
  PRIMARY KEY (`users_id1`,`users_id2`),
  KEY `users_id1` (`users_id1`),
  KEY `users_id2` (`users_id2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_friends`
--

-- --------------------------------------------------------

--
-- Table structure for table `users_has_games`
--

CREATE TABLE IF NOT EXISTS `users_has_games` (
  `user_id` int(10) unsigned NOT NULL,
  `game_id` int(10) unsigned NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `game_id` (`game_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `games`
--
ALTER TABLE `games`
  ADD CONSTRAINT `games_ibfk_1` FOREIGN KEY (`userHost_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `games_ibfk_2` FOREIGN KEY (`server_id`) REFERENCES `servers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_exclusions`
--
ALTER TABLE `users_exclusions`
  ADD CONSTRAINT `users_exclusions_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_exclusions_ibfk_2` FOREIGN KEY (`excludeUser_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_friends`
--
ALTER TABLE `users_friends`
  ADD CONSTRAINT `fk_users_friends_id1` FOREIGN KEY (`users_id1`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_users_friends_id2` FOREIGN KEY (`users_id2`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_has_games`
--
ALTER TABLE `users_has_games`
  ADD CONSTRAINT `users_has_games_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_has_games_ibfk_2` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
