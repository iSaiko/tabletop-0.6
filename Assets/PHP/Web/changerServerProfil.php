<?php 
	include 'functions/functions.php';	
	if(!empty($_REQUEST['changeServerData'])){
		$server = new MOServer($logUser, $logPass, $logHost, $logDbName);
		if($server->changeServerProfil(urldecode($_REQUEST['id']), 
				urldecode($_REQUEST['loginKey']),
				urldecode($_REQUEST['name']),
				urldecode($_REQUEST['mail']))){
			echo 'Success';
		} else {
			echo $server->errorMessage;
		}
	}  elseif(!empty($_REQUEST['changeServerPass'])){
		$server = new MOServer($logUser, $logPass, $logHost, $logDbName);
		if($server->changeServerPass(urldecode($_REQUEST['id']), 
				urldecode($_REQUEST['loginKey']),
				urldecode($_REQUEST['currentPassword']),
				urldecode($_REQUEST['newPassword']))){
			echo 'Success';
		} else {
			echo $server->errorMessage;
		}
	}
?>