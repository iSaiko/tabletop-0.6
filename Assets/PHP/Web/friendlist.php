<?php 
	include 'functions/functions.php';

	$user = new MOUser($logUser, $logPass, $logHost, $logDbName);
	if(!empty($_REQUEST['addPlayer'])){
		if($user->addInFriendlist(urldecode($_REQUEST['id']), urldecode($_REQUEST['loginKey']), urldecode($_REQUEST['friendlistId']))){
			echo 'Success';
		} else {
			echo $user->errorMessage;
		}
	}  else if(!empty($_REQUEST['remove'])) {		
		if($user->removeFromFriendlist(urldecode($_REQUEST['id']), urldecode($_REQUEST['loginKey']), urldecode($_REQUEST['friendlistId']))){
			echo 'Success';
		} else {
			echo $user->errorMessage;
		}
	} else if(!empty($_REQUEST['search'])) {		
		if($user->searchFriendlist(urldecode($_REQUEST['id']), urldecode($_REQUEST['loginKey']))){
			echo $user->XMLoutput;
		} else {
			echo 'ERROR'.$user->errorMessage;
		}
	}
?>